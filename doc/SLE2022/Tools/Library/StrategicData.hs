module Library.StrategicData where 

import Data.Generics.Zipper hiding (left, right, up, down')
import qualified Data.Generics.Zipper as Z (left, right, up, down')

class StrategicData t where
  isTerminal :: Zipper t -> Bool
  -- default isTerminal :: Zipper t -> Bool
  isTerminal _ = False


right :: StrategicData a => Zipper a -> Maybe (Zipper a)
right z = case Z.right z of 
            Just r -> if isTerminal r then right r else Just r 
            Nothing -> Nothing 

left :: StrategicData a => Zipper a -> Maybe (Zipper a)
left z = case Z.left z of 
           Just r -> if isTerminal r then left r else Just r 
           Nothing -> Nothing  

up :: StrategicData a => Zipper a -> Maybe (Zipper a)
up z = case Z.up z of 
         Just r -> if isTerminal r then right r else Just r
         Nothing -> Nothing 
     
down' :: StrategicData a => Zipper a -> Maybe (Zipper a)
down' z = case Z.down' z of 
            Just r -> if isTerminal r then right r else Just r
            Nothing -> Nothing 

isJust (Just _) = True
isJust Nothing  = False


{-
instance Typeable m => StrategicData (Let m) where
  isTerminal z =  isJust (getHole z :: Maybe m) || isJust (getHole z :: Maybe Name) || isJust (getHole z :: Maybe Int)
(for this you need to enable the ScopedTypeVariables extension)
-}
