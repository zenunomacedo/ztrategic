{-# LANGUAGE Rank2Types,DeriveDataTypeable #-}
module Library.Ztrategic where

import Language.Grammars.ZipperAG
import Data.Generics.Zipper hiding (left, right, up, down')
import Data.Generics.Aliases
import Data.Maybe
import Data.Data
import Control.Monad -- (join, mplus, MonadPlus)

import Debug.Trace

import Library.StrategicData

----------
----
--- TP
----
----------

type TP a = Zipper a -> Maybe (Zipper a)
--applyTU :: Zipper a -> m d -> Zipper c -> m d 
applyTP :: TP c -> Zipper c -> Maybe (Zipper c)
applyTP f z = f z 

full_tdTP :: StrategicData a => TP a -> TP a
full_tdTP f = allTPdown (full_tdTP f) `seqTP` allTPright (full_tdTP f) `seqTP` f 

full_buTP :: StrategicData a => TP a -> TP a
full_buTP f = f `seqTP` allTPright (full_buTP f) `seqTP` allTPdown (full_buTP f) 

once_tdTP :: StrategicData a => TP a -> TP a
once_tdTP f =  oneTPdown (once_tdTP f) `choiceTP` oneTPright (once_tdTP f) `choiceTP` f

once_buTP :: StrategicData a => TP a -> TP a  
once_buTP f = f `choiceTP` oneTPright (once_buTP f) `choiceTP` oneTPdown (once_buTP f) 

--https://hackage.haskell.org/package/ZipperAG-0.9/docs/Language-Grammars-ZipperAG-Examples-Algol68.html
--change use to decl

--Experimental
stop_tdTP :: StrategicData a => TP a -> TP a
stop_tdTP f = oneTPdown (stop_tdTP f) `choiceTP` f `seqTP` oneTPright (stop_tdTP f)

--Experimental
stop_buTP :: StrategicData a => TP a -> TP a
stop_buTP f = oneTPright (stop_buTP f) `seqTP` (f  `choiceTP` oneTPdown (stop_buTP f))

adhocTP :: Typeable a => TP e -> (a -> Maybe a) -> TP e
adhocTP f g = maybeKeep f (zTryApplyM g) 

adhocTPZ :: Typeable a => TP e -> (a -> Zipper e -> Maybe a) -> TP e
adhocTPZ f g = maybeKeep f (zTryApplyMZ g)

--Identity function
idTP :: TP a
idTP = return . id

--Failing function
failTP :: TP a 
failTP = const mzero



maybeKeep :: (a -> Maybe a) -> (a -> Maybe a) -> a -> Maybe a
maybeKeep x y z =  case y z of 
              Nothing -> x z 
              Just r -> case x r of 
                  Nothing ->  Just r 
                  k -> k 


--To right node only
allTPright :: StrategicData a => TP a -> TP a
allTPright f z = case right z of 
         Nothing -> return z
         Just r  -> (fromJust . left) <$> f r

--To down node only
allTPdown :: StrategicData a => TP a -> TP a
allTPdown f z = case down' z of 
            Nothing -> return z
            Just d  -> (fromJust . up) <$> f d

--To right node only
oneTPright :: StrategicData a => TP a -> TP a
oneTPright f z = case right z of 
         Nothing -> Nothing
         Just r  -> (fromJust . left) <$> f r

--To down node only
oneTPdown :: StrategicData a => TP a -> TP a
oneTPdown f z = case down' z of 
            Nothing -> Nothing
            Just d  -> (fromJust . up) <$> f d  

--Sequential composition, ignores failure
seqTP :: TP a -> TP a -> TP a
seqTP x y z = maybeKeep x y z
f `mseq` g      =  \x -> g x >>= f 

--Sequential composition, chooses rightmost only if possible
choiceTP :: TP a -> TP a -> TP a
choiceTP x y z = maybe (x z) (return . id) (y z)

--Apply a function, fail the composition if it fails
monoTP :: Typeable a => (a -> Maybe a) -> TP e
monoTP = adhocTP failTP

--Apply a function with access to the zipper, fail the composition if it fails
monoTPZ :: Typeable a => (a -> Zipper e -> Maybe a) -> TP e
monoTPZ = adhocTPZ failTP

--Try to apply a zipper function, and apply identity if it fails
tryTP :: TP a -> TP a
tryTP s = idTP `choiceTP` s

repeatTP :: TP a -> TP a
repeatTP s =  tryTP (mseq (repeatTP s) s)

innermost :: StrategicData a => TP a -> TP a
innermost s =  (tryTP ((innermost s) `mseq` s))
                   `seqTP` oneTPright (innermost s) `seqTP` oneTPdown (innermost s)

innermost'  :: StrategicData a => TP a -> TP a
innermost' s  =  repeatTP (once_buTP s)

outermost  :: StrategicData a => TP a -> TP a
outermost s  =  repeatTP (once_tdTP s)

----------
----
--- TU
----
----------

type TU m d = (forall a. Zipper a -> m d) 

applyTU :: (Zipper a -> m d) -> Zipper a -> m d 
applyTU f z = f z 

foldr1TU :: (Monoid (m d), StrategicData a, Foldable m) => (Zipper a -> m d) -> Zipper a -> (d -> d -> d) -> d
foldr1TU f z red = foldr1 red $ (full_tdTU f z) 

foldl1TU :: (Monoid (m d), StrategicData a, Foldable m) => (Zipper a -> m d) -> Zipper a -> (d -> d -> d) -> d
foldl1TU f z red = foldl1 red $ (full_tdTU f z) 

foldrTU :: (Monoid (m d), StrategicData a, Foldable m) => (Zipper a -> m d) -> Zipper a -> (d -> c -> c) -> c -> c
foldrTU f z red i = foldr red i $ (full_tdTU f z) 

foldlTU :: (Monoid (m d), StrategicData a, Foldable m) => (Zipper a -> m d) -> Zipper a -> (c -> d -> c) -> c -> c
foldlTU f z red i = foldl red i $ (full_tdTU f z) 

full_tdTU :: (Monoid (m d), StrategicData a) => (Zipper a -> m d) -> (Zipper a -> m d)   
full_tdTU f = f `seqTU` allTUdown (full_tdTU f) `seqTU` allTUright (full_tdTU f) 

full_buTU :: (Monoid (m d), StrategicData a) => (Zipper a -> m d) -> (Zipper a -> m d)
full_buTU f = allTUright (full_buTU f) `seqTU` allTUdown (full_buTU f) `seqTU` f

once_tdTU :: (MonadPlus m, Monoid (m d), StrategicData a) => (Zipper a -> m d) -> (Zipper a -> m d) 
once_tdTU f = f `choiceTU` allTUdown (once_tdTU f)  `choiceTU` allTUright (once_tdTU f) 

once_buTU :: (MonadPlus m, Monoid (m d), StrategicData a) => (Zipper a -> m d) -> (Zipper a -> m d)
once_buTU f = allTUright (once_buTU f) `choiceTU` allTUdown (once_buTU f) `choiceTU` f

--Experimental
stop_tdTU :: (MonadPlus m, Monoid (m d), StrategicData a) => (Zipper a -> m d) -> (Zipper a -> m d)
stop_tdTU f = (f `choiceTU` allTUdown (stop_tdTU f)) `seqTU` allTUright (stop_tdTU f)
--stop_tdTU f = (allTUdown (stop_tdTU f) `seqTU` allTUright (stop_tdTU f)) `choiceTU` f
--stop_tdTU f =(allTUdown (stop_tdTU f) `choiceTU` f) `seqTU` allTUright (stop_tdTU f)
--stop_tdTU f =  allTUright (stop_tdTU f) `seqTU` (allTUdown (stop_tdTU f) `choiceTU` f)


--Experimental
stop_buTU :: (MonadPlus m, Monoid (m d), StrategicData a) => (Zipper a -> m d) -> (Zipper a -> m d)
stop_buTU f = allTUright (stop_buTU f) `seqTU` (allTUdown (stop_buTU f) `choiceTU` f)



allTUdown :: (Monoid (m d), StrategicData a) => (Zipper a -> m d) -> (Zipper a -> m d)
allTUdown f z = case down' z of 
            Nothing -> mempty
            Just d  -> f d

allTUright :: (Monoid (m d), StrategicData a) => (Zipper a -> m d) -> (Zipper a -> m d)
allTUright f z = case right z of 
            Nothing -> mempty
            Just r  -> f r

adhocTU :: (Monad m, Typeable a) => (Zipper c -> m d) -> (a -> m d) -> (Zipper c -> m d)
adhocTU f g z = maybe (f z) id (zTryReduceM g z)

adhocTUZ :: (Monad m, Typeable a) => (Zipper c -> m d) -> (a -> Zipper c -> m d) -> (Zipper c -> m d)
adhocTUZ f g z = maybe (f z) id (zTryReduceMZ g z)

--TODO fix this type
seqTU :: (Monoid (m d)) => (Zipper a -> m d)-> (Zipper a -> m d) -> (Zipper a -> m d)
seqTU x y z = (x z) `mappend` (y z)

--Sequential composition 
choiceTU :: (MonadPlus m) => (Zipper a -> m d) -> (Zipper a -> m d) -> (Zipper a -> m d)
choiceTU x y z = x z `mplus` y z 

failTU :: (MonadPlus m) => (Zipper a -> m d)
failTU = const mzero

constTU :: Monad m => d -> (Zipper a -> m d)
constTU = const . return

--Apply a function, fail the composition if it fails
monoTU :: (MonadPlus m, Typeable a) => (a -> m d) -> (Zipper e -> m d)
monoTU = adhocTU failTU

--TODO: fix type signature
--Apply a function with access to the zipper, fail the composition if it fails
monoTUZ :: (MonadPlus m, Typeable a) => (a -> Zipper e -> m d) -> (Zipper e -> m d)
monoTUZ = adhocTUZ failTU


--TODO: fix type signature
--TODO: improve using Data.Generics.Aliases, fitting mkT or mkM inside of transM instead of chaining casts 
--elevate a reduction, which can access the zipper, to the zipper level. If the type does not match, returns Nothing 
zTryReduceMZ :: (Typeable a) => (a -> Zipper c -> b) -> (Zipper c -> Maybe b)
zTryReduceMZ f z = getHole z >>= return . flip f z

--elevate a reduction to the zipper level. If the type does not match, returns Nothing 
zTryReduceM :: (Typeable a) => (a -> b) -> TU Maybe b
zTryReduceM f z = getHole z >>= return . f

--elevate a transformation to the zipper level. If the type does not match, returns Nothing 
zTryApplyMZ :: (Typeable a, Typeable b) => (a -> Zipper c -> Maybe b) -> TP c
zTryApplyMZ f z = transM (join . cast . (flip f z) . fromJust . cast) $ z

--elevate a transformation to the zipper level. If the type does not match, returns Nothing 
zTryApplyM :: (Typeable a, Typeable b) => (a -> Maybe b) -> TP c
zTryApplyM f = transM (join . cast . f . fromJust . cast)

--elevate a transformation to the zipper level. If the type does not match, returns Nothing 
zTryApply :: (Typeable a, Typeable b) => (a -> b) -> TP c
zTryApply f = transM (cast . f . fromJust . cast)

--elevate a transformation to the zipper level. If the type does not match, the zipper remains unchanged
zApply :: (Typeable a, Typeable b) => (a -> b) -> (Zipper c -> Zipper c)
zApply f z = maybe z id (zTryApply f z) 

