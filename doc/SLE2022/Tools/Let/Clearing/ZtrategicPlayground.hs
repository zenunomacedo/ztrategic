module Let.Clearing.ZtrategicPlayground where 

import Data.Generics.Zipper
import Data.Generics.Aliases
import Let.Shared (Root, Errors)

import Let.LetMemo
import Let.Let_Zippers (expand)

import Library.Memo.Ztrategic
import Let.Benchmark (generator)

----------
----
--- Count the "Add" nodes, optimize, count the "Add" nodes again
----
----------

l = toZipper $ buildMemoTree emptyMemo $ generator 10 

baseValue = fst $ adds l 
optValue = fst $ adds $ opt_unclean l 
optValue_unclean  = fst $ adds $ opt_unclean $ snd $ adds l
optValue_clean    = fst $ adds $ opt_clean   $ snd $ adds l








----------
----
--- Definition of Memoized optimization strategy
----
----------

exprZ :: Let MemoTable -> Zipper (Let MemoTable) -> Maybe (Zipper (Let MemoTable))
exprZ (Add e (Const 0 _) m) z         = Just $ setHole e z 
exprZ (Add (Const 0 _) t _) z         = Just $ setHole t z   
exprZ (Add (Const a _) (Const b _) m) z  = Just $ setHole (Const (a+b) m) z
exprZ (Sub a b m)       z   = Just $ setHole (Add a (Neg b emptyMemo) emptyMemo) z  
exprZ (Neg (Neg f _) _) z   = Just $ setHole f z
exprZ (Neg (Const n m) _) z   = Just $ setHole (Const (-n) m) z
exprZ (Var i _) z                    = let (e, z') = env z
                                           (l, z'') = lev z'
                                           expr :: Maybe (Let MemoTable)
                                           expr = fmap (buildMemoTree4 emptyMemo) $ expand (i, l) e
                                       in fmap (\k -> setHole k z'') expr      
exprZ _   z                        = Nothing


opt :: Root -> Root
opt t = letToRoot $ fromZipper $ opt_unclean $ toZipper $ buildMemoTree emptyMemo t 

opt_unclean :: Zipper (Let MemoTable) -> Zipper (Let MemoTable)
opt_unclean z = t'
 where Just (t', x) = applyTP_unclean (innermost step) z
       step = failTP `adhocTPZ` exprZ

opt_clean :: Zipper (Let MemoTable) -> Zipper (Let MemoTable)
opt_clean z = t'
 where Just (t', x) = applyTP (innermost step) z
       step = failTP `adhocTPZ` exprZ
