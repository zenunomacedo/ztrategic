module Let.LetZtrategic where 

import Data.Generics.Zipper
import Data.Maybe (fromJust)

import Library.Ztrategic
import Library.StrategicData

import Let.Shared 
import Let.SharedAG
import Let.Let_Zippers


----------
----
--- Example defined in paper
----
----------

{-
p = let a = b + 0
        c = 2
        b = let c = 3 in c + c
    in a + 7 − c
-}

p :: Let
p =  Let  (Assign     "a"  (Add (Var "b") (Const 0))
          (Assign     "c"  (Const 2)
          (NestedLet  "b"  (Let  (Assign "c" (Const 3)
                                 EmptyList)
                                 (Add (Var "c") (Var "c")))
          EmptyList)))
          (Sub (Add (Var "a") (Const 7)) (Var "c"))

----------
----
--- Optimization via rules 1 through 6
----
----------      

expr :: Exp -> Maybe Exp
expr (Add e (Const 0))          = Just e 
expr (Add (Const 0) t)          = Just t  
expr (Add (Const a) (Const b))  = Just (Const (a+b)) 
expr (Sub a b)                  = Just (Add a (Neg b))    
expr (Neg (Neg f))              = Just f           
expr (Neg (Const n))            = Just (Const (-n)) 
expr _                          = Nothing

opt'  :: Zipper Let -> Maybe (Zipper Let)
opt'  t =  applyTP (innermost step) t
      where step = failTP `adhocTP` expr


t_1 = toZipper p 
run_example_TP = fromZipper $ fromJust $ opt' t_1



-- --------
-- --
-- - Optimization via all 7 rules
-- --
-- --------

expC :: Exp -> Zipper Root -> Maybe Exp
expC (Var i) z = expand (i, lev z) (env z)
expC _ z = Nothing 

opt'' :: Zipper Root -> Maybe (Zipper Root)
opt'' r = applyTP (innermost step) r
 where step = failTP `adhocTPZ` expC `adhocTP` expr  

run_example_opt_t_1 = fromZipper $ fromJust $ opt'' $ toZipper $ Root p 



-- --------
-- --
-- - Definition of terminal symbols
-- --
-- --------

instance StrategicData Let
instance StrategicData Root

-- comment above and uncomment below to enable skipping terminal symbols 
{-
instance StrategicData Let where 
  isTerminal z = isJust (getHole z :: Maybe Name     )
              || isJust (getHole z :: Maybe Int      )
              
instance StrategicData Root where 
  isTerminal z = isJust (getHole z :: Maybe Name     )
              || isJust (getHole z :: Maybe Int      )
-}

opt = fromZipper . fromJust . opt'' . toZipper