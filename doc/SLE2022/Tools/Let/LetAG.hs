module Let.LetAG where 

import Language.Grammars.ZipperAG
import Data.Generics.Zipper

import Let.Shared
import Let.SharedAG
import Let.Let_Zippers

----------
----
--- Partial Optimization as an attribute
----
----------

opt :: Root -> Root
opt t = let ag = mkAG t 
            t' = optRoot ag 
        in if t == t' then t else opt t'

optRoot :: Zipper Root -> Root
optRoot ag = case (constructor ag) of
           CRoot      -> Root $ optLet (ag.$1)

optLet :: Zipper Root -> Let
optLet ag = case (constructor ag) of
           CLet       -> Let (optList (ag.$1)) (optExp (ag.$2))

optList :: Zipper Root -> List
optList ag = case (constructor ag) of
           CEmptyList -> EmptyList
           CAssign    -> Assign    (lexeme_Name ag) (optExp (ag.$2)) (optList (ag.$3))
           CNestedLet -> NestedLet (lexeme_Name ag) (optLet (ag.$2)) (optList (ag.$3))

optExp :: Zipper Root -> Exp
optExp ag = case (constructor ag) of
           -- rules 1, 2, 3
           CAdd       -> case (lexeme_Add1 ag, lexeme_Add2 ag) of 
                           (e, Const 0)       -> e 
                           (Const 0, t)       -> t 
                           (Const a, Const b) -> Const (a+b) 
                           _                  -> Add (optExp (ag.$1)) (optExp (ag.$2))
           -- rule 4                
           CSub       -> Add (lexeme_Sub1 ag) (Neg (lexeme_Sub2 ag)) 
           CConst     -> Const (lexeme_Const ag)
           -- rules 5, 6
           CNeg       -> case (lexeme_Neg ag) of 
                           (Neg (Neg f))   -> f
                           (Neg (Const n)) -> Const (-n)
                           _               -> Neg (optExp (ag.$1))  
           -- rule 7
           CVar       -> case expand (lexeme_Var ag, lev ag) (env ag) of 
                           Just e  -> e
                           Nothing -> Var (lexeme_Var ag)

----------
----
--- Misc
----
----------

lexeme_Var :: Zipper a -> String
lexeme_Var ag = case (getHole ag :: Maybe Exp) of
                          Just (Var v) -> v
                          Just _ -> error "Error in lexeme_Var!"

lexeme_Neg :: Zipper a -> Exp
lexeme_Neg ag = case (getHole ag :: Maybe Exp) of
                          Just (Neg n) -> n
                          Just _ -> error "Error in lexeme_Neg!"

lexeme_Const :: Zipper a -> Int
lexeme_Const ag = case (getHole ag :: Maybe Exp) of
                          Just (Const c) -> c
                          Just _ -> error "Error in lexeme_Const!"

lexeme_Sub1 :: Zipper a -> Exp
lexeme_Sub1 ag = case (getHole ag :: Maybe Exp) of
                          Just (Sub s1 s2) -> s1
                          Just _ -> error "Error in lexeme_Sub1!"

lexeme_Sub2 :: Zipper a -> Exp
lexeme_Sub2 ag = case (getHole ag :: Maybe Exp) of
                          Just (Sub s1 s2) -> s2
                          Just _ -> error "Error in lexeme_Sub2!"

lexeme_Add1 :: Zipper a -> Exp
lexeme_Add1 ag = case (getHole ag :: Maybe Exp) of
                          Just (Add a1 a2) -> a1
                          Just _ -> error "Error in lexeme_Add1!"

lexeme_Add2 :: Zipper a -> Exp
lexeme_Add2 ag = case (getHole ag :: Maybe Exp) of
                          Just (Add a1 a2) -> a2
                          Just _ -> error "Error in lexeme_Add2!"

