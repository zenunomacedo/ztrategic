module RepMin.RepMinTUTP where 

import Data.Data
import Data.Generics.Zipper hiding (left, right, up, down')
import Data.Maybe (fromJust)

import Library.Ztrategic
import Library.StrategicData

import RepMin.Shared

-- --------
-- --
-- - Zipper Examples
-- --
-- --------

t_1 = toZipper t

leafTwo :: Maybe Tree
leafTwo = (getHole  .  fromJust . right    
                    .  fromJust . down' .  fromJust . down') t_1


-- --------
-- --
-- - RepMin defined as two strategies
-- --
-- --------

nodeVal :: Tree -> [Int]
nodeVal (Leaf x)  = [x]
nodeVal _         = []

minimumValue :: Tree -> Int
minimumValue t =  foldr1TU (full_tdTU step) (toZipper t) min
       where   step = failTU `adhocTU` nodeVal




replace :: Int -> Tree -> Maybe Tree
replace i (Leaf r)  = Just (Leaf i)
replace i _         = Nothing 

replaceTree :: Int -> Tree -> Tree
replaceTree v t =  fromZipper $ fromJust $ 
                   applyTP (full_tdTP step) (toZipper t)
  where step = failTP `adhocTP` (replace v) 



repmin t =  let  v = minimumValue t 
          in   replaceTree v t 
