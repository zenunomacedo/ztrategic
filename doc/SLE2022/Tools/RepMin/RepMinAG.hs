module RepMin.RepMinAG where

import RepMin.Shared
import RepMin.SharedAG

import Data.Generics.Zipper
import Language.Grammars.ZipperAG


type AGTree a = Zipper Tree -> a

-- Repmin

globmin :: AGTree Int
globmin t = case constructor t of
            CRoot   -> locmin (t.$1)
            CLeaf   -> globmin (parent t)
            CFork   -> globmin (parent t)

locmin :: AGTree Int
locmin t = case constructor t of
            CLeaf   -> lexeme t 
            CFork   -> min (locmin (t.$1))
                           (locmin (t.$2))


replace :: AGTree Tree
replace t = case constructor t of
            CRoot   -> replace (t.$1)
            CLeaf   -> Leaf (globmin t)
            CFork   -> Fork (replace (t.$1))
                            (replace (t.$2))

mkAG = toZipper 

repmin tree = replace (mkAG tree)
