
\subsection{\todo{FIXME} Terminal Symbols}
Because not every symbol must be traversed at all times when applying strategies, we introduce the concept of terminal nodes in our library. As such, any node that is defined as a terminal in our library will be skipped entirely, therefore reducing the total traversal effort of the strategy, even more if the skipped node is very complex and/or contains a big subtree. 

To allow for this behaviour, any data type to be traversed using this library must define an instance of |StrategicData|. As an example, we can define this instance easily with the default behaviour of not skipping any nodes, for the Let datatype: 

>instance StrategicData Let 

We could optimize this by instead defining our own behaviour for our data, for example by skipping any node that is an |Int| or a |String|, which we expect to never want to traverse directly. That is not to say that we cannot change any |Int| or |String| in our traversals; we would expect to change |Strings| when traversing a |Var| constructor, but not by directly visiting the |String| node (which in itself is a list of |Char| that would also be traversed individually). We can define it like so: 

\begin{code}
instance StrategicData Let where 
  isTerminal z = isJust (getHole z :: Maybe String)
              || isJust (getHole z :: Maybe Int   )
\end{code}

The |isTerminal| function should return true whenever |z| points towards a terminal node. In this case, we return true if |z| points towards a |String| or an |Int|, but we could define more complex logic in this function, such as skipping only negative integers. 

We reflect of the performance impact of this change in section~\ref{benchmarks}. We also require this change to define memoization in a strategic setting. 



\subsection{Memoized Strategies}

\todo{improve}

The strategies previously showcased assume that the underlying data structure does not change. For a tree with several nodes, traversing the first should result in changing that same node without impacting the rest of the tree. However, when combining strategies with memoized AGs, this is not the case anymore. When traversing the first node, we might compute an attribute which will traverse the whole tree and memoize attributes in all of its nodes, effectively changing them. 

Because of this, a memoization-friendly version of the library was developed. For each visited node, instead of returning just the result of traversing that node, we also return the updated zipper of the data structure. 

\todo{find a way to make this readable / explain this}
\begin{code}
exprZ :: Let MemoTable -> Zipper (Let MemoTable) -> Maybe (Zipper (Let MemoTable))
exprZ (Add e (Const 0 _) m) z         = Just $ trans (mkT $ const e) z 
exprZ (Add (Const 0 _) t _) z         = Just $ trans (mkT $ const t) z   
exprZ (Add (Const a _) (Const b _) m) z  = Just $ trans (mkT $ const (Const (a+b) m)) z
exprZ (Sub a b m)       z   = Just $ trans (mkT $ const (Add a (Neg b emptyMemo) emptyMemo)) z  
exprZ (Neg (Neg f _) _) z   = Just $ trans (mkT $ const f) z
exprZ (Neg (Const n m) _) z   = Just $ trans (mkT $ const (Const (-n) m)) z
exprZ (Var i _) z                    = let (e, z') = env z
                                           (l, z'') = lev z'
                                           expr :: Maybe (Let MemoTable)
                                           expr = fmap (buildMemoTree4 emptyMemo) $ NM.expand (i, l) e
                                       in fmap (\k -> trans (mkT $ const k) z'') expr      
exprZ _   z                        = Nothing
\end{code}


\todo{explain letToRoot and the differences of Let data types. Explain buildMemoTree. }
\begin{code}
opt :: Root -> Root
opt t = letToRoot $ fromZipper $ t' 
 where z :: Zipper (Let MemoTable)
       z = mkAG (buildMemoTree emptyMemo t)
       Just (t', x) = applyTP (innermost step) z
       step = failTP `adhocTPZ` exprZ
\end{code}

\todo{Mention that strategies traverse into the memotable and we need to skip them}
