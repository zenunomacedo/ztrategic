
This paper is inspired by the work of Sloane who developed
Kiama~\cite{kiama}: an embedding of strategic term re-writing and AGs
in the Scala programming language. The Kiama library offers the
functionality of our Ztrategic library. Kiama is a Scala based
embedding of strategic AGs, which relies on Scala mechanisms to
navigate in the trees and memoizes attribute values on a global
memoization table to avoid attribute recalculation. Our library relies
on zippers and uses local memo tables to avoid attribute
recalculation. Kiama uses reachability relations and the notion of
attribute families to fully support strategic AGs. As our benchmarks
show this mechanism induces a considerable overhead on the AG
solutions.


%<Eric>

The extensible AG system Silver~\cite{silver} has also been extended
to support strategic term re-writing~\cite{strategicAG}.  Strategic
rewriting rules can use the attributes of a tree to reference
contextual information during re-writing, much like we present in our
work.  They present several practical application examples, namely the
evaluation of $\lambda$-calculus, a regular expression matching via
Brzozowski derivatives, and the normalization of for-loops. All these
examples can be directly expressed in our setting. They also present
an application to optimize translation of strategies. Because our
techniques rely on shallow embeddings, where no data type is used to
express strategies nor AGs, we are not able to express strategy
optimizations, without relying on meta-programming
techniques~\cite{templatehaskell}. Nevertheless, our embeddings result
in very simple and small libraries that are easier to extend and
maintain, specially when compared to the complexity of extending and
maintaining a full language processor system such as Silver.


%<JastAdd>

JastAdd is a reference attribute grammar based
system~\cite{jastadd}. It supports most of AG extensions, namely
reference and circular AGs~\cite{rag2013}. It also supports tree
re-writing, with re-write rules that can reference attributes.
JastAdd, however, provides no support for strategic programming, that
is to say, there is no mechanism to strategic control how the re-write
rules are applied.  The zipper-based AG embedding we integrate in
\ensuremath{\Conid{Ztrategic}} supports all modern AG extensions,
including reference and circular AGs~\cite{scp2016,memoAG19}. Because
strategies and AGs are first class citizens we can smoothly combine
any of such extensions with strategic term re-writing.

%<Strafunski>

In the context of strategic term re-writing, the
\ensuremath{\Conid{Ztrategic}} library is inspired by
Strafunski~\cite{strafunski}. In fact, \ensuremath{\Conid{Ztrategic}}
already provides almost all Strafunski functionality. There is,
however, a key difference between these libraries: while Strafunski
accesses the data structure directly, \ensuremath{\Conid{Ztrategic}}
operates on zippers. As a consequence, we can easily access attributes
from strategic functions and strategic functions from attribute
equations.


%<Stratego>


\begin{comment}
The Stratego program transformation language (now part of the Spoofax Language Workbench~\cite{spoofax}) supports the definition of rewrite rules and programmable rewriting strategies, and it is able to construct new rewrite rules at runtime, to propagate contextual information for concerns such as lexical scope. It is argued in~\cite{strategicAG} that contextual information is better specified through the usage of inherited attributes for issues such as scoping, name-binding, and type-checking. We present this same usage of inherited attributes in our attribute grammar examples. 
\end{comment}
