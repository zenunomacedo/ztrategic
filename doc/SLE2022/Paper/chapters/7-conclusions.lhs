

This paper presented an embedding of strategic attribute grammars,
which combine strategic term re-writing and the attribute grammar
formalism. Both embeddings rely on memoized zippers: attribute values
are memoized in the zipper data structure, thus avoiding their
re-computation, and strategic zipper based functions access
such memoized values.

We compared the performance of our embedding with state-of-the-art
libraries Strafunski and Kiama. Thus, we have implemented in these
three libraries several language engineering tasks, namely, a let 
optimizer, a code refactor, and an advanced pretty
printing algorithm. Our results show that the embedding strategic term
rewriting behaves very similar to Strafunski. Surprisingly, our
results also show that our embedding of strategic AGs vastly
outperforms Kiama's solutions.


\subsection*{Replication Packages} 

\noindent
\fbox{%
\parbox{\columnwidth}{%
\scriptsize
All the necessary resources to replicate this study, as well as the full set of results, are publicly available:
\begin{itemize}
\item  \textbf{ZippersAG:} \textit{\url{https://hackage.haskell.org/package/ZipperAG-0.9}}
\item  \textbf{Strafunski:} \textit{\url{https://hackage.haskell.org/package/Strafunski-StrategyLib}}
\item  \textbf{Ztrategic:} \textit{\url{https://bitbucket.org/zenunomacedo/ztrategic/}}
\item  \textbf{Kiama:} \textit{\url{https://github.com/inkytonik/kiama}}
\item  \textbf{Examples:} \textit{\url{https://tinyurl.com/sle2022tools}}
\end{itemize}
}%
}