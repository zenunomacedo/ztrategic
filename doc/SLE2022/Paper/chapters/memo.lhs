As shown in~\cite{memoAG19} the attribute evaluation of memoized
zipper-based AGs is much faster than the non-memoized one. As we will
show in Section~\ref{sec:performance}, the combined embedding proposed
in~\cite{flops22} suffers from the same performance issues. Before we
discuss such performance results, we introduce a new set of
strategic combinators that do work with memoized attributes, yielding
an efficient embedding of both techniques.



\subsection{Memoized Strategies}

The memoized attributes showcased previously are extremely powerful performance-wise and they can be used directly with the existing Ztrategic library. In fact, the memoized attributes produce two outputs: the actual attribute value, as well as the resulting data structure, with all computed attributes stored in the respective memoization tables. By ignoring the updated data structure and using only the attribute value, these memoized attributes can be plugged directly into Ztrategic. We define |exprM|, similar to previously defined |expr| but operating on the memoized data type |Let MemoTable|. When the node is transformed and there is no memoization table to place in the new node, we use |emptyMemo| to define an empty table.
%\todo{I assume in this case it doesn't matter which memo tables we put in the resulting node, since they are not going to be used. Am I ok? should we say this here?} 

\begin{code}
exprM :: Let MemoTable -> Maybe (Let MemoTable)
exprM (Add e (Const 0 _) m)            = Just $ e
exprM (Add (Const 0 _) t _)            = Just $ t   
exprM (Add (Const a _) (Const b _) m)  = Just $ Const (a+b) m
exprM (Sub a b m)          = Just $ Add a (Neg b emptyMemo) emptyMemo
exprM (Neg (Neg f _) _)                = Just $ f
exprM (Neg (Const n m) _)              = Just $ Const (-n) m
exprM _                                = Nothing
\end{code}

We define |exprX|, similar to previously defined |expC| but
%operating on the zipper directly. 
using memoized attributes. Recall that this function applies
optimization rule $7$, replacing a variable name by its definition
whenever is possible. We use again the
auxiliary function |expand| to perform this task. Because the result is an |Exp| which
is not compatible with the memoized |Let| datatype, we use the
function |buildMemoTreeExp| to convert it.

\begin{code}
exprX  :: Let MemoTable -> Zipper (Let MemoTable) 
       -> Maybe (Let MemoTable)
exprX (Var i _) z =  let  (e, _)  = env z
                          (l, _)  = lev z
                     in   fmap  (buildMemoTreeExp emptyMemo) $
                                expand (i, l) e
exprX _   z       = Nothing
\end{code}

Notice that we are ignoring the second component of the results of the evaluation of |env| and |lev| which is the zipper with the updated memotables.
Having defined the type specific worker functions |exprX| and
|exprM|, we can build now a strategy to apply them to all nodes,
through the |innermost| strategy. Because we have two different data
types here, namely the non-memoized |Let| nodes, here denoted by
|Root|, and the memoized |Let| nodes denoted by |Let MemoTable|, we
use auxiliary functions |buildMemoTree| and |letToRoot| to convert the
types before and after application of the strategy.

\begin{code}
opt :: Root -> Root
opt t = letToRoot (fromZipper t') 
  where  z :: Zipper (Let MemoTable)
         z = toZipper (buildMemoTree emptyMemo t)
         Just t' = applyTP (innermost step) z
         step = failTP `adhocTPZ` exprX `adhocTP` exprM
\end{code}


We have just presented our first zipper-based strategic AG
definition using memoized attributes. However, this optimization 
strategy is much slower than the previous definition
which does not use memoized attributes. To understand and solve this, 
we need to dig deeper into how internal data structure navigation in 
strategies is defined, through the zipper mechanism. 

The navigation in a zipper is provided by the generic zippers
library~\cite{adams2010zippers}. This library includes the |toZipper
:: Data a => a -> Zipper a| function that produces a zipper out of any
data type, requiring only that the data types have an instance of the
Data and Typeable type classes. It includes also functions |right|,
|left|, |down| and |up| to move the focus of the zipper towards the
corresponding directions. They all have type |Zipper a-> Maybe (Zipper
a)|, meaning that such functions take a zipper and return a new zipper
in case the navigation does not fail. There are also functions to get
and set the node the zipper is focusing on, namely |getHole ::
Typeable b => Zipper a -> Maybe b| and |setHole :: Typeable a => a ->
Zipper b -> Zipper b|.

While navigating in a zipper where memotables are nodes of the tree,
we need to guarantee that memotables are not considered when
traversing all the nodes of a memoized zipper.  
In fact, part of the performance loss of our new definition of |let|
derives from unneeded strategic traversals inside each node's memoization 
tables. 
Thus, we start by
defining new versions of the generic zippers functions that do avoid
navigating in some nodes of the tree, namely the memotable. Next, we
define function |right'| that has this behaviour.

\begin{code}
right' :: StrategicData a => Zipper a -> Maybe (Zipper a)
right' z = case right z of 
            Just r   -> if isNavigable r then Just r else right' r 
            Nothing  -> Nothing 
\end{code}

This function checks if the node we are traversing towards is
navigable as defined by function |isNavigable| (defined in class
|StrategicData| that we will explain in Section~\ref{sec:terminal}).  If it is, |right'| behaves as the zipper function 
|right| would.  If it
is not, we skip it by navigating to the right again. There is a
function named |left'| with an equivalent behaviour. In
Section~\ref{sec:terminal} we extend this notion of navigable nodes to
other (AG) symbols, and we will show how to define a memotable to not be navigable.

Having defined the memoization tables as nodes that are not navigable, 
the performance of the new |opt| implementation using memoized attributes 
is better, but still worse than the non-memoized version. 
%
The strategies previously shown assume that the underlying data
structure does not change. For a tree with several nodes, transforming
a single node should result in changing that same node without
impacting the rest of the tree. However, when combining strategies
with memoized AGs, this is not the case anymore. When traversing the
first node, we might compute an attribute which will traverse the
whole tree while computing and/or memoizing attributes in all of its
nodes, effectively changing them.

Because of this, a memoization-friendly version of the Ztrategic
library was developed. The focus of this version of the library will 
be in allowing the propagation of memoization throughout the data 
structure while traversing it. 
For each visited node, instead of returning
just the result of traversing that node, we also return the updated
zipper of the data structure. While keeping this in mind, the
semantics are kept as similar to the original implementation as
possible.

Before we present the combinators that navigate in memoized zippers,
let us start by presenting some basic functions that are their
building blocks. First, we introduce a function to elevate a
user-defined function to the zipper level. Let us recall
the original definition of this function, in |Ztrategic|:

> zTryApplyMZ  ::  (Typeable a, Typeable b)
>              =>  (a -> Zipper c -> Maybe b) -> TP c

\noindent Our new implementation of this function follows a similar type signature:

> zTryApplyMZ  ::  (Typeable a)
>              =>  (a -> Zipper c -> Maybe (Zipper c)) -> TP c

We omit the definition of |zTryApplyMZ| for brevity: it requires a
function that takes the node |a| to be transformed, as well as |Zipper
c| which points to the same node, and returns a |Maybe (Zipper c)|,
meaning either an updated zipper, or a |Nothing| value representing no
changes. 
Note that the required function should output an updated 
|Zipper c|, instead of a plain value |b| as was required in the original
definition, i.e. the function can be a memoized attribute, that updates memotables in the zipper.
The |zTryApplyMZ| function returns a |TP c|, in which TP is a
type for specifying Type-Preserving transformations on zippers, and
|c| is the type of the zipper. It is defined as follows:

> type TP a = Zipper a -> Maybe (Zipper a)

For example, if we are applying transformations on a zipper built upon
the |Let| data type, then those transformations are of type |TP Let|.
Similarly to Strafunski and Ztrategic, we also introduce the type |TU
m d| for Type-Unifying operations, which aim to gather data of type
|d| into the data structure |m|.

> type TU m d = (forall a. Zipper a -> (m d, Zipper a)) 


Unlike in Ztrategic, these transformations also return a |Zipper a|
value, which is the updated version of the input zipper. Therefore,
both Type-Preserving and Type-Unifying strategies will update the data
structure being traversed, as required by the memoized AGs.

Next, we define a combinator to compose two transformations, building a more complex zipper
transformation that tries to apply each of the initial transformations in sequence,
skipping transformations that fail.

> adhocTPZ :: Typeable a => TP (d m) -> 
>         (a -> Zipper (d m) -> Maybe (Zipper (d m))) -> TP (d m)
> adhocTPZ f g = maybeKeep f (zTryApplyMZ g)

Very much like the |adhocTP| combinator described in~\cite{flops22},
the |adhocTPZ| function receives transformations f and g as
parameters, as well as zipper |z|. The previously shown |zTryApplyMZ|
function changes |g| into a |TP| transformation, which is then
combined with |f| by function |maybeKeep|. 
Function |maybeKeep| tries to apply the second function it receives (here it
being the transformed |g| function), and if it fails, |f| is applied
instead.

Let us return to the |Let| optimization described in previous
section. Let us also consider a function |exprZM|, similar to |expr|
but receiving also a zipper as argument and returning an updated
zipper (we will be defining this function later). Then, we can use
|adhocTPZ| to combine the |exprZM| function with the default failing
strategy |failTP|:

> step = failTP `adhocTPZ` exprZM

% The transformation |failTP| is a pre-defined transformation that always fails (returning |Nothing|) and |idTP| is the identity transformation that always succeeds (returning the input unchanged).

The rest of the Ztrategic library is rewritten to accommodate for the
different definitions of the types of transformations |TP| and |TU|,
including the above functions |failTP| and |idTP|.  

Using the |right'| zipper navigation function defined before, 
we can now define for example a combinator that navigates in all navigable nodes
of a tree.


\begin{code}
allTPright :: StrategicData (d m) => TP (d m) -> TP (d m) 
allTPright f z = case right' z of 
                   Nothing  -> return (z, z)
                   Just r   -> fmap (fromJust . left') (f r)
\end{code}

This function is a combinator that, given a type-preserving
transformation |f| for zipper |z|, tries to 
travel to the node located to the right using zipper function |right'|, and if it succeeds,
it applies |f| and returns with function |left'|. If it fails, the original zipper is
returned. Because the result of |f| is an optional |Maybe| value,
we use |fmap| to apply navigation back to the left inside it.
%
%
%However, we opt to use modified versions of the zipper navigation
%functions; we also define the datatype |d m| as an instance of
%|StrategicData|. Any instance of |StrategicData| must define which
%nodes are considered terminal symbols and thus should be skipped.
%
%
As expected, there is also a similar |allTPdown| combinator that navigates
downwards on the zipper.

The definition of high-level strategies, such as |full_tdTP| (full,
top-down, type-preserving), is similar to |Ztrategic|. However, the
input data must be an instance of |StrategicData| so that
they do consider the introduced navigable mechanisms. We refer to the definition of
|innermost| in~\cite{flops22}, and we show our updated definition:

> innermost  :: StrategicData (d m) => TP (d m) -> TP (d m)
> innermost s  =  repeatTP (once_buTP s)

The full API of our extended versions of Ztrategic is available 
in Figure~\ref{code:api} in appendix.

Let us return to our Let running example. Function |exprX| applied 
rule $7$ through the usage of memoized attributes, but with the 
updated data structures they produce being ignored. Let us now 
define function |exprMZ|, similar to function |exprX| but reusing 
the updated zippers that the memoized attributes produce, which we
label |z'| and |z''|.
We change this updated zipper by using the zipper
function |setHole| to set its current value to |expr|, which is the 
value we would return directly in previous definition |exprX|.

\begin{code}
exprZM  :: Let MemoTable -> Zipper (Let MemoTable) 
        -> Maybe (Zipper (Let MemoTable))
exprZM (Var i _)  z
   =  let  (e,  z')   = env z
           (l,  z'')  = lev z'
           expr :: Maybe (Let MemoTable)
           expr = fmap  (buildMemoTreeExp emptyMemo) $
                        expand (i, l) e
      in   fmap (\k -> setHole k z'') expr      
exprZM _          z  =  Nothing
\end{code}

We once again define a strategy for optimization of |Let| expressions, 
using memoized attributes and the new |Ztrategic| library module for 
propagation of memoization tables. We use the |exprM| function defined 
previously, as it does not depend on attributes and thus does not need
any changes. 

\begin{code}
opt :: Root -> Root
opt t = letToRoot (fromZipper $  fromJust
                                 (applyTP (innermost step) z)) 
 where  z :: Zipper (Let MemoTable)
        z = toZipper (buildMemoTree emptyMemo t)
        step = failTP `adhocTPZ` exprZM `adhocTP` exprM
\end{code}

This version of the |Let| expression optimization strategy is much more 
efficient than the non-memoized version presented before. Although this 
memoization mechanism introduces some
intrusive code in our definitions, the improvement in terms of
runtime does pay this effort. We compare the performance of
non-memoized and memoized approaches in detail in
Section~\ref{sec:performance}.


%\todo{One caveat in this approach is that the responsibility on keeping the memoization tables correctly updated is on the user. When  a node in a data structure is transformed, the resulting node might  have an outdated memoization table, storing values which are now  incorrect. Transforming one node might also invalidate the  memoization tables of the surrounding nodes, due to their inherited  or synthesized attributes depending on said node. For our running  example, there are no such cases and the transformations applied do  not invalidate the existing memoization tables, but they did,  additional care would have to be employed to, for example, empty the  relevant memoization tables of their outdated contents.}



%\subsection{Memoization Table Clean-up}
\subsection{Memoization Table Correctness}

The running example in this paper does not address an underlying
concern with attribute memoization, specifically, the invalidation of
outdated attribute computations. Since Type-Preserving strategies
change the underlying data structure, actions such as updates, insertions and
removal of nodes can make the values of certain attributes invalid. If
an attribute value is memoized first and then the data structure is
changed, the previous value is kept memoized, and thus following
attribute computations on that node yield incorrect results.

We address this problem by providing two alternative strategy
application patterns for data structures with memoization. We use
|applyTP_unclean| when we do not care about cleaning the memoization
tables after application of a strategy, which would be the case for
the running example in this paper. If such cleaning of memoization
tables is required, the combinator |applyTP| will perform such
cleaning after application of a strategy.


%\todo{|adds| in appendix? together with |dcli| the full let definition.}

We showcase this problem by defining a simple attribute, named |adds|,
which counts the number of |Add| nodes in a |Let| tree. The number of
|Add| nodes in a |Let| tree decreases when optimization |opt| is
applied due to several optimization rules being targeted at these
nodes. As such, we define:

\begin{code}
optValue_unclean l = adds (opt_unclean (adds l))
optValue_clean   l = adds (opt_clean   (adds l))
\end{code}

Both of these values contain an initial usage of attribute |adds|
which 1) counts the number of |Add| nodes in a given |Let| (we ignore
this value), and 2) memoizes all computed attributes in said
|Let|. With the values of |adds| already memoized in the data
structure, one variant of |opt| is then used to optimize the data
structure. Finally, attribute |adds| is used again to compute the
number of |Add| nodes, and this usage will attempt to look up the
memoized values in the data structure. For |optValue_unclean|, the
incorrect, pre-optimization value is obtained, while |optValue_clean|
will produce the correct result.

We can guarantee node-to-node correctness of attributes through the 
usage of the non-memoized version of |Ztrategic|, such that the usage 
of memoized attributes does not change the underlying data structure's 
memoization tables, computing only the attribute value. We do this 
when defining |exprX|, which we have concluded to be inefficient. 

Thus we have the trade-off of gaining performance at the cost of 
potential correctness of the results when using memoized attributes 
with strategies. It is the responsibility of the programmer to 
be careful on if any memoized attributes change with the transformations
being applied to the data structure, and if they do, to use the proper 
mechanisms to work around it. For type unifying strategies, such 
concerns are unneeded as the data structure is being consumed. 

\subsection{Navigable Symbols}
\label{sec:terminal}

Since strategies (typically) traverse all nodes of a data structure,
every memoization table stored in every node would be treated as
additional data and be unnecessarily traversed.  As shown in the
definition of zipper function |right'|, we use the predicate
|isNavigable| to avoid traversing the memotable nodes.

When defining a strategic AG, however, there are other symbols that we
may not be traversed, since they are usually handled outside the
(attribute) grammar formalism. Indeed, the terminal symbols of a
grammar are usually handled outside the formalism. They are often
specified via regular expressions, and not by grammars. Moreover,
they are efficiently processed via efficient automata-based
recognizers. Thus, we consider terminal symbols as non navigable
symbols/values in our trees, as opposed to Strafunski and Ztrategic.

To allow for this behaviour, any data type to be traversed using this
library must define an instance of |StrategicData|. As an example, we
can define this instance easily with the default behaviour of not
skipping any nodes, for the Let datatype:

>instance StrategicData Let 

We could optimize this by instead of defining our own behaviour for our
data, for example by skipping any node that is an |Int| or a |String|,
which we expect to never want to traverse directly. That is not to say
that we cannot change any |Int| or |String| in our traversals; we
would expect to change |Strings| when traversing a |Var| constructor,
but not by directly visiting the |String| node (which in itself is a
list of |Char| that would also be traversed individually). 
We also define |MemoTable| as not navigable. We can
define it like so:

\begin{code}
instance StrategicData (Let MemoTable) where 
  isNavigable z  = not (isJust (getHole z :: Maybe MemoTable)
                 ||     isJust (getHole z :: Maybe String   )
                 ||     isJust (getHole z :: Maybe Int      ))
\end{code}


The |isNavigable| function should return false whenever |z| points
towards a terminal symbol/node or to the memotable node. In this case,
besides the memotable 
we consider only two terminal symbols, |String| and |Int|, but we
could define more complex logic in this function, such as skipping
only negative integers.

We reflect of the performance impact of this change in
section~\ref{sec:performance}. We also require this change to define
memoization in a strategic setting.
