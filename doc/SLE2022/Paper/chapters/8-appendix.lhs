
\section{Ztrategic API}
\label{sec:appZtrAPI}


\begin{figure*}[h]
\begin{minipage}[t]{.50\textwidth}
\textbf{Strategy types}
> type TP a = Zipper a -> Maybe (Zipper a)
> type TU m d = (forall a. Zipper a -> (m d, Zipper a)) 
\textbf{Strategy Application}
> applyTP :: TP a -> Zipper a -> Maybe (Zipper a)
> applyTP_unclean :: TP a -> Zipper a -> Maybe (Zipper a)
> applyTU :: TU m d -> Zipper a -> (m d, Zipper a) 
> applyTU_unclean :: TU m d -> Zipper a -> (m d, Zipper a) 
\textbf{Primitive strategies}
> idTP      :: TP a
> constTU   :: d -> TU m d
> failTP    :: TP a 
> failTU    :: TU m d
> tryTP     :: TP a -> TP a
> repeatTP  :: TP a -> TP a
\textbf{Strategy Construction}
> monoTP    :: (a -> Maybe b) -> TP e
> monoTU    :: (a -> m d) -> TU m d
> monoTPZ   :: (a -> Zipper e -> Maybe (Zipper e)) -> TP e
> monoTUZ   :: (a -> Zipper e -> (m d, Zipper e)) -> TU m d
> adhocTP   :: TP e -> (a -> Maybe b) -> TP e
> adhocTU   :: TU m d -> (a -> m d) -> TU m d
> adhocTPZ  :: TP e -> (a -> Zipper e -> Maybe (Zipper e)) -> TP e
> adhocTUZ  :: TU m d -> 
>            (a -> Zipper c -> (m d, Zipper c)) -> TU m d
\textbf{Composition / Choice}
> seqTP     :: TP a -> TP a -> TP a
> choiceTP  :: TP a -> TP a -> TP a
> seqTU     :: TU m d -> TU m d -> TU m d
> choiceTU  :: TU m d -> TU m d -> TU m d
\textbf{AG Combinators}
> (.$) :: Zipper a -> Int -> Zipper a
> parent :: Zipper a -> Zipper a
> (.|) :: Zipper a -> Int -> Bool
\end{minipage}
\begin{minipage}[t]{.40\textwidth}
\textbf{Traversal Combinators}
> allTPright  :: TP a -> TP a
> oneTPright  :: TP a -> TP a
> allTUright  :: TU m d -> TU m d
> allTPdown   :: TP a -> TP a
> oneTPdown   :: TP a -> TP a
> allTUdown   :: TU m d -> TU m d
\textbf{Traversal Strategies}
> full_tdTP  :: TP a -> TP a
> full_buTP  :: TP a -> TP a
> once_tdTP  :: TP a -> TP a
> once_buTP  :: TP a -> TP a  
> stop_tdTP  :: TP a -> TP a
> stop_buTP  :: TP a -> TP a
> innermost  :: TP a -> TP a
> outermost  :: TP a -> TP a
> full_tdTU  :: TU m d -> TU m d     
> full_buTU  :: TU m d -> TU m d     
> once_tdTU  :: TU m d -> TU m d   
> once_buTU  :: TU m d -> TU m d  
> stop_tdTU  :: TU m d -> TU m d  
> stop_buTU  :: TU m d -> TU m d
> foldr1TU   :: TU m d -> Zipper e -> (d -> d -> d) -> d
> foldl1TU   :: TU m d -> Zipper e -> (d -> d -> d) -> d
> foldrTU    :: TU m d -> Zipper e -> (d -> c -> c) -> c -> c
> foldlTU    :: TU m d -> Zipper e -> (c -> d -> c) -> c -> c
\textbf{Memoized AG Combinators}
> (.@.) :: (Zipper a -> (r, Zipper a)) 
>           -> Zipper a -> (r, Zipper a)
> atParent :: (Zipper a -> (r, Zipper a)) 
>           -> Zipper a -> (r, Zipper a)
> atRight :: (Zipper a -> (r, Zipper a)) 
>           -> Zipper a -> (r, Zipper a)
> atLeft :: (Zipper a -> (r, Zipper a)) 
>           -> Zipper a -> (r, Zipper a)
> memo :: attr -> AGTree_m d m a -> AGTree_m d m a
\end{minipage}
\caption{Full Memoized Ztrategic API and AG Combinators}
\label{code:api}
\end{figure*}

