
\begin{abstract}


Strategic term re-writing and attribute grammars are two powerful
programming techniques widely used in language engineering. The
former, relies on strategies to apply term re-write rules in defining
large-scale language transformations, while the latter is suitable to
express context-dependent language processing algorithms. These two
techniques can be expressed and combined via a powerful navigation
abstraction: generic zippers. In fact, this results in a concise
zipper-based embedding offering the expressiveness of both techniques.

Such elegant embedding, however, has a severe limitation since it
recomputes attribute values. This paper presents a proper and
efficient embedding of both techniques. First, attribute values are
memoized in the zipper data structure, thus avoiding their
re-computation. Moreover, strategic zipper based functions are adapted
to access such memoized values.  We have implemented our memoized
embedding as the Ztrategic library and we benchmarked it against the
state-of-the-art Strafunski and Kiama libraries. Our first results
show that we are already at least as  fast as those two well established libraries.



\end{abstract}
