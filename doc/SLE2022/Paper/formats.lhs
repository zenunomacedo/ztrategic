



%format box_m = "\fbox{m}"



%format tleft = "\mathit{left}"
%format tright = "\mathit{right}"
%format ^ = " "
%format ^^ = "\;"
%format DATA = "\mathbf{DATA}"
%format ATTR = "\mathbf{ATTR}"
%format SEM = "\mathbf{SEM}"
%format lhs = "\mathbf{lhs}"
%format . = "\;.\;"
%format * = "\times"
%format (A(n)(f)) = @ n . f
%format (ANT(n)) = @ n 



%format down'    = "\textbf{down\textquoteright}"
%format down     = "\textbf{down}"
%format up       = "\textbf{up}"
%format left     = "\textbf{left}"
%format right    = "\textbf{right}"
%format getHole  = "\textbf{getHole}"
%format setHole  = "\textbf{setHole}"
%format toZipper = "\textbf{toZipper}"
%format Zipper   = "\textbf{Zipper}"
%format trans    = "\textbf{trans}"
%format transM   = "\textbf{transM}"



%format >>= = "\bind"

%format full_tdTP = "\mathit{full\_tdTP}"
%format full_tdTU = "\mathit{full\_tdTU}"
%format once_tdTP = "\mathit{once\_tdTP}"
%format once_tdTU = "\mathit{once\_tdTU}"
%format stop_tdTP = "\mathit{stop\_tdTP}"
%format stop_tdTU = "\mathit{stop\_tdTU}"

%format full_buTP = "\mathit{full\_buTP}"
%format full_buTU = "\mathit{full\_buTU}"
%format once_buTP = "\mathit{once\_buTP}"
%format once_buTU = "\mathit{once\_buTU}"
%format stop_buTP = "\mathit{stop\_buTP}"
%format stop_buTU = "\mathit{stop\_buTU}"
%format lexeme_Assign = "\textsf{lexeme\_Assign}"
%format lexeme_Assign2 = "\textsf{lexeme\_Assign}"


%format t_1 = "\mathit{t_1}"
%format t_2 = "\mathit{t_2}"
%format t_3 = "\mathit{t_3}"
%format t_4 = "\mathit{t_4}"
%format t_5 = "\mathit{t_5}"
%format t_6 = "\mathit{t_6}"
%format t_7 = "\mathit{t_7}"


%format mkAG        = "\textsf{mkAG}"
%format AGTree      = "\textsf{AGTree}"
%format .$          = "\textsf{.\$}"
%format .$>         = "\textsf{.\$\textgreater}"
%format .$<         = "\textsf{.\$\textless}"
%format .|          = "\textsf{.$\vert$}"
%format parent      = "\textsf{parent}"
%format lexeme      = "\textsf{lexeme}"
%format constructor = "\textsf{constructor}"

%format memo = "\textbf{memo}"


%format BlockL  = "\matem{Block}"
%format decl    = "\mattt{decl}"
%format use     = "\mattt{use}"

%format P       = "\bnfnt{P}"
%format Its     = "\bnfnt{Its}"
%format It      = "\bnfnt{It}"
%format ConsIts = "\bnfprod{ConsIts}"
%format NilIts  = "\bnfprod{NilIts}"
%format Decl    = "\bnfprod{Decl}"
%format Use     = "\bnfprod{Use}"
%format Block   = "\bnfprod{Block}"

%format Name    = "\bnfnt{Name}"
%format Env     = "\bnfnt{Env}"
%format Errors  = "\bnfnt{Errors}"

%format fun_errors = "\textit{errors}"
%format fun_environment = "\textit{environment}"

%format box_dclo = "\fbox{\attrid{dclo}}"
%format box_dclo_2 = "\fbox{\attrid{$dclo_2$}}"
%format box_lev = "\fbox{\attrid{lev}}"
%format box_lev_1 = "\fbox{\attrid{lev + 1}}"
%format box_errors = "\fbox{\attrid{errors}}"
%format dcli = "\attrid{dcli}"
%format dclo = "\attrid{dclo}"
%format lev = "\attrid{lev}"
%format env = "\attrid{env}"
%format errors = "\attrid{errors}"
%format mustBeIn = "\bnfnt{mustBeIn}"
%format mustNotBeIn = "\bnfnt{mustNotBeIn}"


%format errorsAsBlock = "\attrid{errorsAsBlock}"
%format errorsAsBlockRoot = "\attrid{errorsAsBlockP}"
%format concatErrors = "\bnfnt{concatErrors}"

%format box_errors_1 = "\fbox{\attrid{$errors_1$}}"
%format box_errors_2 = "\fbox{\attrid{$errors_2$}}"


%format CRoot   = "\bnfprod{$\mathit{Root_{P}}$}"
%format CLet   = "\bnfprod{$\mathit{Let_{Let}}$}"
%format CAssign      = "\bnfprod{$\mathit{Assign_{List}}$}"
%format CNestedLet   = "\bnfprod{$\mathit{NestedLet_{List}}$}"
%format CEmptyList   = "\bnfprod{$\mathit{EmptyList_{List}}$}"
%format CAdd   = "\bnfprod{$\mathit{Add_{Exp}}$}"
%format CSub   = "\bnfprod{$\mathit{Sub_{Exp}}$}"
%format CNeg   = "\bnfprod{$\mathit{Neg_{Exp}}$}"
%format CVar   = "\bnfprod{$\mathit{Var_{Exp}}$}"
%format CConst   = "\bnfprod{$\mathit{Const_{Exp}}$}"

%format visit = "\textbf{visit}"
%format LRC = "\textit{Lrc}"


%format Its_2 = "\bnfnt{$Its_{2}$}"
%format It_2 = "\bnfnt{$It_{2}$}"

%format ConsIts_2 = "\bnfprod{$\mathit{ConsIts_{2}}$}"
%format NilIts_2  = "\bnfprod{$\mathit{NilIts_{2}}$}"
%format Block_2   = "\bnfprod{$\mathit{Block_{2}}$}"
%format Decl_2    = "\bnfprod{$\mathit{Decl_{2}}$}"
%format Use_2     = "\bnfprod{$\mathit{Use_{2}}$}"

%format eval_P    = "\mathit{eval\_P}"
%format eval_Its    = "\mathit{eval\_Its}"
%format eval_It     = "\mathit{eval\_It}"
%format eval_Its_1  = "\mathit{eval\_Its_{1}}"
%format eval_It_1   = "\mathit{eval\_It_{1}}"
%format eval_Its_2  = "\mathit{eval\_Its_{2}}"
%format eval_It_2   = "\mathit{eval\_It_{2}}"

%format its_2  = "\mathit{its_{2}}"
%format it_2   = "\mathit{it_{2}}"

%format errors_1   = "\attrid{$errors_{1}$}"
%format errors_2   = "\attrid{$errors_{2}$}"
%format errors_3   = "\attrid{$errors_{3}$}"

%format dclo_2   = "\attrid{$dclo_{2}$}"