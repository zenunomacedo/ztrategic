
%%%% Generic manuscript mode, required for submission
%%%% and peer review
%\documentclass[manuscript,screen,review]{acmart}

\documentclass[sigplan,anonymous,review]{acmart}
%\documentclass[sigplan]{acmart}


%include lhs2TeX.fmt
%include lhs2TeX.sty


%%%%% quick and dirty workaround to the Bbbk problem: read https://github.com/kosmikus/lhs2tex/issues/82
\let\Bbbk\undefined
%include polycode.fmt
\let\Bbbk\undefined


\usepackage{amsmath}
\usepackage{caption}
\usepackage{float}
\usepackage{xspace}
%\usepackage[T1]{fontenc}

%\usepackage[usenames]{color}

%\input{haskell_newcolors.sty}
%\input{ag.sty}
\input{haskell_newcolorsBW.sty}
\input{agBW.sty}


%include formats.lhs


\newcommand{\discuss}[1]{\textcolor{blue}{#1}\xspace}
\newcommand{\todo}[1]{\textcolor{red}{#1}\xspace}
\renewcommand{\hscodestyle}{\small}


%%
%% \BibTeX command to typeset BibTeX logo in the docs
\AtBeginDocument{%
  \providecommand\BibTeX{{%
    \normalfont B\kern-0.5em{\scshape i\kern-0.25em b}\kern-0.8em\TeX}}}


%% Rights management information.  This information is sent to you
%% when you complete the rights form.  These commands have SAMPLE
%% values in them; it is your responsibility as an author to replace
%% the commands and values with those provided to you when you
%% complete the rights form.
\setcopyright{acmcopyright}
\copyrightyear{2018}
\acmYear{2018}
\acmDOI{10.1145/1122445.1122456}

%% These commands are for a PROCEEDINGS abstract or paper.
%\acmConference[Woodstock '18]{Woodstock '18: ACM Symposium on Neural
%  Gaze Detection}{June 03--05, 2018}{Woodstock, NY}
%\acmBooktitle{Woodstock '18: ACM Symposium on Neural Gaze Detection,
%  June 03--05, 2018, Woodstock, NY}
%\acmPrice{15.00}
%\acmISBN{978-1-4503-XXXX-X/18/06}


%%
%% Submission ID.
%% Use this when submitting an article to a sponsored event. You'll
%% receive a unique submission ID from the organizers
%% of the event, and this ID should be used as the parameter to this command.
%%\acmSubmissionID{123-A56-BU3}

%%
%% The majority of ACM publications use numbered citations and
%% references.  The command \citestyle{authoryear} switches to the
%% "author year" style.
%%
%% If you are preparing content for an event
%% sponsored by ACM SIGGRAPH, you must use the "author year" style of
%% citations and references.
%% Uncommenting
%% the next command will enable that style.
%%\citestyle{acmauthoryear}


\begin{document}

%%
%% The "title" command has an optional parameter,
%% \title[short title]{full title}
%% allowing the author to define a "short title" to be used in page headers.
\title{Efficient Embedding of Strategic Attribute Grammars via Memoization}

%%
%% The "author" command and its associated commands are used to define
%% the authors and their affiliations.
%% Of note is the shared affiliation of the first two authors, and the
%% "authornote" and "authornotemark" commands
%% used to denote shared contribution to the research.
\author{Jos\'e Nuno Macedo}
\email{jose.n.macedo@@inesctec.pt}
\orcid{0000-0002-0282-5060}
\affiliation{
  \institution{University of Minho}
  \city{Braga}
  \country{Portugal}
}

\author{Marcos Viera}
\email{mviera@@fing.edu.uy}
%\orcid{0000-0002-5686-7151}
\affiliation{
  \institution{Universidad de la República}
  \city{Montevideo}
  \country{Uruguay}
}

\author{Jo\~ao Saraiva}
\email{saraiva@@di.uminho.pt}
\orcid{0000-0002-5686-7151}
\affiliation{
  \institution{University of Minho}
  \city{Braga}
  \country{Portugal}
}


%\renewcommand{\shortauthors}{Trovato and Tobin, et al.}


%%
%% Keywords. The author(s) should pick words that accurately describe
%% the work being presented. Separate the keywords with commas.
\keywords{Strategic Programming, Attribute Grammars,  Zippers, Generic Traversals}


%include chapters/abstract.lhs

\maketitle


\section{Introduction}
\label{sec1}

%include chapters/introduction.lhs


\section{Zipping Strategies and Attribute Grammars}
\label{sec2}

%include chapters/zipperStrategicAG.lhs



\section{Combining Attribute Memoization with Zippers}
\label{sec:memoZippers}

%include chapters/memo.lhs


\section{Performance}

\label{sec:performance}

%include chapters/performance.lhs


\section{Related Work}
\label{sec:related}

%include chapters/6-relatedWork.lhs





\section{Conclusion}
\label{sec:conc}

%include chapters/7-conclusions.lhs

\begin{comment}
\section*{Acknowledgements}
\label{ack}
\small

\todo{This work is financed by National Funds through the Portuguese funding agency, FCT - Fundação para a Ciência e a Tecnologia, within project \texttt{LA/P/0063/2020}.} The first author is also sponsored by FCT grant \texttt{2021.08184.BD}. \todo{Emanuel grant? Thank Tony for his help with Kiama?}
\end{comment}

%%
%% The next two lines define the bibliography style to be used, and
%% the bibliography file.
\bibliographystyle{ACM-Reference-Format}
\bibliography{bibliography}

%%
%% If your work has an appendix, this is the place to put it.

\appendix

%include chapters/8-appendix.lhs


\end{document}
\endinput
