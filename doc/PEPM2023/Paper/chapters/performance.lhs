



In this section, we compare the performance of the Ztrategic library
with state-of-the-art libraries Strafunski and Kiama. In terms of
expressiveness, Ztrategic is capable of representing AGs, strategies,
and the combination of AGs and strategies in a unified setting. Thus,
we focus our analysis in the runtime and memory consumption of
different implementations of a Haskell code smell eliminator, a
|repmin| program, a |Let| optimizer (as described in this paper), and
an advanced multiple layout pretty printing algorithm.

All implementations of these strategic AGs, together with the
necessary resources (tests, scripts, etc) to replicate our study, are
available in our replication package as detailed in
Section~\ref{sec:conc}.  All tests were run 10 times and averaged 
in a ThinkPad 13 (2nd Gen,
Intel i7-7500U (4) 3.500GHz) laptop with 8 Gb RAM and EndeavourOS
Linux x86 64 bits.




\begin{figure*}[t]
\begin{minipage}[t]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{figure/smells_loc.png}
%\end{minipage}
%\begin{minipage}[t]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{figure/smellsM_loc.png}
\end{minipage}
\caption{Haskell Smells elimination: Ztrategic versus Strafunski}
\label{fig:smells}
\end{figure*}

\subsection{Strategic Haskell Smell Elimination}



Source code smells make code harder to comprehend. A smell is not an
error, but it indicates a bad programming practice. Smells occur in
any language and Haskell is no exception. For example, inexperienced
Haskell programmers often write |l == []| to check whether a list is
empty, instead of using the predefined |null| function. We implemented
this full Haskell language refactoring tools as a pure strategic
program. It detects and eliminates all Haskell smells as reported
in~\cite{Cowie}.


In order to compare Ztrategic with the Haskell state-of-the-art
Strafunski counterpart we run both strategic solutions with a large
\textit{smelly} input. We consider 150 Haskell projects developed by
first-year students as presented in~\cite{projects}. In these projects
there are 1139 Haskell files totaling 82124 lines of code, of which
exactly 1000 files were syntactically correct~\footnote{The student
projects used in this benchmark are available at this work's
repository.}. Both Ztrategic and Strafunski smell eliminators detected
and eliminated 850 code smells in those files. Figure~\ref{fig:smells}
shows the runtime (left) and memory consumption (right) of running
both libraries.

There are three entries in these figures: a normal Ztrategic
implementation, a Ztrategic implementation in which we skip
unnecessary nodes (corresponding to \textit{terminal symbols}) in the
traversal, and a similar implementation in Strafunski. 

Strafunski outperforms Ztrategic, which is to be expected as Ztrategic
library has an additional overhead of creating and handling a zipper
over the traversed data. However, when skipping terminals, Ztrategic
has almost the same performance of the well established and fully
optimized Strafunski system.




\subsection{Repmin as a Strategic Program}

\begin{figure*}[t]
\begin{minipage}[t]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{figure/repminS.png}
%\end{minipage}
%\begin{minipage}[t]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{figure/repminSM.png}
\end{minipage}
\caption{Strategic Repmin: Ztrategic versus Strafunski versus Kiama}
\label{fig:repmin}
\end{figure*}


The |repmin| problem is a
well-known problem widely used to show the power of circular, lazy
evaluation as shown by Bird\cite{Bird84}.  The goal of this program is
to transform a binary leaf tree of integers into a new tree with the
exact same shape but where all leaves have been replaced by the
minimum leaf value of the original tree. The |repmin| problem can be
easily implemented by two strategic functions: First, a Type Unifying
strategy traverses the tree and computes its minimum value. Then, a
Type Preserving strategy traverses again the tree and constructs the
new tree, using the previously computed minimum.
In Figure~\ref{fig:repmin} we show the results of implementing these solutions using strategies in Ztrategic, Strafunski and Kiama.
%
%
Here, \textit{Repmin size} refers to the number of nodes the input binary tree contains. Again, Ztrategic behaves very similar to Strafunski and both outperform Kiama's implementation in speed and memory consumption.
%
\vspace{-.5cm}
%
%our embedding of strategic attribute grammars is able to perform large scale, real language transformations. Moreover, our embedding \discuss{is comparable to the performance of the state-of-the-art strategic library for Haskell. Our library, however, is more expressive as it allows for the usage of AG constructs. }
%do compare to the performance of the state-of-the-art strategic library for Haskell.  
%
%
%
%
\subsection{Repmin as an Attribute Grammar}

We also compare the performance of |repmin| when fully expressed as an
AG. Actually, the Kiama implementation of |repmin| is part of the
Kiama library. It is very similar to the Ztrategic
version of |repmin| in~\cite{memoAG19}, which we use here.

Figure~\ref{fig:repminAG} shows the results of comparing our memoized
implementation of |repmin| in Ztrategic using AGs, with Kiama. To be
able to compare the performance the strategic and AG solutions, 
we run them with exactly the same
inputs.

In Figure~\ref{fig:repminAG} (left) we can see that the non-memoized
version of Ztrategic increases its execution time exponentially and is
much slower than the other versions. 
~\footnote{All these runtime numbers, and the numbers used to produced all Figures, are available in a spreadsheet included in our replication package.}
However, when we zoom in to the
behavior of the other implementations in Figure~\ref{fig:repminAG}
right, we can notice that memoized Ztrategic outperforms Kiama. In
fact, for a tree with $40000$
nodes, the memoized Ztrategic AG runs in $0.3$ seconds, while Kiama
needs $0.78$ seconds to perform the same task. When we compare these
results with the strategic solution (shown in Figure~\ref{fig:repmin}), 
where Kiama is outperformed by Ztrategic and Strafunski, we see that, 
for $40000$ nodes, the AG version of Kiama's implementation is $5.65$ times
faster than the strategic implementation for the same library. 
Comparatively, Ztrategic's non-memoized AG is $292$ times slower than 
the strategic approach, while the non-memoized AG is $2.4$ times faster
than the strategic approach. The overall faster implementation is the 
strategic Strafunski's with a runtime of $0.27$ seconds, thus $1.11$ times 
faster than the memoized AG. The memoized AG is extremely competitive 
considering that is has added overload of handling zippers and 
a memoization table.

\begin{figure*}[t]
\begin{minipage}[t]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{figure/repminAG-expand.png}
%\end{minipage}
%\begin{minipage}[t]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{figure/repminAG.png}
\end{minipage}
\caption{AG Repmin. Left: Ztrategic versus memoized Ztrategic and Kiama. Right: memoized Ztrategic and Kiama.  
}
\label{fig:repminAG}
\end{figure*}

\vspace{-0.2cm}

\subsection{Let Optimization}

We have implemented the |let| strategic AG 
algorithm
in Kiama, as it also provides strategies and AGs. 
%; 
%it is
%very similar to the one presented in this paper: the optimization
%rules are implemented via strategies, and strategies in Kiama also
% have access to attribute values.

Figure~\ref{fig:letKiama} shows the performance of optimizing several
Let inputs, in terms of runtime and memory usage. Along the X axis,
\textit{Let size} refers to the number of nested |let| blocks
contained in the input data to be optimized. The Ztrategic
implementation once again vastly outperforms Kiama in both runtime and
memory consumption, for any input size. Because the Kiama
implementation shows a poor performance, compared to our memoized
strategic AG, we also include Kiama's baseline execution: it just
generates the input AST and prints it without performing any
optimization. 
%That is to say, with no strategy nor attribute being
%computed. 
This task already takes more time than the optimization in
the memoized Ztrategic. Kiama uses an advanced mechanism to combine
strategies and AGs where ASTs are defined by reachability
relations~\cite{respectyourparents}. The mechanism to transform a tree
into relations already induces a significant overhead in the Kiama
baseline execution. This also drastically influences the memory
usage of Kiama's solutions as we can see in the Kiama's 
solution for the |repmin| problem in Figure~\ref{fig:repmin}.

%
%
%Obviously, Kiama is based on Scala, which does
%not compile directly to binary code (as the Haskell compiler
%does). However, this s
%
%\todo{somewhere we should also say that memo + cleaning performs even worse than Kiama}
%
\begin{figure*}[htb!]
\begin{minipage}[htb!]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{figure/let.png}
%\end{minipage}
%\begin{minipage}[htb!]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{figure/letM.png}
\end{minipage}
\caption{Let Optimization: Ztrategic versus Kiama}
\label{fig:letKiama}
\end{figure*}
%
\begin{table}[]
\caption{Runtime of pretty printing |Let| Expressions for increasingly larger |let| inputs.  }
\centering \small
\begin{tabular}{||l||r||r||r||r||r||r||} 
\hline
\textbf{}                    & \textbf{let 1} & \textbf{let 2} & \textbf{let 3} & \textbf{let 4}              & \textbf{let 5}              & \textbf{let 6}                            \\ \hline
\textbf{AG}      & 0          & 0,02       & 0,1        & 0,91                    & 7,95                    & 68,49                                     \\ \hline
\textbf{MemoAG}  & 0,01       & 0,01       & 0,02       & 0,03                    & 0,06                    & 0,1                                       \\ \hline
\textbf{Kiama}   & 0,72       & 5,79       & 292,69     & \multicolumn{1}{c||}{--} & \multicolumn{1}{c||}{--} & \multicolumn{1}{c||}{--}  \\ \hline
\end{tabular}
\label{tab:prettyprint}
\vspace{-0.6cm}
\end{table}
%

\vspace{-0.2cm}

\subsection{Multiple Layout Pretty Printing}

We have expressed the large and complex optimal pretty printing AG,
presented in~\cite{Swierstra1999Combinator}, both in the Ztrategic and
Kiama libraries. This AG specifies a multiple layout pretty printer
 that adapts the layout according to the available width of
the page. Indeed, it defines a complex four traversal algorithm and it
is one of the most complex AG available.

We used the Ztrategic and Kiama versions of this algorithm to pretty
print |let| expressions from our running
example. Table~\ref{tab:prettyprint} presents the runtime in seconds
of executing the same pretty printing with the non-memoized and
memoized Ztrategic and Kiama AG embeddings. Here, the number 
of a |Let| expression refers to its nesting level, such that |Let n| will
have |n| nested let declarations as well as $10$ variable declarations for 
each nested declaration.

As expected the memoized Ztrategic solution is much faster than the
non-memoized counterpart. The Kiama solution shows the
poorest performance and is only able to pretty print the
smaller three |let| expressions. This large AG defines many attributes
and the tree/attribute relations mechanism used by Kiama to fully
support strategic AGs do induce a considerable overhead.


\vspace{-0.4cm}

%The results of these two benchmarks clearly show that
%Ztrategic is capable of consistently outperform Kiama for different
%input sizes for different benchmarks. These results could be partially
%explained due to the fact that Kiama runs on the Java Virtual Machine
%which is expected to underperform when compared to natively compiled
%software.
