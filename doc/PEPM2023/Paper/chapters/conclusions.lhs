This paper presented an embedding of strategic attribute grammars,
which combine strategic term re-writing and the attribute grammar
formalism. Both embeddings rely on memoized zippers: attribute values
are memoized in the zipper data structure, thus avoiding their
re-computation. Strategic zipper based functions access
such memoized values.

We compared the performance of our embedding with state-of-the-art
libraries Strafunski and Kiama. We have implemented in these
three libraries several language engineering tasks, namely, a let 
optimizer, a code refactor, and an advanced pretty
printing algorithm. Our results show that the embedding strategic term
re-writing behaves very similar to Strafunski. Our
results also show that our embedding of strategic AGs vastly
outperforms Kiama's solutions.