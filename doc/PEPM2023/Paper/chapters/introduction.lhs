


Strategic term re-writing~\cite{strategies97} and Attribute Grammars
(AG) \cite{knuth1968semantics} are two powerful language engineering
techniques. The former provides an abstraction to define
program/tree transformations: a set of re-write rules is applied while
traversing the tree in some pre-defined recursion pattern, the
strategy. The latter extends context-free grammars with attributes in order to specify static, context-dependent language algorithms.

There are many tools that support these techniques for the
implementation of (domain specific) programming
languages~\cite{eli,syngen,lrc,lisa,jastadd,uuag,silver,asfsdf,tom,strafunski,txl,kiama,stratego}. Unfortunately,
most of these tools are large systems supporting one of the
techniques, using their own AG or strategic specification language. As
a consequence, they would require a considerable effort to extend and
combine. There are, however, two exceptions: the Silver
system~\cite{silver} and the Kiama library~\cite{kiama} do support
both techniques.

More recently, a combined embedding of the two techniques has been
proposed in~\cite{flops22}. This embedding relies on a generic
mechanism to navigate on both homogeneous and heterogeneous trees:
generic zippers~\cite{thezipper,adams2010zippers}. Since both
attribute grammars and strategies rely on the same generic tree
traversal mechanism, each of the techniques can be expressed by
generic zippers as shown in ~\cite{zipperAG,scp2016}, for AGs, and
in~\cite{flops22}, for strategic term re-writing. The embedding of the
two techniques in the same simple setting has a key advantage: AGs and
strategies embeddings can be easily combined, thus providing language
engineers the best of the two worlds.

As previously shown in~\cite{memoAG19}, the simple zipper-based
embedding of AGs~\cite{zipperAG,scp2016} does not provide a proper
embedding of the formalism: attribute values are re-calculated during
the decoration of the tree. This not only goes against the semantics
of AG formalism, where one attribute value is computed at most once,
but it also dramatically affects the attribute evaluator's
performance. The combined embedding of strategies and AGs in that
setting, as proposed in~\cite{flops22}, has exactly the same
performance issues.

In order to provide an efficient zipper-based embedding of strategic
term re-writing and attribute grammars, that we call strategic AGs, we
implement zipper-based strategies on top of the memoized zipper-based
embedding of AGs~\cite{memoAG19}. Thus, strategies access memoized
attribute values in the tree nodes, rather than having to re-compute
such attribute values via the inefficient (non-memoized) embedding of
AGs, as proposed in~\cite{flops22}. The purpose of this paper is three-fold:

\begin{itemize}


\item Firstly, we define zipper-based strategic combinators that can
access memoized attribute values as supported by the efficient memoized
embedding of zipper-based AGs~\cite{memoAG19}. Thus, we extend the
Ztrategic library, developed in~\cite{flops22}, with new combinators
which work on trees where attribute values are memoized in the tree's
nodes.

\item Secondly, we improve the performance of the zipper-based
strategic combinators. Influenced by the (attribute) grammar
formalism, where terminal symbols are more suitable handled outside
the formalism (usually specified via regular expressions and processed
via efficient automata-based recognizers), we update zipper-based
Ztrategic library combinators to not traverse such symbols. This does
not limit the expressiveness of the strategic library, but does result
in a considerable performance improvement of the implementations.

\item Thirdly, we perform a detailed study on the performance of the
non-memoized implementation proposed in~\cite{flops22} and our
implementations.  We consider four well-known language engineering
tasks, namely, name analysis, program optimization, code smell
elimination and pretty printing, which we elegantly expressed in the
strategic and/or AG programming styles. Then, we compare the
performance of our implementations against the state of the art
Strafunski~\cite{strafunski} system - the Haskell incarnation of
strategic term re-writing - and Kiama~\cite{kiama} - the combined
embedding of strategies and AGs in Scala.


\end{itemize}

Our preliminary results are surprising: the embedding of
strategic term re-writing behaves similarly to Strafunski. However, the
embedding of strategic AGs vastly outperforms Kiama's solutions.

This paper is organized as follows: Section~\ref{sec2} introduces 
strategic term re-writing, attribute grammars, a combined embedding 
of strategic AGs, and memoized AGs. 
Section~\ref{sec:memoZippers} combines memoized AGs with strategies, 
and details a different implementation of |Ztrategic| that maximizes 
efficiency for the usage of memoized AGs; the concept of navigable 
symbols is also introduced in this library. 
Section~\ref{sec:performance} compares the performance of our work 
with the Strafunski and Kiama libraries, and elaborates on the 
obtained results. 
Section~\ref{sec:related} details the relevant state of the art on 
strategic programming and AGs. 
Section~\ref{sec:conc} concludes our work and presents links to 
the relevant libraries and to a replication package. 
