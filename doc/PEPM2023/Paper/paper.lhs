
%%%% Generic manuscript mode, required for submission
%%%% and peer review

%\documentclass[manuscript,screen,review]{acmart}

%\documentclass[sigplan,anonymous,review]{acmart}
%screen option recommended by the sigplan page, adds colors to hyperlinks
%\documentclass[sigplan,screen,review]{acmart}
\documentclass[sigplan,screen]{acmart}

%We can control the size of code blocks by changing these values. 
%We can obtain the original ones by commenting this out
\AtBeginDocument{
\setlength{\abovedisplayskip}{1pt}
\setlength{\belowdisplayskip}{1pt}
\setlength{\abovedisplayshortskip}{1pt}
\setlength{\belowdisplayshortskip}{1pt}
}

%include lhs2TeX.fmt
%include lhs2TeX.sty


%%%%% quick and dirty workaround to the Bbbk problem: read https://github.com/kosmikus/lhs2tex/issues/82
\let\Bbbk\undefined
%include polycode.fmt
\let\Bbbk\undefined


\usepackage{amsmath}
\usepackage{caption}
\usepackage{float}
\usepackage{xspace}
\usepackage{microtype}
\usepackage{enumitem}
\usepackage{cuted}
%\usepackage[T1]{fontenc}

%\usepackage[usenames]{color}

%\input{haskell_newcolors.sty}
%\input{ag.sty}
\input{haskell_newcolorsBW.sty}
\input{agBW.sty}


%include formats.lhs


\newcommand{\discuss}[1]{\textcolor{blue}{#1}\xspace}
\newcommand{\todo}[1]{\textcolor{red}{#1}\xspace}
\renewcommand{\hscodestyle}{\small}


%%
%% \BibTeX command to typeset BibTeX logo in the docs
\AtBeginDocument{%
  \providecommand\BibTeX{{%
    \normalfont B\kern-0.5em{\scshape i\kern-0.25em b}\kern-0.8em\TeX}}}


%%% The following is specific to PEPM '23 and the paper
%%% 'Efficient Embedding of Strategic Attribute Grammars via Memoization'
%%% by José Nuno Macedo, Emanuel Rodrigues, Marcos Viera, and João Saraiva.
%%%
\setcopyright{acmcopyright}
\acmPrice{15.00}
\acmDOI{10.1145/3571786.3573019}
\acmYear{2023}
\copyrightyear{2023}
\acmSubmissionID{poplws23pepmmain-p10-p}
\acmISBN{979-8-4007-0011-8/23/01}
\acmConference[PEPM '23]{Proceedings of the 2023 ACM SIGPLAN International Workshop on Partial Evaluation and Program Manipulation}{January 16--17, 2023}{Boston, MA, USA}
\acmBooktitle{Proceedings of the 2023 ACM SIGPLAN International Workshop on Partial Evaluation and Program Manipulation (PEPM '23), January 16--17, 2023, Boston, MA, USA}
\received{2022-10-18}
\received[accepted]{2022-11-15}


%%
%% The majority of ACM publications use numbered citations and
%% references.  The command \citestyle{authoryear} switches to the
%% "author year" style.
%%
%% If you are preparing content for an event
%% sponsored by ACM SIGGRAPH, you must use the "author year" style of
%% citations and references.
%% Uncommenting
%% the next command will enable that style.
%%\citestyle{acmauthoryear}


\begin{document}

%%
%% The "title" command has an optional parameter,
%% \title[short title]{full title}
%% allowing the author to define a "short title" to be used in page headers.
\title{Efficient Embedding of Strategic Attribute Grammars via Memoization}

%%
%% The "author" command and its associated commands are used to define
%% the authors and their affiliations.
%% Of note is the shared affiliation of the first two authors, and the
%% "authornote" and "authornotemark" commands
%% used to denote shared contribution to the research.
\author{Jos\'e Nuno Macedo}
\email{jose.n.macedo@@inesctec.pt}
\orcid{0000-0002-0282-5060}
\affiliation{
  \institution{HASLab \& INESC TEC, University of Minho}
  \city{Braga}
  \country{Portugal}
}

\author{Emanuel Rodrigues}
\email{jose.e.rodrigues@@inesctec.pt}
\orcid{0000-0003-4317-1144}
\affiliation{
  \institution{HASLab \& INESC TEC, University of Minho}
  \city{Braga}
  \country{Portugal}
}

\author{Marcos Viera}
\email{mviera@@fing.edu.uy}
\orcid{0000-0003-2291-6151}
\affiliation{
  \institution{Universidad de la República}
  \city{Montevideo}
  \country{Uruguay}
}

\author{Jo\~ao Saraiva}
\email{saraiva@@di.uminho.pt}
\orcid{0000-0002-5686-7151}
\affiliation{
  \institution{HASLab \& INESC TEC, University of Minho}
  \city{Braga}
  \country{Portugal}
}


%\renewcommand{\shortauthors}{Trovato and Tobin, et al.}

\begin{CCSXML}
<ccs2012>
   <concept>
       <concept_id>10011007.10011074.10011081.10011082</concept_id>
       <concept_desc>Software and its engineering~Software development methods</concept_desc>
       <concept_significance>500</concept_significance>
       </concept>
   <concept>
       <concept_id>10011007.10011006.10011060.10011690</concept_id>
       <concept_desc>Software and its engineering~Specification languages</concept_desc>
       <concept_significance>300</concept_significance>
       </concept>
   <concept>
       <concept_id>10003752.10010124.10010138</concept_id>
       <concept_desc>Theory of computation~Program reasoning</concept_desc>
       <concept_significance>100</concept_significance>
       </concept>
 </ccs2012>
\end{CCSXML}

\ccsdesc[500]{Software and its engineering~Software development methods}
\ccsdesc[300]{Software and its engineering~Specification languages}
\ccsdesc[100]{Theory of computation~Program reasoning}

%%
%% Keywords. The author(s) should pick words that accurately describe
%% the work being presented. Separate the keywords with commas.
\keywords{Strategic Programming, Attribute Grammars,  Zippers, Generic Traversals}


%include chapters/abstract.lhs

\maketitle


\section{Introduction}
\label{sec1}

%include chapters/introduction.lhs


\section{Zipping Strategies and Attribute Grammars}
\label{sec2}

%include chapters/zipperStrategicAG.lhs



\section{Combining Attribute Memoization with Zippers}
\label{sec:memoZippers}

%include chapters/memo.lhs


\section{Performance}

\label{sec:performance}
%
%include chapters/performance.lhs
%
\section{Related Work}
\label{sec:related}
%
%include chapters/relatedWork.lhs
%
\section{Conclusion}
\label{sec:conc}
%
%include chapters/conclusions.lhs
%
%
\vspace{-0.4cm}
\section*{Acknowledgements}
\label{ack}
%\small
This work is financed by National Funds through the Portuguese funding agency, FCT - Fundação para a Ciência e a Tecnologia, within project LA/P/0063/2020. The first author is also sponsored by FCT grant \texttt{2021.08184.BD}.
We would like to thank Tony Sloane for his validation of the Kiama implementations used in Section~\ref{sec:performance}. 


\vspace{-0.4cm}
\section*{Replication Packages} 

\noindent
All the necessary resources to replicate this study, as well as the full set of results, are publicly available as embedded hyperlinks for the base \href{https://hackage.haskell.org/package/ZipperAG-0.9}{ZippersAG}, \href{https://hackage.haskell.org/package/Strafunski-StrategyLib}{Strafunski}, \href{https://bitbucket.org/zenunomacedo/ztrategic/}{Ztrategic} and \href{https://github.com/inkytonik/kiama}{Kiama} libraries as well as \href{https://tinyurl.com/pepm2023tools}{Examples} used in this paper.
%\fbox{%
%\parbox{0.9\columnwidth}{%
%\scriptsize
%All the necessary resources to replicate this study, as well as the full set of results, are publicly available:
%\begin{itemize}[leftmargin=*]
%\item  \textbf{ZippersAG:} \textit{\url{https://hackage.haskell.org/package/ZipperAG-0.9}}
%\item  \textbf{Strafunski:} \textit{\url{https://hackage.haskell.org/package/Strafunski-StrategyLib}}
%\item  \textbf{Ztrategic:} \textit{\url{https://bitbucket.org/zenunomacedo/ztrategic/}}
%\item  \textbf{Kiama:} \textit{\url{https://github.com/inkytonik/kiama}}
%\item  \textbf{Examples:} \textit{\url{https://tinyurl.com/pepm2023tools}}
%\end{itemize}
%}%
%}

\appendix
%%
%% If your work has an appendix, this is the place to put it.


%include chapters/appendix.lhs

%%
%% The next two lines define the bibliography style to be used, and
%% the bibliography file.
\bibliographystyle{ACM-Reference-Format}
\bibliography{bibliography}



\end{document}
\endinput
