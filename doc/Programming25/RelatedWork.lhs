
%related work

Our work is inspired by the novel work on a theory on name resolution~\cite{nameresolution15} and scopes as types~\cite{scopesastypes} developed at Delft Univ. by the group of Eelco Visser. There is a key difference between our approaches: we model name resolution as an embedded DSL that can be easily incrementally extended. In fact, we have presented the full specification on the scope rules used in two languages - Haskell and C - but new scope rules can be easily incorporated/adapted within our technique. 
Eelco's work models name resolution via the Statix regular DSL whose syntax is defined in SDF3, part of the Spoofax Language Design Workbench~\cite{spoofax}. Moreover, their technique relies on (language-independent) scope graphs, where nodes represent scopes and an edge between two nodes represents declaration reachability from one scope to another - name resolution consists of finding paths in the scope graph. 
%dependency graphs / scope graphs and on establishing relations on those graphs (ver os nome que dão aos grafos). 
Thus, in comparison to our technique, the Statix DSL is not modular as it is restricted to the Spoofax Workbench, nor it is incremental as it is not easy to extend with new rules, possibly requiring a restructuring of the scope graphs or their resolution processes.  
% their technique is hard to extend with new rules and to be reused outside Spoofax.


Our techniques rely on Higher-Order AGs which are closely related to Reference Attribute Grammars~\cite{referenceAG}. In both AG extensions the original AST is not fixed during attribute evaluation and a better AST can be first computed and then decorated. In Reference AGs, attributes of that more suitable AST are available (via references) from the original AST. This was not possible in HOAG until now. In Section~\ref{sec:blockToAst} we presented a technique that offers this (bidirectional) mechanism to HOAG. Because an HOAG can be transformed into a classical AG, the powerful static analysis techniques developed for AGs - namely the static detection of circular dependencies - are orthogonal to our approach. Reference AGs, on the contrary, rely on a dynamic (fix-point) attribute evaluation and therefore lose all AG static guarantees~\cite{evalRAG}.  

%In this paper we consider a shallow embedding of our technique and thus we are also losing the AG static guarantees, but we may easily express it using a deep embedding and this reuse all AG techniques.
