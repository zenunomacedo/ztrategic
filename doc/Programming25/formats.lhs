



%format box_m = "\fbox{m}"
%format Tree = "\bnfnt{Tree}"
%format Leaf = "\bnfprod{Leaf}"
%format Fork = "\bnfprod{Fork}"
%format NT_Int = "\bnfnt{Int}"
%format Prog = "\bnfnt{Prog}"
%format Root = "\bnfprod{Root}"
%format attr_lm = "\attrid{lm}"
%format attr_gm = "\attrid{gm}"
%format attr_nt = "\attrid{nt}"

%format CRootT  = "\bnfprod{$\mathit{Root_{Prog}}$}"
%format CLeaf   = "\bnfprod{$\mathit{Leaf_{Tree}}$}"
%format CFork   = "\bnfprod{$\mathit{Fork_{Tree}}$}"


%format t_1    = "\mathit{t_1}"
%format t_2    = "\mathit{t_2}"
%format t1'    = "\mathit{t\textquoteright_1}"
%format rt_1   = "\mathit{rt_1}"


%format VAG_i     = "\scalebox{0.8}{$\mathit{(VAG_i)}$}"
%format VAG_ii_a  = "\scalebox{0.8}{$\mathit{(VAG_{ii_a})}$}"
%format VAG_ii_b  = "\scalebox{0.8}{$\mathit{(VAG_{ii_b})}$}"
%format VAG_iii   = "\scalebox{0.8}{$\mathit{(VAG_{iii})}$}"

%format AG_i     = "\scalebox{0.8}{$\mathit{(AG_i)}$}"
%format AG_ii_a  = "\scalebox{0.8}{$\mathit{(AG_{ii_a})}$}"
%format AG_ii_b  = "\scalebox{0.8}{$\mathit{(AG_{ii_b})}$}"
%format AG_iii   = "\scalebox{0.8}{$\mathit{(AG_{iii})}$}"

%format EAG_i     = "\scalebox{0.8}{$\mathit{(EAG_i)}$}"
%format EAG_ii_a  = "\scalebox{0.8}{$\mathit{(EAG_{ii_a})}$}"
%format EAG_ii_b  = "\scalebox{0.8}{$\mathit{(EAG_{ii_b})}$}"
%format EAG_iii   = "\scalebox{0.8}{$\mathit{(EAG_{iii})}$}"

%format EAG_ii   = "\scalebox{0.8}{$\mathit{(EAG_{ii})}$}"

%format MAG_i     = "\scalebox{0.8}{$\mathit{(MAG_i)}$}"
%format MAG_ii    = "\scalebox{0.8}{$\mathit{(MAG_{ii})}$}"
%format MAG_iii   = "\scalebox{0.8}{$\mathit{(MAG_{iii})}$}"


%format tleft = "\mathit{left}"
%format tright = "\mathit{right}"
%format ^ = " "
%format ^^ = "\;"
%format DATA = "\mathbf{DATA}"
%format ATTR = "\mathbf{ATTR}"
%format SEM = "\mathbf{SEM}"
%format lhs = "\mathbf{lhs}"
%format . = "\;.\;"
%format * = "\times"
%format (A(n)(f)) = @ n . f
%format (ANT(n)) = @ n 


%format otimes = "\otimes"

%format zipper_Tree = "\mathit{zipper}"
%format left_Tree   = "\mathit{left}"
%format right_Tree  = "\mathit{right}"
%format parent_Tree = "\mathit{parent}"
%format value_Tree  = "\mathit{value}"



%format down'    = "\textsf{down\textquoteright}"
%format down     = "\textsf{down}"
%format up       = "\textsf{up}"
%format left     = "\textsf{left}"
%format right    = "\textsf{right}"
%format getHole  = "\textsf{getHole}"
%format toZipper = "\textsf{toZipper}"
%format Zipper   = "\textsf{Zipper}"


%format >>= = "\bind"

%format mkAG        = "\textsf{mkAG}"
%format AGTree      = "\textsf{AGTree}"
%format .$          = "\textsf{.\$}"
%format .$>         = "\textsf{.\$\textgreater}"
%format .$<         = "\textsf{.\$\textless}"
%format .|          = "\textsf{.$\vert$}"
%format parent      = "\textsf{parent}"
%format lexeme      = "\textsf{lexeme}"
%format constructor = "\textsf{constructor}"

%format memo = "\textbf{memo}"


%format BlockL  = "\matem{Block}"
%format decl    = "\mattt{decl}"
%format use     = "\mattt{use}"

%format P       = "\bnfnt{P}"
%format Its     = "\bnfnt{Its}"
%format It      = "\bnfnt{It}"
%format ConsIts = "\bnfprod{ConsIts}"
%format NilIts  = "\bnfprod{NilIts}"
%format Decl    = "\bnfprod{Decl}"
%format Use     = "\bnfprod{Use}"
%format Block   = "\bnfprod{Block}"

%format Name    = "\bnfnt{Name}"
%format Env     = "\bnfnt{Env}"
%format Errors  = "\bnfnt{Errors}"

%format fun_errors = "\textit{errors}"
%format fun_environment = "\textit{environment}"

%format box_dclo = "\fbox{\attrid{dclo}}"
%format box_dclo_2 = "\fbox{\attrid{$dclo_2$}}"
%format box_lev = "\fbox{\attrid{lev}}"
%format box_lev_1 = "\fbox{\attrid{lev + 1}}"
%format box_errors = "\fbox{\attrid{errors}}"
%format dcli = "\attrid{dcli}"
%format dclo = "\attrid{dclo}"
%format lev = "\attrid{lev}"
%format env = "\attrid{env}"
%format errors = "\attrid{errors}"
%format mustBeIn = "\bnfnt{mustBeIn}"
%format mustNotBeIn = "\bnfnt{mustNotBeIn}"


%format errorsAsBlock = "\attrid{errorsAsBlock}"
%format errorsAsBlockRoot = "\attrid{errorsAsBlockP}"
%format concatErrors = "\bnfnt{concatErrors}"

%format letAsBlock = "\attrid{letAsBlock}"
%format letErrors = "\attrid{letErrors}"
%format concatIts = "\bnfnt{concatIts}"

%format box_errors_1 = "\fbox{\attrid{$errors_1$}}"
%format box_errors_2 = "\fbox{\attrid{$errors_2$}}"


%format CRoot   = "\bnfprod{$\mathit{Root_{P}}$}"
%format CConsIts   = "\bnfprod{$\mathit{ConsIts_{Its}}$}"
%format CNilIts   = "\bnfprod{$\mathit{NilIts_{Its}}$}"
%format CBlock   = "\bnfprod{$\mathit{Block_{It}}$}"
%format CDecl   = "\bnfprod{$\mathit{Decl_{It}}$}"
%format CUse   = "\bnfprod{$\mathit{Use_{It}}$}"

%format visit = "\textbf{visit}"
%format LRC = "\textit{Lrc}"


%format Its_2 = "\bnfnt{$Its_{2}$}"
%format It_2 = "\bnfnt{$It_{2}$}"

%format ConsIts_2 = "\bnfprod{$\mathit{ConsIts_{2}}$}"
%format NilIts_2  = "\bnfprod{$\mathit{NilIts_{2}}$}"
%format Block_2   = "\bnfprod{$\mathit{Block_{2}}$}"
%format Decl_2    = "\bnfprod{$\mathit{Decl_{2}}$}"
%format Use_2     = "\bnfprod{$\mathit{Use_{2}}$}"

%format eval_P    = "\mathit{eval\_P}"
%format eval_Its    = "\mathit{eval\_Its}"
%format eval_It     = "\mathit{eval\_It}"
%format eval_Its_1  = "\mathit{eval\_Its_{1}}"
%format eval_It_1   = "\mathit{eval\_It_{1}}"
%format eval_Its_2  = "\mathit{eval\_Its_{2}}"
%format eval_It_2   = "\mathit{eval\_It_{2}}"

%format its_2  = "\mathit{its_{2}}"
%format it_2   = "\mathit{it_{2}}"

%format errors_1   = "\attrid{$errors_{1}$}"
%format errors_2   = "\attrid{$errors_{2}$}"
%format errors_3   = "\attrid{$errors_{3}$}"

%format dclo_2   = "\attrid{$dclo_{2}$}"



%format Let       = "\bnfnt{Let}"
%format LetP       = "\bnfprod{Let}"
%format List      = "\bnfnt{List}"
%format Exp       = "\bnfnt{Exp}"
%format Assign    = "\bnfprod{Assign}"
%format NestedLet = "\bnfprod{NestedLet}"
%format EmptyList = "\bnfprod{EmptyList}"
%format Add       = "\bnfprod{Add}"
%format Sub       = "\bnfprod{Sub}"
%format Neg       = "\bnfprod{Neg}"
%format Const     = "\bnfprod{Const}"
%format Var       = "\bnfprod{Var}"



%format CLet       = "\bnfprod{$\mathit{Let_{Let}}$}"
%format CAssign    = "\bnfprod{$\mathit{Assign_{List}}$}"
%format CNestedLet = "\bnfprod{$\mathit{NestedLet_{List}}$}"
%format CEmptyList = "\bnfprod{$\mathit{EmptyList_{List}}$}"
%format CAdd       = "\bnfprod{$\mathit{Add_{Exp}}$}"
%format CSub       = "\bnfprod{$\mathit{Sub_{Exp}}$}"
%format CNeg       = "\bnfprod{$\mathit{Neg_{Exp}}$}"
%format CConst     = "\bnfprod{$\mathit{Const_{Exp}}$}"
%format CVar       = "\bnfprod{$\mathit{Var_{Exp}}$}"


%format TP = "\textsf{TP}"
%format TU = "\textsf{TU}"
%format zTryApplyM = "\textsf{zTryApplyM}"
%format idTP = "\textsf{idTP}"
%format tryTP = "\textsf{tryTP}"
%format constTU = "\textsf{constTU}"
%format monoTU = "\textsf{monoTU}"
%format monoTP = "\textsf{monoTP}"
%format monoTUZ = "\textsf{monoTUZ}"
%format monoTPZ = "\textsf{monoTPZ}"
%format applyTP = "\textsf{applyTP}"
%format applyTU = "\textsf{applyTU}"
%format adhocTP = "\textsf{adhocTP}"
%format adhocTU = "\textsf{adhocTU}"
%format adhocTPZ = "\textsf{adhocTPZ}"
%format adhocTUZ = "\textsf{adhocTUZ}"
%format failTP = "\textsf{failTP}"
%format failTU = "\textsf{failTU}"
%format idTP = "\textsf{idTP}"
%format idTU = "\textsf{idTU}"
%format repeatTP = "\textsf{repeatTP}"
%format seqTP = "\textsf{seqTP}"
%format seqTU = "\textsf{seqTU}"
%format choiceTP = "\textsf{choiceTP}"
%format choiceTU = "\textsf{choiceTU}"

%format allTPdown = "\textsf{allTPdown}"
%format allTUdown = "\textsf{allTUdown}"
%format allTPright = "\textsf{allTPright}"
%format allTUright = "\textsf{allTUright}"
%format oneTPdown = "\textsf{oneTPdown}"
%format oneTPright = "\textsf{oneTPright}"

%format full_tdTP = "\textsf{full\_tdTP}"
%format full_tdTU = "\textsf{full\_tdTU}"
%format full_buTP = "\textsf{full\_buTP}"
%format full_buTU = "\textsf{full\_buTU}"
%format once_buTP = "\textsf{once\_buTP}"
%format once_tdTP = "\textsf{once\_tdTP}"
%format once_buTU = "\textsf{once\_buTU}"
%format once_tdTU = "\textsf{once\_tdTU}"
%format stop_buTP = "\textsf{stop\_buTP}"
%format stop_buTU = "\textsf{stop\_buTU}"
%format stop_tdTP = "\textsf{stop\_tdTP}"
%format stop_tdTU = "\textsf{stop\_tdTU}"
%format innermost = "\textsf{innermost}"
%format outermost = "\textsf{outermost}"



%format StrategyTypes         = "\textbf{\small Strategy Types}"
%format StrategicConstruction = "\textbf{\small Strategic Construction}"
%format PrimitiveStrategies   = "\textbf{\small Primitive Strategies}"
%format CompositionChoice     = "\textbf{\small Composition / Choice}"
%format TraversalCombinators  = "\textbf{\small Traversal Combinators}"
%format TraversalStrategies   = "\textbf{\small Traversal Strategies}"
