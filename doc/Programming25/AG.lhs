


Name resolution was in the genesis of Knuth's definition of the
attribute grammar (AG) formalism~\cite{Knuth90}: A powerful formalism
to specify the static semantics of programming
languages~\cite{Knuth68}.
Attribute grammars are formal grammars that extend context-free grammars by associating
attributes with grammar symbols and production rules, which are used to carry semantic information.
These attributes can be synthesized (computed from children to parent)
or inherited (passed from parent to children), enabling the specification
of semantic properties that may involve multiple traversals in a clear an concise way.




\subsection{Expressing Scope Rules via Attribute Grammars}
\label{sec:block}

|Haskell| |let| expressions reuse scope rules that were introduced in
one of the very first high level programming languages, namely Algol
68~\cite{algol68}.


The |Block| language is a simple DSL, commonly used example in the context
of attribute grammars to illustrate how to elegantly specify  
name resolution. It models a minimal programming language with block-structured scoping,
where variables declared inside a block are only visible within that block and nested sub-blocks.
%Before we implement the |BlockL| processor,
Let us present the
(abstract) syntax of the language via the following |Haskell| data
types, as well as its usage to model previously shown |let| |program|:

\medskip
\begin{minipage}[htb!]{.50\textwidth}
\begin{code}
data P      =  Root     Its

data Its    =  ConsIts  It Its
            |  NilIts

data It     =  Decl    Name
            |  Use     Name
            |  Block   Its
\end{code}
\end{minipage}
\begin{minipage}[htb!]{.50\textwidth}
\begin{code}
[ 
  Decl a, Use b, 
  Decl c, 
  Decl b, Use c, Use c
  Use a, Use c    
]
\end{code}
\end{minipage}
%\medskip

\noindent where |P| is the root symbol consisting of items |Its|, that
is a user-defined list of item |It|. An |It| is a declaration or an use of a
|Name|, or a nested block of |Its|. |Name| is a type synonym for |String|.

The name resolution algorithm involves accumulating an environment of declarations through the nested blocks.
This can be specified by defining two attributes, an inherited attribute (|dcli|) that distributes the
already declared names, and a synthesized attribute (|dclo|) where we collect the newly declared ones.
Figure~\ref{fig:synacumenv} shows a graphic representation of such attributes.
For instance, at the root we start distributing an empty (|[]|) list of names.
In the case of |ConsIts|, the environment |dcli|, which has been constructed so far, is inherited by the head item of the list (|It|). This head item synthesizes |dclo|, potentially incorporating a new declaration. The synthesized environment (|dclo|) from the head is then passed down as |dcli| to the rest of the list (|Its|), where the final |dclo| is synthesized by combining it with further declarations, if any.
Declarations are added at the |Decl| productions; the synthesized environment |dclo| is constructed by prepending a pair with the new name and its |level| (another attribute) to the inherited environment |dcli|. 

\begin{figure}
\center
\begin{minipage}[htb!]{.70\textwidth}
\includegraphics[width=\textwidth,keepaspectratio]{Figures/block_decls.png}
\end{minipage}
\caption{Accumulating the environment via inherited (dcli) and synthesized (dclo) attributes.}
\label{fig:synacumenv}
\end{figure}

Once the environment has been accumulated, it is distributed using the inherited attribute |env|~\footnote{The zipper definition of |env| follows directly from Figure~\ref{fig:inhenv}. All attributes, including |env| and |lev|, are fully defined in our \hyperref[sec:replpackage]{replication package}.} as shown in Figure~\ref{fig:inhenv}. For nested blocks, notice that |dcli| (representing the declarations in the outer scope) is initialized with the current environment, while the internal environment of the block is represented by the accumulated dclo (synthesized by the block and combining its internal declarations with those inherited from the outer environment).

\begin{figure}
\center
\begin{minipage}[htb!]{.70\textwidth}
\includegraphics[width=\textwidth,keepaspectratio]{Figures/block_env.png}
\end{minipage}
\caption{Distributing the environment.}
\label{fig:inhenv}
\end{figure}

%\medskip

We implement the attribute computations using a zipper-based AG embedding in Haskell \cite{scp2016}. 
For instance, the corresponding zipper-based function |dclo| closely follows this
visual AG notation. Note that auxiliary function |.$n| accesses the $n^{th}$ child and |lexeme| is a syntactic reference, in this case accessing the name of the declaration.


\medskip

\begin{code}
dclo :: AGTree Env
dclo t =  case constructor t of
                    CNilIts   -> dcli t
                    CConsIts  -> dclo (t.$2)
		    CDecl     -> (lexeme t,lev t) : (dcli t)
                    CUse      -> dcli t
                    CBlock    -> dcli t
\end{code}

\medskip




When visiting nodes of type |Its| (recall that |Its| type constructors
are |NilIts| and |ConsIts|), the zipper function has to consider the
three alternatives where those subtrees inherit |dcli| from. Thus, 
with auxiliary function |.$<n| to access the $n^{th}$ sibling node,
 the
zipper-based function |dcli| is expressed as follows:

\medskip
\begin{code}
dcli :: AGTree Env 
dcli t =  case constructor t of
                    CNilIts   -> case  constructor (parent t) of
                                             CConsIts  -> dclo (t.$<1)
                                             CBlock    -> env (parent t)
                                             CRoot     -> []
                    CConsIts  -> case  constructor ( parent t) of
                                             CConsIts  -> dclo (t.$<1)
                                             CBlock    -> env (parent t)
                                             CRoot     -> []
                    CBlock    -> dcli (parent t)
                    CUse      -> dcli (parent t)
                    CDecl     -> dcli (parent t)
\end{code}


\begin{figure}
\center
\begin{minipage}[htb!]{.70\textwidth}
\includegraphics[width=\textwidth,keepaspectratio]{Figures/block_errors-ipe7.0.png}
\end{minipage}
\caption{Synthesizing the list of name resolution errors.}
\label{fig:synerrors}
\end{figure}


A synthesized attribute |errors| can be defined to compute the list
of possible errors produced by the name analysis.
Figure~\ref{fig:synerrors} shows its specification graphically.
In a declaration, the declared name must not be in (|mustNotBeIn|) the
environment, otherwise an error is produced. The case of
the use of a name is the other way around.
This attribute can be implemented as follows:


\medskip
\begin{code}
errors :: AGTree Errors
errors t =  case constructor t of
                       CRoot     -> errors (t.$1)    
                       CNilIts   -> []
                       CConsIts  -> (errors (t.$1)) ++ (errors (t.$2))
                       CBlock    -> errors (t.$1)		     
                       CUse      -> (lexeme t)  `mustBeIn` (env t)
                       CDecl     -> (lexeme t,lev t) `mustNotBeIn` (dcli t)
\end{code}
\medskip

\noindent
where |mustBeIn| and |mustNotBeIn| are simple lookup functions in data type |Env|,
that return either an empty list or a singleton list with the respective error.


As this small example shows, Knuth's AGs are a suitable formalism to
specify name resolution tasks. Our shallow embedding of AGs
has another key ingredient to provide a generic solution to name
resolution: we can modularly and incrementally extend our definition
to include new scope rules, while reusing definitions (for example,
attributes) expressed in similar rules. Next, we present such an
extension.


\subsubsection{C scope Rules Extension}


Let us consider now that we need to express name resolution that follows
C policy: every name has to be defined before it is used. 
\\
In our setting, we can extend the previously defined attribute grammar
to also incorporate C's scope rules. In a separate module we can
include the definition of a new attribute, |errorsC|, to implement C
invalid declarations/use of names.  Note that existing attributes,
particularly those related to accumulation of declarations - attribute
|dcli| - are reused.

\begin{code}
errorsC :: AGTree Errors
errorsC t =  case constructor t of
                       CRoot     -> errorsC (t.$1)    
                       CNilIts   -> []
                       CConsIts  -> (errorsC (t.$1)) ++ (errorsC (t.$2))
                       CBlock    -> errorsC (t.$1)		     
                       CUse      -> (lexeme t)  `mustBeIn` (dcli t)
                       CDecl     -> (lexeme t,lev t) `mustNotBeIn` (dcli t)
\end{code}
\medskip

As a result, we have just implemented a generic C-like name
resolution specification that can be reused as an off-the-shelf
library to express name resolution in any language that uses C scope
rules. In fact, our name resolution framework already provides
reusable definitions for several known resolution strategies, such as
the ones used by Haskell, C, and Pascal.


\subsection{From |Let| to |Block| via Higher-Order Attribute Grammars}


%\todo{remove first 2 paragraphs?} JNM: no, we need HOAGs
AGs offer a modular and extensible software development setting: a new
extension can be added in a modular way, without having to change the
existing solution. Moreover, in our embedding the added module can be
compiled independently. AGs, however, have a severe limitation: when
an algorithm is not easily expressed over the underlying AG data
structure, a better suited structure can not be used/computed.


Swierstra noticed this limitation and introduced \textit{Higher-Order
Attribute Grammar} (HOAG) \cite{VSK89,hoag89}, where conventional AGs
are augmented with \textit{higher-order attributes}, the so-called
\textit{attributable attributes}. Higher-order attributes are
attributes whose value is a tree. We may associate, once again,
attributes with such a tree. Attributes of these so-called
\textit{higher-order trees}, may be higher-order attributes
again. Higher-order attribute grammars have a key characteristic that
is in the core of our technique, namely when a computation can not be easily expressed in terms
of the inductive structure of the underlying tree, a better suited
structure can be computed before.



This allows, for example, to transform a |let| expression to a |Block|
program, defining the declaration and use of names. The attribute
equations define a (synthesized) higher-order attribute representing
the |Block| tree. As a result, the decoration of a |let| tree
constructs a higher-order tree: the |Block| tree. The attribute
equations of the |Block| AG define the scope rules of the |let|
language.


Let us consider again the |let| language. Next, we define a
heterogeneous data type |Let|, taken from~\cite{scp2016}, that models
such expressions in |Haskell| itself.

\medskip

\begin{code}
data Let   =  Let        List Exp
data List  =  NestedLet  Name Let List
           |  Assign     Name Exp List
           |  EmptyList
data Exp   =  Add     	 Exp Exp
           |  Sub        Exp Exp
	   |  Neg        Exp
	   |  Const      Int
	   |  Var        Name
\end{code}

\medskip


We start by defining a (first-order) AG fragment where we synthesize a
list of declarations and uses of names in a |List| of |let|
expressions. Thus, we write attribute equations such that a |Var|
constructor induces a |Use| of the respective name, while an |Assign|
induces a |Decl| of that name. Next, we show the zipper-based
definition of the required equations.

\smallskip
\begin{code}
letAsBlock :: AGTree Its
letAsBlock t = case constructor t of
                CAssign     -> ConsIts  (Decl (lexeme_Name t))
                                        (concatIts (letAsBlock (t.$2)) (letAsBlock (t.$3)))  
                CNestedLet  -> ConsIts  (Decl (lexeme_Name t))
                                        (  ConsIts (Block (concatIts  (letAsBlock (t.$2))
					                              (letAsBlock (t.$3))))
                                           NilIts)
                CEmptyList  -> NilIts   
                CConst      -> NilIts
                CVar        -> ConsIts (Use (lexeme_Name t)) NilIts
                CNeg        -> letAsBlock (t.$1)
                _           -> concatIts (letAsBlock (t.$1)) (letAsBlock (t.$2))
\end{code}

\smallskip


\noindent
where |concatIts| joins two separate blocks of instructions. 
Now, we define an attributable
attribute in the |Let| production to synthesize the desired |Block|
higher-order tree. 

\medskip

\begin{code}

letErrors :: AGTree Errors
letErrors t = case constructor t of
               CLet ->  let  ata :: Zipper P
                             ata = mkAG (Root (letAsBlock (t.$1)))
                        in errors ata
\end{code}

\medskip


Higher-order attribute grammars can be transformed into an equivalent
first-order attribute grammar~\cite{hoag91,Saraiva99}. Thus, all
classic static analysis (first-order) AG techniques can be applied to
HOAGs. Thus, HOAGs inherit all nice static AG guarantees, such as the
static termination of attribute evaluators provided by Kasten's
ordered AGs~\cite{Kastens80,Kastens91b}. They have, however, a severe
limitation: attributes of the higher-order tree are not accessible from
the first-order one! The exceptions are the attributes synthesized in
the root of the higher-order tree.

In our name resolution example, this limitation results in a HOAG that
has access to the (synthesized) full list of errors - the invalid
declaration and use of names - but without the possibility to relate
those individual errors to the nodes/constructors of the original
first-order tree that are the root cause of the error. In
Section~\ref{sec:blockToAst} we introduce a technique to extend HOAG
with this expressiveness.




