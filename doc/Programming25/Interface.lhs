
So far, we have presented an attribute grammar for the |Block| language, that can model declarations, usages and nesting blocks for generically any language. Both the |errors| and |errorsC| can be used to compute the errors of a |Block| assuming different scope rules. Moreover, we defined |letAsBlock| as a translator from |let| to |Block|, and |letErrors| as a function that transforms a |let| into a |Block| and then computes errors on it. 

We still have many shortcomings in this approach: as |let| program developers we are forced to code a |letAsBlock| translator, which requires knowledge of |Block| in addition to our presumed knowledge of |let|, thus requiring both a great deal of additional work to produce such a translator, and additional knowledge on a different language. If we next work on |Haskell| name analysis, for example, we would need to write a |haskellAsBlock| translator, where the majority of lines of code would be recursion on the AST to find the relevant usage and declarations of variables. 

\subsection{An interface for name analysis specification} \label{sec:interfaceapi}

In this section we present an interface for the generic derivation of translation functions into the |Block| language. The user of this interface only needs to specify what nodes of the AST of their language represent the use or declaration of a variable, and which nodes open a new nesting level. For finer control, the user can provide an initial state, with names that should be considered to be declared - for basic libraries and keywords their specific language might contain. A basic support for global variables, such as objects, is also provided. Next, we show the Scopes class, which defines this interface: 

\smallskip
\begin{code}
class  (Typeable a, StrategicData a, Data a) => Scopes a where
       isDecl        :: Zipper a -> Bool
       isUse         :: Zipper a -> Bool
       isBlock       :: Zipper a -> Bool
       isGlobal      :: Zipper a -> Bool
       getUse        :: Zipper a -> String
       getDecl       :: Zipper a -> [String]
       setUse        :: Zipper a -> String -> Zipper a
       setDecl       :: Zipper a -> String-> Zipper a
       initialState  :: a -> [String]
\end{code}
\smallskip

%\todo{Do we introduce the AGMaker somewhere? Where do we do it?}

Here, |isDecl|, |isUse| and |isBlock| are mandatory for the interface
to be correctly used, and all other definitions are optional, with
default definitions provided. For example, we can encode the |Let|
example discussed previously in our interface as follows:

\smallskip
\begin{code}
instance Scopes Let where
    isDecl ag   = constructor ag `elem` [CAssign,CNestedLet]
    isUse ag    = constructor ag `elem` [CVar]
    isBlock ag  = constructor ag `elem` [CLet]
\end{code}
\smallskip

%if False
\smallskip
\begin{code}
instance Scopes Let where
    isDecl ag   =  case  constructor ag of
                         CAssign      -> True
                         CNestedLet   -> True
                         _            -> False
    isUse ag    =  case  constructor ag of
                         CVar  -> True
                         _     -> False
    isBlock ag  = case  constructor ag of
                        CLet  -> True
                        _     -> False
\end{code}
\smallskip
%endif

%We use patterns similar to the code showed in the attribute grammars section to peek into the node that is passed as an argument, and we flag it appropriately. For example, in the |isDecl| function, we return |True| for any node that produces a declaration of a variable, which are |Assign| and |NestedLet| nodes. For any other node, we return |False|. 
In the |isDecl| function, we use |constructor| function to peek into the current node and check if it is contained inside a list with all nodes that produce declarations of a variable, which are |CAssign| and |CNestedLet|. 
Note that we can have arbitrarily complex code here to decide if a node should contextually be considered a declaration. Similarly, in |isUse| we define that only |Var| nodes indicate an usage of a variable, and in |isBlock| we define that |Let| nodes open a new nesting level. 

These definitions are enough for our system to be able to perform name analysis on |Let| programs. There are several optional definitions on the interface that the user should re-define if needed. When a node is flagged by the |isDecl| or |isUse| functions, our code will locate the variable name to either store it and change it, with functions |getUse|, |getDecl|, |setUse| and |setDecl|. If the user does not specify how to access the variable names, then the default implementations |getString| and |modifyFunc| are used, which will use the first |String| they are able to find - this is not always the correct behavior, but a reasonable generalized assumption. An |Assign Name Exp List| node will have its variable name stored in |Name|, which is the first |String| these functions will find. The |isGlobal| check makes a declaration visible in the entire program and is only relevant when global variable names are part of the language to consider, and therefore it defaults to always return |False|. 

Most programming languages will have pre-defined functions and/or names that the name analysis tools should be aware of. Depending on the tool that produces the AST of the language, it is possible that such names are mixed with user-defined names and are therefore indistinguishable by analyzing the AST alone. For  this, we provide |initialState|, a function that (due to technical limitations only) receives a value that is discarded, and provides a list of names that are fed into the name analysis machinery to be considered as declared. For example, for a specific parser for |Haskell|, basic operators such as the numeric sum operator |(+)| are internally the same as user-defined functions, and therefore it is reasonable to signal |(+)| as part of the initial state, otherwise our tool signals all numeric sums as errors.    

For the above |Let| example to be complete, we are still missing a definition of |Let| as an instance of |StrategicData|~\cite{VSISLE}. This is a small modification on zippers where we can signal nodes to be considered terminal nodes and therefore be automatically skipped. We can use the default implementation of |StrategicData| in which no nodes are skipped like this: 

\smallskip
>instance StrategicData Let 
\smallskip

While the default implementation is perfectly fine for our purposes, we provide a basic implementation to skip integer values and names. Skipping certain nodes can provide benefits in terms of both efficiency by reducing computations, and expressiveness by being able to cut the generic traversal into specific sub-trees. 

\smallskip
\begin{code}
instance StrategicData Let where
    isTerminal t = isJust (getHole t :: Maybe Int) || isJust (getHole t :: Maybe Name)
\end{code}
\smallskip

We skip integer nodes and |Name| nodes as these are fairly common nodes in the |Let| language and in many other languages, but they contain no relevant information for name analysis. We do not care about the |Name| nodes themselves as they only contain a single string each, instead we will later focus on the nodes that contain a |Name| inside it, because the context of those nodes is what defines if that |Name| is a definition or usage of a variable. 

With this, we have defined |Let| to be usable with our interface. The available functionality is fairly simple: 

\smallskip
\begin{code}
toBlock          :: Scopes a => a -> Block
processor_a68    :: Scopes a => a -> Errors
processor_io     :: Scopes a => a -> Errors
applyErrors_a68  :: Scopes a => a -> a
applyErrors_io   :: Scopes a => a -> a
\end{code}
\smallskip

For any AST of type |a| (in our example, the type variable |a| is |Let|) which obeys our |Scopes| interface, the function |toBlock| produces a representation of the AST in the Block language, the functions |processor_a68| and |processor_io| compute the list of errors according to the Algol68 scope rules and C scope rules respectively, and the functions |applyErrors_a68| and |applyErrors_io| return the input AST modified such that any names where an error occurs have their strings modified to signal that error. An user can thus use |applyErrors| on an AST and then use its prettyPrinter to obtain back readable code, but with the error messages imbued in the code.  

\subsection{From any language to |Block| via generic traversals} \label{sec:astToBlock}

In this subsection, we show how our interface converts a program into its |Block| representation, and what this entails. 

We will be working on an AST |a| which we know to obey our interface |Scopes|, which is represented in the code by the fragment |Scopes a =>| that will be very frequent in our function types. We will be making extensive use of |Zippers| to navigate this generic tree |a|. For starters, we define a (simplified version of a) function to access the immediate children nodes of our current node. Whenever a node is found, we apply |build| to it, which we show later in detail: 

\smallskip
\capstartfalse
\begin{figure}[htb!]
\begin{minipage}[t]{.46\textwidth}
\begin{code}
children ag  =  case down' ag of 
                     Nothing  -> []
                     Just n   -> build n : brothers n 
\end{code}\end{minipage}
\begin{minipage}[t]{.50\textwidth}
\begin{code}
brothers ag  =  case right ag of 
                     Nothing  -> []
                     Just n   -> build n : brothers n 
\end{code}
\end{minipage}
\end{figure}
\capstarttrue
\smallskip

The function |children| will travel into the leftmost immediate child of our current node, by using the |down'| zipper function. If |Nothing| is found, then there are no more children and an empty list of results is returned. If there is a node |n|, we apply |build| to node |n|, and then we continue gathering the next |brothers| of this node |n| by using the auxiliary function |brothers|. This function |brothers| behaves exactly like function |children|, but it travels to the right, gathering all remaining nodes and applying |build| to them. The navigation will be performed as shown in Figure~\ref{fig:children}. 

\smallskip
\begin{figure}
\center
\begin{minipage}[htb!]{.50\textwidth}
\includegraphics[width=\textwidth,keepaspectratio]{Figures/children.png}
\end{minipage}
\caption{Accumulating nodes using the |children| function.}
\label{fig:children}
\end{figure}
\smallskip
We mention that these are simplified versions of the code. The real implementation includes an additional component, which we call |Directions|. Whenever we navigate a data structure with zippers, we keep track of the movements we made in a list. This way, we always have a way to navigate back to a node, by going to the starting position and mimicking the same navigation we kept track of. This is relevant when we later need to move errors from the |Block| we will produce back into the original AST. 
\smallskip
\begin{code}
data Direction = U | D | R | L
type Directions = [Direction]
\end{code}
\smallskip

A |Direction| will be a movement up, down, right or left. A |Directions| is a list of |Direction|. Next, we show the code of |children| with the added directions. 
\smallskip
\capstartfalse
\begin{figure}[htb!]
\begin{minipage}[t]{.52\textwidth}
\begin{code}
children ag d = case down' ag of 
           Nothing  ->  []
           Just n   ->  let x = d ++ [D]  
                        in build n x : brothers n x
\end{code}\end{minipage}
\begin{minipage}[t]{.45\textwidth}
\begin{code}
brothers ag d = case right ag of 
           Nothing  ->  []
           Just n   ->  let x = d ++ [R]  
                        in build n x : brothers n x
\end{code}
\end{minipage}
\end{figure}
\capstarttrue
\smallskip

Now these functions also receive an argument |d| which is the directions to the current node, and if they navigate into the next node |n|, they also define the updated list of directions |x|, which will be passed to |build|. For example, for the tree seen in Figure~\ref{fig:children}, the directions to |node| is an empty list because that's the starting position, the directions to |child1| are |[D]|, and the directions to |child3| are |[D, R, R]|. 

Next, we discuss the aforementioned |build| function, and also the |buildChildren| auxiliary function. The |build| function will transform one node into its |Block| correspondent. 


\smallskip
\begin{code}
build :: Scopes a => Zipper a -> Directions -> Its
build a d  |  isDecl a   =  Decl (getDecl a) d <+> buildChildren a d
           |  isUse a    =  Use (getUse a) d <+> buildChildren a d
           |  isBlock a  =  Block (buildChildren a d) <+> NilIts
           |  otherwise  =  buildChildren a d

buildChildren ag d = foldr mergeIts NilIts (children ag d) 
\end{code}
\smallskip

This |build| function takes a |Zipper| |a| pointing towards the current node in the tree, a list of |Directions| |d| and outputs an |Its|, which is a list of |Block| instructions. We check the current node |a| with the functions the user defined in the interface: |isDecl|, |isUse|, and |isBlock|. 

If |isDecl a| is true, then we produce a |Decl| declaration instruction, which stores the name of the declaration as computed by |getDecl a|, and the list of directions |d|. We then recursively iterate over the children of the node |a| using |buildChildren|, and we join our produced |Decl| with the result of iterating on the children with auxiliary combinator |<+>| that joins |Block| instructions. The |isUse| clause is very similar to |isDecl|. 

If |isBlock a| is true, then we know the current node |a| opens a new nesting level. Thus, we signal a new nesting level was opened with the |Block| constructor, and we put inside it the results of transforming the children into |Block|. We have a single nested |Block| instruction (that can contain many instructions inside, but in itself is just one instruction), but we need to return a list of instructions, so we produce a list of one element by appending this |Block| we created to |NilIts|, the empty list constructor. 

If none of the previous checks were true, then we enter the |otherwise| clause and node |a| is irrelevant for name analysis purposes. In this case, we return the result of using |buildChildren| to transform the children nodes.  

As for the function |buildChildren|, it calls |children| as presented before, but transforms the usage of standard |Haskell| lists into |Block|-specific lists with the usage of |mergeIts| to join elements into the starting |NilIts| empty list. 

We finalize this code by defining |buildRoot|, the entry point of these functions: 

\smallskip
\begin{code}
buildRoot :: Scopes a => Zipper a -> P
buildRoot a = Root (buildChildren a [])
\end{code}
%\smallskip


This function signals that we are currently at the |Root| (i.e. the first node) of the |Block| program, and starts the list of |Directions| as an empty list. From then on, |buildChildren| uses |children| to visit the immediate children nodes. Then |children| uses |build| to transform them and recursively calls |buildChildren| on their children. These functions cycle between each other until the final |Block| program is produced and there are no more nodes to be visited. 


\subsection{From |Block| back to the original language} \label{sec:blockToAst}

We have seen how we can take any language and transform it into a |Block| program using our interface. Moreover, we have seen in Section~\ref{sec:block} that we can compute the errors of any |Block|. Attentive readers might have noticed that the |Block| syntax does not match with the code we present in the |build| function. This is because the |Block| grammar has been slightly extended to also include a list of directions with each variable declaration and usage: 

\smallskip
\begin{code}
data It     =  Decl    Name   Directions
            |  Use     Name   Directions
            |  Block   Its
\end{code}
%\smallskip

The |Block| attribute grammar is slightly changed so that each declaration and use contains a path on the original AST pointing towards the node that declaration/use. In other languages, this could more efficiently be expressed with pointers, but we argue the expressiveness is unchanged. 

Next, we take the computed errors and reflect them on the original AST. First, we need a way to move in the original AST using one of the stored |Directions|: 

\smallskip
\begin{code}
applyDirections :: Scopes a => Zipper a -> Directions -> Zipper a 
applyDirections ag [] = ag
applyDirections ag (h:t)  =  case move h ag of 
                                   Nothing -> error "malformed directions list"
                                   Just n -> applyDirections n t 
\end{code}
\smallskip

The function |applyDirections| will take a |Zipper| pointing towards any AST as an input, as well as a list of |Directions|. This function assumes that the zipper is pointing to the root of the AST, that is, the initial node. Then, it will process the |Directions| accordingly, one by one. If there are none to be processed, the current zipper |ag| is returned. If there are one or more |Direction| to be processed, we peek into the first direction |h| of the list, and use auxiliary function |move| that applies one the zipper functions |down'|, |left|, |right| or |up|, according to the direction |h| we need to take. If the result of applying |move| to our zipper |ag| is |Nothing|, then we signal an error because it should be impossible to reach this state - our |Directions| list should at all times be a valid list because it was generated from the same AST we are traversing at this point. If the result is |Just n|, then the movement successfully placed us in node |n|, and we execute |applyDirections n t| to continue moving from |n| using the rest of unprocessed directions |t|. 

Next, we define a function that takes the original AST and one |Error| as computed by the |Block| attribute grammar, and reflects said error on the AST. 

\smallskip
\begin{code}
reflectOneError :: Scopes a => Zipper a -> (Name, It, String) -> Zipper a
reflectOneError ag (_, B.Use   _ d, err)  = setUse  (applyDirections ag d) err
reflectOneError ag (_, B.Decl  _ d, err)  = setDecl (applyDirections ag d) err
\end{code}
\smallskip

We note that each error computed by the |Block| attribute grammar is a triplet with the name of the variable (which we ignore here), the |Block| instruction itself which contains the directions |d| to the original node, and an error message string which we call |err|. This function receives an AST zipper |ag| and one such error as arguments, and it uses our previously-defined |applyDirections| to move the zipper to where the directions are pointing towards, after which |setUse| or |setDecl| (depending on the type of error) are used to change the node itself to contain the error message |err|. 

We finalize by writing a function to reflect all |Errors| on the original AST: 

\smallskip
\begin{code}
reflectErrors :: Scopes a => Zipper a -> Errors -> Zipper a
reflectErrors ag [] = ag
reflectErrors ag (err: t)  =  let     oneErrorApplied = reflectOneError ag err
                                      pullBack = moveToRoot oneErrorApplied
                              in reflectErrors pullBack t 
\end{code}
\smallskip

When there are no |Errors| to reflect on our AST zipper, we simply return it. When there are, the first error |err| is applied via previously-defined |reflectOneError| function. Then, because the zipper is no longer pointing towards the root of the AST, we use auxiliary zipper function |moveToRoot| to move the zipper back to the root of the AST. Finally, we call |reflectErrors| recursively, using the updated AST and the remaining errors |t|. 

With this, we have all tools to implement the functionalities our interface provides. We recall the functions we showed before, but now we include their full definition: 

\smallskip
\begin{code}
toBlock :: Scopes a => a -> Block
toBlock ast = buildRoot (toZipper ast)

processor_a68 :: Scopes a => a -> Errors
processor_a68 ast = block_a68 (initialEnv ast) (toBlock ast)

processor_io :: Scopes a => a -> Errors
processor_io ast = block_io (initialEnv ast) (toBlock ast)

applyErrors_a68 :: Scopes a => a -> a
applyErrors_a68 ast  =  let errors = processor_a68 ast
                        in fromZipper (reflectErrors (toZipper ast) errors)

applyErrors_io :: Scopes a => a -> a
applyErrors_io ast  =  let errors = processor_io ast
                       in fromZipper (reflectErrors (toZipper ast) errors)
\end{code}
\smallskip

Function |toBlock| is a shorthand to converting an AST to a zipper and then building a |Block| with it. The |processor_| functions build an initial environment with |initialEnv|; this internally uses |initialState| to provide an environment of names that are always defined. With this initial environment and the |Block| of the AST, we use the attribute grammar to compute the errors. The |applyErrors_| functions use the |processor_| functions to compute the errors and then use |reflectErrors| to apply them to the original AST. Manipulating only the strings of the original AST is not the most efficient way of signaling errors, but we provide this functionality as an easy way to visualize the errors by pretty-printing the AST. An user of the interface can use the provided functionalities to navigate to the nodes with errors, and then manipulate them as they see fit. 

We note that we have two |processor_| functions because we have two corresponding |Block| name analysis processors for Algol68 and C. Should we need a different set of scope rules, we only need to define them on the |Block| language, and then we can trivially implement functions |processor_| and |applyErrors_| that depend on them. 
