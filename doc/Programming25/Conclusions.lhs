
We have presented a technique for the generic mapping and transformation of any programming language into a DSL representing its uses and declarations of names, and its nesting levels. With this DSL, we can readily compute attributes to perform name analysis according to the scope rules we require for the original language. The errors can then be reported on the original program via a zipper-based generic mechanism, mapping the errors detected at the name resolution DSL to the original program location where they occur. 

After developing the proposed techniques and implementing name resolution for Haskell we answer now our research questions: 
\begin{itemize}

\item[RQ1] \textit{Can we define a technique that fully automates name resolution
for any programming language?}

We perform the full name resolution for the |Haskell| programming language, the exception being the use of qualified names. Our technique already is able to detect such errors by declaring all names twice - non-qualified and qualified - however it does not report the errors in the most adequate way. Thus, we need to extend the definition of our |Block| language with a new |DeclQ| constructor to model qualified names. This is a simple extension of our DSL that is fully supported by the techniques we have presented.

\item[RQ2] \textit{Can such a technique be easily extensible and modular?}

\textit{Yes}, our technique is easily extensible and modular. To add more AST nodes as declarations, uses or nesting levels, the user needs only to add the respective constructors to the interface functions. We provide name analysis processors for |Haskell|-like and |C|-like scope rules, but the user can add their own by defining new attributes over the |Block| DSL, for which existing attributes can be re-used. The process of transforming the original language into a specialized DSL, the computation of errors on the DSL, and the reflection of the errors on the original language, are all independent components that the user can use separately. For example, a user could use our interface to produce a |Block| DSL and compute its errors, but then decide to write its own software to manipulate the original AST as they see fit accordingly to these computed errors. 

\end{itemize}
%has downsides
%still is expressive

%We argue the main downside of our work is the existence of situations that are impossible to model with our current interface. The solutions we studied proved to be too complex for us to be satisfied with them - we consider the simplicity of our technique to be an upside we do not want to relinquish. 
For future work, we will explore variations of this interface with the same foundations in the |Block| AG, but with different, possibly more expressive systems of producing this DSL. While modeling a real language such as |Haskell| is a solid case study for our work, we also intend to model other programming languages with different scope rules, and compare the capacities of our technique for different languages. 

%future work: make other analysis of other complex languages
%future work: more complex, more expressive variation

