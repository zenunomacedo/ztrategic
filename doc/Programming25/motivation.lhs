

Name resolution, also known as the name analysis phase of language
processors, plays a crucial role in the design and implementation of
programming languages.  It helps in identifying declarations of
program entities such as variables, functions, types, and modules,
enabling these entities to be easily referenced from various parts of
the program. The scope rules of the language specify how names can be
declared and used 
%(\discuss{references?}) 
in a program. Different
languages use different scope rules, although the basic concepts of
resolution reappear in similar form across a broad range of languages.

Let us consider a simple and concise example of the name resolution of
a programming language. We consider the (sub)language of |let|
expressions as incorporated in most functional languages, including
|ML| and |Haskell|. While being a concise example, our |let| language
holds central characteristics of name resolution, such as the
mandatory but unique declarations of names. Next, we show an example
of a |let| program, where we use |Haskell| comments to clarify the
declaration and use of names.


\medskip
\begin{minipage}[htb!]{.40\textwidth}
\begin{code}
program =  let  a = b + 3     
                c = 2
                b = c * 3 - c
           in   (a + 7) * c             
\end{code}
\end{minipage}
\begin{minipage}[htb!]{.40\textwidth}
\begin{code}
    -- decl a , use b
    -- decl c
    -- decl b , use c, use c
    -- use a, use c			
\end{code}
\end{minipage}

\medskip

In our |let| language a program is valid when all names used are
indeed declared, and a name is not declared more than once. In fact,
|program| is a valid |let| program (it is also a valid |Haskell|
expression which evaluates to \texttt{28}). This happens, because
|let| uses the scope rules of |Haskell| which do not force a
\textit{declare-before-use} discipline, meaning that a variable can be
declared after its first use.  There are other languages, however,
where a name must be declared before is first use. This is the case of
the |C| language. Thus, if we would consider the scope rules of |C| in
defining the static semantics of our |let| language, then |program| is
invalid: name |b| is used before it is declared!

Like in most programming languages, |let| also supports nested block-based
structures as our next |nested| |let| program shows.


\medskip


%format box_e = "\fbox{e}"

\begin{minipage}[htb!]{.50\textwidth}
\begin{code}
nested =  let  a  = b + 3     
               c  = 2
	       d  =  let  c = a * b
		          e = 6
		     in   c * b
               b  = c * 3 - c
          in   (a + 7) * c + box_e             
\end{code}
\end{minipage}
\begin{minipage}[htb!]{.40\textwidth}
\begin{code}
    -- decl a , use b
    -- decl c
    -- decl d, decl c, use a, use b
    -- decl e
    -- use c, use b
    -- decl b , use c, use c
    -- use a, use c, use e	
\end{code}
\end{minipage}

\medskip

If we consider |Haskell| scope rules, then the use of |a| and |b| in
the inner |let| block comes from the outer block expression given that they
are not defined in the inner one. Since |c| is defined both in the
inner and in the outer block, then we must use the inner |c| (defined
to be |a * b|) when calculating |c * b|, but we use the outer |c|
(defined to be |2|) when calculating |( c * 3 ) - c|.  However, this
is an invalid |let| program: the inner declaration of |e| is not
visible in its outer block. Thus, an error should report the invalid
use of |e| in the |in| part of the outer block. Instead, if we would
consider the scope rules of C, then we have two more errors:
the two invalid uses of name |b|, since in both cases |b| is defined
after being used.

 
Name resolution and the implementation of scope rules is a complex
task:

\begin{itemize}

\item \textit{Real Languages are Represented by Large and Complex
Heterogeneous ASTs}: Programming languages contain many types and
constructors. Let us consider, for example, the |Haskell| language. In
the |Haskell| library, the AST is defined by 116 constructors across
30 data types\footnote{We refer to the \texttt{haskell-src} library 
for this AST: \url{https://hackage.haskell.org/package/haskell-src}}.
Because a name introduced
by a let node in the AST may be referenced by an arbitrary distant
node, then an |Haskell| name resolution algorithm needs to traverse
most of the nodes of the AST. Thus, this immediately leads to large
recursive functions consisting of (almost) 120 different case
statements or pattern matching alternatives!



\item \textit{Scope Rules Rely on Complex Multiple Traversal
Algorithms}: 
Many programming languages have scopes rules that
allow the use of names before they are defined. 
For example, all names in |Haskell| can be used before 
definition, as well as all methods in |Java|.
This leads
to name resolution algorithms that need to traverse the AST twice:
once to collect all declarations in the environment while detecting
duplicated declarations, and a second traversal to check for invalid
uses. Such a clear and straightforward algorithm, however, may require
a complex scheduling of the traversal functions. Let us consider the
|let| language again. In |let|, a nested definition inherits the names
defined in its outer one. As a consequence, only after collecting the
declarations of a outer let (performed in its first traversal), it can
descend to its nested definitions. Thus, only in the second traversal
of an outer let, the implementation performs the first traversal of
inner ones.  To make things even harder, values computed in a
first traversal need to be explicitly passed to the second one, which makes the scheduling of traversals complex and requires a mechanism to pass values from one traversal to the following one~\footnote{In an imperative setting such values are stored - as side effects - in the AST nodes, while a functional setting requires the use of a gluing data structure~\cite{SS99}.}.


\item \textit{Name Resolution is a Recurring problem in Programming Languages}: 
The basic concepts of name resolution reappear in similar form
across a broad range of programming languages. However, 
there is no off-the-shelf solution that a developer can easily adapt 
to perform name resolution on their language of choice. 
Thus, we need a suitable 
formalism to express name resolution algorithms that can be easily
reused, adapted, and extended in order to support languages that use
slight different name resolution rules.


\end{itemize}




Thus, in this paper we aim at answering the following research questions:



\begin{itemize}

\item[RQ1] \textit{Can we define a technique that fully automates name resolution
for any programming language?}


\item[RQ2] \textit{Can such a technique be easily extensible and modular?}

\end{itemize}


An automated name resolution technique should rely on a powerful
formalism suitable to describe intricate scope rules of programming
languages. Thus, this formalism should allow to concisely and elegantly
express scope rules, such as |Haskell| scope rules, which can then be
automatically applied to other programing languages. For example, if we
apply the reusable |Haskell| rules to our |let| example, then we
should obtain for free the following (pretty printed) AST with the error annotated:


\medskip


%format box_e = "\fbox{e $\leftarrow$ error}"
%format box_b = "\fbox{b $\leftarrow$ error}"

\noindent

\begin{minipage}[t]{.40\textwidth}
\begin{code}
nested =  let  a  = b + 3     
               c  = 2
	       d  =  let  c = a * b
		          e = 6
		     in   c * b
               b  = c * 3 - c
          in   (a + 7) * c + box_e             
\end{code}
\end{minipage}


\medskip

Because scope rules differ between programming languages, new rules
should be easily defined by reusing and extending existing ones. That
is to say that existing scope rules are the building blocks to express
new ones.  For example, the scope rules of |C| could be
defined by reusing and extending the |Haskell| ones. Then, if we use
the |C| scope rules in our |let| example we should get the following
result:

\medskip


\begin{minipage}[t]{.52\textwidth}
\begin{code}
program =  let  a = box_b + 3     
                c = 2
                b = c * 3 - c
           in   (a + 7) * c

\end{code}
\end{minipage}

\medskip


We have just shown the first example of applying our automated name
resolution technique to a programming language: the |let| language. In
fact, the pretty printed ASTs showing the |Haskell| and |C| name
resolution results were automatically produced by our off-the-shelf
scope rules library.







