\documentclass[english,submission]{programming}
%% First parameter: the language is 'english'.
%% Second parameter: use 'submission' for initial submission, remove it for camera-ready (see 5.1)

\usepackage[backend=biber]{biblatex}
\addbibresource{biblio.bib}

%include lhs2TeX.fmt
%include lhs2TeX.sty

\usepackage{hyperref}
%\usepackage{color}
\usepackage{xspace}


\newcommand{\discuss}[1]{\textcolor{blue}{#1}\xspace}
\newcommand{\todo}[1]{\textcolor{red}{#1}\xspace}
\def\ie{\textit{i.e.}}


%
% Packages and Commands specific to article (see 3)
%
% These ones  are used in the guide, replace with your own.
% 
\usepackage{multicol}
\usepackage{ccicons}
\lstdefinelanguage[programming]{TeX}[AlLaTeX]{TeX}{%
  deletetexcs={title,author,bibliography},%
  deletekeywords={tabular},
  morekeywords={abstract},%
  moretexcs={chapter},%
  moretexcs=[2]{title,author,subtitle,keywords,maketitle,titlerunning,authorinfo,affiliation,authorrunning,paperdetails,acks,email},
  moretexcs=[3]{addbibresource,printbibliography,bibliography},%
}%
\lstset{%
  language={[programming]TeX},%
  keywordstyle=\firamedium,
  stringstyle=\color{RosyBrown},%
  texcsstyle=*{\color{Purple}\mdseries},%
  texcsstyle=*[2]{\color{Blue1}},%
  texcsstyle=*[3]{\color{ForestGreen}},%
  commentstyle={\color{FireBrick}},%
  escapechar=`,}
\newcommand*{\CTAN}[1]{\href{http://ctan.org/tex-archive/#1}{\nolinkurl{CTAN:#1}}}
%%


%%%%%%%%%%%%%%%%%%
%% These data MUST be filled for your submission. (see 5.3)
\paperdetails{
  %% perspective options are: art, sciencetheoretical, scienceempirical, engineering.
  %% Choose exactly the one that best describes this work. (see 2.1)
  perspective=art,
  %% State one or more areas, separated by a comma. (see 2.2)
  %% Please see list of areas in http://programming-journal.org/cfp/
  %% The list is open-ended, so use other areas if yours is/are not listed.
  area={Social Coding, General-purpose programming},
  %% You may choose the license for your paper (see 3.)
  %% License options include: cc-by (default), cc-by-nc
  % license=cc-by,
}
%%%%%%%%%%%%%%%%%%

%format <+> = "\textless\!\!+\!\!\textgreater"
%format .$< = ".\$\!\!\!\textless"


\begin{document}

\title{Name Resolution for Free}

\subtitle{A Zipper-based, Modular Embedding of Scope Rules}
%\subtitle{Scopes As Types as an Embedded and Modular DSL}% optional


%\titlerunning{Preparing Articles for Programming} %optional, in case that the title is too long; the running title should fit into the top page column

%\authorinfo{is the author of this {LaTeX} class. Contact him at
%  \email{andre.nunes@@di.uminho.pt}.}

\author[a]{José Nuno Macedo}[https://orcid.org/0000-0002-0282-5060]
\affiliation[a]{Department of Informatics, University of Minho, Portugal}

%\authorinfo{is associate editor for the first two issues of The Art, Science,
%  and Engineering of Programming. Contact her at \email{lopes@@ics.uci.edu}.}

%\author[a]{Emanuel Rodrigues}[https://orcid.org/0000-0003-4317-1144]

%\authorinfo{is associate editor for the first two issues of The Art, Science,}
\author[a]{André Nunes}
%\affiliation[a]{Department of Informatics, University of Minho, Portugal}

\author[b]{Marcos Viera}[https://orcid.org/0000-0003-2291-6151]
\affiliation[b]{Instituto de Computación, Universidad de la República, Uruguay}

\author[a]{João Saraiva}[https://orcid.org/0000-0003-0551-3908]
%\affiliation[a]{Department of Informatics, University of Minho, Portugal}



%\authorrunning{André Nunes et al.} % Optional, for long author lists

\keywords{static semantics, type system, name resolution, attribute grammars, domain-specific language} % please provide 1--5 keywords


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Please go to https://dl.acm.org/ccs/ccs.cfm and generate your Classification
% System [view CCS TeX Code] stanz and copy _all of it_ to this place.
%% From HERE

% To HERE
%%%%%%%%%%%%%%%%%%%%%%%

\maketitle

% Please always include the abstract.
% The abstract MUST be written according to the directives stated in 
% http://programming-journal.org/submission/
% Failure to adhere to the abstract directives may result in the paper
% being returned to the authors.


\begin{comment}
Each submission must be accompanied by a plain-language abstract of up to 500 words that presents the key points in the paper in a manner understandable by experienced practitioners and researchers in nearby disciplines. The abstract should avoid mathematical symbols whenever possible, and it must address the following:

Context: What is the broad context of the work? What is the importance of the general research area?
Inquiry: What problem or question does the paper address? How has this problem or question been addressed by others (if at all)?
Approach: What was done that unveiled new knowledge?
Knowledge: What new facts were uncovered? If the research was not results oriented, what new capabilities are enabled by the work?
Grounding: What argument, feasibility proof, artifacts, or results and evaluation support this work?
Importance: Why does this work matter?
NOTE: The absence of an abstract conforming to this specification is grounds for the rejection of the paper without review.
\end{comment}

\begin{abstract}

Name resolution in programming languages is the process of ensuring that the names in a program are able to be properly referenced such that the semantics of the language are respected. This is a complicated problem, as it generally requires multiple traversals over the Abstract Syntax Tree of a programming language, which is complicated by itself. While there are similar concepts of name resolution in different programming languages, there is no technique that leverages these similarities to provide a generic solution for name resolution. 

We present a generic interface for name resolution for any programming language. Users of our interface only have to specify which nodes in their language represent name definitions, usages and new nested scopes. From here, our system uses a generic tree navigation mechanism to produce a Domain-Specific Language (DSL) representing only the information relevant for name resolution, and we use the Attribute Grammar formalism to effectively report errors on this DSL, which we then reflect on the original source code. 

To validate this approach, we use it to implement name resolution on the Haskell programming language. We solve the task with 60 lines of code, successfully traversing any Haskell program to build a DSL where we compute its errors, and then signal these errors on the original source code. We discuss the obtained results, the obstacles found and our proposed solutions. 

\end{abstract}



\section{Introduction} \label{sec:intro}

%include intro.lhs


\section{Motivation} \label{sec:motivation}


%include motivation.lhs


\section{Name Resolution via Higher-Order Attribute Grammars}  \label{sec:AG}

%include AG.lhs

\section{A Generic Interface for Name Resolution} \label{sec:interface}

%include Interface.lhs

\section{Name Analysis of Haskell programs} \label{sec:haskellanalysis}

%include Haskell.lhs

\section{Related Work} \label{sec:relatedwork}

%include RelatedWork.lhs 

\section{Conclusions} \label{sec:conclusions}

%include Conclusions.lhs

\section*{Replication Package} \label{sec:replpackage}
All source code discussed in this work, as well as supporting code, is available at \url{https://tinyurl.com/ToolsProgramming25}.


%include Appendix.lhs

%\bibliographystyle{elsarticle-num}
%\bibliography{biblio}
\printbibliography

\end{document}

