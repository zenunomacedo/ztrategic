Review #27A

Paper summary
The paper describes a way to test attribute grammars (AGs) using property-based testing (PbT). It starts with an introduction to attribute grammars, followed by the general contribution of testing them using property-based testing, and then presents a Haskell implementation with two problem-solution-pairs. The evaluation is limited to the presented problem-solution-pairs as a prove-off-concept. However, the general idea appears to be generalizable to other languages and frameworks for AG and PbT.

Detailed comments for authors

Pro

- This is a very understandable and well-structured paper. It is self-contained.

- The presented idea is portable. It can be generalized to other languages and frameworks for AG and PbT.

- For my perspective, solving a parametric problem and the generation of random input data for this problem, should always be done (together). I think this will never lose relevance.

- I like the simplistic but overall understandable descriptions. For example, on line 281, relating the difficulty to the universal quantification is very intuitive. I may borrow this way of explaining it.

- The correspondence between section 2 and 3 is a good idea.

Con

- Section 3 was a bit too many technical details for me. Can this be abstracted or outsourced more? I see that the correspondence between section 2 and 3 is partially doing this, but section 3 is still very lengthy.

- I am not entirely sure if this paper presents a very novel contribution. From what I see in the related work section, attribute grammars have already been suggested for the generation of data. From this generation, simple evaluation of some 'test' properties feels not too far away (and this is what the paper does).

- I assume the most critical point of this paper is to better clarify this relation to such related work, that brings together AGs and generation. The related work section is not strong in describing the conceptual differences. In particular, it lacks a precise delta on how it relates to the existing work where: "Attribute grammars have been suggested for the generation of test input for programs [5], and there is work using attribute grammars for random generation of large amounts of tests for data models" (direct copy from line 1223). The related work section needs to be improved here.

- The section 2 reads like an excellent introduction to AG and PbT, but it is not immediately clear what the exact contribution of the paper is. The section should stress the actual contribution (what is new) a bit more, while also making clear what is borrowed (what is old).

- The introduction does not focus on introducing the paper and its contribution, but on introducing Ags and PbT. Maybe a restructuring, moving parts of the content into a background, will resolve this. Best would be an introduction immediately selling the contribution and the difference to existing work.

- The evaluation is provided as a prove-of-concept (introducing two problem-solution-pairs). This is ok, but also not very strong. Are there options to improve this?

- What might be interesting to add to the related work is the relationship to automatic proof assistance. Showing that some property holds, with universal quantification, might be solved using automatic proof assistance (e.g., by structural induction over the tree). It might complete the picture of alternatives with a more general perspective on alternatives. In the trivial cases shown in this paper, it might even be possible (but I am not an expert).

Evaluation summary

The paper does not provide a strong evaluation, and I am also not entirely sure if the contribution is very novel. I would hope that the other reviewers have a better understating of the novelty. Related work can be improved. However, the paper is very understandable, self-contained, and well-structured. The generation of random input appears relevant for me.






Review #27B


Paper summary
The paper introduces and discusses an approach for the property-based testing of attribute grammar specifications. The central ingredients of a general approach are laid out: property specifications, generator specifications, a tool for checking properties, and, to a lesser extent, strategic programming. The approach is demonstrated in Haskell, combining a Zipper-based implementation of Attribute Grammars, a strategic programming library, and QuickCheck for property-based testing. The RepMin problem is used as a running example. A small let-based expression language serves as the main case study and demonstrates, besides testing attributes, also the generation of trees using attributes.

Detailed comments for authors
The following are the main weaknesses of the paper that motivate my rejection:

- The paper does not make a critically assessed contribution that (demonstrably) transfers to other usage scenarios or contexts (host language or toolset). As is, the paper focus on the exploration of its ideas using an implementation of Haskell and does little to generalise to a wider applicability. For example, there is no explicit attempt to define a library of useful abstractions, combinators or generators that can be reused across use cases (including those that the reader might wish to pursue). Alternatively, there is no discussion that makes very explicit the advantages and disadvantages of the choices made and the extent to which the presented approach transfers to other host languages.
- The paper is not structured in a way that serves the reader. There is no explicit mention (up front) of a method of evaluation. The description of the general (and presumably transferable) approach is too high-level. Certainly for those familiar with Haskell and AGs, the abstract description of the running example does not add much on top of the Haskell exposition. There is no background section that separates prior work from the contextual contribution of this paper.
- The Let-language section mixes testing attributes and generating trees based on attributes. A clear separation of concerns would have helped here. The testing can perhaps be layered? First testing certain attributes using generators that do not depend on attributes. Then develop additional generators using the previously tested attributes, in order to test additional attributes. In the presentation: first the properties should be expressed (whether informally or using logical notation), then the required generators can be defined, and finally the properties implemented. First give the intention, then the realisation. By focussing on the individual properties, it remains unclear what is the overall testing strategy for this example. Can we trust the (implementation of the) properties, especially when they involve (untested) attributes? How does this approach give us more certainty then, for example, carefully scrutinizing our AG definition using equational reasoning?
- The writing can be improved significantly; the paper uses imprecise and informal language at times and contains quite a large number of spelling mistakes. There are also cases of redundancy and repetition.


These are the main ideas expressed in the paper:

- Although Attribute Grammars are a concise way to define (static) semantics, and other evaluations on abstract syntax trees, one can still make subtle mistakes that can be discovered through property-based testing. For example, testing that a certain transformation (using higher-order AGs) is isomorphic.
- Generators for property-based testing can be defined that consider the (static) semantics expressed using attributes when attribute evaluation can be made available for this purpose. For example, generating abstract syntax trees that satisfy the name-binding rules expressed using attributes.
- Haskell has a number of nice features (lazy evaluation) and libraries (zipper AGs, ADT-generics, and strategic programming) that make this possible.

The first and last idea are not surprising or novel to those familiar with Haskell and the different embeddings of the AG formalism within Haskell. Although it is good to have confirmation, this requires a structured evaluation, e.g. using real-world case studies (existing AG projects of significant size). A follow-up question is then the feasibility for projects at scale. A brief mention of performance is made but a thorough analysis is lacking. In particular, it would be good to understand the extent to which attribute evaluations are repeated and whether sharing, caching or fusing could help.

The second idea, as described above, is most novel and through-provoking. A clear presentation requires separation of concerns and confidence in correctness requires a methodological approach to developing the properties, tests and generators (as also described above).



Specific comments
l8. "part of [the toolset] any software"
l16. "trees [and] by"
abstract does not mention any form of evaluation
l69. remove "do"
l72. access -> assess
l74. use of exclamation point, informal language.
l75. quickcheck and property-based testing is not exactly novel (20+ years)
l79. use of "just"
l94. "In fact, there are many ..." no need for this sentence
l120. "namely". I believe you want to use "such as in" here.
l124. use of exclamation point
Section 2. This section is more of a "motivating example" section rather than a section that describes a general methodology. I do appreciate the effort of trying to generalise your approach. In this case, this could perhaps better be done in an extensive subsection in a discussion section in which you generalise after giving the specifics (see also the suggestions in general comments).
l217. usage of the word "state" rather than "node".
l226. "In many programming languages, this is a complex idea ...". Not at all. Inherited attributes correspond to parameters in a purely functional programming setting, Reader monads (Haskell), and implicits (Scala, Haskell).
l263. "we can never be certain". I see your point. However, the artifact that you are implementing could have been (under-)specified through axiomating equations (see Axiomatic Semantics). In these cases your properties could potentially fully cover the axioms.
l266. "and not the leaves themselves". this comment is redundant given that we are in the context of a PL with immutability
Note that the isomorphic property makes the counting property redundant
l348. strategic programming has not been properly introduced yet. I would suggest adding a background section in which to introduce (at least) property-based testing, attribute grammars and strategic programming
S3.1.1 the descriptions of "constructor" and "lexeme" are confusing to me, despite their usage in the following examples being intuitive
l427. By including Root as a constructor of Tree, Roots can now appear as sub-trees. Your generator does not do this, but it is still a counter-intuitive definition. Would your story have been different if Root was a newtype over Tree instead?
locmin definition. It is not explained why we get CLeaf and CFork (instead of Leaf and Fork). You have to remark somewhere that the Zipper library you use takes advantage of a meta-representation of user defined datatypes.
l533. "we'd". informal language -> "we would".
l548. not clear why "can" is emphasised.
forallNodes and existsNodes are the only functions you defined, I believe, which are generic over the datatype (do not have Tree in their type) and are therefore candidates for a testing library you might like to develop. However, of only these functions do you omit the type signature! Would it also be possible to define a function forall type p ast, where type is a predicate over constructor-names that the user can use to select which (type of) node they wish to enumerate? Note that your sentence about the difference between existsNodes and forallNodes is enough, no need to show the full definition of existsNodes.
count. I find it surprising that you use recursion to define count, rather than using a strategy.
propAllEqual. the double iteration is not necessary; just collect all values in a set and check whether its length is 1.
l781. "tree is holds" -> "tree it holds".
syntax definition of Let language. The definition of List combines the notion of Lists and the notion of Assignment and Nesting. Is it not possible in your approach to use the build in lists?
prop_nameInEnv. The definition of aux only considers names occurring as variable occurrences, not at assignment-sites (contrary to what the paragraph below says).
equation (12) if l.dclBlock is always the empty set, the property is satisfied. This property is clearly not enough to verify the definition of dclBlock.
"extremely large sizes". I doubt these programs are comparable in size to many to real programs, in which case they would certainly not be "extremely" large. Either way, you have to give some numbers here to give a ball-park intuition about the size we are talking about. Do not that in many industrial/professional settings, the speed of the automated testing certainly does matter. This is also the place to discuss inherent inefficiencies of your approach and in what ways the could be addressed (caching, sharing, fusing, ...).
s3.2.3. The circulator generator is elegant and perhaps preferred over the one in A.1 (despite the limitations mentioned). However, it is not discussed explicitly why it is preferred. You also should note here that this kind of generator does not transfer to languages without lazy evaluation (which are mainstream).
"an usage" -> "a usage".
l1111. mention the use of "RecursiveDo" language feature (not mainstream).
"haven't" -> informal. "have not".
The related work section would benefit from a previous section in which the general properties of your approach are explicitly stated, as these can then be used to assess the extent to which other languages/toolsets can benefit from the approach described here.
"and the list goes on". no it does not, it ends here. informal language.
"Silver" and "low code". What is meant by "low code" here? What exactly is the target language for Silver?
"the an attribute grammar". remove "the".


Evaluation summary

The work is very much in scope for SLE and presents ideas that will be of interest to part of the SLE community interested in Attribute Grammars and Functional Programming techniques. The idea to build generators based on attribute evaluation is most interesting and novel. The work requires significant improvements though, as explained in the detailed comments, before it can be published as an academic article.





Review #27C


Paper summary

The paper describes an approach for property-based testing of attributed grammars (AGs). Its implementation relies on interpreting the AG in Haskell, with the grammar rules as datatype, and the attribute evaluation functions reflected into a separate generic structure called zipper to allow generic traversals for the implementation of (typed) quantifiers.

The paper illustrates the approach for a simple free algebraic datatype (trees) and for one with complex invariants (let-expressions), both with a number of inherited and synthetic attributes and properties.


Detailed comments for authors

Presentation:

The paper is well written (see below for a few minor details) and easy to read. It follows a "functional pearl" writing style and focuses on the actual code required for two well chosen examples.

Novelty:

The paper combines two well-understood ideas (property-based testing and strategic programming) and shows how they can be applied to a new domain, i.e., testing of AGs.

The combination is nice and, while not groundbreaking, new and interesting enough to warrant publication.

Relevance and Soundness:

The paper addresses a relevant problem in SLE. However, in some sense it fails to deliver on its title, since the approach relies on the re-interpretation and re-implementation of the AGs in Haskell: the work as is cannot be used to test AGs directly, in any of the existing formalisms, and requires a translation that is not given here.

The paper relies on stratetic programming to implement the generic traversal functions for (typed) quantifiers that are used in the properties, but this also requires the reflection of the attribute evaluation functions into the generic zipper structure. This makes the approach a bit clumsy as values need to be translated repeatedly between object and meta level, and functions often take both the object and the zippered meta level representations as arguments. It also weakens the assurance provided, because what is tested are the (manually) translated attribute evaluation functions, not the underlying AGs.

It should be possible to automatically translate from a given AG formalism into the Haskell representation used here; however, then specialized traversal functions for the (typed) quantifiers could be generated and the need for the strategic programming solution proposed here would fall away. It would improve the paper if the authors would discuss these design alternatives.

Replicability:

The accompanying replication package contains the complete Haskell sources for the two examples used in the paper. I did not try to execute these, though.

Minor presentation issues:

l38, all programming languages: that is a strong claim, tone down
l61, can give guarantees: give indications (?)
l91, Eq (2): this is not necessarily true - if the tree already contains a copy of x, then removing one x may lead to a different tree.
l226: shouldn't an inherited attribute be implemented simply by passing the inherited attribute value as argument into evaluation function? Or do I mis- understand this?
l502: the recursion will terminate when the value eventuall reaches 1 (not 0)

Evaluation summary

Pros:

- relevant problem

Cons:

- tests manually re-implemented versions of the attribute evaluation functions

The paper is well written and addresses a relevant problem. My main reason to not recommend accepting the paper is that the approach relies on a manual re-implementation of the attribute evaluation functions and only tests these, rather than the original AG.

I believe it is possible to translate from an AG formalism into the format used here, but that translation could be specialized to the specific AG, and lead to a simpler solution.

In some sense, the paper is more of a new idea paper describing a blueprint rather than a full research paper.





Review #27D


Paper summary

The paper explores the application of property-based testing methods to attribute grammars. It describes a framework where properties are specified for attribute instances and then validated through the generation of abstract syntax trees that comply with these attributes. The paper details the process of automating test case generation and property validation to improve the correctness of attribute grammars. The integration is then realized in a Haskell implementation for the attribute grammar of leaf trees and attribute grammar for Let expressions.


Detailed comments for authors

Soundness

- The paper presents a sound methodological approach. The implementation provides a solid proof of concept.


Relevance

- There is barely any motivation why testing attribute grammars is relevant and a good idea. Is there any evidence that attribute grammar writers need to test their specifications?

- There is a bit a lack of related work (or a short section about related work) or existing techniques for improving attribute grammars. Consequently, there is also no comparison in the paper to other techniques.


Novelty

- In the abstract, the authors claim that there is no work on testing attribute grammars and then present a property-based testing approach. It remains unclear why property-based testing is the chosen testing technique. Why is exactly property-based testing the best approach?

- There is some technical contribution about how to use property-based testing for attribute grammars. Several non-trivial and interesting properties are described in detail in the paper.


Presentation

- Neither are there explicit bullet points about the specific contributions of the paper, nor about the research questions that the paper aims to answer.

- The paper introduces well chosen examples that illustrate the approach. This makes it easy to follow as a reader.

- The introduction of property-based testing and attribute grammars is well done.

- Overall, the paper reads well with and the code or grammar is well embedded and connected to the text.


Replicability

- The authors make the code used in their work public. Moreover, the approach could be easily implemented in another programming language than Haskell.


minor comments:

line 70: "task[24]" -> "task [24]"
line 75: "a novel software testing technique called property-based testing". The idea of property-based testing is arguably old and not really novel.
line 121: "Scala[29]" -> "Scala [29]
line 200: "acesss" -> "access"
line 1079: "an usage" -> "a usage"
line 1052/1052: "an use" -> "a use"
line 1151: "substraction" -> probably "subtraction"



Evaluation summary

Pros:

- Decent technical contributions in terms of properties that can help to effectively use property-based testing for attribute grammars.

- The presentation of the paper and the approach is overall well done, which facilitates future work in this research line.

- The implementation in Haskell is a promising proof of concept.

Cons:

- The motivation of the work is weak and should be improved (the introduction section in particular).

- There are no specific enumeration of contributions or research questions that the paper aims to answer.

- The paper focuses solely on evaluating the approaches presented in the paper. There is no comparison to existing techniques (or reasoning why there is no technique to compare to).