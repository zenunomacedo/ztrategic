
Software testing has become an integral part of modern software development.  In fact, all programming languages include now powerful testing frameworks.  Language testing frameworks include mechanisms to automate the execution of programs with test cases as inputs while reporting which tests have passed/failed. A test that fails is an indication of a program's fault. Although a set of tests that pass is not a proof of the program's correction, it can give guarantees that the program behaves as expected.

Testing can be performed at different software development levels, such as unit testing,  integration testing, regression testing, system testing, etc. Test cases are usually defined as  pairs of inputs and expected outputs that  define assertions programs have to \textit{respect}. This is the case, for example, in the widely used unit testing technique. Writing good test cases that do test all features or components of software systems is a complex task\cite{testsmells}. As a result, several techniques were developed to automatically generate tests~\cite{evosuite} and to access the quality of the test suite, namely test coverage~\cite{testcoverage} and mutation testing~\cite{mutation}. Nevertheless, automated software testing is surprisingly manual still! 


There is, however, a novel software testing technique - called Property-based Testing (PbT)~\cite{quickcheck} - that does not rely on the (manual) definition of test cases. On the contrary, test cases are automatically generated. As a result, in property-based testing programmers/testers just define properties that the functions/method of the software under test should obey. A property is a boolean function stating high level properties of the program. A classical example of a property is a boolean function that states that reversing the concatenation of two lists $l_1$ and $l_2$ is equal to concatenate the reverse of $l_1$ with the reverse of $l_1$. Such a property can be written as follows:

\begin{equation}
\forall l_1,\forall l_2 : reverse\ (l_1\ +\!\!\!+\ l_2) == reverse\ l_2\ +\!\!\!+\ reverse\ l_1
\end{equation}

A \textit{roundtrip} property stating that removing 
 an element $x$ from the tree that is the result of inserting $x$ in tree $t$, yields the same  tree $t$ %is equal to the original tree $t$ 
 can be written as follows:

\begin{equation}
\forall x,\forall t \in Tree : remove (x, (insert(x,t)))  == t
\end{equation}

In fact, there many other properties that we may express in PbT. 

Quantifiers are used to instantiate values in these properties, but most PbT systems allow for a finer control over values through the definition and usage of generators. A generator is a mechanism that produces values, and developers can fine-tune how the generation process behaves, to then use them in more specialized properties.

Indeed, property-based testing was quickly incorporated in most programming languages, as shown by powerful systems such as hypothesis\footnote{\url{https://hypothesis.works/}} (Python), junit-quickcheck (Java), etc.  


Attribute Grammars (AGs)~\cite{Knuth68} are a well-known and
convenient formalism for specifying the semantic analysis phase of those software languages and for  modeling complex multiple traversal algorithms. Indeed, AGs have been used not only to implement general purpose programming
languages, for example Haskell~\cite{DijkstraFS09},
Java~\cite{jastaddj13}, or Oberon0~\cite{KAMINSKI2015,kiamaoberon}, and domain specific
languages, such as Modelica~\cite{AKESSON2010}, and
EasyTime~\cite{FisterMFH11}, but also to specify sophisticated pretty printing algorithms~\cite{SPS99}, deforestation techniques~\cite{SS99,joao07pepm} and powerful type systems~\cite{MiddelkoopDS10}. These works have been performed with the support of traditional AG-based systems~\cite{lrc,lisa,eli92,jastadd,silver} or by  embedding AGs in  a general purpose language, namely Scala\cite{kiama13} and Haskell~\cite{scp2016}.

Unfortunately, there is no work in incorporating (automated) software testing within the attribute grammar formalism! As a consequence, attribute grammar writers are not able to test their specifications as regular programmers test their programs.

This paper introduces property-based testing of attribute grammars that combines property-based testing~\cite{quickcheck} and the Attribute Grammar (AG) formalism. Very much like in PbT, in property-based AGs we specify \textit{properties} on attribute instances that decorate abstract syntax trees. 
Moreover, we define generators that use attribute values to steer the generation process. Such generators can ensure that the generated (abstract syntax) trees not only obey the syntactic rules of the language (as in PbT) but also to the static semantics defined by the AG.

The remainder of this paper is organized as follows: Section~\ref{sec:technique} describes an abstract technique for property-based testing of attribute grammars, Section~\ref{sec:implementation} implements this technique in the Haskell programming language, Section~\ref{sec:relatedwork} discusses related work relevant to this paper, and Section~\ref{sec:conclusion} summarizes our conclusions and proposes future work to evolve this technique. We also provide a replication package with all code used in this work, as well the full code for two generators we discuss in Appendix~\ref{appendix}. 
%\discuss{MARCOS: I think the intro could finish here, adding only a summary of the rest of the paper. Because the rest of the intro somehow overlaps with Section 2}
%\discuss{briefly present AGs: AGs associate attributes to gramamrs. Attribute evaluators compute the values of attribute instances that are associated to nodes in the AST. The result of such attribute evaluation is a decorated AST.}
%
%\discuss{example: %Consider that we have a synthesized attribute named $decls$ where we accumulate all declared names in a program. 
%Consider that an inherited attribute $env$ is used to pass down the accumulated declared names as the (global) environment so that we can detect uses of non-defined names. Let us consider that the use of names occur in nodes of the AST that are instances of production/constructor $(Var\ n)$ (where $n$ is the string with the defined name). We may define a property that checks whether all used names are in the environment as follows:}
%
%\begin{equation}
%%\forall (Var \ n) \in AST :   n \in env
%\forall t \in AST, \forall n \in vars(t) :   n.name \in n.env
%\end{equation}
%
%\todo{discuss: do we use (Var n) in AST, or do we use n in vars(AST)?}
%\todo{say/itemize what we need exactly, maybe with examples: pattern matching, higher-order attributes allow more expressive properties, (generic?) recursion on trees to apply the properties, generators, properties themselves}
%
%where $env$ is the inherited attribute of parent (left-hand side) of that production.
%
%\discuss{Refactoring is a source-to-source transformation that preserves the semantics of the original program. Thus we  may define a property to test }
%
%\begin{equation}
%%\forall t \in AST :   eval\ (opt\ t) == eval\ t
%%\\
%\forall t \in AST : t.refactoring.eval = t.eval     
%\end{equation}
%
%
%
%we decorate the ASTs with the AG's attribute evaluator to  
%verify whether the attribute property remains true under all generated ASTs.
%
%
%
%Property-based testing is a powerful technique that doesn’t require specific examples of inputs and expected outputs. Instead, it utilizes a generative engine to create randomized inputs that ensure correct feature requirements.
%
%The secret to property-based testing is all about properties! These tests are designed to verify that a property remains true under various inputs, significantly reducing the number of tests required with different examples instead. These properties don’t necessarily have to provide detailed output information, but they must check for specific characteristics required in the output.
%
%
%While unit tests check that the output is identical to a pre-computed expected result, property-based tests 