%\subsection{Refactoring Haskell Code}

\begin{comment}
As we have shown, zippers can express strategic term rewriting.  We 
developed a full library, named Ztrategic.
%Figure~\ref{code:api} presents the full API, which is based on Strafunski's API,
The API of this library is based on Strafunski's API,
but adding the possibility to manipulate the zipper that traverses the tree, in order to be able to, for example,
compute attributes.
We define two Strategy types, |TP| and |TU| which stand for Type-preserving and Type-Unifying. The former represents transformations, while the latter represents reductions. 

We define Primitive Strategies that can be used as building blocks, such as identities (|idTP| and |constTU|), failing strategies |failTP| and |failTU|, the |tryTP| combinator which applies a transformation once if possible and always succeeds, and |repeatTP| which applies a transformation as many times as possible. 
Strategy Construction combinators allow for composition of more complex transformations. The |adhoc| combinators compose an existing strategy with a Haskell function, and the |mono| combinators produce a strategy out of one Haskell function. There are variants that allow access to the zipper, denoted by a $Z$ suffix. 

For the composition of traversals, |seq| defines a sequence (perform both traversals) and |choice| an alternative (perform just one traversal). 
We use Traversal Combinators |right| and |down| to travel the zipper; the |all| variants always succeed and the |one| variants can fail. 
Traversal Strategies are defined by combining the previous tools, and come in |td| (top-down) and |bu| (bottom-up) variants. The |full| strategies traverse the whole tree, while |once| strategies perform at most one operation and |stop| strategies stop cut off the traversal in a specific sub-tree when any operation in it succeeds. Finally, |innermost| and |outermost| perform a transformation as many times as possible, starting from the inside or outside nodes, respectively.  


\begin{figure*}[h]
\begin{minipage}[t]{.55\textwidth}
\textbf{Strategy types}
> type TP a = Zipper a -> Maybe (Zipper a)
> type TU m d = (forall a. Zipper a -> m d) 
\textbf{Primitive strategies}
> idTP      :: TP a
> constTU   :: d -> TU m d
> failTP    :: TP a 
> failTU    :: TU m d
> tryTP     :: TP a -> TP a
> repeatTP  :: TP a -> TP a
\textbf{Strategy Construction}
> monoTP    :: (a -> Maybe b) -> TP e
> monoTU    :: (a -> m d) -> TU m d
> monoTPZ   :: (a -> Zipper e -> Maybe b) -> TP e
> monoTUZ   :: (a -> Zipper e -> m d) -> TU m d
> adhocTP   :: TP e -> (a -> Maybe b) -> TP e
> adhocTU   :: TU m d -> (a -> m d) -> TU m d
> adhocTPZ  :: TP e -> (a -> Zipper e -> Maybe b) -> TP e
> adhocTUZ  :: TU m d -> (a -> Zipper c -> m d) -> TU m d
\textbf{Composition / Choice}
> seqTP     :: TP a -> TP a -> TP a
> choiceTP  :: TP a -> TP a -> TP a
> seqTU     :: TU m d -> TU m d -> TU m d
> choiceTU  :: TU m d -> TU m d -> TU m d
\end{minipage}
\begin{minipage}[t]{.35\textwidth}
\textbf{Traversal Combinators}
> allTPright  :: TP a -> TP a
> oneTPright  :: TP a -> TP a
> allTUright  :: TU m d -> TU m d
> allTPdown   :: TP a -> TP a
> oneTPdown   :: TP a -> TP a
> allTUdown   :: TU m d -> TU m d
\textbf{Traversal Strategies}
> full_tdTP  :: TP a -> TP a
> full_buTP  :: TP a -> TP a
> once_tdTP  :: TP a -> TP a
> once_buTP  :: TP a -> TP a  
> stop_tdTP  :: TP a -> TP a
> stop_buTP  :: TP a -> TP a
> innermost  :: TP a -> TP a
> outermost  :: TP a -> TP a
> full_tdTU  :: TU m d -> TU m d     
> full_buTU  :: TU m d -> TU m d     
> once_tdTU  :: TU m d -> TU m d   
> once_buTU  :: TU m d -> TU m d  
> stop_tdTU  :: TU m d -> TU m d  
> stop_buTU  :: TU m d -> TU m d
\end{minipage}
\caption{Full Ztrategic API}
\label{code:api}
\end{figure*}

Let us show now the expressiveness of our Ztrategic library in
implementing useful transformations of a real programming language
such as Haskell. We reuse the available support for parsing and
pretty printing as part of the Haskell core libraries (in the
\textit{haskell-src} package).

\end{comment}

In order to evaluate our combined zipper-based embedding of attribute
grammars and strategic term-rewriting we consider three language
engineering problems: First, we define a refactoring that eliminates the monadic \textit{do-notation} from Haskell programs.
Second, we evaluate the performance of our
library by comparing the runtimes of an implementation in  Ztrategic of a Haskell smell eliminator with its Strafunski counterpart when processing a large set of smelly
Haskell programs. Third, we express in Ztrategic the largest
language specification developed in this setting: the Oberon-0
language. The construction of a processor for Oberon-0 was proposed in
the LDTA Tool Challenge~\cite{ldta}, and it was concisely and
efficiently specified using AGs and strategies in
Kiama~\cite{oberonkiama}. 

\medskip

\noindent
\textit{Do-notation elimination:}
We start by defining a refactoring that eliminates the syntactic sugar
introduced by the monadic \textit{do-notation}.
%In fact, we used this
%notation in |sumBZero'|, and we can rewrite in an applicative
%functional style as expressed by the monadic binding function |(>>=)|.
\begin{comment}
\begin{code}
sumBZero'' :: Maybe Exp
sumBZero'' = down' t_1 >>= down' >>= right >>= getHole 
\end{code}
\end{comment}
In order to automate this refactoring, a type-preserving strategy is
used to perform a full traversal in the Haskell tree, since such
expressions can be arbitrarily nested. The rewrite step
behaves like the identity function by default with a
type-specific case for pattern matching the \textit{do-notation} in the
Haskell tree (constructor  |HsDo|).

The following type-specific transformation function |doElim| just
matches |HsDo| nodes and returns the correct desugared node,
%expresses the refactoring we showed for the concrete |sumBZero'| to
%|sumBZero''| example. Obviously, it is 
expressed at abstract syntax
tree level. We omit the details of its representation
as Haskell data types.

\begin{code}
refactor :: Zipper HsModule -> Maybe (Zipper HsModule)
refactor  h = applyTP (innermost step) h where step = failTP `adhocTP` doElim

doElim    :: HsExp -> Maybe HsExp
doElim (HsDo [HsQualifier e])            = Just e
doElim (HsDo (HsQualifier e:stmts))
        = Just ((HsInfixApp  e (HsQVarOp (hsSymbol ">>")) (HsDo stmts)))
doElim (HsDo (HsGenerator _ p e:stmts))  = Just (letPattern p e stmts))
doElim (HsDo (HsLetStmt decls:stmts))    = Just (HsLet decls (HsDo stmts))
doElim _                                 = Nothing
\end{code}

We conclude that our library allows for the definition of powerful
source code transformations in a concise manner. We also include a list 
desugaring implementation in our work's repository.
%, which we omit as the approach is similar.


\medskip
\noindent
\textit{Smells Elimination:}
Source code smells make code harder to comprehend. A smell is not an
error, but it indicates a bad programming practice.
% They do occur in any language and Haskell is no exception.
For example, inexperienced
Haskell programmers often write |l == []| to check whether a list is
empty, instead of using the predefined |null| function.  Next, we
present a strategic function that eliminates several Haskell smells
as reported in~\cite{Cowie}.

\begin{code}
smellElim h = applyTP (innermost step) h
  where step =  failTP  `adhocTP` joinList         `adhocTP` nullList 
                        `adhocTP` redundantBoolean `adhocTP` reduntantIf 
\end{code}

\noindent where |joinList| detects patterns where list concatenations are
inefficiently defined, |nullList| detects patterns where a list is checked for
emptiness, |redundantBoolean| detects redundant boolean checks, and
|reduntantIf| detects redundant  |if| clauses.


In order to assess the runtime performance of our zipper-based
strategic term rewriting implementation, we compare it with the
state-of-the-art, fully optimized Strafunski system. A detailed
analysis of runtime performance of the zipper-based embedding of AGs
is presented in~\cite{memoAG19},
in which \textit{memoized} zipper-based attribute
grammars with very large inputs are benchmarked, showing that this AG 
embedding is not
only concise and elegant, but also efficient. 
%\todo{"In this work we have also
%incorporated memoization in order to avoid attribute recalculation." 
%i believe this is not correct...} %%%%!!
%We
%have executed zipper-based attribute
%grammars with very large inputs, showing that our AG embedding is not
%only concise and elegant, but is also efficient.

\begin{wraptable}{r}{0.5\textwidth}
%\begin{table}[]
\centering
%\begin{tabular}{lll}
\begin{tabular}{p{2.75cm} p{1.5cm} p{1.5cm}}
\hline
                         & Ztrategic & Strafunski \\ \hline
Lines of Code            & 22        & 22         \\
Runtime                  & 16.2s     & 10.2s      \\
%Runtime per file         & 16.2ms    & 10.2ms     \\ 
Average Memory     & 6607Kb & 6580Kb  \\ \hline
\end{tabular}
\caption{Haskell Smell Eliminators in Ztrategic and Strafunski.}
\end{wraptable}
%\end{table}
Let us consider the Haskell smell eliminator expressed in both 
Ztrategic and Strafunski. To run both tools with large \textit{smelly}
inputs, we consider 150 Haskell projects developed by first-year
students as presented in~\cite{projects}. In these projects there are
1139 Haskell files totaling 82124 lines of code, of which exactly
1000 files were syntactically correct~\footnote{The student projects
used in this benchmark are available at this work's repository.}. Both
Ztrategic and Strafunski smell eliminators detected and eliminated 850
code smells in those files.
To compare the runtime performance of both implementations, we computed
an average of 5 runs, on a Ubuntu 16.04 machine, i5-7200U Dual Core,
with 8 GB RAM. 
%our library measured $16.2$ seconds of processing,
%while Strafunski measured $10.2$ seconds. 
In this case, the very first
version of Ztrategic, while being more expressive, 
is only 60\% slower than the Strafunski library.


\begin{wraptable}{r}{0.5\textwidth}
%\begin{table}[]
%\begin{tabular}{|l|l|l|}
\centering
\vspace{-0.5cm}
\begin{tabular}{p{2.5cm} p{1.5cm} p{1.5cm}}
\hline
Task              & Ztrategic & Kiama \\ \hline
Oberon-0 Tree    & 57        & 99    \\
%Syntax Analyser   & 80        & 187   \\
%Oberon-0 printer & 73        & 163   \\
%Sub-total        & 206       & 469   \\ \hline
Name analyzer     & 50        & 222   \\ %\hline
Type analyzer     & 34        & 117   \\ %\hline
Lifter            & 6         & 23    \\
Desugarer         & 76        & 123   \\
%Sub-total        & 82        & 146   \\ \hline
Total             & 223       & 584   \\ \hline
\end{tabular}
\caption{Numbers of Lines of Code for the Oberon-0 L2 tasks.}
\label{table2}
%\end{table}
\end{wraptable}
\medskip
\noindent
\textit{Oberon-0 in Ztrategic:}
The LDTA Tool Challenge~\cite{ldta} was a challenge focused on
the construction of a compiler for the Oberon-0 language, with the goal of comparing the
formalisms and toolsets used in it. The challenge was divided into 5
tasks: parsing and pretty-printing, name binding, type checking,
desugaring and C code generation. These tasks were to be performed on
the Oberon-0 language, which in itself was divided into 5 increasingly
complex levels. We consider the level 2 (L2) of the Oberon-0 problem, and we specified
the name binding, type checking and desugaring tasks in our Ztrategic
AG approach. We use attributes for contextual information when needed,
for example in name analysis to check whether a used name has been
declared. This language level requires the desugaring of |For| and |Case|
statements into semantically equivalent |While| and (nested) |If|
statements. Such desugaring is implemented using Ztrategic 
type-preserving strategies, and the result is a 
%\todo{(higher-order) tree} %explain this? 
new tree in which name analysis and type checking is performed 
through strategic traversals that use attributes. 
%that is
%then decorated to perform name analysis and type checking via
%ZippersAG. 
Because desugaring a |For| statement induces a new assignment (before
the new |WhileStmt| statement) whose variable needs to be added to the declarations
part of the original AST, we use the attribute |numForDown| which is a
synthesized attribute of the original tree.  Having the desugared AST 
%(represented by the 
%\todo{attributable attribute} %explain this?
%|ata|) 
and the 
number of |For| statements refactored, then we return the final
higher-order tree where the induced variables are properly declared.

\begin{code}
desugar m =  let  numberOfFors = numForsDown (toZipper m)
                  step = failTP `adhocTP` desugarFor `adhocTPZ` desugarCase 
                  ata = fromJust (applyTP (innermost step) (toZipper m))
             in   injectForVars numberOfFors (fromZipper ata) 
\end{code}

We omit here the definition of the worker function |desugarFor|. Its
definition is fully included in~\cite{ztrategicRepo},
and it is also similar to the Kiama definition presented
in~\cite{oberonkiama}.
%if False
Next, we show the work that is performed when the |innermost| strategy
visits |ForStmt| nodes:


\begin{code}
desugarFor :: Statement -> Zipper Root -> Maybe Statement
desugarFor  (ForStmt idFor startFor dirFor stopFor stepFor ssFor) z = 
            Just (SeqStmt assign1 (SeqStmt assign2 loop))
 where  assign1       = AssigStmt idFor startFor       
        upperVarName  = "_forCounter" ++ show (numFors z)
        assign2       = AssigStmt upperVarName stopFor
        loop          = WhileStmt whileExp whileStmt
        whileExp      = IntCmpExp op (IdExp ifFor) (IdExp upperVarName)
        op            = case  dirFor of 
                              To      -> LECmp
                              Downto  -> GECmp
        whileStmt     = SeqStmt ssFor updateVar
        updateVar     = AssigStmt ifFor (IntBOpExp Plus (IdExp ifFor) (stepFor))
desugarFor _ z = Nothing 
\end{code}

\noindent
where, three new statements are produced: |assign1|, |assign2| and
|loop|, where the first two statements are assignments defining
variables for the initial and final values of the loop, respectively,
and |loop| is the transformed While loop. We also use attribute
|numFors| that counts the number of |For| loops that occur before this
loop, allowing us to identify this loop uniquely. 

%endif
In Table~\ref{table2}, we compare our approach to the results presented
in~\cite{oberonkiama} for the L2 language level. 
Notably, we show that our approach, even in its earliest versions,
 is suited for large-scale and 
real world usage.
%. We consider the  L2 language level and we report the 
%the number of lines of code of both solutions.

\begin{comment}
\begin{table}[]
\centering
%\begin{tabular}{llll}
\begin{tabular}{p{3cm} p{2cm} p{2cm} p{2cm}}
\hline
               & Ztrategic & Kiama & Strafunski \\ \hline
Oberon-0 Tree  & 57        & 99    & --         \\
Name analyser  & 50        & 222   & --         \\
Type analyser  & 34        & 117   & --         \\
Lifter         & 6         & 23    & --         \\
Desugarer      & 76        & 123   & --         \\
Oberon-0 Total & 223       & 584   & --         \\
Do-notation    & 16        & --    & 16         \\
Haskell Smells & 22        & --    & 22         \\ \hline
\end{tabular}
\caption{Numbers of Lines of Code for the previous tasks.}
%the Oberon-0 L2 tasks, Do-Notation elimination and Haskell Smell elimination.}
\end{table}
\end{comment}

\begin{comment}
Next, we show the strategy for type checking an Oberon-0 program. We
rely on a full, top-down approach, as we do not need more than one
strategic traversal on the tree - we typecheck each node at most once.

\begin{code}
types :: Zipper Root -> [String]
types r = applyTU (full_tdTU step) r 
 where step = failTU `adhocTUZ` typesExp `adhocTUZ` typesAssig
\end{code}

We use the |typesExp| and |typesAssig| functions to define our type analysis - we define |typesAssig| and ommit |typesExp| as it is similar albeit more verbose. 

\begin{code}
typesAssig :: Statement -> Zipper Root -> [String] 
typesAssig (AssigStmt v e) z = mBSame (typeOf z) (typeOf $ z.$2)
typesAssig _               z = []
\end{code}

We make use of a |typeOf| attribute, which specifies the type of a variable or expression, to compare the types being matched here. More precisely, we compare the type of the assignment node itself (denoted by |z|) with the type of its righthand side expression (denoted by |z.$2|), and we make use of the helper function |mBSame| for this comparison, which will produce an error message when the types do not match. Note that for every new type of statement that gets introduced into the language, if it needs typechecking, then it should require one more line of code in this function, else the function remains unchanged, thus making this approach very compact.
The behaviour defined in the |typesExp| function is similar but it uses more lines of code as there are more relevant type-checking rules for expressions that need to be handled. Should a new data type need type-checking, we can extend this code by adding a new function to the definition of |step| which handles the new data type. 
\end{comment}




\begin{comment}
I think we can omit this: 
Note that the comparison between the two examples is not perfectly
linar - for example, both implementations for desugaring of for-loops
optimize the expressions of the cycle by introducing variables that
store the result of the computation, so that it is not repeated on
each iteration. However, the Kiama implementation uses the name
"\_limit" for all cycle upper limits, therefore incorrectly reusing
said variable when in nested loops. Our implementation, for the
$N\textsuperscript{th}$ for-loop, will declare a "\_forCounterN", and
thus we need to compute N, for which the attribute |numFors| is
used. Moreover, our implementation of syntax analyser and printer are
inefficient, as they were built only as supporting steps of our main
focus tasks.
\end{comment}

\begin{comment}

-- component - without emptylines - with emptylines
tree - 57 - 86
parser - 80 - 109
prettyprinter - 73 - 89

errors - 13 - 16 - (+attributes=37)

type - 13 - 15 (+attributes=21)

lifter - 6
desugarer - 39 - 47 (attributes=37)
\end{comment}

\begin{comment}
In fact, the code smells these optimizations solve are typical mistakes of beginner Haskell programmers. Through strategic application of these optimization functions, we can precisely define the transformations we intend to apply without needing to worry about boilerplate code. 
\end{comment}

\begin{comment}
This approach, of course, has the disadvantage
of having to build and maintain the zipper throughout the traversal of
the data structures. However, the loss in performance is compensated
by having more expressive power - for each node that is processed, the
context can be extracted by traversing the zipper. This will be
further explored in Section \ref{sec3}, by using a zipper-based
attribute grammar embedding.
\end{comment}

\begin{comment}
Focus on the technical part
Show code - explain that zippers (down, left etc) + overhead of actual zipper = heavy
Tradeoff performance - usability (step function only applies at most once = better performance, but more inconvenient)
\subsection{Benchmark? Repo?}
\end{comment}





