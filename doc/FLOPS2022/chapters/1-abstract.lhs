
Strategic term rewriting and attribute grammars are two powerful programming techniques widely used in language engineering. The former relies on \textit{strategies} 
(recursion schemes) 
to apply term rewrite rules in defining transformations, while the latter is suitable for expressing context-dependent language processing algorithms. Each of these techniques, however, is usually implemented by its own powerful and large  processor system. As a result, it makes such systems harder to extend and to combine.

%In this paper,
We present the embedding of both strategic tree rewriting and attribute grammars in a zipper-based, purely functional setting.
%Zippers provide a simple, but generic tree-walk mechanism that is the building block technique we use to express the purely-functional embedding of both techniques. 
The embedding of the two techniques in the same setting has several advantages: First, we easily combine/zip attribute grammars and strategies, thus providing language engineers the best of the two worlds. Second, the combined embedding is easier to maintain and extend since it is written in a concise and uniform setting.
%This results in a very small library  which is able to express advanced (static) analysis and transformation tasks.
We show the expressive power of our library in optimizing Haskell let expressions, expressing several Haskell refactorings and solving several language processing tasks for an Oberon-0 compiler.
%the LDTA Tool Challenge.



%de momento, 224 linhas. Inclui um pouco de comentários e legacy code...

%This paper presents Ztrategic, a library for strategic generic data traversal using zippers in Haskell. %This library implements strategies, which allow easy transformation and reduction of generic data %structures, and complements them with zippers, which enable the concept of local context and motion %through the data structure. This enables context-dependant strategic operations, as it is possible to %derive context from moving the zipper. We integrate this work with a zipper-based embedding of attribute %grammars, thus improving an already powerful formalism for algorithms that rely on multiple structure %traversals. As a running example, we simplify a simple expression language, but we also show examples of %transformations on Haskell source code. 


%
%
%
%
%Strategic programming allows traversal of heterogeneous data structures, and it is possible to define either generic behaviour or non-generic behaviour for certain types of the data structure. 

