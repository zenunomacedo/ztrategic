
\documentclass[runningheads]{llncs}

%include lhs2TeX.fmt
%include lhs2TeX.sty

%We can control the size of code blocks by changing these values. 
%We can obtain the original ones by commenting this out
\AtBeginDocument{
\setlength{\abovedisplayskip}{6pt}
\setlength{\belowdisplayskip}{6pt}
\setlength{\abovedisplayshortskip}{6pt}
\setlength{\belowdisplayshortskip}{6pt}
}

%include polycode.fmt
\usepackage{amsmath}
\usepackage{wrapfig}
\usepackage{caption}
\usepackage{float}
\usepackage{xspace}
\usepackage{xcolor}
\usepackage{comment}
\usepackage{graphicx}
\usepackage{microtype}
%\usepackage[T1]{fontenc}

%\usepackage[usenames]{color}

%\input{haskell_newcolors.sty}
%\input{ag.sty}
\input{haskell_newcolorsBW.sty}
\input{agBW.sty}


%include formats.lhs


\newcommand{\discuss}[1]{\textcolor{blue}{#1}\xspace}
\newcommand{\todo}[1]{\textcolor{red}{#1}\xspace}

%Sets the size of displayed code
\renewcommand{\hscodestyle}{\small}
\setlength\mathindent{0cm}

%%
%% \BibTeX command to typeset BibTeX logo in the docs
%\AtBeginDocument{%
%  \providecommand\BibTeX{{%
%    \normalfont B\kern-0.5em{\scshape i\kern-0.25em b}\kern-0.8em\TeX}}}


\begin{document}

%%
%% The "title" command has an optional parameter,
%% \title[short title]{full title}
%% allowing the author to define a "short title" to be used in page headers.
\title{Zipping Strategies and Attribute Grammars}

%%
%% The "author" command and its associated commands are used to define
%% the authors and their affiliations.
%% Of note is the shared affiliation of the first two authors, and the
%% "authornote" and "authornotemark" commands
%% used to denote shared contribution to the research.


\author{Jos\'e Nuno Macedo\inst{1}%\orcidID{0000-0002-0282-5060}
\and
Marcos Viera\inst{2} \and
Jo\~ao Saraiva\inst{1}%\orcidID{0000-0002-5686-7151}
}
%
\authorrunning{J. N. Macedo et al.}

\institute{Dep. of Informatics \& HASLab/INESC TEC, University of Minho, Braga, Portugal\\ 
\email{jose.n.macedo@@inesctec.pt} , \email{saraiva@@di.uminho.pt} \\
\and Universidad de la Rep\'ublica, Montevideo, Uruguay\\
\email{mviera@@fing.edu.uy}}
%\and
%Springer Heidelberg, Tiergartenstr. 17, 69121 Heidelberg, Germany
%\\ \url{http://www.springer.com/gp/computer-science/lncs} 




\maketitle
%\renewcommand{\shortauthors}{Trovato and Tobin, et al.}


\begin{abstract}
%\input{chapters/1-abstract}
%include chapters/1-abstract.lhs
\keywords{Attribute Grammars, Zippers, Strategic Term Rewriting}
\end{abstract}

%\maketitle

\section{Introduction}
\label{sec1}
%\input{chapters/2-introduction}
%include chapters/2-introduction.lhs

\section{Ztrategic: Zipper-Based Strategic Programming}
\label{sec2}
%\input{chapters/3-zipperStrategic}
%include chapters/3-zipperStrategic.lhs

\section{Strategic Attribute Grammars}
\label{sec3}
%\input{chapters/4-strategicAG}
%include chapters/4-strategicAG.lhs

\section{Expressiveness and Performance}
\label{sec4}
%\input{chapters/5-ztrategic}
%include chapters/5-ztrategic.lhs

\section{Related Work}
\label{sec5}
%\input{chapters/6-relatedwork}
%include chapters/6-relatedwork.lhs
\section{Conclusions}%and Future Work}
\label{sec6}
%\input{chapters/7-conclusion}
%include chapters/7-conclusion.lhs
\section*{Acknowledgements}
\label{ack}
\small
%
%This work is financed by National Funds through the Portuguese funding agency, FCT - Fundação para a Ciência e a Tecnologia within project POCI-01-0145-FEDER-016718 and UID/EEA/50014/2013. The first author is also sponsored by FCT grant SFRH/BD/112733/2015.

%The first author is financed by National Funds through the Portuguese funding agency, FCT - Funda\c{c}\~{a}o para a Ci\^{e}ncia e
%a Tecnologia by FCT grant \texttt{2021.08184.BD}.

This work is financed by National Funds through the Portuguese funding agency, FCT - Fundação para a Ciência e a Tecnologia, within project \texttt{LA/P/0063/2020}. The first author is also sponsored by FCT grant \texttt{2021.08184.BD}.

%%
%% The next two lines define the bibliography style to be used, and
%% the bibliography file.
\bibliographystyle{splncs04}
\bibliography{bibliography}

%%
%% If your work has an appendix, this is the place to put it.

%\appendix

%%include chapters/appendix.lhs

\end{document}
\endinput
