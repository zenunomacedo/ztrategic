
%%%% Generic manuscript mode, required for submission
%%%% and peer review
%\documentclass[manuscript,screen,review]{acmart}

\documentclass[format=sigconf]{acmart}
%\documentclass[sigplan,review]{acmart}
%\documentclass[sigplan]{acmart}

%include lhs2TeX.fmt
%include lhs2TeX.sty


%%%%% quick and dirty workaround to the Bbbk problem: read https://github.com/kosmikus/lhs2tex/issues/82
\let\Bbbk\undefined
%include polycode.fmt
\let\Bbbk\undefined


\usepackage{amsmath}
\usepackage{caption}
\usepackage{float}
\usepackage{xspace}
%\usepackage[T1]{fontenc}

%\usepackage[usenames]{color}

%\input{haskell_newcolors.sty}
%\input{ag.sty}
\input{haskell_newcolorsBW.sty}
\input{agBW.sty}


%include formats.lhs


\newcommand{\discuss}[1]{\textcolor{blue}{#1}\xspace}
\newcommand{\todo}[1]{\textcolor{red}{#1}\xspace}
\renewcommand{\hscodestyle}{\small}


%%
%% \BibTeX command to typeset BibTeX logo in the docs
\AtBeginDocument{%
  \providecommand\BibTeX{{%
    \normalfont B\kern-0.5em{\scshape i\kern-0.25em b}\kern-0.8em\TeX}}}


%% Rights management information.  This information is sent to you
%% when you complete the rights form.  These commands have SAMPLE
%% values in them; it is your responsibility as an author to replace
%% the commands and values with those provided to you when you
%% complete the rights form.
\setcopyright{acmcopyright}
\copyrightyear{2018}
\acmYear{2018}
\acmDOI{10.1145/1122445.1122456}

%% These commands are for a PROCEEDINGS abstract or paper.
%\acmConference[Woodstock '18]{Woodstock '18: ACM Symposium on Neural
%  Gaze Detection}{June 03--05, 2018}{Woodstock, NY}
%\acmBooktitle{Woodstock '18: ACM Symposium on Neural Gaze Detection,
%  June 03--05, 2018, Woodstock, NY}
%\acmPrice{15.00}
%\acmISBN{978-1-4503-XXXX-X/18/06}


%%
%% Submission ID.
%% Use this when submitting an article to a sponsored event. You'll
%% receive a unique submission ID from the organizers
%% of the event, and this ID should be used as the parameter to this command.
%%\acmSubmissionID{123-A56-BU3}

%%
%% The majority of ACM publications use numbered citations and
%% references.  The command \citestyle{authoryear} switches to the
%% "author year" style.
%%
%% If you are preparing content for an event
%% sponsored by ACM SIGGRAPH, you must use the "author year" style of
%% citations and references.
%% Uncommenting
%% the next command will enable that style.
%%\citestyle{acmauthoryear}



\begin{document}

%%
%% The "title" command has an optional parameter,
%% \title[short title]{full title}
%% allowing the author to define a "short title" to be used in page headers.
\title{Ztrategic: Strategic Programming with Zippers}

%%
%% The "author" command and its associated commands are used to define
%% the authors and their affiliations.
%% Of note is the shared affiliation of the first two authors, and the
%% "authornote" and "authornotemark" commands
%% used to denote shared contribution to the research.
\author{Jos\'e Nuno Macedo}
\email{jose.n.macedo@@inesctec.pt}
\orcid{0000-0002-0282-5060}
\affiliation{
  \institution{HASLab \& INESC TEC, University of Minho}
  \city{Braga}
  \country{Portugal}
}
  
\author{Emanuel Rodrigues}
\email{jose.e.rodrigues@@inesctec.pt}
%\orcid{0000-0002-0282-???}
\affiliation{
  \institution{HASLab \& INESC TEC, University of Minho}
  \city{Braga}
  \country{Portugal}
}
  
  
\author{Marcos Viera}
\email{mviera@@fing.edu.uy}
%\orcid{0000-0002-5686-7151}
\affiliation{
  \institution{Universidad de la República}
  \city{Montevideo}
  \country{Uruguay}
}

\author{Jo\~ao Saraiva}
\email{saraiva@@di.uminho.pt}
\orcid{0000-0002-5686-7151}
\affiliation{
  \institution{HASLab \& INESC TEC, University of Minho}
  \city{Braga}
  \country{Portugal}
}


%\renewcommand{\shortauthors}{Trovato and Tobin, et al.}


%%
%% Keywords. The author(s) should pick words that accurately describe
%% the work being presented. Separate the keywords with commas.
\keywords{Strategic Programming, Zippers, Generic Traversals}


%include chapters/abstract.lhs

\maketitle


\section{Introduction}
\label{sec:intro}
%include chapters/introduction.lhs

\section{Strategic Programming with Zippers}
\label{sec:zipping}
%include chapters/strategic.lhs


\section{Performance of the Ztrategic Library}
\label{sec:performance}
%include chapters/performance.lhs

%\section{Related Work}
%\label{sec:related}
%%include chapters/relatedwork.lhs

\section{Conclusion}
\label{sec:conc}
%include chapters/conclusions.lhs




%%
%% The next two lines define the bibliography style to be used, and
%% the bibliography file.
\bibliographystyle{ACM-Reference-Format}
\bibliography{bibliography}

%%
%% If your work has an appendix, this is the place to put it.

\clearpage
\appendix
%include chapters/api.lhs
%%include chapters/appendix.lhs


\end{document}
\endinput
