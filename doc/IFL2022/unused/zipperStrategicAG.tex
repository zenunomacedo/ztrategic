



Before we present our efficient embedding in detail, let us consider a motivating example
that requires two widely used language engineering techniques: language analysis and language optimization.   Consider the (sub)language of \ensuremath{\Conid{Let}} expressions as incorporated in most functional languages, including \ensuremath{\Conid{Haskell}}. Next, we show an example of a valid \ensuremath{\Conid{Haskell}} \ensuremath{\mathbf{let}}
expression


\begin{hscode}\SaveRestoreHook
\column{B}{@{}>{\hspre}l<{\hspost}@{}}%
\column{6}{@{}>{\hspre}l<{\hspost}@{}}%
\column{12}{@{}>{\hspre}l<{\hspost}@{}}%
\column{E}{@{}>{\hspre}l<{\hspost}@{}}%
\>[B]{}\Varid{p}\mathrel{=}{}\<[6]%
\>[6]{}\mathbf{let}\;{}\<[12]%
\>[12]{}\Varid{a}\mathrel{=}\Varid{b}\mathbin{+}\mathrm{0}{}\<[E]%
\\
\>[12]{}\Varid{c}\mathrel{=}\mathrm{2}{}\<[E]%
\\
\>[12]{}\Varid{b}\mathrel{=}\mathbf{let}\;\Varid{c}\mathrel{=}\mathrm{3}\;\mathbf{in}\;\Varid{b}\mathbin{+}\Varid{c}{}\<[E]%
\\
\>[6]{}\mathbf{in}\;{}\<[12]%
\>[12]{}\Varid{a}\mathbin{+}\mathrm{7}\mathbin{-}\Varid{c}{}\<[E]%
\ColumnHook
\end{hscode}\resethooks

\noindent
and, we define the heterogeneous data type \ensuremath{\Conid{Let}} that models let expressions in \ensuremath{\Conid{Haskell}} itself.

\begin{hscode}\SaveRestoreHook
\column{B}{@{}>{\hspre}l<{\hspost}@{}}%
\column{7}{@{}>{\hspre}l<{\hspost}@{}}%
\column{13}{@{}>{\hspre}c<{\hspost}@{}}%
\column{13E}{@{}l@{}}%
\column{16}{@{}>{\hspre}l<{\hspost}@{}}%
\column{23}{@{}>{\hspre}l<{\hspost}@{}}%
\column{27}{@{}>{\hspre}l<{\hspost}@{}}%
\column{E}{@{}>{\hspre}l<{\hspost}@{}}%
\>[B]{}\mathbf{data}\;{}\<[7]%
\>[7]{}\Conid{Let}{}\<[13]%
\>[13]{}\mathrel{=}{}\<[13E]%
\>[16]{}\Conid{Let}\;\Conid{List}\;\Conid{Exp}{}\<[E]%
\\
\>[B]{}\mathbf{data}\;{}\<[7]%
\>[7]{}\Conid{List}{}\<[13]%
\>[13]{}\mathrel{=}{}\<[13E]%
\>[16]{}\Conid{NestedLet}\;{}\<[27]%
\>[27]{}\Conid{String}\;\Conid{Let}\;\Conid{List}{}\<[E]%
\\
\>[13]{}\mid {}\<[13E]%
\>[16]{}\Conid{Assign}\;{}\<[27]%
\>[27]{}\Conid{String}\;\Conid{Exp}\;\Conid{List}{}\<[E]%
\\
\>[13]{}\mid {}\<[13E]%
\>[16]{}\Conid{EmptyList}{}\<[E]%
\\
\>[B]{}\mathbf{data}\;{}\<[7]%
\>[7]{}\Conid{Exp}{}\<[13]%
\>[13]{}\mathrel{=}{}\<[13E]%
\>[16]{}\Conid{Add}\;{}\<[23]%
\>[23]{}\Conid{Exp}\;\Conid{Exp}{}\<[E]%
\\
\>[13]{}\mid {}\<[13E]%
\>[16]{}\Conid{Sub}\;{}\<[23]%
\>[23]{}\Conid{Exp}\;\Conid{Exp}{}\<[E]%
\\
\>[13]{}\mid {}\<[13E]%
\>[16]{}\Conid{Neg}\;{}\<[23]%
\>[23]{}\Conid{Exp}{}\<[E]%
\\
\>[13]{}\mid {}\<[13E]%
\>[16]{}\Conid{Var}\;{}\<[23]%
\>[23]{}\Conid{String}{}\<[E]%
\\
\>[13]{}\mid {}\<[13E]%
\>[16]{}\Conid{Const}\;{}\<[23]%
\>[23]{}\Conid{Int}{}\<[E]%
\ColumnHook
\end{hscode}\resethooks

Consider now that we wish to implement a  simple arithmetic optimizer for our language.  Figure~\ref{rules} presents such optimization rules literally taken from~\cite{strategicAG}.

\begin{figure}
\begin{align}\label{eq:rules}
add(e, const(0)) &\rightarrow e \\
add(const(0), e) &\rightarrow e \\
add(const(a), const(b)) &\rightarrow const(a+b) \\
sub(e1, e2) &\rightarrow add(e1, neg(e2)) \\
neg(neg(e)) &\rightarrow e \\
neg(const(a)) &\rightarrow const(-a) \\
var(id) \mid (id, just(e)) \in env &\rightarrow e
\end{align}
\caption{Optimization Rules}
\label{rules}
\end{figure}

The first six optimization rules define context-free arithmetic rules.
If we consider those six rules only, then strategic term re-writing is an extremely suitable formalism to express the desired optimization, since it provides a solution that just defines the work to be done in the constructors (tree nodes) of interest, and "ignore" all the others. 

%\subsection{Optimization Rules via Strategic Term Re-writing}

\paragraph{Strategic Term Re-writing:}
In fact, we can easily express this optimization in Strafunski: the strategic term re-writing library for Haskell~\cite{strafunski}. We start by defining the \textit{worker} function, that directly follows the six rules we are considering: 


\begin{hscode}\SaveRestoreHook
\column{B}{@{}>{\hspre}l<{\hspost}@{}}%
\column{33}{@{}>{\hspre}l<{\hspost}@{}}%
\column{E}{@{}>{\hspre}l<{\hspost}@{}}%
\>[B]{}\Varid{expr}\mathbin{::}\Conid{Exp}\to \Conid{Maybe}\;\Conid{Exp}{}\<[E]%
\\
\>[B]{}\Varid{expr}\;(\Conid{Add}\;\Varid{e}\;(\Conid{Const}\;\mathrm{0})){}\<[33]%
\>[33]{}\mathrel{=}\Conid{Just}\;\Varid{e}{}\<[E]%
\\
\>[B]{}\Varid{expr}\;(\Conid{Add}\;(\Conid{Const}\;\mathrm{0})\;\Varid{t}){}\<[33]%
\>[33]{}\mathrel{=}\Conid{Just}\;\Varid{t}{}\<[E]%
\\
\>[B]{}\Varid{expr}\;(\Conid{Add}\;(\Conid{Const}\;\Varid{a})\;(\Conid{Const}\;\Varid{b})){}\<[33]%
\>[33]{}\mathrel{=}\Conid{Just}\;(\Conid{Const}\;(\Varid{a}\mathbin{+}\Varid{b})){}\<[E]%
\\
\>[B]{}\Varid{expr}\;(\Conid{Sub}\;\Varid{a}\;\Varid{b}){}\<[33]%
\>[33]{}\mathrel{=}\Conid{Just}\;(\Conid{Add}\;\Varid{a}\;(\Conid{Neg}\;\Varid{b})){}\<[E]%
\\
\>[B]{}\Varid{expr}\;(\Conid{Neg}\;(\Conid{Neg}\;\Varid{f})){}\<[33]%
\>[33]{}\mathrel{=}\Conid{Just}\;\Varid{f}{}\<[E]%
\\
\>[B]{}\Varid{expr}\;(\Conid{Neg}\;(\Conid{Const}\;\Varid{n})){}\<[33]%
\>[33]{}\mathrel{=}\Conid{Just}\;(\Conid{Const}\;(\mathbin{-}\Varid{n})){}\<[E]%
\\
\>[B]{}\Varid{expr}\;\anonymous {}\<[33]%
\>[33]{}\mathrel{=}\Conid{Nothing}{}\<[E]%
\ColumnHook
\end{hscode}\resethooks


Having expressed all re-writing rules in function \ensuremath{\Varid{expr}}, now we need to use  strategic combinators that navigate in the tree while
applying the rules.  To guarantee that all the possible optimizations
are applied we use Strafunski \ensuremath{\Varid{innermost}} traversal scheme. Thus, our optimization is expressed as:

\begin{hscode}\SaveRestoreHook
\column{B}{@{}>{\hspre}l<{\hspost}@{}}%
\column{E}{@{}>{\hspre}l<{\hspost}@{}}%
\>[B]{}\Varid{opt}\mathbin{::}\Conid{Let}\to \Conid{Maybe}\;\Conid{Let}{}\<[E]%
\\
\>[B]{}\Varid{opt}\;\Varid{t}\mathrel{=}\Varid{applyTP}\;(\Varid{innermost}\;\Varid{step})\;\Varid{t}{}\<[E]%
\\
\>[B]{}\mathbf{where}\;\Varid{step}\mathrel{=}\Varid{failTP}\mathbin{`\Varid{adhocTP}`}\Varid{expr}{}\<[E]%
\ColumnHook
\end{hscode}\resethooks

Function $opt$ defines a Type Preserving (\ensuremath{\Conid{TP}}) transformation since the input and result trees have the same type. 
Here, \ensuremath{\Varid{step}} is the transformation applied by function
\ensuremath{\Varid{applyTP}} to all nodes of the input tree \ensuremath{\Varid{t}} (of type \ensuremath{\Conid{Let}})
using the $innermost$ strategy combinator. 
%The re-write step behaves like the identity function (\ensuremath{\Varid{idTP}}) by default
The re-write step fails silently (\ensuremath{\Varid{failTP}}) by default
with worker function \ensuremath{\Varid{expr}} to perform the type-specific transformation.


Let us consider now the context dependent rule $7$ in our optimization.  This rule requires that the environment, where a name is used, is computed first. This environment has to be computed according to the non-trivial scope rules of the \ensuremath{\Conid{Let}} language.
The semantics of \ensuremath{\Conid{Let}} does not force a
declare-before-use discipline, meaning that a variable can be declared
after its first use. Consequently, a conventional  implementation of the scope rules
naturally leads to an algorithm, that traverses each block twice: once
for accumulating the declarations of names and constructing an
environment and a second time to process the uses of names (using the
computed environment) in order to check for the use of non-declared
identifiers. 

In fact, both the scope rules and context dependent re-writing are not easily expressed within strategic term re-writing. 



%\subsection{Scope Rules via Attribute Grammars}

\paragraph{Attribute Grammars:} The formal specification of  scope rules is in the genesis of the Attribute Grammar formalism~\cite{Knuth90}. AGs are particularly suitable to specify language engineering tasks, where context information needs to be first collected before it can be used. 

Before we show our previous work on combining AGs and strategic term re-writing in a single (\discuss{uniform}?) Haskell embedding, we start by specifying the scope rules of \ensuremath{\Conid{Let}} via an AG. 
Due to space limitations, we adopt a visual AG notation that is often used by AG writers to sketch a first draft of their grammars.  Thus, the scope rules of \ensuremath{\Conid{Let}} are visually expressed in Figure~\ref{fig:AG}. 


\begin{figure*}[h]
%\begin{minipage}[htb!]{\textwidth}
\includegraphics[width=\textwidth,keepaspectratio]{figure/AG_shorterBW_String.png}
%\end{minipage}
\caption{Attribute Grammar Specifying the Scope Rules of \ensuremath{\Conid{Let}}}
\label{fig:AG}
\end{figure*}

The diagrams in the figure are read as follows. For each constructor/production
(labeled by its name) we have the type of the production above and
below those of its children. To the left of each symbol we have the
so-called \emph{inherited attributes}: values that are computed
top-down in the grammar. To the right of each symbol we have the
so-called \emph{synthesized attributes}: values that are computed
bottom-up. The arrows between attributes specify the information flow
to compute an attribute. Thus, the AG expressed in Figure~\ref{fig:AG} is the following.
The inherited attribute \ensuremath{\attrid{dcli}} is used as an accumulator to
collect all \ensuremath{\Conid{Strings}} defined in a \ensuremath{\Conid{Let}}: it starts as an empty list in
the \ensuremath{\Conid{Root}} production, and when a new name is defined (productions
\ensuremath{\Conid{Assign}} and \ensuremath{\Conid{NestedLet}}) it is added to the accumulator. The total
list of defined \ensuremath{\bnfnt{String}} is synthesized in attribute \ensuremath{\attrid{dclo}}, which at
the \ensuremath{\Conid{Root}} node is passed down as the environment (inherited attribute
\ensuremath{\attrid{env}}). Moreover, a nested let inherits (attribute \ensuremath{\attrid{dcli}}) the environment of its outer let.
The type of the three attributes is a list of pairs,
associating the \ensuremath{\bnfnt{String}} to its \ensuremath{\Conid{Let}} expression definition\footnote{We
will use this definition to expand the \ensuremath{\bnfnt{String}} as required by
optimization rule \ensuremath{\mathrm{7}}.}.

\discuss{Root data type?}

To allow functional programmers to write functions very much like attribute grammar programmers write their grammars, we have extended the generic zippers library~\cite{adams2010zippers} with a set of simple AG-like combinators~\cite{zipperAG}, namely the combinator ``\textit{child}'', written as the infix function $.\$$, to access the child of a tree node given its index, and the combinator $parent$  to move the focus to the parent of a tree node.

With these two zipper-based AG combinators, we are able to express in a AG programming style the scope rules of $Let$.  For example, let us consider the synthesized attribute 
\ensuremath{\attrid{dclo}}. In the diagrams of our visual AG
the \ensuremath{\Conid{NestedLet}} and \ensuremath{\Conid{Assign}} productions we see that \ensuremath{\attrid{dclo}} is defined
as the \ensuremath{\attrid{dclo}} of the third child. Moreover, in production \ensuremath{\Conid{EmptyList}}
attribute \ensuremath{\attrid{dclo}} is a copy of \ensuremath{\attrid{dcli}}. This is exactly how such
equations are written in the zipper-based AG, as we can see in the
next function\footnote{The function \ensuremath{\textsf{constructor}} and the constructors used in the case
alternatives is boilerplate code needed by the AG embedding. This code
has to be defined once per tree structure (\textit{i.e.}, AG), and can
be generated by using template \ensuremath{\Conid{Haskell}}~\cite{templatehaskell}}:  



\begin{hscode}\SaveRestoreHook
\column{B}{@{}>{\hspre}l<{\hspost}@{}}%
\column{11}{@{}>{\hspre}l<{\hspost}@{}}%
\column{14}{@{}>{\hspre}l<{\hspost}@{}}%
\column{26}{@{}>{\hspre}l<{\hspost}@{}}%
\column{E}{@{}>{\hspre}l<{\hspost}@{}}%
\>[B]{}\attrid{dclo}\mathbin{::}\textsf{AGTree}\;[\mskip1.5mu (\bnfnt{String},\textbf{Zipper}\;\Conid{Root})\mskip1.5mu]{}\<[E]%
\\
\>[B]{}\attrid{dclo}\;\Varid{t}\mathrel{=}{}\<[11]%
\>[11]{}\mathbf{case}\;(\textsf{constructor}\;\Varid{t})\;\mathbf{of}{}\<[E]%
\\
\>[11]{}\hsindent{3}{}\<[14]%
\>[14]{}\bnfprod{$\mathit{Let_{Let}}$}{}\<[26]%
\>[26]{}\to \attrid{dclo}\;(\Varid{t}\textsf{.\$}\mathrm{1}){}\<[E]%
\\
\>[11]{}\hsindent{3}{}\<[14]%
\>[14]{}\bnfprod{$\mathit{NestedLet_{List}}$}{}\<[26]%
\>[26]{}\to \attrid{dclo}\;(\Varid{t}\textsf{.\$}\mathrm{3}){}\<[E]%
\\
\>[11]{}\hsindent{3}{}\<[14]%
\>[14]{}\bnfprod{$\mathit{Assign_{List}}$}{}\<[26]%
\>[26]{}\to \attrid{dclo}\;(\Varid{t}\textsf{.\$}\mathrm{3}){}\<[E]%
\\
\>[11]{}\hsindent{3}{}\<[14]%
\>[14]{}\bnfprod{$\mathit{EmptyList_{List}}$}{}\<[26]%
\>[26]{}\to \attrid{dcli}\;\Varid{t}{}\<[E]%
\ColumnHook
\end{hscode}\resethooks


Consider now the case of defining the inherited attribute
\ensuremath{\attrid{env}} that we will need to express optimization $(7)$. In most diagrams an occurrence of attribute \ensuremath{\attrid{env}} is defined as
a copy of the parent. There are two exceptions: in productions \ensuremath{\Conid{Root}}
and \ensuremath{\Conid{NestedLet}}. In both cases, \ensuremath{\attrid{env}} gets its value from the
synthesized attribute \ensuremath{\attrid{dclo}} of the same non-terminal/type. Thus, the
\ensuremath{\Conid{Haskell}} \ensuremath{\attrid{env}} function looks as follows:


\begin{hscode}\SaveRestoreHook
\column{B}{@{}>{\hspre}l<{\hspost}@{}}%
\column{10}{@{}>{\hspre}l<{\hspost}@{}}%
\column{11}{@{}>{\hspre}l<{\hspost}@{}}%
\column{18}{@{}>{\hspre}l<{\hspost}@{}}%
\column{E}{@{}>{\hspre}l<{\hspost}@{}}%
\>[B]{}\attrid{env}\mathbin{::}\textsf{AGTree}\;[\mskip1.5mu (\bnfnt{String},\textbf{Zipper}\;\Conid{Root})\mskip1.5mu]{}\<[E]%
\\
\>[B]{}\attrid{env}\;\Varid{t}\mathrel{=}{}\<[10]%
\>[10]{}\mathbf{case}\;(\textsf{constructor}\;\Varid{t})\;\mathbf{of}{}\<[E]%
\\
\>[10]{}\hsindent{1}{}\<[11]%
\>[11]{}\bnfprod{$\mathit{Root_{P}}$}{}\<[18]%
\>[18]{}\to \attrid{dclo}\;\Varid{t}{}\<[E]%
\\
\>[10]{}\hsindent{1}{}\<[11]%
\>[11]{}\bnfprod{$\mathit{Let_{Let}}$}{}\<[18]%
\>[18]{}\to \attrid{dclo}\;\Varid{t}{}\<[E]%
\\
\>[10]{}\hsindent{1}{}\<[11]%
\>[11]{}\anonymous {}\<[18]%
\>[18]{}\to \attrid{env}\;(\textsf{parent}\;\Varid{t}){}\<[E]%
\ColumnHook
\end{hscode}\resethooks

We omit here the definition of attribute \ensuremath{\attrid{dcli}}, where the declared names are being accumulated. The remain specification of the $Let$ scope rules is included in Appendix~\ref{sec:LetAG}.



\paragraph{Combining Strategies and Attribute Grammars:} AG evaluators decorate the underlying trees with attribute values. Thus, an instance of attribute \ensuremath{\attrid{env}} is associated to every \ensuremath{\Conid{Var}} node, defining its environment. Recall that  \ensuremath{\attrid{env}} of $var(id)$ is the missing ingredient to implement rule $(7)$.

Unfortunately, Strafunski works on user defined tree structures, while the embedding of AGs works on zippers. As a consequence, Strafunski combinators are not able to access attribute values defined by the AG. Strategic term re-writing relies on a generic navigation mechanism to walk up and down heterogeneous trees while applying type specific re-write rules.  Such a generic navigation mechanism is offered by zippers.  As we have shown in previous work, generic zippers also offer the necessary abstractions to express strategic term re-writing in a functional programming setting~\cite{flops22}. 



Then, we can define a function with this type, that implements rule 7:

\begin{comment}
\begin{hscode}\SaveRestoreHook
\column{B}{@{}>{\hspre}l<{\hspost}@{}}%
\column{17}{@{}>{\hspre}l<{\hspost}@{}}%
\column{20}{@{}>{\hspre}l<{\hspost}@{}}%
\column{22}{@{}>{\hspre}l<{\hspost}@{}}%
\column{31}{@{}>{\hspre}l<{\hspost}@{}}%
\column{E}{@{}>{\hspre}l<{\hspost}@{}}%
\>[B]{}\Varid{expC}\mathbin{::}\Conid{Exp}\to \textbf{Zipper}\;\Conid{Root}\to \Conid{Maybe}\;\Conid{Exp}{}\<[E]%
\\
\>[B]{}\Varid{expC}\;(\Conid{Var}\;\Varid{x})\;\Varid{z}{}\<[17]%
\>[17]{}\mathrel{=}{}\<[20]%
\>[20]{}\mathbf{case}\;\Varid{lookup}\;\Varid{x}\;(\fbox{\attrid{env}\;\Varid{z}})\;\mathbf{of}{}\<[E]%
\\
\>[20]{}\hsindent{2}{}\<[22]%
\>[22]{}\Conid{Just}\;\Varid{e}{}\<[31]%
\>[31]{}\to \textsf{lexeme\_Assign}\;\Varid{e}{}\<[E]%
\\
\>[20]{}\hsindent{2}{}\<[22]%
\>[22]{}\Conid{Nothing}{}\<[31]%
\>[31]{}\to \Conid{Nothing}{}\<[E]%
\\
\>[B]{}\Varid{expC}\;\anonymous \;\Varid{z}{}\<[17]%
\>[17]{}\mathrel{=}\Conid{Nothing}{}\<[E]%
\ColumnHook
\end{hscode}\resethooks

The variable \ensuremath{\Varid{x}} is searched into the environment returned by the \ensuremath{\attrid{env}} attribute;
in case it is found, the associated expression\footnote{The function \ensuremath{\textsf{lexeme\_Assign}} is another syntactic reference that in this case takes a \ensuremath{\textbf{Zipper}} and, if it is focused on an \ensuremath{\Conid{Assign}}, returns its expression.} is returned, otherwise the optimization is not performed.
\end{comment}

\begin{hscode}\SaveRestoreHook
\column{B}{@{}>{\hspre}l<{\hspost}@{}}%
\column{E}{@{}>{\hspre}l<{\hspost}@{}}%
\>[B]{}\Varid{expC}\mathbin{::}\Conid{Exp}\to \textbf{Zipper}\;\Conid{Root}\to \Conid{Maybe}\;\Conid{Exp}{}\<[E]%
\\
\>[B]{}\Varid{expC}\;(\Conid{Var}\;\Varid{i})\;\Varid{z}\mathrel{=}\Varid{expand}\;(\Varid{i},\fbox{\attrid{lev}\;\Varid{z}})\;(\fbox{\attrid{env}\;\Varid{z}}){}\<[E]%
\\
\>[B]{}\Varid{expC}\;\anonymous \;\Varid{z}\mathrel{=}\Conid{Nothing}{}\<[E]%
\ColumnHook
\end{hscode}\resethooks

\todo{ expand i (lev z) (env z) better no?}

The variable \ensuremath{\Varid{i}} is expanded according to its environment, as defined by rule $(7)$. Because, the $let$ language has nesting we use an attribute named \ensuremath{\attrid{lev}} to distinguish definitions with the same name at different nested levels. Thus, the \ensuremath{\attrid{expand}} function looks up the defined variable $i$ in its level \ensuremath{\attrid{lev}}, \discuss{or outer one}, in its environment \ensuremath{\attrid{env}}. In case it is found, the expanded definition of variable $i$ is returned, otherwise the optimization is not performed. The full definition of functions \ensuremath{\attrid{lev}} and \ensuremath{\attrid{expand}} is included in appendix~\ref{sec:LetAG}.


Now we can combine this rule with the previously defined \ensuremath{\Varid{expr}}, that implements rules 1 to 6, and apply them to all nodes. 
\discuss{We also drop the Strafunski library in favour of Ztrategic, \todo{(link to library or reference here?}, which is built over zippers and thus enables the usage of attributes, as we do in function \ensuremath{\attrid{expC}}.}
%\footnote{The library and complete examples showed in this paper are available at 
%\url{https://bitbucket.org/zenunomacedo/ztrategic/}}

\begin{hscode}\SaveRestoreHook
\column{B}{@{}>{\hspre}l<{\hspost}@{}}%
\column{8}{@{}>{\hspre}l<{\hspost}@{}}%
\column{E}{@{}>{\hspre}l<{\hspost}@{}}%
\>[B]{}\Varid{opt'}{}\<[8]%
\>[8]{}\mathbin{::}\textbf{Zipper}\;\Conid{Root}\to \Conid{Maybe}\;(\textbf{Zipper}\;\Conid{Root}){}\<[E]%
\\
\>[B]{}\Varid{opt'}\;{}\<[8]%
\>[8]{}\Varid{r}\mathrel{=}\Varid{applyTP}\;(\Varid{innermost}\;\Varid{step})\;\Varid{r}{}\<[E]%
\\
\>[8]{}\mathbf{where}\;\Varid{step}\mathrel{=}\Varid{failTP}\mathbin{\fbox{`\Varid{adhocTPZ}`}}\Varid{expC}\mathbin{`\Varid{adhocTP}`}\Varid{expr}{}\<[E]%
\ColumnHook
\end{hscode}\resethooks

Our motivating example shows the abstraction and expressiveness provided by combining strategies and attribute grammars in the same zipper-based setting~\cite{flops22}. However, this embedding of AGs has a severe limitation since when decorating the tree, it re-computes the same attribute instances. The reader may have noticed that every time the worker function \ensuremath{\Varid{expC}} is called, then the call to \ensuremath{\fbox{\attrid{env}\;\Varid{z}}}  does lead to the (re)decoration of the full tree. Thus, the number of  calls to rule $(7)$ results in the same number of full tree (re)decorations. As expected, 
this drastically affects the performance of the AG embedding\cite{memoAG19} and, consequently, of the combined one, as well. 


\subsection{Memoized Strategic Attribute Grammars}

In order to avoid attribute re-computation and, consequently, to  improve the performance of our AG embedding, we have incorporated memoization into the zipper-based AGs~\cite{memoAG19}. In the next
section we will use the same approach: First, we combine memoization with the zipper-based strategic combinators, which are then combined with the memoized zipper-based AGs. After that, we will study the performance of our solution by comparing it with the state-of-the-art, namely the Strafunski~\cite{strafunski} and Kiama~\cite{kiama} systems.



The definition of the instances of \ensuremath{\Conid{Let}} and its attributes as instances of \ensuremath{\Conid{Memo}} and \ensuremath{\Conid{Memoizable}} allow for the usage of the \ensuremath{\textbf{memo}} function, which hides all the memoization work and enables writing of memoized attributes in a similar fashion to the non-memoized examples shown before. Next, we re-defined the \ensuremath{\attrid{dclo}} attribute presented in the previous section, using memoization: 

\begin{hscode}\SaveRestoreHook
\column{B}{@{}>{\hspre}l<{\hspost}@{}}%
\column{7}{@{}>{\hspre}c<{\hspost}@{}}%
\column{7E}{@{}l@{}}%
\column{11}{@{}>{\hspre}l<{\hspost}@{}}%
\column{19}{@{}>{\hspre}l<{\hspost}@{}}%
\column{31}{@{}>{\hspre}l<{\hspost}@{}}%
\column{E}{@{}>{\hspre}l<{\hspost}@{}}%
\>[B]{}\attrid{dclo}{}\<[7]%
\>[7]{}\mathbin{::}{}\<[7E]%
\>[11]{}(\Conid{Memo}\;\Conid{Att\char95 dclo}\;\Conid{MemoTable}\;\bnfnt{Env}){}\<[E]%
\\
\>[7]{}\Rightarrow {}\<[7E]%
\>[11]{}\Conid{AGTree\char95 m}\;\Conid{Let}\;\Conid{MemoTable}\;\bnfnt{Env}{}\<[E]%
\\
\>[B]{}\attrid{dclo}{}\<[7]%
\>[7]{}\mathrel{=}{}\<[7E]%
\>[11]{}\textbf{memo}\;\Conid{Att\char95 dclo}\mathbin{\$}{}\<[E]%
\\
\>[11]{}\lambda \Varid{ag}\to {}\<[19]%
\>[19]{}\mathbf{case}\;(\textsf{constructor}\;\Varid{ag})\;\mathbf{of}{}\<[E]%
\\
\>[19]{}\bnfprod{$\mathit{Let_{Let}}$}{}\<[31]%
\>[31]{}\to \attrid{dclo}\mathbin{.@.}(\Varid{ag}\textsf{.\$}\mathrm{1}){}\<[E]%
\\
\>[19]{}\bnfprod{$\mathit{NestedLet_{List}}$}{}\<[31]%
\>[31]{}\to \attrid{dclo}\mathbin{.@.}(\Varid{ag}\textsf{.\$}\mathrm{3}){}\<[E]%
\\
\>[19]{}\bnfprod{$\mathit{Assign_{List}}$}{}\<[31]%
\>[31]{}\to \attrid{dclo}\mathbin{.@.}(\Varid{ag}\textsf{.\$}\mathrm{3}){}\<[E]%
\\
\>[19]{}\bnfprod{$\mathit{EmptyList_{List}}$}{}\<[31]%
\>[31]{}\to \attrid{dcli}\;\Varid{ag}{}\<[E]%
\ColumnHook
\end{hscode}\resethooks


This attribute will be computed similarly to the non-memoized version when there is no value computed for it previously, and the result will be automatically stored in the memoization table. If the attribute was computed previously, then the previous value is re-used. 

In this memoized version of our embedding, attribute $env$ also looks very much like its previous definition.

\begin{hscode}\SaveRestoreHook
\column{B}{@{}>{\hspre}l<{\hspost}@{}}%
\column{6}{@{}>{\hspre}c<{\hspost}@{}}%
\column{6E}{@{}l@{}}%
\column{10}{@{}>{\hspre}l<{\hspost}@{}}%
\column{18}{@{}>{\hspre}l<{\hspost}@{}}%
\column{25}{@{}>{\hspre}l<{\hspost}@{}}%
\column{E}{@{}>{\hspre}l<{\hspost}@{}}%
\>[B]{}\attrid{env}{}\<[6]%
\>[6]{}\mathbin{::}{}\<[6E]%
\>[10]{}(\Conid{Memo}\;\Conid{Att\char95 env}\;\Conid{MemoTable}\;\bnfnt{Env}){}\<[E]%
\\
\>[6]{}\Rightarrow {}\<[6E]%
\>[10]{}\Conid{AGTree\char95 m}\;\Conid{Let}\;\Conid{MemoTable}\;\bnfnt{Env}{}\<[E]%
\\
\>[B]{}\attrid{env}{}\<[6]%
\>[6]{}\mathrel{=}{}\<[6E]%
\>[10]{}\textbf{memo}\;\Conid{Att\char95 Env}\mathbin{\$}{}\<[E]%
\\
\>[10]{}\lambda \Varid{ag}\to {}\<[18]%
\>[18]{}\mathbf{case}\;(\textsf{constructor}\;\Varid{ag})\;\mathbf{of}{}\<[E]%
\\
\>[18]{}\bnfprod{$\mathit{Root_{P}}$}{}\<[25]%
\>[25]{}\to \attrid{dclo}\;\Varid{ag}{}\<[E]%
\\
\>[18]{}\bnfprod{$\mathit{Let_{Let}}$}{}\<[25]%
\>[25]{}\to \attrid{dclo}\;\Varid{ag}{}\<[E]%
\\
\>[18]{}\anonymous {}\<[25]%
\>[25]{}\to \attrid{env}\mathbin{`\Varid{atParent}`}\Varid{ag}{}\<[E]%
\ColumnHook
\end{hscode}\resethooks




\subsubsection{Memoized Strategies}

The embedded of strategic term re-writing accesses attribute values by calling a zipper-based AG that computes that attribute, as we have shown in Section~\ref{sec:zipping} . When we consider the memoized embedding of AGs, attributes are memoized in tree nodes, and, thus, they can not be accessed by the presented strategic embedding. As a consequence, we need to extend our embedded strategies so that they access the values of attributes stored in the memoization table. 

In fact, we developed a memoization-friendly version of our strategic library. In this new version of the library, the worker functions for each visited node, instead of returning just the result of traversing that node, as we were doing before, we also return the updated zipper of the data structure. 

Let us recall our definitions of of the optimization rules that we expressed as functions \ensuremath{\attrid{exp}} and \ensuremath{\attrid{expC}}. Both functions,  receive an expression as argument and  return the value that the node should be updated to. In the memoized counterparts of these worker functions, we return the dated\todo{typo?} zipper instead of returning the updated value.  This means that, for the first 6 optimization rules, we will update the zipper by transforming the current node. We do this by using the \ensuremath{\attrid{trans}} zipper function.

For rule 7 optimizations, we compute the \ensuremath{\attrid{env}} of our zipper \ensuremath{\Varid{z}}, producing environment \ensuremath{\Varid{e}} and updated zipper \ensuremath{\Varid{z'}}. Similarly, we compute the \ensuremath{\attrid{lev}} of this new zipper \ensuremath{\Varid{z'}}, producing an integer \ensuremath{\Varid{l}} and a new updated zipper \ensuremath{\Varid{z''}}. Then we expand the definition of our variable in the environment \ensuremath{\Varid{e}}, update the zipper \ensuremath{\Varid{z''}} with the expanded definition, and return it. Logically, it is similar to the previous definition of \ensuremath{\attrid{expC}}, but we take care to always handle the updated zipper produced by an attribute.



\begin{hscode}\SaveRestoreHook
\column{B}{@{}>{\hspre}l<{\hspost}@{}}%
\column{8}{@{}>{\hspre}l<{\hspost}@{}}%
\column{11}{@{}>{\hspre}l<{\hspost}@{}}%
\column{19}{@{}>{\hspre}l<{\hspost}@{}}%
\column{22}{@{}>{\hspre}l<{\hspost}@{}}%
\column{25}{@{}>{\hspre}l<{\hspost}@{}}%
\column{27}{@{}>{\hspre}l<{\hspost}@{}}%
\column{28}{@{}>{\hspre}l<{\hspost}@{}}%
\column{32}{@{}>{\hspre}l<{\hspost}@{}}%
\column{38}{@{}>{\hspre}l<{\hspost}@{}}%
\column{40}{@{}>{\hspre}l<{\hspost}@{}}%
\column{41}{@{}>{\hspre}l<{\hspost}@{}}%
\column{E}{@{}>{\hspre}l<{\hspost}@{}}%
\>[B]{}\Varid{exprZ}{}\<[8]%
\>[8]{}\mathbin{::}\Conid{Let}\;\Conid{MemoTable}\to \textbf{Zipper}\;(\Conid{Let}\;\Conid{MemoTable}){}\<[E]%
\\
\>[8]{}\to \Conid{Maybe}\;(\textbf{Zipper}\;(\Conid{Let}\;\Conid{MemoTable})){}\<[E]%
\\
\>[B]{}\Varid{exprZ}\;(\Conid{Add}\;\Varid{e}\;(\Conid{Const}\;\mathrm{0}\;\anonymous )\;\Varid{m})\;\Varid{z}{}\<[32]%
\>[32]{}\mathrel{=}\Conid{Just}\mathbin{\$}\textbf{trans}\;(\Varid{mkT}\mathbin{\$}\Varid{const}\;\Varid{e})\;\Varid{z}{}\<[E]%
\\
\>[B]{}\Varid{exprZ}\;(\Conid{Add}\;(\Conid{Const}\;\mathrm{0}\;\anonymous )\;\Varid{t}\;\anonymous )\;\Varid{z}{}\<[32]%
\>[32]{}\mathrel{=}\Conid{Just}\mathbin{\$}\textbf{trans}\;(\Varid{mkT}\mathbin{\$}\Varid{const}\;\Varid{t})\;\Varid{z}{}\<[E]%
\\
\>[B]{}\Varid{exprZ}\;(\Conid{Add}\;(\Conid{Const}\;\Varid{a}\;\anonymous )\;(\Conid{Const}\;\Varid{b}\;\anonymous )\;\Varid{m})\;\Varid{z}{}\<[E]%
\\
\>[B]{}\hsindent{19}{}\<[19]%
\>[19]{}\mathrel{=}\Conid{Just}\mathbin{\$}\textbf{trans}\;(\Varid{mkT}\mathbin{\$}\Varid{const}\;(\Conid{Const}\;(\Varid{a}\mathbin{+}\Varid{b})\;\Varid{m}))\;\Varid{z}{}\<[E]%
\\
\>[B]{}\Varid{exprZ}\;(\Conid{Sub}\;\Varid{a}\;\Varid{b}\;\Varid{m})\;{}\<[25]%
\>[25]{}\Varid{z}{}\<[E]%
\\
\>[B]{}\hsindent{19}{}\<[19]%
\>[19]{}\mathrel{=}\Conid{Just}\mathbin{\$}\Varid{tran}\;\Varid{s}\;({}\<[38]%
\>[38]{}\Varid{mkT}\mathbin{\$}\Varid{const}\;(\Conid{Add}\;\Varid{a}\;(\Conid{Neg}\;\Varid{b}\;\Varid{emptyMemo}){}\<[E]%
\\
\>[38]{}\Varid{emptyMemo}))\;\Varid{z}{}\<[E]%
\\
\>[B]{}\Varid{exprZ}\;(\Conid{Neg}\;(\Conid{Neg}\;\Varid{f}\;\anonymous )\;\anonymous )\;\Varid{z}{}\<[28]%
\>[28]{}\mathrel{=}\Conid{Just}\mathbin{\$}\textbf{trans}\;(\Varid{mkT}\mathbin{\$}\Varid{const}\;\Varid{f})\;\Varid{z}{}\<[E]%
\\
\>[B]{}\Varid{exprZ}\;(\Conid{Neg}\;(\Conid{Const}\;\Varid{n}\;\Varid{m})\;\anonymous )\;\Varid{z}{}\<[E]%
\\
\>[B]{}\hsindent{19}{}\<[19]%
\>[19]{}\mathrel{=}\Conid{Just}\mathbin{\$}\textbf{trans}\;(\Varid{mkT}\mathbin{\$}\Varid{const}\;(\Conid{Const}\;(\mathbin{-}\Varid{n})\;\Varid{m}))\;\Varid{z}{}\<[E]%
\\
\>[B]{}\Varid{exprZ}\;(\Conid{Var}\;\Varid{i}\;\anonymous )\;\Varid{z}\mathrel{=}{}\<[22]%
\>[22]{}\mathbf{let}\;{}\<[27]%
\>[27]{}(\Varid{e},\Varid{z'})\mathrel{=}\fbox{\attrid{env}\;\Varid{z}{}}\<[E]%
\\
\>[27]{}(\Varid{l},\Varid{z''})\mathrel{=}\attrid{lev}\;\Varid{z'}{}\<[E]%
\\
\>[27]{}\Varid{expr}\mathbin{::}\Conid{Maybe}\;(\Conid{Let}\;\Conid{MemoTable}){}\<[E]%
\\
\>[27]{}\Varid{expr}\mathrel{=}\Varid{fmap}\;{}\<[40]%
\>[40]{}(\Varid{buildMemoTree4}\;\Varid{emptyMemo}){}\<[E]%
\\
\>[40]{}\hsindent{1}{}\<[41]%
\>[41]{}\mathbin{\$}\Varid{\Conid{NM}.expand}\;(\Varid{i},\Varid{l})\;\Varid{e}{}\<[E]%
\\
\>[22]{}\mathbf{in}\;\Varid{fmap}\;(\lambda \Varid{k}\to \textbf{trans}\;(\Varid{mkT}\mathbin{\$}\Varid{const}\;\Varid{k})\;\Varid{z''})\;\Varid{expr}{}\<[E]%
\\
\>[B]{}\Varid{exprZ}\;\anonymous \;{}\<[11]%
\>[11]{}\Varid{z}{}\<[19]%
\>[19]{}\mathrel{=}\Conid{Nothing}{}\<[E]%
\ColumnHook
\end{hscode}\resethooks

Finally, we define a strategy to apply this \ensuremath{\attrid{expZ}} function to all nodes. As the memoized attributes operate on a similar but different definition of \ensuremath{\Varid{Let}}, such that each node contains its memoization table, we transform a \ensuremath{\Varid{Let}} into its memoized counterpart with helper function \ensuremath{\attrid{buildMemoTree}}, and after optimization of the memoized data type, we recover the original data type with the function \ensuremath{\attrid{letToRoot}}.

\begin{hscode}\SaveRestoreHook
\column{B}{@{}>{\hspre}l<{\hspost}@{}}%
\column{9}{@{}>{\hspre}l<{\hspost}@{}}%
\column{E}{@{}>{\hspre}l<{\hspost}@{}}%
\>[B]{}\Varid{opt}\mathbin{::}\Conid{Root}\to \Conid{Root}{}\<[E]%
\\
\>[B]{}\Varid{opt}\;\Varid{t}\mathrel{=}\Varid{letToRoot}\mathbin{\$}\Varid{fromZipper}\mathbin{\$}\Varid{t'}{}\<[E]%
\\
\>[B]{}\mathbf{where}\;{}\<[9]%
\>[9]{}\Varid{z}\mathbin{::}\textbf{Zipper}\;(\Conid{Let}\;\Conid{MemoTable}){}\<[E]%
\\
\>[9]{}\Varid{z}\mathrel{=}\textsf{mkAG}\;(\Varid{buildMemoTree}\;\Varid{emptyMemo}\;\Varid{t}){}\<[E]%
\\
\>[9]{}\Conid{Just}\;(\Varid{t'},\Varid{x})\mathrel{=}\Varid{applyTP}\;(\Varid{innermost}\;\Varid{step})\;\Varid{z}{}\<[E]%
\\
\>[9]{}\Varid{step}\mathrel{=}\Varid{failTP}\mathbin{`\Varid{adhocTPZ}`}\Varid{exprZ}{}\<[E]%
\ColumnHook
\end{hscode}\resethooks

%\discuss{The first benchmarks of this memoized implementation of \ensuremath{\attrid{opt}} pointed towards it being several orders of magnitude slower than the non-memoized version. Further research would reveal that, as the strategies always traverse the whole data structure, the memoization tables themselves were being traversed, thus adding a very large unnecessary workload. We solve this by adding the concept of terminal nodes to strategies.}

Although the use of memoization in our embedding of strategic AGs \todo{adds some intrusive code?} to our definitions, the performance of the resulting evaluators does show the positive impact of memoization. Figure~\ref{fig:let} shows the results of running both evaluators with a large $let$ input (the size of $let$ refers to the level of nesting). 


\begin{figure}[htb!]
\includegraphics[width=0.5\textwidth,keepaspectratio]{figure/Let-MemoNon.png}
\caption{Let Optimization: Non-Memoized versus Memoized Zipper-based Strategic AGs.}
\label{fig:let}
\end{figure}