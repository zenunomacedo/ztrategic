
\begin{figure*}[htb!]
\begin{minipage}[htb!]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{figure/new/smells_loc.png}
%\end{minipage}
%\begin{minipage}[htb!]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{figure/new/smellsM_loc.png}
\end{minipage}
\caption{Haskell Smells elimination: Ztrategic versus Strafunski}
\label{fig:smells}
\end{figure*}

\begin{figure*}[htb!]
\begin{minipage}[htb!]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{figure/new/Repmin-Strategies.png}
%\end{minipage}
%\begin{minipage}[htb!]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{figure/new/RepminM-Strategies.png}
\end{minipage}
\caption{Repmin: Ztrategic versus Strafunski}
\label{fig:repmin}
\end{figure*}

In this section, we compare the performance of the Ztrategic library
with the state-of-the-art Strafunski library. We focus our
analysis in the runtime and memory consumption of different
implementations of a Haskell code smell eliminator, and the $repmin$
program.



\subsection{Haskell Smell Elimination}

In order to compare Ztrategic with the Haskell state-of-the-art Strafunski counterpart 
we run both strategic solution with a large \textit{smelly}
input. We consider 150 Haskell projects developed by first-year
students as presented in~\cite{projects}. In these projects there are
1139 Haskell files totaling 82124 lines of code, of which exactly
1000 files were syntactically correct~\footnote{The student projects
used in this benchmark are available at this work's repository.}. Both
Ztrategic and Strafunski smell eliminators detected and eliminated 850
code smells in those files. Figure~\ref{fig:smells} shows the runtime (left) and memory consumption (right) of running both libraries.

There are three entries in these figures: a normal Ztrategic
implementation, a Ztrategic implementation in which we skip
unnecessary nodes in the traversal, and a similar implementation in
Strafunski. In the X axis, each value represents an aggregation of 25
source code files, that is, the first value is for 25 files, the
second value is for 50 files, and so on until the last value
representing 500 files. Strafunski outperforms our library, which is
to be expected as our library has an additional overhead of creating
and handling a zipper over the traversed data. Still, when skipping
terminals, our library is competitive with Strafunski runtime-wise and
memory consumption-wise, for both small and large inputs.

Figure~\ref{fig:smells} shows that our zipper-based embedding of
strategies presents a similar performance to Strafunski both in terms
of runtime and memory consumption. It should also be noted that our
simple optimization that skips terminal nodes of the tree does improve
our solution, makes it runtime very similar to Strafunski.



\subsection{Repmin as a Strategic Program}

We have also executed the strategic $repmin$ solution with large input
binary leaf trees. We consider trees storing between ten to forty
thousand integers. Because integers are not traversable symbols, our
optimization that skips symbols does not apply to this example. Thus,
we just execute the regular Ztrategic and Strafunski solutions with 
those large trees, as shown in Figure~\ref{fig:repmin}.

As expected, Strafunski outperforms Ztrategic in terms of runtime, 
for example being $2.67$ times faster for the largest input in the 
graphic. However, Ztrategic is more efficient in terms of memory
consumption.

%solidifying our library as a robust and viable alternative to the
%existing state of the art of strategic programming.}



\medskip

The results of these two benchmarks show that our zipper-based
embedding of strategic term re-write is able to perform large scale,
real language transformations. Moreover, our embedding is
comparable to the performance of the state-of-the-art strategic
library for Haskell.


%Our library, however, is more expressive as it
%allows for the usage of AG constructs. } %do compare to the
%\todo{performance of the state-of-the-art strategic library for Haskell.}


