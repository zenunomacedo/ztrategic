
\begin{abstract}


Strategic term re-writing is a powerful programming technique to
define large scale program transformations. It relies on a set of
recursive patterns, so called strategies, to traverse a tree while
applying a set of re-write rules. Strategies can be expressed by a
simple, but powerful generic navigation mechanism: functional
zippers. In this paper we present a zipper-based strategic library,
named Ztrategic. Moreover, we benchmark it against the Haskell
strategic library Strafunski. The first preliminary results show that
the performance of our library does compare to the well established
Strafunski library.


\end{abstract}

