

This paper presented the Ztrategic library, a zipper-based strategic
library for |Haskell|. We used the library to implement two strategic
solutions: a simple strategic program that implements the |repmin|
program, and a useful, real language transformation - a smell
detector for |Haskell|.  Moreover, we compare the performance of our
library against Strafunski. Our first results show that our
zipper-based embedding of strategies is efficient: its performance is
on par with Strafunski.