

Strategic term re-writing is a powerful generic programming technique
to define program transformations~\cite{strategies97}. 
It relies on a set of recursive
patterns, so called strategies, to traverse a tree while applying a
set of re-write rules. Strategic term re-writing is particularly
suited to express large scale transformations, namely, when such
transformation involve large heterogeneous trees consisting of large
sets of types and their constructors. In strategic programming,
solutions only include the constructors where work has to be
defined, and omit the constructors otherwise.

Strategies can be expressed by a simple, but powerful generic
navigation mechanism: functional zippers~\cite{flops22}. In this paper
we present a zipper-based strategic library, named Ztrategic. We also
show two strategic programs written in Ztrategic: the well-known
functional |repmin| program, and a smells detector and eliminator for 
full |Haskell| programs.  Moreover, we present the first detailed
study on the performance of our implementations. We compare the
performance of our implementations against the state-of-the-art
Strafunski~\cite{strafunski} system - the Haskell incarnation of
strategic term re-writing.

Our results show that our zipper-based Ztrategic library is able to
perform large scale, real language transformations. Moreover, our
embedding is comparable to the performance of the Strafunski library.