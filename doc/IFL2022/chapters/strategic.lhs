
In this section we start by briefly present generic functional
zippers. Next, we describe in detail the definition of the Ztrategic
library expressed with zippers. After that, we present two strategic
programs: the well-known $repmin$ program and a smell detector and
eliminator for |Haskell|.


\subsection{The Zipper Data Structure}

Zippers were introduced by Huet~\cite{thezipper} to represent a tree
together with a subtree that is the \emph{focus} of attention. During a
computation the focus may move left, up, down or right within the
tree.  Generic manipulation of a zipper is provided through a set of
predefined functions that allow access to all of the nodes of a tree
for inspection or modification.

A generic implementation of this concept is available as the
\emph{generic zipper} Haskell library~\cite{adams2010zippers}, which
works for both homogeneous and heterogeneous data types.  In order to
illustrate the use of zippers, let us begin by considering 
the following |Haskell| data type to
represent a leaf tree of integers. We also define |t|, as an example
of a tree storing the numbers from 1 to 4.

\begin{code}
data Tree  =  Fork Tree Tree 
           |  Leaf Int
            deriving (Data, Typeable)

t = Fork  (Fork (Leaf 1) (Leaf 2))
          (Fork (Leaf 3) (Leaf 4))
\end{code}


We build a zipper |t_1| from the previous |Tree| expression |t| through
the use of the |toZipper :: Data a => a -> Zipper a| function. This
function produces a zipper out of any data type, requiring only that
the data types have an instance of the |Data| and |Typeable| type
classes\footnote{These can be easily obtained via the Haskell data
type |deriving| mechanism.}.

\begin{code}
t_1 = toZipper t
\end{code} 

We can navigate |t_1| using pre-defined functions from the zipper
library. The function |down'| moves the focus down to the
leftmost child of a node, while |down| moves the focus to the
rightmost child instead.  Similarly, functions |right|, |left| and
|up|, move towards the corresponding directions.
They all have type |Zipper a -> Maybe (Zipper a)|, meaning that such functions
take a zipper and return a new zipper in case the navigation does not fail.
They have types:

\begin{code}
down,down',right,left,up ::  Zipper a -> Maybe (Zipper a)
\end{code}

Consider our expression |t|, we can \textit{unsafely}\footnote{By using the function |fromJust :: Maybe a -> a| we assume a |Just| value is returned.}  move the focus of the zipper towards the |Leaf 2| sub-expression and obtain its value as follows:

\begin{code}
leafTwo :: Maybe Tree
leafTwo = (getHole  .  fromJust . right    
                    .  fromJust . down' .  fromJust . down') t_1
\end{code}


Finally, the zipper function |getHole :: Typeable b => Zipper a ->
Maybe b| extracts the actual node the zipper is focusing on.
Notice that the type of the hole (|b|) can be different than the type
of the root of the Zipper (|a|), since the tree can be heterogeneous.
Using these functions, we can freely navigate through this newly created
zipper.


The zipper library also contains functions for the transformation of the
data structure being traversed. The function |trans :: GenericT ->
Zipper a -> Zipper a| applies a generic transformation to the node the
zipper is currently pointing to; while  |transM ::
GenericM m -> Zipper a -> m (Zipper a)| applies a generic monadic
transformation.


In fact, generic zippers is a simple, but very expressive technique to
navigate in heterogeneous data structures. Since strategic term
rewriting relies on generic traversals of trees, and on the
transformation of specific nodes, zippers provide the necessary
machinery to embed strategic programming in Haskell, as we will show
in the next section.




\subsection{Zipper-based Strategic Combinators}


This section describes Ztrategic, our embedding of strategic
programming using generic zippers. The embedding directly follows the
work of Laemmel and Visser~\cite{strafunski} on the Strafunski
library~\cite{StrafunskiAppLetter}.

%Before we present the powerful and
%reusable strategic functions providing control on tree traversals,
%such as top-down, bottom-up, innermost, etc.,

Before we present the strategic functions, let us show some simple
basic combinators that work at the zipper level, and are the building
blocks of our embedding.

We start by defining a function that elevates a transformation to the
zipper level.  In other words, we define how a function that is
supposed to operate directly on one data type is converted into a
zipper transformation.


%Ultimately, the user does not need to use this
%function directly, because it is only used as a building block for
%other functions.
\begin{comment}
\begin{code}
zTryApplyM  :: (Typeable a, Typeable b)
            => (a -> Maybe b) -> TP c
zTryApplyM f = transM (join. cast. f. fromJust. cast)
\end{code} 
\end{comment}

\begin{code}
zTryApplyM  :: (Typeable a, Typeable b) => (a -> Maybe b) -> TP c
\end{code}

The definition of |zTryApplyM|, which we omit for brevity, relies on
transformations on zippers, thus reusing the generic zipper library
|transM| function.

To build a valid transformation for the |transM| function, we
use the |cast :: a -> Maybe b| function, that tries to cast a given
data from type |a| to type |b|. In this case, we use it to cast the
data the zipper is focused on into a type our original transformation
|f| can be applied to.  Then, function |f| is applied and its result
is cast back to its original type. Should any of these casts, or the
function |f| itself, fail, the failure propagates and the resulting
zipper transformation will also fail. The use of the monadic version
of the zipper generic transformation guarantees the handling of such
partiality.  It should be noticed that failure can occur in two
situations: the type cast fails when the types do not match. Moreover,
the function |f| fails when the function itself dictates that no
change is to be applied.  Signaling failure in the application of
transformations is important for strategies where a transformation is
applied once, only.


%This function has a single argument: the original transformation
%function as argument, named |f|. This function |f| generically
%receives an argument |a| and can transform it into a |b| or fail (as
%defined by the |Maybe| type result). 

|zTryApplyM| returns a |TP c|, in which |TP| is a type for specifying
Type-Preserving transformations on zippers, and |c| is the type of the
zipper. For example, if we are applying transformations on a zipper
built upon the |Let| data type, then those transformations are of type
|TP Let|.

> type TP a = Zipper a -> Maybe (Zipper a)

Very much like Strafunski, we introduce the type |TU m d| for
Type-Unifying operations, which aim to gather data of type |d| into
the data structure |m|.

> type TU m d = (forall a. Zipper a -> m d)

For example, to collect in a list all the defined names in a |Let|
expression, the corresponding type-unifying strategy would be of type
|TU [] String|. We will present such a transformation and implement it
later in this section.

Next, we define a combinator to compose two transformations, building
a more complex zipper transformation that tries to apply each of the
initial transformations in sequence, 
skipping transformations that fail.

%. Because each of the
%transformations may fail, we have to skip transformations that fail.

\begin{comment}
\begin{code}
adhocTP  ::  (Typeable a, Typeable b)
         =>  TP e -> (a -> Maybe b) -> TP e
adhocTP f g z = maybeKeep f (zTryApplyM g) z
\end{code} 
\end{comment}

\begin{code}
adhocTP  ::  Typeable a =>  TP e -> (a -> Maybe a) -> TP e
adhocTP f g z = maybeKeep f (zTryApplyM g) z
\end{code}

The |adhocTP| function receives transformations |f| and |g| as
parameters, as well as zipper |z|. It converts |g|, which is a simple
(\textit{i.e.} non-zipper) Haskell function, into a zipper.  Then, the
zipper transformations |f| and |g| are passed as arguments to
|maybeKeep|, which is an auxiliary function that applies the
transformations in sequence, discarding either failing transformation
(\textit{i.e.} that produces |Nothing|).  We omit the definition of
|maybeKeep| for brevity.

%
%, and uses the auxiliary function |maybeKeep| to try
%to apply the two transformations to the zipper |z|, 
%ignoring either if it fails.
%
%
%apply each of the transformations to the zipper |z|, ignoring the
%transformations that fail.  

%Note that |f| is of type |TP e|, meaning
%it is a transformation on zippers, while |g| is a normal Haskell
%function. Because |g| is a non-zipper based function, |adhocTP| allows
%the definition of transformations where we use simple (\textit{i.e.}
%non-zipper) Haskell functions. 


Next, we use |adhocTP|, written as an infix operator, which combines
the zipper function |failTP| with our basic transformation |expr|
function:

\begin{code}
step = failTP `adhocTP` expr
\end{code} 

Thus, we do not need to express type-specific transformations as
functions that work on zippers. It is the use of |zTryApplyM| in
|adhocTP| that transforms a  Haskell function (|expr| in this
case) to a zipper one, hidden from these definitions.

The transformation |failTP| is a pre-defined transformation that
always fails (returning |Nothing|) and |idTP| is the identity
transformation that always succeeds (returning the input
unchanged). They provide the basis for construction of complex
transformations through composition.

% \discuss{We omit here their simple definitions.}
\begin{code}
failTP  = const mzero
idTP    = return . id
\end{code} 


The functions we have presented already allow the definition of
arbitrarily complex transformations for zippers. Such transformations,
however, are always applied on the node the zipper is focusing on. Let
us consider a combinator that navigates in the zipper.

\begin{code}
allTPright :: TP a -> TP a
allTPright f z =  case right z of 
                  Nothing  -> return z
                  Just r   -> fmap (fromJust . left) (f r)
\end{code} 

This function is a combinator that, given a type-preserving
transformation |f| for zipper |z|, will attempt to apply |f| to the
node that is located to the right of the node the zipper is pointing
to. To do this, the zipper function |right| is used to try to navigate
to the right; if it fails, we return the original zipper. If it
succeeds, we apply transformation |f| and then we navigate |left|
again. There is a similar combinator |allTPdown| that navigates
downwards and then upwards.

%There are also two similar combinators, that fail when there is no
%|right| node to travel to - these allow for more freedom in defining
%partial strategies.

With all these tools at our disposal, we can define generic traversal
schemes by combining them. Next, we define the traversal scheme used
in the function |opt| we defined at the start of the section. This
traversal scheme navigates through the whole data structure, in a
top-down approach.

\begin{code}
full_tdTP :: TP a -> TP a
full_tdTP f =  allTPdown   (full_tdTP f) `seqTP`
               allTPright  (full_tdTP f) `seqTP` f 
\end{code} 

We skip the explanation of the |seqTP| operator as it is relatively
similar to the |adhocTP| operator we described before, albeit simpler. We interpret this function as a sequence operator. This function receives as
input a type-preserving transformation |f|, and (reading the code
from right to left) it applies it to the focused node itself, then to the
nodes below the currently focused node, then to the nodes to the right
of the focused node. To apply this transformation to the nodes below
the current node, for example, we use the |allTPdown| combinator we
mentioned above, and we recursively apply |full_tdTP f| to the node
below. The same logic applies in regards to navigating to the right.

We can define several traversal schemes similar to this one by
changing the combinators used, or their sequence. For example, by
inverting the order in which the combinators are sequenced, we define
a bottom-up traversal. By using different combinators, we can define
choice, allowing for partial traversals in the data structure.

%\discuss{We previously defined a rewrite strategy where we use |full_tdTP| to
%define a full, top-down traversal, which is not ideal. Because we
%intend to optimize |Exp| nodes, changing one node might make it
%possible to optimize the node above, which would have already been
%processed in a top-down traversal. Instead, we define a different
%traversal scheme, for repeated application of a transformation until a
%fixed point is reached:}

We previously defined the |full_tdTP| rewrite strategy to
define a full, top-down traversal, which sometimes is not ideal. For example
 in optimization scenarios, changing one node might make it
possible to optimize the node above, which would have already been
processed in a top-down traversal. Instead, we define a different
traversal scheme, for repeated application of a transformation until a
fixed point is reached:

\begin{code}
innermost  :: TP a -> TP a
innermost s  =  repeatTP (once_buTP s)
\end{code} 

We omit the definitions of |once_buTP| and |repeatTP| as they are
similar to the presented definitions. The combinator |repeatTP|
applies a given transformation repeatedly until a fixed point is
reached, that is, until the data structure stops being changed by the
transformation. The transformation being applied repeatedly is defined
with the |once_buTP| combinator, which applies |s| once, anywhere on
the data structure. When the application |once_buTP| fails, |repeatTP|
understands a fixed point is reached. Because the |once_buTP|
bottom-up combinator is used, the traversal scheme is |innermost|,
since it prioritizes the innermost nodes. The pre-defined |outermost|
strategy uses the |once_tdTP| combinator instead.

The full API of the Ztrategic Library is included in appendix~\ref{sec:api}.

%\subsection{simple strategy to find the minimum TU TP}

\subsection{The Repmin Strategic Program} 


The $RepMin$ problem is a well-known problem widely used to show the
power of circular, lazy evaluation as shown by Bird\cite{Bird84}. The
goal of this program is to transform a binary leaf tree of integers
into a new tree with the exact same shape but where all leaves have
been replaced by the minimum leaf value of the original tree. The
$RepMin$ problem can be easily implemented by two strategic functions:
First, a type unifying strategy traverses the tree and computes its
minimum value. Then, a type preserving strategy traverses again the
tree and constructs the new tree, using the previously computed
minimum.


We will begin by writing a strategy to compute the minimum value of
any given |Tree|.  Let us now write a function that, for a given
|Tree| node, will return its value inside a list if the node in
question contains a value, returning an empty list for any other case:

\begin{code}
nodeVal :: Tree -> [Int]
nodeVal (Leaf x)  = [x]
nodeVal _         = []
\end{code}

Let us now use this function in all nodes of a |Tree|, to gather its 
results. These will be processed by a fold, reducing all elements into
a single result. We will fold using the |min| function to compute the 
minimum value.

%and gather its
%results into a list. From then, the pre-defined |Haskell| function
%|minimum| can provide us with the value we are interested in.

%\discuss{should we already use a king of fold?}
%

\begin{code}
minimumValue :: Tree -> Int
minimumValue t =  foldr1TU (full_tdTU step) (toZipper t) min
       where   step = failTU `adhocTU` nodeVal
\end{code}


During such traversals, each visited node can be transformed or
reduced according to a specific function. Here, we use |step| as such
function, which is a composition of our |nodeVal| function for
gathering integer values, with the default failing function
|failTU|. The combinator |adhocTU| simply join these two functions
into one.

We use the |full_tdTU| combinator to define a full, top-down
Type-Unifying traversal of the data structure, applying |step| into
each node and reducing the data structure into a value; the |foldr1TU| 
combinator joins all gathered values with a reduction function, in 
this case, |min|.
%we apply this
%traversal with the |applyTU| combinator.

Next, we define a function that given a new value, and a |Tree| node,
updates said node to contain the value.

\begin{code}
replace :: Int -> Tree -> Maybe Tree
replace i (Leaf r)  = Just (Leaf i)
replace i _         = Nothing 
\end{code}

Function |replace| updates a node if it is a |Leaf| node, and does
|Nothing| otherwise. Next, we define |replaceTree|, a strategy to
|replace| a value given as parameter in all nodes of a |Tree|:

\begin{code}
replaceTree :: Int -> Tree -> Tree
replaceTree v t =  fromZipper $ fromJust $ 
                   applyTP (full_tdTP step) (toZipper t)
  where step = failTP `adhocTP` (replace v) 
\end{code}

This strategy differs from the previous one in that this is a
Type-Preserving strategy, thus only transforming the traversed data
structure. 
We use the |applyTP| combinator to apply a strategy, preserving
the data structure.
Traversing the data structure in a top-down way, for each
node, we apply |step|, which will use |replace v| to replace any value
with |v|, failing otherwise. Finally, we combine the two previous
definitions such that said |v| value is computed by |minimumValue|:

\begin{code}
repmin t =  let  v = minimumValue t 
            in   replaceTree v t 
\end{code}

We have thus defined |repmin| as a combination of two strategies.


\subsection{Smell Elimination in Haskell Code}

Strategic term re-writing is particularly suited to express large
scale tree transformations, namely, when such transformation involve
large heterogeneous trees consisting of a large set of types and their
constructors. In strategic programming, only the constructors where
work has to be performed are, in fact, included in the solution.

Large heterogeneous trees consisting of a large set of data types and
respective constructors are used to define real programming
languages. Haskell is no exception with its 30 data types and 116 type
constructors. Next, we show a transformation for the full |Haskell|
language/tree, where we show the power of strategic programming.

\medskip

Source code smells make code harder to comprehend. A smell is not an
error, but it indicates a bad programming practice. Smells do occur in
any language and Haskell is no exception. For example, inexperienced
Haskell programmers often write |[h] ++ t| to append a single element
to a list, instead of writing |h:t| using the predefined single
element append function.

Let us write a function that corrects this code smell. Do note that
this function manipulates the |HsExp| data type, which is part of the
|Haskell| Abstract Syntax Tree. Specifically, replaces with |h:t| any
occurrence of the |[h] ++ t| pattern:

\begin{code}
joinList :: HsExp -> Maybe HsExp
joinList  (HsInfixApp (HsList [h]) 
          (HsQVarOp (UnQual (HsSymbol "++"))) (HsList t)) 
      = Just $ HsInfixApp h (HsQConOp (Special HsCons)) (HsList t)
joinList _ = Nothing
\end{code}

Next, we present a strategic function that eliminates several Haskell
smells as reported in~\cite{Cowie}. To guarantee that all the possible
optimizations are applied we use an |innermost| traversal scheme,
which will repeatedly apply transformations in the data structure
until they no longer succeed.

\begin{code}
smellElim h = applyTP (innermost step) h
  where step =  failTP  `adhocTP` joinList         
                        `adhocTP` nullList 
                        `adhocTP` redundantBoolean 
                        `adhocTP` reduntantIf 
\end{code}

\noindent where |joinList|, as shown before, detects patterns where
list concatenations are inefficiently defined, |nullList| detects
patterns where a list is checked for emptiness, |redundantBoolean|
detects redundant boolean checks, and |reduntantIf| detects redundant
|if| clauses.

Note that, even though there are 116 constructors across 30 data types
contained in the Haskell AST, we only write code for the nodes that we
intend to transform (in |joinList|'s case, the |HsExp| node).

\subsection{Terminal Symbols}

Because not every symbol must be traversed at all times when applying
strategies, we introduce the concept of terminal nodes in our
library. As such, any node that is defined as a terminal in our
library will be skipped entirely, therefore reducing the total
traversal effort of the strategy, even more if the skipped node is
very complex and/or contains a big subtree.

To allow for this behaviour, any data type to be traversed using this
library must define an instance of |StrategicData|. We will define
that, for the |HsModule| data type, a node is a terminal if it is of
type |String|. In fact, any types known to be skippable with no impact
result-wise could be added. Note that skipping |String| nodes does not
stop the strategy from traversing and modifying any nodes that contain
|Strings| themselves. We would expect to change |Strings| when
traversing any constructor that contains |Strings| inside, but not by
directly visiting the |String| node (which in itself is a list of
|Char| that would also be traversed individually).

\begin{code}
instance StrategicData HsModule where 
  isTerminal z = isJust (getHole z :: Maybe String)
\end{code} 

The |isTerminal| function should return true whenever |z| points
towards a terminal node. In this case, we return true if |z| points
towards a |String|, but we could define more complex logic in this
function, such as skipping only negative integers.