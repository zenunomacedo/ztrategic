\nopagebreak[4]
\begin{figure*}[h!]
\section{Ztrategic API}
\label{sec:api}
\begin{minipage}[t]{.55\textwidth}
\textbf{Strategy types}
> type TP a = Zipper a -> Maybe (Zipper a)
> type TU m d = (forall a. Zipper a -> m d) 
\textbf{Strategy Application}
> applyTP :: TP a -> Zipper a -> Maybe (Zipper a)
> applyTP_unclean :: TP a -> Zipper a -> Maybe (Zipper a)
> applyTU :: TU m d -> Zipper a -> (m d, Zipper a) 
> applyTU_unclean :: TU m d -> Zipper a -> (m d, Zipper a) 
\textbf{Primitive strategies}
> idTP      :: TP a
> constTU   :: d -> TU m d
> failTP    :: TP a 
> failTU    :: TU m d
> tryTP     :: TP a -> TP a
> repeatTP  :: TP a -> TP a
\textbf{Strategy Construction}
> monoTP    :: (a -> Maybe b) -> TP e
> monoTU    :: (a -> m d) -> TU m d
> monoTPZ   :: (a -> Zipper e -> Maybe b) -> TP e
> monoTUZ   :: (a -> Zipper e -> m d) -> TU m d
> adhocTP   :: TP e -> (a -> Maybe b) -> TP e
> adhocTU   :: TU m d -> (a -> m d) -> TU m d
> adhocTPZ  :: TP e -> (a -> Zipper e -> Maybe b) -> TP e
> adhocTUZ  :: TU m d -> (a -> Zipper c -> m d) -> TU m d
\textbf{Composition / Choice}
> seqTP     :: TP a -> TP a -> TP a
> choiceTP  :: TP a -> TP a -> TP a
\end{minipage}
\begin{minipage}[t]{.35\textwidth}
> seqTU     :: TU m d -> TU m d -> TU m d
> choiceTU  :: TU m d -> TU m d -> TU m d
\textbf{Traversal Combinators}
> allTPright  :: TP a -> TP a
> oneTPright  :: TP a -> TP a
> allTUright  :: TU m d -> TU m d
> allTPdown   :: TP a -> TP a
> oneTPdown   :: TP a -> TP a
> allTUdown   :: TU m d -> TU m d
\textbf{Traversal Strategies}
> full_tdTP  :: TP a -> TP a
> full_buTP  :: TP a -> TP a
> once_tdTP  :: TP a -> TP a
> once_buTP  :: TP a -> TP a  
> stop_tdTP  :: TP a -> TP a
> stop_buTP  :: TP a -> TP a
> innermost  :: TP a -> TP a
> outermost  :: TP a -> TP a
> full_tdTU  :: TU m d -> TU m d     
> full_buTU  :: TU m d -> TU m d     
> once_tdTU  :: TU m d -> TU m d   
> once_buTU  :: TU m d -> TU m d  
> stop_tdTU  :: TU m d -> TU m d  
> stop_buTU  :: TU m d -> TU m d
> foldr1TU   :: TU m d -> Zipper e -> (d -> d -> d) -> d
> foldl1TU   :: TU m d -> Zipper e -> (d -> d -> d) -> d
> foldrTU    :: TU m d -> Zipper e -> (d -> c -> c) -> c -> c
> foldlTU    :: TU m d -> Zipper e -> (c -> d -> c) -> c -> c

\end{minipage}
\caption{Full Ztrategic API}
\label{code:api}
\end{figure*}
