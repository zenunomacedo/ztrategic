
\section{Let Attribute Grammar}
\label{sec:LetAG}

%dcli
The \ensuremath{\attrid{dcli}} attribute computes the accumulated declarations, up to the node the zipper is pointing towards. 

\begin{hscode}\SaveRestoreHook
\column{B}{@{}>{\hspre}l<{\hspost}@{}}%
\column{3}{@{}>{\hspre}l<{\hspost}@{}}%
\column{9}{@{}>{\hspre}l<{\hspost}@{}}%
\column{14}{@{}>{\hspre}l<{\hspost}@{}}%
\column{25}{@{}>{\hspre}l<{\hspost}@{}}%
\column{27}{@{}>{\hspre}l<{\hspost}@{}}%
\column{33}{@{}>{\hspre}l<{\hspost}@{}}%
\column{E}{@{}>{\hspre}l<{\hspost}@{}}%
\>[B]{}\attrid{dcli}\mathbin{::}\textbf{Zipper}\;\Conid{Root}\to \bnfnt{Env}{}\<[E]%
\\
\>[B]{}\attrid{dcli}\;\Varid{ag}\mathrel{=}\mathbf{case}\;(\textsf{constructor}\;\Varid{ag})\;\mathbf{of}{}\<[E]%
\\
\>[B]{}\hsindent{3}{}\<[3]%
\>[3]{}\bnfprod{$\mathit{Let_{Let}}$}{}\<[9]%
\>[9]{}\to \mathbf{case}\;(\textsf{constructor}\;(\textsf{parent}\;\Varid{ag}))\;\mathbf{of}{}\<[E]%
\\
\>[9]{}\hsindent{5}{}\<[14]%
\>[14]{}\bnfprod{$\mathit{Root_{P}}$}{}\<[25]%
\>[25]{}\to [\mskip1.5mu \mskip1.5mu]{}\<[E]%
\\
\>[9]{}\hsindent{5}{}\<[14]%
\>[14]{}\bnfprod{$\mathit{NestedLet_{List}}$}\to \attrid{env}\;{}\<[33]%
\>[33]{}(\textsf{parent}\;\Varid{ag}){}\<[E]%
\\
\>[B]{}\hsindent{3}{}\<[3]%
\>[3]{}\anonymous {}\<[9]%
\>[9]{}\to \mathbf{case}\;(\textsf{constructor}\;(\textsf{parent}\;\Varid{ag}))\;\mathbf{of}{}\<[E]%
\\
\>[9]{}\hsindent{5}{}\<[14]%
\>[14]{}\bnfprod{$\mathit{Let_{Let}}$}{}\<[25]%
\>[25]{}\to \attrid{dcli}\;(\textsf{parent}\;\Varid{ag}){}\<[E]%
\\
\>[9]{}\hsindent{5}{}\<[14]%
\>[14]{}\bnfprod{$\mathit{Assign_{List}}$}{}\<[25]%
\>[25]{}\to (\Varid{lexeme\char95 Name}\;(\textsf{parent}\;\Varid{ag}),\attrid{lev}\;(\textsf{parent}\;\Varid{ag}),{}\<[E]%
\\
\>[25]{}\hsindent{2}{}\<[27]%
\>[27]{}\Varid{lexeme\char95 Exp}\mathbin{\$}\textsf{parent}\;\Varid{ag})\mathbin{:}(\attrid{dcli}\;(\textsf{parent}\;\Varid{ag})){}\<[E]%
\\
\>[9]{}\hsindent{5}{}\<[14]%
\>[14]{}\bnfprod{$\mathit{NestedLet_{List}}$}\to (\Varid{lexeme\char95 Name}\;(\textsf{parent}\;\Varid{ag}),\attrid{lev}\;(\textsf{parent}\;\Varid{ag}),{}\<[E]%
\\
\>[14]{}\hsindent{13}{}\<[27]%
\>[27]{}\Conid{Nothing})\mathbin{:}(\attrid{dcli}\;(\textsf{parent}\;\Varid{ag})){}\<[E]%
\ColumnHook
\end{hscode}\resethooks

%lev

In the $let$ language, we need to distinguish declaration of the same name ate diferent nesting level. In order to do it we use an attribute which defines the level of each nested let block. Thus,  the \ensuremath{\attrid{lev}} attribute computes the current nesting level the zipper is pointing towards. 

\begin{hscode}\SaveRestoreHook
\column{B}{@{}>{\hspre}l<{\hspost}@{}}%
\column{3}{@{}>{\hspre}l<{\hspost}@{}}%
\column{9}{@{}>{\hspre}l<{\hspost}@{}}%
\column{14}{@{}>{\hspre}l<{\hspost}@{}}%
\column{25}{@{}>{\hspre}l<{\hspost}@{}}%
\column{E}{@{}>{\hspre}l<{\hspost}@{}}%
\>[B]{}\attrid{lev}\mathbin{::}\textbf{Zipper}\;\Conid{Root}\to \Conid{Int}{}\<[E]%
\\
\>[B]{}\attrid{lev}\;\Varid{ag}\mathrel{=}\mathbf{case}\;(\textsf{constructor}\;\Varid{ag})\;\mathbf{of}{}\<[E]%
\\
\>[B]{}\hsindent{3}{}\<[3]%
\>[3]{}\bnfprod{$\mathit{Let_{Let}}$}{}\<[9]%
\>[9]{}\to \mathbf{case}\;(\textsf{constructor}\mathbin{\$}\textsf{parent}\;\Varid{ag})\;\mathbf{of}{}\<[E]%
\\
\>[9]{}\hsindent{5}{}\<[14]%
\>[14]{}\bnfprod{$\mathit{NestedLet_{List}}$}\to (\attrid{lev}\mathbin{\$}\textsf{parent}\;\Varid{ag})\mathbin{+}\mathrm{1}{}\<[E]%
\\
\>[9]{}\hsindent{5}{}\<[14]%
\>[14]{}\bnfprod{$\mathit{Root_{P}}$}{}\<[25]%
\>[25]{}\to \mathrm{0}{}\<[E]%
\\
\>[B]{}\hsindent{3}{}\<[3]%
\>[3]{}\anonymous {}\<[9]%
\>[9]{}\to \attrid{lev}\mathbin{\$}\textsf{parent}\;\Varid{ag}{}\<[E]%
\ColumnHook
\end{hscode}\resethooks

%expand
The \ensuremath{\attrid{expand}} auxiliary function is used to search an \ensuremath{\Varid{Env}} environment for a given variable name, at a given nesting level. For this, variables with different names or invalid nesting levels are filtered out, and the resulting values are sorted on the nesting level. If there is any resulting variable in the environment, we return its definition, else we return \ensuremath{\Varid{Nothing}}.

\todo{Too hard to understand this code... }

\begin{hscode}\SaveRestoreHook
\column{B}{@{}>{\hspre}l<{\hspost}@{}}%
\column{4}{@{}>{\hspre}l<{\hspost}@{}}%
\column{5}{@{}>{\hspre}l<{\hspost}@{}}%
\column{7}{@{}>{\hspre}l<{\hspost}@{}}%
\column{E}{@{}>{\hspre}l<{\hspost}@{}}%
\>[B]{}\Varid{expand}\mathbin{::}(\bnfnt{Name},\Conid{Int})\to \bnfnt{Env}\to \Conid{Maybe}\;\Conid{Exp}{}\<[E]%
\\
\>[B]{}\Varid{expand}\;(\Varid{i},\Varid{l})\;\Varid{e}\mathrel{=}\mathbf{case}\;\Varid{results}\;\mathbf{of}{}\<[E]%
\\
\>[B]{}\hsindent{7}{}\<[7]%
\>[7]{}((\Varid{nE},\Varid{lE},\Varid{eE})\mathbin{:\char95 })\to \Varid{eE}{}\<[E]%
\\
\>[B]{}\hsindent{7}{}\<[7]%
\>[7]{}\anonymous \to \Conid{Nothing}{}\<[E]%
\\
\>[B]{}\hsindent{4}{}\<[4]%
\>[4]{}\mathbf{where}\;\Varid{results}\mathrel{=}\Varid{sortBy}{}\<[E]%
\\
\>[4]{}\hsindent{1}{}\<[5]%
\>[5]{}(\lambda (\Varid{nE1},\Varid{lE1},\anonymous )\;(\Varid{nE2},\Varid{lE2},\anonymous )\to \Varid{compare}\;\Varid{lE2}\;\Varid{lE1})\mathbin{\$}{}\<[E]%
\\
\>[4]{}\hsindent{1}{}\<[5]%
\>[5]{}\Varid{filter}\;(\lambda (\Varid{nE},\Varid{lE},\anonymous )\to \Varid{nE}\equiv \Varid{i}\mathrel{\wedge}\Varid{lE}\leq \Varid{l})\;\Varid{e}{}\<[E]%
\ColumnHook
\end{hscode}\resethooks

\section{Smell elimination}
\label{sec:smells}

We define refactoring \ensuremath{\Varid{smells}} through an \ensuremath{\Varid{innermost}} strategy that
applies a myriad of transformations, as many times as possible. The
use of \ensuremath{\Varid{innermost}} is necessary because performing one
refactoring can enable the application of another refactoring.

\begin{hscode}\SaveRestoreHook
\column{B}{@{}>{\hspre}l<{\hspost}@{}}%
\column{9}{@{}>{\hspre}l<{\hspost}@{}}%
\column{30}{@{}>{\hspre}l<{\hspost}@{}}%
\column{E}{@{}>{\hspre}l<{\hspost}@{}}%
\>[B]{}\Varid{smells}{}\<[9]%
\>[9]{}\mathbin{::}\textbf{Zipper}\;\Conid{HsModule}\to \Conid{Maybe}\;(\textbf{Zipper}\;\Conid{HsModule}){}\<[E]%
\\
\>[B]{}\Varid{smells}\;{}\<[9]%
\>[9]{}\Varid{h}\mathrel{=}\Varid{applyTP}\;(\Varid{innermost}\;\Varid{step})\;\Varid{h}{}\<[E]%
\\
\>[9]{}\mathbf{where}\;\Varid{step}\mathrel{=}\Varid{failTP}{}\<[30]%
\>[30]{}\mathbin{`\Varid{adhocTP}`}\Varid{joinList}{}\<[E]%
\\
\>[30]{}\mathbin{`\Varid{adhocTP}`}\Varid{nullList}{}\<[E]%
\\
\>[30]{}\mathbin{`\Varid{adhocTP}`}\Varid{redundantBoolean}{}\<[E]%
\\
\>[30]{}\mathbin{`\Varid{adhocTP}`}\Varid{reduntantIf}{}\<[E]%
\ColumnHook
\end{hscode}\resethooks

These functions are simple in the sense that they try to match a
pattern and replace them with another. They are similar in nature to
\ensuremath{\Varid{expr}} we defined in Section~\ref{sec2}, but the data types themselves
are more complex. We define a transformation to refactor the pattern
\ensuremath{[\mskip1.5mu \Varid{x}\mskip1.5mu]\plus \Varid{xs}} into \ensuremath{\Varid{x}\mathbin{:}\Varid{xs}}.

\begin{hscode}\SaveRestoreHook
\column{B}{@{}>{\hspre}l<{\hspost}@{}}%
\column{13}{@{}>{\hspre}l<{\hspost}@{}}%
\column{21}{@{}>{\hspre}l<{\hspost}@{}}%
\column{26}{@{}>{\hspre}l<{\hspost}@{}}%
\column{34}{@{}>{\hspre}l<{\hspost}@{}}%
\column{45}{@{}>{\hspre}l<{\hspost}@{}}%
\column{E}{@{}>{\hspre}l<{\hspost}@{}}%
\>[B]{}\Varid{joinList}\mathbin{::}\Conid{HsExp}\to \Conid{Maybe}\;\Conid{HsExp}{}\<[E]%
\\
\>[B]{}\Varid{joinList}\;{}\<[13]%
\>[13]{}(\Conid{HsInfixApp}\;{}\<[26]%
\>[26]{}(\Conid{HsList}\;[\mskip1.5mu \Varid{h}\mskip1.5mu])\;{}\<[E]%
\\
\>[26]{}(\Conid{HsQVarOp}\;(\Conid{UnQual}\;{}\<[45]%
\>[45]{}(\Conid{HsSymbol}\;\text{\ttfamily \char34 ++\char34})))\;{}\<[E]%
\\
\>[26]{}(\Conid{HsList}\;\Varid{t})){}\<[E]%
\\
\>[13]{}\mathrel{=}\Conid{Just}\;{}\<[21]%
\>[21]{}(\Conid{HsInfixApp}\;{}\<[34]%
\>[34]{}\Varid{h}\;{}\<[E]%
\\
\>[34]{}(\Conid{HsQConOp}\;(\Conid{Special}\;\Conid{HsCons}))\;{}\<[E]%
\\
\>[34]{}(\Conid{HsList}\;\Varid{t})){}\<[E]%
\\
\>[B]{}\Varid{joinList}\;\anonymous {}\<[13]%
\>[13]{}\mathrel{=}\Conid{Nothing}{}\<[E]%
\ColumnHook
\end{hscode}\resethooks

Next, we find patterns of bad null checking of lists, which are refactored to \ensuremath{\Varid{null}\;\Varid{x}}. The patterns are \ensuremath{\Varid{length}\;\Varid{x}\equiv \mathrm{0}}, \ensuremath{\mathrm{0}\equiv \Varid{length}\;\Varid{x}}, \ensuremath{\Varid{x}\equiv [\mskip1.5mu \mskip1.5mu]} and \ensuremath{[\mskip1.5mu \mskip1.5mu]\equiv \Varid{x}}, each represented by one line of the function.
\begin{hscode}\SaveRestoreHook
\column{B}{@{}>{\hspre}l<{\hspost}@{}}%
\column{3}{@{}>{\hspre}l<{\hspost}@{}}%
\column{11}{@{}>{\hspre}l<{\hspost}@{}}%
\column{13}{@{}>{\hspre}l<{\hspost}@{}}%
\column{23}{@{}>{\hspre}l<{\hspost}@{}}%
\column{24}{@{}>{\hspre}l<{\hspost}@{}}%
\column{31}{@{}>{\hspre}l<{\hspost}@{}}%
\column{37}{@{}>{\hspre}l<{\hspost}@{}}%
\column{38}{@{}>{\hspre}l<{\hspost}@{}}%
\column{E}{@{}>{\hspre}l<{\hspost}@{}}%
\>[B]{}\Varid{nullList}\mathbin{::}\Conid{HsExp}\to \Conid{Maybe}\;\Conid{HsExp}{}\<[E]%
\\
\>[B]{}\Varid{nullList}\;(\Conid{HsInfixApp}\;{}\<[23]%
\>[23]{}(\Conid{HsApp}\;{}\<[31]%
\>[31]{}(\Conid{HsVar}\;(\Conid{UnQual}{}\<[E]%
\\
\>[31]{}\hsindent{7}{}\<[38]%
\>[38]{}(\Conid{HsIdent}\;\text{\ttfamily \char34 length\char34})))\;{}\<[E]%
\\
\>[31]{}\Varid{a})\;{}\<[E]%
\\
\>[23]{}(\Conid{HsQVarOp}\;(\Conid{UnQual}\;(\Conid{HsSymbol}\;\text{\ttfamily \char34 ==\char34})))\;{}\<[E]%
\\
\>[23]{}(\Conid{HsLit}\;(\Conid{HsInt}\;\mathrm{0}))){}\<[E]%
\\
\>[B]{}\hsindent{3}{}\<[3]%
\>[3]{}\mathrel{=}\Conid{Just}\mathbin{\$}{}\<[13]%
\>[13]{}\Conid{HsApp}\;(\Conid{HsVar}\;(\Conid{UnQual}\;(\Conid{HsIdent}\;\text{\ttfamily \char34 null\char34})))\;\Varid{a}{}\<[E]%
\\
\>[B]{}\Varid{nullList}\;(\Conid{HsInfixApp}\;{}\<[23]%
\>[23]{}(\Conid{HsLit}\;(\Conid{HsInt}\;\mathrm{0}))\;{}\<[E]%
\\
\>[23]{}(\Conid{HsQVarOp}\;(\Conid{UnQual}\;(\Conid{HsSymbol}\;\text{\ttfamily \char34 ==\char34})))\;{}\<[E]%
\\
\>[23]{}(\Conid{HsApp}\;(\Conid{HsVar}\;(\Conid{UnQual}{}\<[E]%
\\
\>[23]{}\hsindent{14}{}\<[37]%
\>[37]{}(\Conid{HsIdent}\;\text{\ttfamily \char34 length\char34})))\;\Varid{a})){}\<[E]%
\\
\>[B]{}\hsindent{3}{}\<[3]%
\>[3]{}\mathrel{=}\Conid{Just}\mathbin{\$}\Conid{HsApp}\;(\Conid{HsVar}\;(\Conid{UnQual}\;(\Conid{HsIdent}\;\text{\ttfamily \char34 null\char34})))\;\Varid{a}{}\<[E]%
\\
\>[B]{}\Varid{nullList}\;{}\<[11]%
\>[11]{}(\Conid{HsInfixApp}\;{}\<[24]%
\>[24]{}\Varid{a}\;{}\<[E]%
\\
\>[24]{}(\Conid{HsQVarOp}\;(\Conid{UnQual}\;(\Conid{HsSymbol}\;\text{\ttfamily \char34 ==\char34})))\;{}\<[E]%
\\
\>[24]{}(\Conid{HsList}\;[\mskip1.5mu \mskip1.5mu])){}\<[E]%
\\
\>[B]{}\hsindent{3}{}\<[3]%
\>[3]{}\mathrel{=}\Conid{Just}\mathbin{\$}{}\<[13]%
\>[13]{}\Conid{HsApp}\;(\Conid{HsVar}\;(\Conid{UnQual}\;(\Conid{HsIdent}\;\text{\ttfamily \char34 null\char34})))\;\Varid{a}{}\<[E]%
\\
\>[B]{}\Varid{nullList}\;{}\<[11]%
\>[11]{}(\Conid{HsInfixApp}\;{}\<[24]%
\>[24]{}(\Conid{HsList}\;[\mskip1.5mu \mskip1.5mu])\;{}\<[E]%
\\
\>[24]{}(\Conid{HsQVarOp}\;(\Conid{UnQual}\;(\Conid{HsSymbol}\;\text{\ttfamily \char34 ==\char34})))\;{}\<[E]%
\\
\>[24]{}\Varid{a}){}\<[E]%
\\
\>[B]{}\hsindent{3}{}\<[3]%
\>[3]{}\mathrel{=}\Conid{Just}\mathbin{\$}{}\<[13]%
\>[13]{}\Conid{HsApp}\;(\Conid{HsVar}\;(\Conid{UnQual}\;(\Conid{HsIdent}\;\text{\ttfamily \char34 null\char34})))\;\Varid{a}{}\<[E]%
\\
\>[B]{}\Varid{nullList}\;\anonymous \mathrel{=}\Conid{Nothing}{}\<[E]%
\ColumnHook
\end{hscode}\resethooks

We remove redundant boolean checks, such as \ensuremath{\Varid{x}\equiv \Conid{True}} and \ensuremath{\Conid{True}\equiv \Varid{x}}, by refactoring them to \ensuremath{\Varid{x}}. Similarly, \ensuremath{\Varid{x}\equiv \Conid{False}} and \ensuremath{\Conid{False}\equiv \Varid{x}} are refactored to \ensuremath{\neg \;\Varid{x}}. 

\begin{hscode}\SaveRestoreHook
\column{B}{@{}>{\hspre}l<{\hspost}@{}}%
\column{3}{@{}>{\hspre}l<{\hspost}@{}}%
\column{19}{@{}>{\hspre}l<{\hspost}@{}}%
\column{E}{@{}>{\hspre}l<{\hspost}@{}}%
\>[B]{}\Varid{redundantBoolean}\mathbin{::}\Conid{HsExp}\to \Conid{Maybe}\;\Conid{HsExp}{}\<[E]%
\\
\>[B]{}\Varid{redundantBoolean}\;{}\<[19]%
\>[19]{}(\Conid{HsInfixApp}{}\<[E]%
\\
\>[19]{}(\Conid{HsCon}\;(\Conid{UnQual}\;(\Conid{HsIdent}\;\text{\ttfamily \char34 True\char34}))){}\<[E]%
\\
\>[19]{}(\Conid{HsQVarOp}\;(\Conid{UnQual}\;(\Conid{HsSymbol}\;\text{\ttfamily \char34 ==\char34}))){}\<[E]%
\\
\>[19]{}\Varid{a}){}\<[E]%
\\
\>[B]{}\hsindent{3}{}\<[3]%
\>[3]{}\mathrel{=}\Conid{Just}\;\Varid{a}{}\<[E]%
\\
\>[B]{}\Varid{redundantBoolean}\;{}\<[19]%
\>[19]{}(\Conid{HsInfixApp}{}\<[E]%
\\
\>[19]{}\Varid{a}{}\<[E]%
\\
\>[19]{}(\Conid{HsQVarOp}\;(\Conid{UnQual}\;(\Conid{HsSymbol}\;\text{\ttfamily \char34 ==\char34}))){}\<[E]%
\\
\>[19]{}(\Conid{HsCon}\;(\Conid{UnQual}\;(\Conid{HsIdent}\;\text{\ttfamily \char34 True\char34})))){}\<[E]%
\\
\>[B]{}\hsindent{3}{}\<[3]%
\>[3]{}\mathrel{=}\Conid{Just}\;\Varid{a}{}\<[E]%
\\
\>[B]{}\Varid{redundantBoolean}\;{}\<[19]%
\>[19]{}(\Conid{HsInfixApp}{}\<[E]%
\\
\>[19]{}(\Conid{HsCon}\;(\Conid{UnQual}\;(\Conid{HsIdent}\;\text{\ttfamily \char34 False\char34}))){}\<[E]%
\\
\>[19]{}(\Conid{HsQVarOp}\;(\Conid{UnQual}\;(\Conid{HsSymbol}\;\text{\ttfamily \char34 ==\char34}))){}\<[E]%
\\
\>[19]{}\Varid{a}){}\<[E]%
\\
\>[B]{}\hsindent{3}{}\<[3]%
\>[3]{}\mathrel{=}\Conid{Just}\mathbin{\$}(\Conid{HsApp}\;(\Conid{HsVar}\;(\Conid{UnQual}\;(\Conid{HsIdent}\;\text{\ttfamily \char34 not\char34})))\;\Varid{a}){}\<[E]%
\\
\>[B]{}\Varid{redundantBoolean}\;{}\<[19]%
\>[19]{}(\Conid{HsInfixApp}{}\<[E]%
\\
\>[19]{}\Varid{a}{}\<[E]%
\\
\>[19]{}(\Conid{HsQVarOp}\;(\Conid{UnQual}\;(\Conid{HsSymbol}\;\text{\ttfamily \char34 ==\char34}))){}\<[E]%
\\
\>[19]{}(\Conid{HsCon}\;(\Conid{UnQual}\;(\Conid{HsIdent}\;\text{\ttfamily \char34 False\char34})))){}\<[E]%
\\
\>[B]{}\hsindent{3}{}\<[3]%
\>[3]{}\mathrel{=}\Conid{Just}\mathbin{\$}(\Conid{HsApp}\;(\Conid{HsVar}\;(\Conid{UnQual}\;(\Conid{HsIdent}\;\text{\ttfamily \char34 not\char34})))\;\Varid{a}){}\<[E]%
\\
\>[B]{}\Varid{redundantBoolean}\;\anonymous \mathrel{=}\Conid{Nothing}{}\<[E]%
\ColumnHook
\end{hscode}\resethooks

Finally, we remove redundant usages of \ensuremath{\mathbf{if}} clauses by refactoring \ensuremath{\mathbf{if}\;\Varid{x}\;\mathbf{then}\;\Conid{True}\;\mathbf{else}\;\Conid{False}} into \ensuremath{\Varid{x}} and, conversely, \ensuremath{\mathbf{if}\;\Varid{x}\;\mathbf{then}\;\Conid{False}\;\mathbf{else}\;\Conid{True}} into \ensuremath{\neg \;\Varid{x}}. 

\begin{hscode}\SaveRestoreHook
\column{B}{@{}>{\hspre}l<{\hspost}@{}}%
\column{3}{@{}>{\hspre}l<{\hspost}@{}}%
\column{20}{@{}>{\hspre}l<{\hspost}@{}}%
\column{E}{@{}>{\hspre}l<{\hspost}@{}}%
\>[B]{}\Varid{reduntantIf}\mathbin{::}\Conid{HsExp}\to \Conid{Maybe}\;\Conid{HsExp}{}\<[E]%
\\
\>[B]{}\Varid{reduntantIf}\;(\Conid{HsIf}\;{}\<[20]%
\>[20]{}\Varid{a}\;{}\<[E]%
\\
\>[20]{}(\Conid{HsCon}\;(\Conid{UnQual}\;(\Conid{HsIdent}\;\text{\ttfamily \char34 True\char34})))\;{}\<[E]%
\\
\>[20]{}(\Conid{HsCon}\;(\Conid{UnQual}\;(\Conid{HsIdent}\;\text{\ttfamily \char34 False\char34})))){}\<[E]%
\\
\>[B]{}\hsindent{3}{}\<[3]%
\>[3]{}\mathrel{=}\Conid{Just}\;\Varid{a}{}\<[E]%
\\
\>[B]{}\Varid{reduntantIf}\;(\Conid{HsIf}\;{}\<[20]%
\>[20]{}\Varid{a}\;{}\<[E]%
\\
\>[20]{}(\Conid{HsCon}\;(\Conid{UnQual}\;(\Conid{HsIdent}\;\text{\ttfamily \char34 False\char34})))\;{}\<[E]%
\\
\>[20]{}(\Conid{HsCon}\;(\Conid{UnQual}\;(\Conid{HsIdent}\;\text{\ttfamily \char34 True\char34})))){}\<[E]%
\\
\>[B]{}\hsindent{3}{}\<[3]%
\>[3]{}\mathrel{=}\Conid{Just}\mathbin{\$}\Conid{HsApp}\;(\Conid{HsVar}\;(\Conid{UnQual}\;(\Conid{HsIdent}\;\text{\ttfamily \char34 not\char34})))\;\Varid{a}{}\<[E]%
\\
\>[B]{}\Varid{reduntantIf}\;\anonymous \mathrel{=}\Conid{Nothing}{}\<[E]%
\ColumnHook
\end{hscode}\resethooks

We can easily extend \ensuremath{\Varid{smells}} by implementing more refactor
transformations as simple \ensuremath{\Conid{Haskell}} functions and appending them in
the definition of \ensuremath{\Varid{step}}.
