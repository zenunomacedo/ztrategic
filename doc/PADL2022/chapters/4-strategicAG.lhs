
Zipper-based strategic term rewriting provides a powerful mechanism to
express tree transformations. There are, however, transformations that
rely on context information that needs to be collected %first, before
the transformation can be applied. Our optimization rule $7$ of
Figure~\ref{rules} is such an example.  In this section we will
briefly explain the Zipper-based embedding of attribute grammars,
through the |Let| example.  Then, we are going to introduce how to
combine strategies and AGs, ending with an implementation of rule 7.


\subsection{Zipper-based Attribute Grammars}
\label{sec:zipperAg}

The attribute grammar formalism is particularly suitable to specify
language-based algorithms, where context information needs to be first
collected before it can be used. Language-based algorithms such as
name analysis~\cite{scp2016}, pretty printing~\cite{Swierstra1999Combinator},
type inference~\cite{Middelkoop2010TypeInference}, etc.  are elegantly
specified using attribute grammars.

Our running example is no exception and the name analysis task of
|Let| is a non-trivial one.  While being a concise example, it holds
central characteristics of software languages, such as (nested)
block-based structures and mandatory but unique declarations of
names. In addition, the semantics of |Let| does not force a
declare-before-use discipline, meaning that a variable can be declared
after its first use. Consequently, a conventional  implementation of name analysis
naturally leads to a processor that traverses each block twice: once
for processing the declarations of names and constructing an
environment and a second time to process the uses of names (using the
computed environment) in order to check for the use of non-declared
identifiers. The uniqueness of identifiers is efficiently checked in
the first traversal: for each newly encountered name it is checked
whether that it has already been declared at the same lexical level
(block). As a consequence, semantic errors resulting from duplicate
definitions are computed during the first traversal and errors
resulting from missing declarations, in the second one. In fact,
expressing this straightforward algorithm is a complex task in most
programming paradigms, since they require a complex scheduling of tree
traversals\footnote{Note that only after building the environment of an
outer block, nested ones can be traversed: they
inherited that environment. Thus, traversals are intermingled.}, and
intrusive code may be needed to pass information computed in
one traversal to a specific node and used in a following
one\footnote{This is the case when we wish to produce a list of errors
that follows the sequential structure of the input program~\cite{Saraiva99}.}.

In the attribute grammar paradigm, the programmer does not need to be
concerned with scheduling of traversals, nor the use of intrusive code
to glue traversals together. As a consequence, AG writers do not need
to transform/adapt the clear and elegant algorithms in order to avoid
those issues.


Instead of presenting the formal AG definition of |Let|, we will adopt
a visual notation that is often used by AG writers to sketch a first
draft of their grammars.  The scope rules of |Let| are visually
expressed in Figure~\ref{fig:AG}. 
\begin{figure}[t]
%\begin{minipage}[htb!]{\textwidth}
\includegraphics[width=\textwidth,keepaspectratio]{figure/AG_shorterBW.png}
%\end{minipage}
\caption{Attribute Grammar Specifying the Scope Rules of |Let|}
\label{fig:AG}
\end{figure}
%if False

Attribute grammars are context-free~\cite{knuth1968semantics}. That is
to say that the root symbol does not have inherited attributes. Since
this not the case of symbol |Let| (due to its nested occurrence in the
grammar/data types), we will add a root symbol to our grammar as
follows:

\begin{code}
data Root = Root Let
\end{code}

The diagrams in the figure are read as follows. For each production
(labeled by its name) we have the type of the production above and
below those of its children. To the left of each symbol we have the
so-called \emph{inherited attributes}: values that are computed
top-down in the grammar. To the right of each symbol we have the
so-called \emph{synthesized attributes}: values that are computed
bottom-up. The arrows between attributes specify the information flow
to compute an attribute. For example, in the production |Assign|, to
compute the inherited attribute |dcli| in the third child (|List|), we
take the value of the first child (a |Name|) and insert it (with
|(:)|) to the beginning of the attribute |dcli| of the parent. That is
to say that we are accumulating identifiers in |dcli| while descending
in the list of statements of let expressions.

%endif
% Such a visual notation can be directly expressed as an AG. It can also
% be directly written as a zipper-based AG via our embedding. The rules
% expressed in this AG follow the straightforward algorithm we described
% earlier.
The presented AG is the following:  The
inherited attribute |dcli| is used as an accumulator to collect all
|Names| defined in a |Let|: it starts as an empty list in the |Root|
production, and when a new name is defined (productions |Assign| and
|NestedLet|) it is added to the accumulator. The total list of defined
|Name| is synthesized in attribute |dclo|, which at the |Root| node is
passed down as the environment (inherited attribute |env|). The type
of the three attributes is a list of pairs, associating the |Name| to
its |Let| expression definition\footnote{We will use this definition
to expand the |Name| as required by optimization rule |7|.}.

%if False
Very much like strategic term rewriting, AGs also rely on a generic
tree walk mechanism, usually called tree-walk
evaluators~\cite{Alblas91b}, to walk up and down the tree to evaluate
attributes. In fact, generic zippers~\cite{thezipper} also offer the
necessary abstractions to express the embedding of AGs in a functional
programming setting~\cite{scp2016,memoAG19}. Next, we briefly describe
this embedding, and after that we present the embedded AG that express
the scope rules of |Let|. It also computes (attribute) |env|, that is
needed by the optimization rule $7$.
%endif

To allow programmers to write zipper-based functions very much
like AG writers do, the generic
zippers library~\cite{adams2010zippers} is extended with:
the combinator ``\textit{child}'', written as the infix function
|(.$) :: Zipper a -> Int -> Zipper a| to access the child of a tree node given its index (starting from
1) and
%the combinator 
|parent :: Zipper a -> Zipper a| to move the focus to the parent of a
tree node. 
%and the combinators |(.$<)| (left) and |(.$>)| (right) navigate to the
%i$^{th}$ sibling on the left/right of the current node.

%\begin{code}
%(.$<) , (.$>) :: Zipper a -> Int -> Zipper a
%\end{code}


%if False
Having presented these zipper-based AG combinators, we can now express
the scope rules of |Let| expressions as an attribute grammar. To make
our definitions more AG friendly we define the following
(type) synonym:

\begin{code}
type AGTree a  = Zipper Root -> a
\end{code}
%mkAG = toZipper



We will show the definitions of two of the attributes used in the implementation of the scope rules: |dclo| and |env|.
%endif



\begin{figure}[t]
\begin{minipage}{.5\textwidth}
\begin{code}
dclo :: AGTree [(Name, Zipper Root)]
dclo t =  case (constructor t) of
             CLet        -> dclo (t.$1)
             CNestedLet  -> dclo (t.$3)
             CAssign     -> dclo (t.$3)
             CEmptyList  -> dcli t
\end{code}
\end{minipage}
\begin{minipage}{.5\textwidth}
\begin{code}
env :: AGTree [(Name, Zipper Root)]
env t =  case (constructor t) of
          CRoot  -> dclo t
          CLet   -> dclo t
          _      -> env (parent t)
\end{code}
\end{minipage}
\caption{Definitions of |dcli| and |env|}\label{fig:clienv}
\end{figure}



We start by defining the equations of the synthesized attribute
|dclo|. For each definition of an occurrence of |dclo| we define an
equation in our zipper-based function. For example, in the diagrams of
the |NestedLet| and |Assign| productions we see that |dclo| is defined
as the |dclo| of the third child. Moreover, in production |EmptyList|
attribute |dclo| is a copy of |dcli|. This is exactly how such
equations are written in the zipper-based AG, as we can see in
Figure~\ref{fig:clienv} We may use the default rule of a case
statement to express similar AG equations. Consider the case of
defining the inherited attribute |env|. In most diagrams an occurrence
of attribute |env| is defined as a copy of the parent. There are two
exceptions: in productions |Root| and |NestedLet|. In both cases,
|env| gets its value from the synthesized attribute |dclo| of the same
non-terminal/type\footnote{We provide a full AG specification in an extended technical report~\cite{techrep}.}. 



%if False
Let us define now the accumulator attribute |dcli|.  The zipper
function when visiting nodes of type |Let|, has to consider two
alternatives: the parent node can be a |Root| or a |NestedLet| (the two
occurrences of |Let| as a child in the diagrams). This happens because
the rules to define its value differ: in the |Root| node it corresponds
to an empty list (our outermost |Let| is context-free), while in a
nested block, the accumulator |dcli| starts as the |env| of the outer
block. Thus, the zipper-based function |dcli| is expressed as follows:


\begin{code}
dcli :: AGTree [(Name, Zipper Root)]
dcli  t = case (constructor t) of
      CRoot  ->  []
      CLet   ->  case (constructor (parent t)) of
                 CRoot      ->  []
                 CNestedLet ->  env  (parent t)
       _     ->  case (constructor (parent t)) of
                   CAssign     ->  (lexeme (parent t), parent t) : (dcli (parent t)) 
                   CNestedLet  ->  (lexeme (parent t), parent t) : (dcli (parent t)) 
                   CLet        ->  dcli (parent t)    
\end{code}


In this AG function we use the function |lexeme|, which implements the
so-called \textit{syntactic references} in attribute
equations~\cite{Saraiva99}. In this case, |lexeme| returns the |Name|
argument of constructors |Assign| and |NestedLet|. This function is
other boilerplate code also included in appendix~\ref{sec:app}.
%endif


In order to specify the complete name analysis task of |Let|
expression we need to report which names violate the scope rules of
the language. We can incrementally extend our
 AG, and define a new attribute |errors| to
report such violations. 
%In Appendix~\ref{sec:app} we include its definition.
%\todo{In the rebuttal, we say that we will move the actual definition to here. Will this be possible with our space constraints?}
In the next section 
|errors| is expressed as a concise strategic function.



\subsection{Strategic Attribute Grammars}
\label{sec:strategicAG}

%\todo{"In section 3.2 we will clarify the meaning of combining attribution with re-writing. As an example we include the above version of errors that uses the attributable attribute (the rewritten tree) and thus getting the context available before the rewrite." Can we fit this?!}

By having embedding both strategic term rewriting and attribute
grammars in the same zipper-based setting,
and given that both are embedded as first-class citizens,
we can easily combine these
two powerful language engineering techniques. As a result, attribute
computations that do useful work on few productions/nodes can be
efficiently expressed via our |Ztrategic| library, while rewriting
rules that rely on context information can access attribute values.
% Next, we extend our |Let| AG where we will rely on each of the
% techniques to efficiently specify such new language features.

\noindent
\textbf{Accessing Attribute Values from Strategies:} 
As we mentioned in Section~\ref{sec3}, rule 7 of Figure~\ref{rules} cannot
be implemented using a trivial strategy, since it depends on the
context.  The rule states that a variable occurrence can be changed by
its definition. Thus, we need to compute an environment of
definitions, which is what we have done with the attribute |env|,
previously.  If we have access to such attribute in the
definition of a strategy, we would be able to implement this rule.

Given that both attribute grammars and strategies use the zipper to walk through the tree, such combinations can be easily performed if
the strategy exposes the zipper, in order to be used to apply the given attribute.
This is done in our library by the |adhocTPZ| combinator:

\vspace{-.1cm}
> adhocTPZ  :: TP e -> (a -> Zipper e -> Maybe b) -> TP e
\vspace{-.4cm}

Notice that instead of taking a function of type |(a -> Maybe b)|, as does the combinator |adhocTP| introduced in Section~\ref{sec2},
it receives a function of type |(a -> Zipper e -> Maybe b)|, with the zipper as a parameter.
Then, we can define a function with this type, that implements rule 7:

\vspace{-.2cm}
\begin{code}
expC :: Exp -> Zipper Root -> Maybe Exp
expC (Var x) z  =  case lookup x (env z) of 
                     Just e   -> lexeme_Assign e
                     Nothing  -> Nothing 
expC _ z        = Nothing 
\end{code}
\vspace{-.3cm}

The variable |x| is searched in the environment returned by the |env| attribute;
in case it is found, the associated expression\footnote{The function |lexeme_Assign| is another syntactic reference that in this case takes a |Zipper| and, if it is focused on an |Assign|, returns its expression.} is returned, otherwise the optimization is not performed. As a final step, we  combine this rule with the previously defined |expr| (rules 1 to 6) and apply them to all nodes.

\vspace{-.1cm}
\begin{code}
opt''  :: Zipper Root -> Maybe (Zipper Root)
opt''  r = applyTP (innermost (failTP `adhocTPZ` expC `adhocTP` expr)) r  
\end{code} 
\vspace{-.5cm}


\noindent
\textbf{Synthesizing Attributes via Strategies:}
We have shown how attributes and strategies can be combined by using the former while defining the latter.
Now we show how to combine them the other way around; i.e. to express attribute computations as strategies.
As an example, let us consider the |errors| attribute, that returns
the list of names that violate the scope rules:
%if False
. Moreover, to
 provide better error reporting, we wish to produce the list of errors
following the structure of input program.
We want to evaluate
the following input to the list |["b","b","z","c"]|:

\vspace{-.1cm}
\begin{minipage}[htb!]{.50\textwidth}
\begin{code}
letWithErrors =  let  a  = b + 3     
                      c  = 2
		      w  =  let  c = a - b in   c + z
                      c  = c + 3 - c
                 in   (a + 7) + c + w             
\end{code}
\end{minipage}
\vspace{-.2cm}

%endif
Recall that duplicated definitions that are efficiently detected when
a new |Name| (defined in nodes |Assign| and |NestedLet|) is
accumulated in |dcli|. Thus. the newly defined |Name| \textit{must not
be in} the environment |dcli| accumulated till that
definition/node. Invalid uses are detected when a |Name| is used in an
arithmetic expression (|Exp|). In this case, the |Name| \textit{must
be in}\footnote{Functions |mNBIn| and |mBIn| are trivial lookup
functions. They are presented in~\cite{techrep}.} the total
environment |env|.
This is expressed by the following zipper functions:

\vspace{-.1cm}
\noindent
\begin{minipage}[htb!]{.70\textwidth}
\begin{code}
decls :: List -> Zipper Root -> [Name]
decls (Assign       s _ _) z  = mNBIn (lexeme z, z)  (dcli z)
decls (NestedLet    s _ _) z  = mNBIn (lexeme z, z)  (dcli z) 
decls _ _                     = []
\end{code}
\end{minipage}
\begin{minipage}[htb!]{.35\textwidth}
\begin{code}
uses :: Exp -> Zipper Root -> [Name]
uses (Var i) z  = mBIn (lexeme z) (env z)
uses _       z  = []
\end{code}
\end{minipage}
\vspace{-.3cm}


Now, we define a type-unifying strategy that produces the
list of errors. 

\vspace{-.1cm}
\begin{code}
errors :: Zipper Root -> [Name]
errors  t = applyTU (full_tdTU (failTU `adhocTUZ` uses `adhocTUZ` decls)) t
\end{code}
\vspace{-.4cm}

Although the applied function combines |decls| and |uses| in this order,
the resulting list does not report duplications first, and invalid
uses after. The strategic function |adhocTUZ| does combine the two
functions 
and the default failing function 
into one, which is applied while traversing (in a top-down
traversal) the tree. In fact, it produces the errors in the proper
order.
If we define |errors| as an attribute, most
of the attribute equations are just propagating attribute values
upwards without doing useful work!
This is particularly relevant when we
consider the |Let| sub-language as part of a real programming language
(such as |Haskell| with its 116 constructors across 30 data
types).

\vspace{-.4cm}

%The difference in complexity between the strategic definition
%of |errors| and its AG counterpart is much higher.


%if False
Attribute grammar systems provide the so-called attribute propagation
patterns to avoid polluting the specification with
\textit{copy-rules}. In most systems, a special notation and pre-fixed
behavior is associated with a set of off-the-shelf patterns that can
be reused across AGs~\cite{eli,uuag}. For example, in the UUAG
system~\cite{uuag}, the propagation patterns are the default rules
for any attribute. Thus, only the specific/interesting equations
have to be specified. However, being
a special notation, hard-wired to the AG system, makes the extension
or change of the existing rules almost impossible: the full system has
to be updated. Our embedding of strategic term rewriting provides a
powerful setting to express attribute propagation patterns: no special
additional notation/mechanism is needed.

%endif



%if False

\begin{comment}
\subsection{Higher-Order, Circular Attribute Grammars}


As we have presnted in~\cite{}, in order to evaluate a |let|
expression we do need to compute a symbols table, and then evaluate it. Higher-order attribute grammars allows this.

Moreover, to evaluate such environment we need to express a fix-point
computation.  Circular attribnute grammars ...
\end{comment}




\todo{review the whole section}
\todo{add some concluding words}

%endif
