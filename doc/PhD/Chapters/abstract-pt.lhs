%!TEX root = ../template.tex
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% abstract-pt.tex
%% NOVA thesis document file
%%
%% Abstract in Portuguese
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%include lhs2TeX.fmt
%include polycode.fmt

\typeout{NT FILE abstract-pt.tex}%

A travessia e transformação de estruturas de dados são preocupações constantes no desenvolvimento de software. É trivial escrever código para estas tarefas sem preocupações de legibilidade e reutilização, mas alguns problemas exigem algoritmos complexos e recursivos 
%e com múltiplas travessias 
que são difíceis de compreender 
%e manter 
após estarem expressos. Entre soluções para estes problemas encontram-se as Gramáticas de Atributos --- um formalismo para expressar algoritmos dependentes do contexto --- e a Programação Estratégica --- uma aplicação de esquemas recursivos para definir regras de transformação de dados.

Esta tese utiliza a Programação Estratégica, as Gramáticas de Atributos e outros formalismos genéricos para construir um conjunto de ferramentas para a travessia e transformação de estruturas de dados, resultando numa biblioteca embutida em |Haskell| denominada |Ztrategic|.
%
%Introduzimos o |Ztrategic|, uma biblioteca embutida em |Haskell| onde a Programação Estratégica e as Gramáticas de Atributos podem ser livremente entrelaçadas, com todo o poder expressivo de ambas as técnicas, e estendemos a biblioteca com memorização de atributos. 
%
Uma versão desta biblioteca em |Python|, denominada |pyZtrategic|, foi implementada, provando que a abordagem é independente da linguagem. Testamos o desempenho das nossas bibliotecas
% |Ztrategic| e |pyZtrategic|, 
comparando-as
% entre si e 
com o |Strafunski| e o |Kiama|, duas bibliotecas que oferecem funcionalidades semelhantes.

Fornecemos uma base para o teste e validação de Gramáticas de Atributos definindo Testes Baseados em Propriedades para Gramáticas de Atributos. %, onde propriedades são definidas em instâncias de atributos. 
Apresentamos esta técnica de forma abstrata e implementámo-la usando a biblioteca |Ztrategic|.
%
Apresentamos uma interface genérica para a resolução de nomes para qualquer linguagem de programação. Os utilizadores desta interface especificam quais os 
%nodos na sua linguagem que representam definições de nomes, utilizações e escopos aninhados, e um sistema completo de análise de nomes é automaticamente derivado.
nodos relevantes na sua linguagem, e um sistema de análise de nomes é automaticamente derivado.

Com este trabalho, pretendemos 
%alargar os horizontes para o desenvolvimento de software e promover a 
destacar a relevância da 
Programação Estratégica e das Gramáticas de Atributos como ferramentas poderosas para transformação de dados.%, que podem ser usadas separadamente ou em conjunto.

\keywords{
  Gramáticas de Atributos \and
  Programação Estratégica \and
%  Programação Funcional \and
%  Reescrita de Termos \and
  Reescrita \and
  Travessia Genérica
}

\begin{comment}
Independentemente da língua em que a dissertação está escrita, geralmente esta contém pelo menos dois resumos: um resumo na mesma língua do texto principal e outro resumo numa outra língua.

A ordem dos resumos varia de acordo com a escola. Se a sua escola tiver regulamentos específicos sobre a ordem dos resumos, o template (\LaTeX) \gls{novathesis} irá respeitá-los. Caso contrário, a regra padrão no template \gls{novathesis} é ter em primeiro lugar o resumo \emph{no mesmo idioma do texto principal} e depois o resumo \emph{no outro idioma}. Por exemplo, se a dissertação for escrita em português, a ordem dos resumos será primeiro o português e depois o inglês, seguido do texto principal em português. Se a dissertação for escrita em inglês, a ordem dos resumos será primeiro em inglês e depois em português, seguida do texto principal em inglês.
%
No entanto, esse pedido pode ser personalizado adicionando um dos seguintes ao arquivo \verb+5_packages.tex+.

\begin{verbatim}
    \abstractorder(<MAIN_LANG>):={<LANG_1>,...,<LANG_N>}
\end{verbatim}

Por exemplo, para um documento escrito em Alemão com resumos em Alemão, Inglês e Italiano (por esta ordem), pode usar-se:
\begin{verbatim}
    \ntsetup{abstractorder={de={de,en,it}}}
\end{verbatim}

Relativamente ao seu conteúdo, os resumos não devem ultrapassar uma página e frequentemente tentam responder às seguintes questões (é imprescindível a adaptação às práticas habituais da sua área científica):

\begin{enumerate}
  \item Qual é o problema?
  \item Porque é que é um problema interessante/desafiante?
  \item Qual é a proposta de abordagem/solução?
  \item Quais são as consequências/resultados da solução proposta?
\end{enumerate}

% E agora vamos fazer um teste com uma quebra de linha no hífen a ver se a \LaTeX\ duplica o hífen na linha seguinte se usarmos \verb+"-+… em vez de \verb+-+.
%
% zzzz zzz zzzz zzz zzzz zzz zzzz zzz zzzz zzz zzzz zzz zzzz zzz zzzz zzz zzzz comentar"-lhe zzz zzzz zzz zzzz
%
% Sim!  Funciona! :)

% Palavras-chave do resumo em Português
% \begin{keywords}
% Palavra-chave 1, Palavra-chave 2, Palavra-chave 3, Palavra-chave 4
% \end{keywords}
\keywords{
  Primeira palavra-chave \and
  Outra palavra-chave \and
  Mais uma palavra-chave \and
  A última palavra-chave
}
% to add an extra black line
\end{comment}