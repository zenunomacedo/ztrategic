%!TEX root = ../template.tex
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% chapter4.tex
%% NOVA thesis document file
%%
%% Chapter with lots of dummy text
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%include lhs2TeX.fmt
%include polycode.fmt

\typeout{NT FILE chapter4.tex}%

\chapter{Efficient Embedding of Strategic Attribute Grammars via Memoization}
\label{sec:memoztrategic}


\paragraph{Abstract:} 
%include Chapters/4-MemoZtrategic/1-abstract.lhs

\section{Introduction}
\label{memo:sec1}
%include Chapters/4-MemoZtrategic/2-introduction.lhs

\section{Memoized Attribute Grammars}
\label{memo:sec2}
%include Chapters/4-MemoZtrategic/3-zipperStrategicAG.lhs

\section{Combining Attribute Memoization with Zippers}
\label{memo:memoZippers}
%include Chapters/4-MemoZtrategic/4-memo.lhs

\section{New Directions}
\label{memo:newdir}
%include Chapters/4-MemoZtrategic/5-patterns.lhs

\section{Strategies and Attribute Grammars in Python}
\label{memo:python}
%include Chapters/4-MemoZtrategic/6-python.lhs

\section{Performance}
\label{memo:performance}
%include Chapters/4-MemoZtrategic/7-performance.lhs

%\section{Related Work}
%\label{sec:related}
%%include Chapters/4-MemoZtrategic/8-relatedWork.lhs

\section{Conclusions}
\label{memo:conc}
%include Chapters/4-MemoZtrategic/9-conclusions.lhs

\section{API}
\label{sec:memoZtrAPI}
%include Chapters/4-MemoZtrategic/10-api.lhs