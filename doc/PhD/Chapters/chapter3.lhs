%!TEX root = ../template.tex
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% chapter3.tex
%% NOVA thesis document file
%%
%% Chapter with a short latex tutorial and examples
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%include lhs2TeX.fmt
%include polycode.fmt

\typeout{NT FILE chapter3.tex}%

\chapter{Zipping Strategies and Attribute Grammars}
\label{sec:ztrategic}

\paragraph{Abstract:} 
%include Chapters/3-Ztrategic/1-abstract.lhs


\section{Introduction}
\label{ztr:sec1}
%include Chapters/3-Ztrategic/2-introduction.lhs


\section{Ztrategic: Zipper-Based Strategic Programming}
\label{ztr:sec2}
%include Chapters/3-Ztrategic/3-zipperStrategic.lhs

\section{Strategic Attribute Grammars}
\label{ztr:sec3}
%include Chapters/3-Ztrategic/4-strategicAG.lhs


\section{Expressiveness and Performance}
\label{ztr:sec4}
%include Chapters/3-Ztrategic/5-ztrategic.lhs

%% we do not repeat related work

\section{Conclusions}%and Future Work}
\label{ztr:sec6}
%include Chapters/3-Ztrategic/7-conclusion.lhs

%\begin{comment}

\section{API}
\label{ztr:api}
%include Chapters/3-Ztrategic/8-api.lhs

%\end{comment}