%!TEX root = ../template.tex
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% chapter5.tex
%% NOVA thesis document file
%%
%% Chapter with lots of dummy text
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%include lhs2TeX.fmt
%include polycode.fmt
%include Config/formats.lhs 

\typeout{NT FILE chapter5.tex}%

\chapter{Property-based Testing of Attribute Grammars}
\label{sec:pbtAG}

\paragraph{Abstract:} 
%include Chapters/5-Properties/1-abstract.lhs

\section{Introduction}
%include Chapters/5-Properties/2-introduction.lhs

\section{Abstract technique}
\label{prop:technique}
%include Chapters/5-Properties/3-technique.lhs

\section{Implementation}
\label{prop:implementation}
%include Chapters/5-Properties/4-implementation.lhs

%only to check if anything's forgotten
%%include unused/allproperties.lhs 

%\section{Related Work}
%\label{sec:relatedwork}
%%include Chapters/5-Properties/5-relatedwork.lhs

\section{Conclusions}
\label{prop:conclusion}
%include Chapters/5-Properties/6-conclusions.lhs