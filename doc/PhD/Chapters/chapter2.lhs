%!TEX root = ../template.tex
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% chapter2.tex
%% NOVA thesis document file
%%
%% Chapter with the template manual
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%include lhs2TeX.fmt
%include polycode.fmt

\typeout{NT FILE chapter2.tex}%

\chapter{State of the Art}
\label{sec:sota}

\section{Attribute Grammars}

%The formal specification of scope rules is in the genesis of the Attribute Grammar formalism~\citep{Knuth90}. AGs are particularly suitable to specify language engineering tasks, where context information needs to be first collected before it can be used. 
The \gls{AG}~\citep{Knuth68} is a well-known and convenient formalism for specifying the semantic analysis phase of software languages and for modeling complex multiple traversal algorithms. Indeed, \gls{AG}s have been used not only to implement general purpose programming languages, for example Haskell~\citep{DijkstraFS09}, Java~\citep{jastaddj13}, or Oberon0~\citep{KAMINSKI2015,oberonkiama}, and domain specific languages, such as Modelica~\citep{AKESSON2010}, and EasyTime~\citep{FisterMFH11}, but also to specify sophisticated pretty printing algorithms~\citep{SPS99}, deforestation techniques~\citep{SS99,joao07pepm} and powerful type systems~\citep{MiddelkoopDS10}. These works have been performed with the support of traditional \gls{AG}-based systems~\citep{lrc,lisa,eli,jastadd,silver} or by embedding \gls{AG}s in a general purpose language, namely Scala\citep{kiama13} and Haskell~\citep{scp2016}.

Attribute grammars extend context-free grammars by adding \textit{attributes} to the grammar rules, which provide a structured way to define semantics or other properties of language constructs. Attributes are categorized as: 
\begin{itemize}
    \item \textbf{Synthesized Attributes} - Calculated from the children of a symbol and \textit{synthesized} up to parent nodes. They propagate information upwards.
    %passed up the grammar tree. 
    \item \textbf{Inherited Attributes} - \textit{Inherited} from parent nodes and passed down to child nodes, these attributes allow information to propagate downwards.
    %Passed down from parent nodes to provide context to the children.
\end{itemize}

Each rule of the grammar can have the computation of any number of attributes associated with it. 
%embedded? 
%extensions? HOAG? referenceAG? 
%kiama silver jastadd spoofax stratego strafunski 

\subsection{Extensions to Attribute Grammars}
The \gls{HOAG}~\citep{VSK89} formalism extends traditional attribute grammars by allowing attributes to be entire trees (subtrees or syntax trees), not just simple values. This capability enables a grammar to dynamically create and manipulate tree structures during computation of attributes. These newly computed trees can either be the desired computation result, or they can themselves be Attribute Grammars and have attributes more adequate for the computation of the desired result on the original grammar. 

The \gls{RAG}~\citep{referenceAG} formalism is an extension of traditional attribute grammars that allow attributes to refer to nodes outside the immediate parent-child relationship in the syntax tree. 
Reference attributes contain references that enable non-local information flow, allowing attributes to \textit{point} to other nodes within the tree structure, regardless of their position, very much like memory pointers and references in traditional programming languages.
This capability is especially useful in language processing tasks that require non-local dependencies, such as symbol resolution, scope handling, and type checking in programming languages.

The \gls{CAG}~\citep{Farrow86} formalism is an extension of attribute grammars that allows \textit{circular dependencies} among attributes. Unlike traditional attribute grammars, which require a strict, acyclic evaluation order for attributes, \gls{CAG}s permit cycles in the dependency graph, making them useful for problems that require iterative, fixed-point computations, such as dependency resolution and program type-checking. Of course, the circular attributes must converge to a fixed point, i.e. be finitely recursive, otherwise the computation will not terminate. 

Our work relies on \gls{HOAG}s which are closely related to \gls{RAG}s. In both \gls{AG} extensions, the original \gls{AST} is not fixed during attribute evaluation and a better \gls{AST} can be first computed and then decorated. In \gls{RAG}s, attributes of that more suitable \gls{AST} are available (via references) from the original \gls{AST}. This was not possible in \gls{HOAG}s until now. In Section~\ref{if:blockToAst} we present a technique that offers this (bidirectional) mechanism to \gls{HOAG}s. Because a \gls{HOAG} can be transformed into a classical \gls{AG}, the powerful static analysis techniques developed for \gls{AG}s - namely the static detection of circular dependencies - are orthogonal to our approach. \gls{RAG}s, on the contrary, rely on a dynamic (fix-point) attribute evaluation and therefore lose all \gls{AG} static guarantees~\citep{evalRAG}. 

%%
%%
%
%\subsection{RACR}
RACR~\citep{christoff2015} is an embedding of \gls{RAG}s in Scheme, that allows graph re-writing and incremental attribute evaluation. A \emph{dynamic attribute dependency graph} is constructed during evaluation in order to determine which attributes are affected by a re-writing and therefore should be re-evaluated. To keep our embedding simple we do not perform that kind of analysis, although incorporating it is an interesting line of possible future work.

%\subsection{JastAdd}
JastAdd is a \gls{RAG}-based system~\citep{jastadd}. It supports most \gls{AG} extensions, namely Reference and Circular AGs~\citep{rag2013}. It also supports tree re-writing, with re-write rules that can reference attributes. JastAdd, however, provides no support for strategic programming, that is, there is no mechanism to control how the re-write rules are applied.  The zipper-based \gls{AG} embedding we integrate in |Ztrategic| supports all modern \gls{AG} extensions, including Reference and Circular \gls{AG}s~\citep{scp2016,memoAG19}. Because strategies and \gls{AG}s are first class citizens we can smoothly combine any of such extensions with strategic term re-writing.

\section{Strategic Programming}
\label{rw:strategic}

Functional Strategic Programming is an idiom for (general purpose) generic programming based on the notion of a functional strategy: a first-class generic function that can not only be applied to terms of any type, but which also allows generic traversal into subterms and can be customized with type-specific behavior~\citep{strategicdesign}. With such functional strategies, dealing with transformation of heterogeneous data structures becomes much more concise --- only the relevant type-specific behavior needs to be specified by the software developer, and any other cases are handled by the default behavior of the strategy. Moreover, most libraries contain a myriad of pre-defined, easily extensible strategies, and allow for the definition of different strategies if needed. Software built with strategies is also extremely modular: since we only write type-specific code, extending the original data type does not require changing the strategic code, and evolving strategic code can be reduced to adding more type-specific cases to existing code. 

%
%not to be confused with the Strategy design pattern! 
%embedded? 
%w/ or w/o AGs?
%
%\subsection{Strafunski}
In the context of strategic term re-writing, our |Ztrategic| library is inspired by
|Strafunski|~\citep{strafunski}. In fact, |Ztrategic| already provides almost all |Strafunski| functionality. There is, however, a key difference between these libraries: while |Strafunski|
accesses the data structure directly, |Ztrategic| operates on zippers. As a consequence, we can easily access \gls{AG} attributes from strategic functions and strategic functions from attribute equations. Moreover, we can traverse not only into a data structure, but also outwards, that is, traverse regressing into previously-visited nodes. This is possible also due to the powerful abstraction provided by the zipper data structure.

%\subsection{Kiama}
%This paper 
Our work in attribute memoization 
is inspired by the work of Sloane who developed |Kiama|~\citep{kiama}: an embedding of strategic term re-writing and \gls{AG}s in the |Scala| programming language. 
%The Kiama library offers the
%functionality of our Ztrategic library. Kiama is a Scala based
%embedding of strategic AGs, which 
It is an embedding of strategic \gls{AG}s, and relies on |Scala| mechanisms to navigate in the trees and memoizes attribute values on a global memoization table to avoid attribute recalculation. Our library relies on zippers and uses local memo tables to avoid attribute recalculation. |Kiama| uses reachability relations and the notion of attribute families to fully support strategic \gls{AG}s.
Attributes that depend on their context are defined parametrized by the tree they belong to. This allows subtrees to be shared while maintaining the correctness of context-dependent attributes, since the same attribute has a different entry in the memoization table for the original tree and the one resulting from a re-write. On the other hand, context-independent attributes can use previously memorized values on modified trees. 
While our approach is fundamentally different, we provide a similar behavior by giving the software developer the option to clear the memoization tables after application of a strategy.  
%This approach is similar to our use of |applyTP| and |applyTP_unclean|, albeit finer-grained, as only context-dependent attributes are cleaned. 

%\subsection{Silver}
The extensible \gls{AG} system |Silver|~\citep{silver} has also been extended to support strategic term re-writing~\citep{strategicAG}. Strategic re-writing rules can use the attributes of a tree to reference contextual information during re-writing, much like we present in our work. While we use a functional embedding, |Silver| compiles its own Strategic \gls{AG} specification into low code. The paper includes several practical application examples, namely the evaluation of $\lambda$-calculus, a regular expression matching via Brzozowski derivatives, and the normalization of for-loops. All these examples can be directly expressed in our setting. They also present an application to optimize translation of strategies. Because our techniques rely on shallow embeddings, where no data type is used to express strategies nor \gls{AG}s, we are not able to express strategy optimizations, without relying on meta-programming techniques~\citep{templatehaskell}. Nevertheless, our embeddings result in very simple and small libraries that are easier to extend and maintain, specially when compared to the complexity of extending and maintaining a full language system such as |Silver|. |Silver| translates strategies into equations for higher-order attributes, while our strategies are just data traversal mechanisms built on top of zippers, and its only relation to \gls{AG}s is that our \gls{AG}s also depend on zippers, and therefore we can combine them.

%\subsection{Stratego}
The Stratego program transformation language (now part of the Spoofax Language Workbench~\citep{spoofax}) supports the definition of rewrite rules and programmable rewriting strategies, and it is able to construct new rewrite rules at runtime, to propagate contextual information for concerns such as lexical scope. It is argued in~\citep{strategicAG} that contextual information is better specified through the usage of inherited attributes for issues such as scoping, name-binding, and type-checking. We present this same usage of inherited attributes in our attribute grammar examples. 