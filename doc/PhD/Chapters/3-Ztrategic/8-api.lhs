%As we have shown, zippers can express strategic term rewriting.  We 
%developed a full library, named Ztrategic.
%Figure~\ref{code:api} presents the full API, which is based on Strafunski's API,
%The API of this library is based on Strafunski's API,
%but adding the possibility to manipulate the zipper that traverses the tree, in order to be able to, for example,
%compute attributes.
We define two \textbf{Strategy types}, |TP| and |TU| which stand for Type-preserving and Type-Unifying. The former represents transformations, while the latter represents reductions. 

We define \textbf{Primitive Strategies} that can be used as building blocks, such as identities (|idTP| and |constTU|), failing strategies |failTP| and |failTU|, the |tryTP| combinator which applies a transformation once if possible and always succeeds, and |repeatTP| which applies a transformation as many times as possible. 
\textbf{Strategy Construction} combinators allow for composition of more complex transformations. The |adhoc| combinators compose an existing strategy with a Haskell function, and the |mono| combinators produce a strategy out of one Haskell function. There are variants that allow access to the zipper, denoted by a $Z$ suffix. 

For the \textbf{composition} of traversals, |seq| defines a sequence (perform both traversals) and |choice| defines an alternative (perform just one traversal). 
We use \textbf{Traversal Combinators} |right| and |down| to travel the zipper; the |all| variants always succeed and the |one| variants can fail. 
\textbf{Traversal Strategies} are defined by combining the previous tools, and come in |td| (top-down) and |bu| (bottom-up) variants. The |full| strategies traverse the whole tree, while |once| strategies perform at most one operation and |stop| strategies stop cut off the traversal in a specific sub-tree when any operation in it succeeds. Finally, |innermost| and |outermost| perform a transformation as many times as possible, starting from the inside or outside nodes, respectively.  


%\begin{figure*}[h]
%\begin{minipage}[t]{.55\textwidth}
\noindent\textbf{Strategy types}
> newtype TP m = MkTP (forall a. Zipper a -> m (Zipper a))
> type TU m d = (forall a. Zipper a -> m d) 
\textbf{Primitive strategies}
> idTP      :: TP m
> constTU   :: d -> TU m d
> failTP    :: TP m 
> failTU    :: TU m d
> tryTP     :: TP m -> TP m
> repeatTP  :: TP m -> TP m
\textbf{Strategy Construction}
> monoTP    :: (b -> m b) -> TP m
> monoTU    :: (b -> m d) -> TU m d
> monoTPZ   :: (b -> Zipper a -> m b) -> TP m
> monoTUZ   :: (b -> Zipper a -> m d) -> TU m d
> adhocTP   :: TP m -> (b -> m b) -> TP m
> adhocTU   :: TU m d -> (b -> m d) -> TU m d
> adhocTPZ  :: TP m -> (b -> Zipper a -> m b) -> TP m
> adhocTUZ  :: TU m d -> (b -> Zipper a -> m d) -> TU m d
\textbf{Composition / Choice}
> seqTP     :: TP m -> TP m -> TP m
> choiceTP  :: TP m -> TP m -> TP m
> seqTU     :: TU m d -> TU m d -> TU m d
> choiceTU  :: TU m d -> TU m d -> TU m d
%\end{minipage}
%\begin{minipage}[t]{.35\textwidth}
\textbf{Traversal Combinators}
> allTPright  :: TP m -> TP m
> oneTPright  :: TP m -> TP m
> allTUright  :: TU m d -> TU m d
> allTPdown   :: TP m -> TP m
> oneTPdown   :: TP m -> TP m
> allTUdown   :: TU m d -> TU m d
\textbf{Traversal Strategies}
> full_tdTP  :: TP m -> TP m
> full_buTP  :: TP m -> TP m
> once_tdTP  :: TP m -> TP m
> once_buTP  :: TP m -> TP m  
> stop_tdTP  :: TP m -> TP m
> stop_buTP  :: TP m -> TP m
> innermost  :: TP m -> TP m
> outermost  :: TP m -> TP m
> full_tdTU  :: TU m d -> TU m d     
> full_buTU  :: TU m d -> TU m d     
> once_tdTU  :: TU m d -> TU m d   
> once_buTU  :: TU m d -> TU m d  
> stop_tdTU  :: TU m d -> TU m d  
> stop_buTU  :: TU m d -> TU m d
%\end{minipage}
%\caption{Full Ztrategic API}
%\label{code:api}
%\end{figure*}