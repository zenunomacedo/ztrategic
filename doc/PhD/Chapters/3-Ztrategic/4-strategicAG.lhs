Zipper-based strategic term rewriting provides a powerful mechanism to
express tree transformations. There are, however, transformations that
rely on contextual information that needs to be collected so %first, before
the transformation can be applied. Our optimization rule $7$ of
Fig.~\ref{rules} is such an example.  In this section we will
briefly explain the Zipper-based embedding of attribute grammars,
through the |Let| example.  Then, we are going to explain how to
combine strategies and \gls{AG}s, ending with an implementation of rule $7$.


\subsection{Zipper-based Attribute Grammars}
\label{sec:zipperAg}

The attribute grammar formalism is particularly suitable for specifying
language-based algorithms, where contextual information needs to be %first
collected before it can be used. Language-based algorithms such as
name analysis~\citep{scp2016}, pretty printing~\citep{SPS99},
type inference~\citep{MiddelkoopDS10}, etc. are elegantly
specified using \gls{AG}s.

Our running example is no exception and the name analysis task of
|Let| is a non-trivial one. Despite being a concise example, it has 
central characteristics of software languages, such as (nested)
block-based structures and mandatory but unique declarations of
names. In addition, the semantics of this implementation of 
|Let| does not force a
declare-before-use discipline, meaning that a variable can be declared
after its first use. Consequently, a conventional  implementation of name analysis
naturally leads to a processor that traverses each block twice: once
for processing the declarations of names and constructing an
environment and a second time to process the uses of names (using the
computed environment) in order to check for the use of non-declared
identifiers. The uniqueness of identifiers is efficiently checked in
the first traversal: for each newly encountered name it is checked
whether that it has already been declared at the same lexical level
(block). As a consequence, semantic errors resulting from duplicate
definitions are computed during the first traversal, and errors
resulting from missing declarations in the second one. In fact,
expressing this straightforward algorithm is a complex task in most
programming paradigms, since it requires a complex scheduling of tree
traversals\footnote{Note that only after building the environment of an
outer block can the nested ones be traversed: they
inherited that environment. Thus, traversals are intermingled.}, and
intrusive code may be needed to pass information computed in
one traversal to a specific node and used in a subsequent 
one\footnote{This is the case when we wish to produce a list of errors
that follows the sequential structure of the input program~\citep{Saraiva99}.}.

In the attribute grammar paradigm, the programmer does not need to be
concerned with scheduling of traversals, nor the use of intrusive code
to glue traversals together. As a consequence, 
they do not need to adapt algorithms in order to 
avoid those issues.
\gls{AG}s associate \emph{attributes} to grammar symbols (types in a functional setting),
which are called \emph{synthesized attributes} if they are computed bottom-up
or \emph{inherited attributes} if they are computed top-down.

Very much like strategic term rewriting, \gls{AG}s also rely on a generic
tree walk mechanism, usually called tree-walk
evaluators~\citep{Alblas91b}, to walk up and down the tree to evaluate
attributes. In fact, generic zippers also offer the
necessary abstractions to express the embedding of \gls{AG}s in a functional
programming setting~\citep{scp2016,memoAG19}. Next, we briefly describe
this embedding, and after that we present the embedded \gls{AG} that express
the scope rules of |Let|. It also computes (attribute) |env|, that is
needed by the optimization rule $7$.


To allow programmers to write zipper-based functions as \gls{AG} writers do, the generic
zipper library~\citep{genericZipper} is 
%extended with the following set of combinators among others:
extended with some combinators:
%
\begin{itemize}
\item The combinator ``\textit{child}'', written as the infix function
|.$| to access the child of a tree node given its index (starting from
1).
\begin{code}
(.$) :: Zipper a -> Int -> Zipper a
\end{code}

\item The combinator |parent| to move the focus to the parent of a
tree node.
\begin{code}
parent :: Zipper a -> Zipper a
\end{code}
\end{itemize}

%\vspace{-4pt}
Having presented these zipper-based \gls{AG} combinators, we now show in Fig.~\ref{fig:clienv}
the scope rules specified in the |Let| \gls{AG} directly as a Haskell-based \gls{AG}.
We also show a visual representation of the \gls{AG} in Fig.~\ref{fig:AG}.
Productions are shown with the parent node above and children nodes below,
inherited attributes are on their left and synthesized attributes on their right, and arrows show how information flows between productions and their children to compute attributes.
%The scope rules of |Let| are visually expressed in Fig.~\ref{fig:AG}.


\begin{figure}[t]
%\begin{minipage}[htb!]{\textwidth}
%\includegraphics[width=\textwidth,keepaspectratio]{Chapters/Figures/AG_updBW.png}
\includegraphics[width=\textwidth,keepaspectratio]{Chapters/Figures/AG_upd.png}
%\end{minipage}
\caption{Attribute Grammar Specifying the Scope Rules of |Let|}
\label{fig:AG}
\end{figure}

In this \gls{AG} the inherited attribute |dcli| is used as an
accumulator to collect all |Names| defined in a |Let|: it starts as an
empty list in the |Root| production, and when a new |Name| is defined
(productions |Assign| and |NestedLet|) it is added to the
accumulator. The total list of defined |Name| is synthesized in
attribute |dclo|, which at the |Root| node is passed down as the
environment (inherited attribute |env|). The type of the three
attributes is a list of triples, associating the |Name| to the level
it is defined (used to distingish declarations with the same name) and its |Let| expression definition\footnote{We will
use this definition to expand the |Name| as required by optimization
rule |7|.}. Thus, we define a type synonym

> type Env = [(Name, Int,Maybe Exp)]



\begin{figure}[t]
\begin{minipage}[t]{.41\textwidth}
\begin{code}
dclo :: Zipper Root -> Env
dclo t =  case (constructor t) of
  CLet        -> dclo (t.$1)
  CNestedLet  -> dclo (t.$3)
  CAssign     -> dclo (t.$3)
  CEmptyList  -> dcli t
\end{code}
\end{minipage}
\begin{minipage}[t]{.52\textwidth}
\begin{code}
lev :: Zipper Root -> Int
lev t = case (constructor t) of
  CLet  -> case (constructor (parent t)) of
    CNestedLet -> (lev (parent t)) + 1
    CRoot      -> 0
  _     -> lev (parent t)
\end{code} 
\end{minipage} 
%\vspace{1cm}
\begin{minipage}[t]{.62\textwidth} \vspace{.4cm}
\begin{code} 
dcli :: Zipper Root -> Env
dcli t = case (constructor t) of
  CLet  -> case (constructor (parent t)) of
      CRoot       ->  []
      CNestedLet  ->  env  (parent t)
  _     -> case (constructor (parent t)) of
      CLet        ->  dcli (parent t)
      CNestedLet  ->  (lexeme_Name (parent t), lev (parent t), Nothing) : (dcli (parent t))
      CAssign     ->  (lexeme_Name (parent t), lev (parent t), lexeme_Exp (parent t))
                      : (dcli (parent t))
\end{code}
\end{minipage}
\begin{minipage}[t]{.37\textwidth} \vspace{.4cm}
\begin{code}
env  :: Zipper Root  -> Env
env t =  case (constructor t) of
  CLet    -> dclo t
  _       -> env (parent t)
\end{code}
\end{minipage}
%new
%\caption{Definitions of |dclo| and |env| attributes}\label{fig:clienv}
\caption{Definitions of |dclo|, |lev|, |dcli| and |env| Attributes}\label{fig:clienv}
\end{figure}

We start by defining the equations of the synthesized attribute
|dclo|. For each definition of an occurrence of |dclo| we define an
equation in our zipper-based function. For example, in the diagrams of
the |NestedLet| and |Assign| productions in Fig.~\ref{fig:AG} we see
that |dclo| is defined as the |dclo| of the third child. Moreover, in
production |EmptyList| attribute |dclo| is a copy of |dcli|.
% This is exactly how such equations are written in the zipper-based Haskell AG, as we can see in Fig.~\ref{fig:clienv}.
Let us consider the case of
defining the inherited attribute |env|. In most diagrams an occurrence
of attribute |env| is defined as a copy of the parent. There are two
exceptions: in productions |Root| and |NestedLet| where |Let| subtrees
occur. In both cases, |env| gets its value from the synthesized
attribute |dclo| of the same non-terminal/type. We use the default
rule of the case statement to express similar \gls{AG} copy equations.

The inherited attribute |lev| is used to distinguish declarations with
the same name in different scopes. We omitted this attribute in the
visual \gls{AG} of Fig.~\ref{fig:AG} since its equations are simple.  This
attribute is passed downwards as a copy of the parent node/symbol,
with two exceptions: when visiting a |Let| subtree whose parent is a
Root, and when visiting a |NestedLet|. In the former the (initial)
level is |0|, while in the latter since we are descending to a nested
block, we increment the level of the outer one.

Finally, let us define now the accumulator attribute |dcli|.  The
zipper function, when visiting nodes of type |Let| (which have |dcli|
attributes) has to consider two alternatives: the parent node can be a
|Root| or a |NestedLet| (the two occurrences of |Let| as a child in
the diagrams of Fig.~\ref{fig:AG}). This happens because the rules
to define its value differ: in the |Root| node it corresponds to an
empty list (our outermost |Let| is context-free), while in a nested
block, the accumulator |dcli| starts as the |env| of the outer block.
When visiting all other subtrees (expressed by the default rule), we
need to define the inherited attribute |dcli| of |List| subtrees. There
are three different cases: when the parent is a |Let| node, |dcli|
is a copy of the parent.  When the parent is an |Assign| then the |Name|,
|level| and the associated |Exp| are accumulated in the |dcli| of the
parent. Finally, in the case of |NestedLet| the |Name|, |level| and a |Nothing|  expression is accumulated in
|dcli|\footnote{In this \gls{AG} function we use boilerplate code
|lexeme_Name| and |lexeme_Exp|, which implement the so-called
\textit{syntactic references} in attribute
equations~\citep{syngen}. They return the |Name| and |Exp|
arguments of constructor |Assign|, respectively.}.


In order to specify the complete name analysis task of |Let|
expression we need to report which names violate the scope rules of
the language. We can modular and incrementally extend our \gls{AG}~\citep{Saraiva02}, and define a new
attribute |errors| to report such violations.  In the next section
|errors| is expressed as a strategic function.



\subsection{Strategic Attribute Grammars}
\label{sec:strategicAG}

%\todo{"In section 3.2 we will clarify the meaning of combining attribution with re-writing. As an example we include the above version of errors that uses the attributable attribute (the rewritten tree) and thus getting the context available before the rewrite." Can we fit this?!}

By having embedding both strategic term rewriting and attribute
grammars in the same zipper-based setting,
and given that both are embedded as first-class citizens,
we can easily combine these
two powerful language engineering techniques. As a result, attribute
computations that do useful work on few productions/nodes can be
efficiently expressed via our Ztrategic library, while rewriting
rules that rely on context information can access attribute values.
% Next, we extend our |Let| AG where we will rely on each of the
% techniques to efficiently specify such new language features.

\medskip
\noindent
\textit{Accessing Attribute Values from Strategies:} 
As we mentioned in Section~\ref{ztr:sec3}, rule $7$ of Fig.~\ref{rules} cannot
be implemented using a trivial strategy, since it depends on the
context.  The rule states that a variable occurrence can be changed by
its definition. Thus, we need to compute an environment of
definitions, which is what we have done with the attribute |env|,
previously.  If we had access to such attribute in the
definition of a strategy, we would be able to implement this rule.

Given that both attribute grammars and strategies use the zipper to walk through the tree, such combinations can be easily performed if
the strategy exposes the zipper, so it can be used to apply the given attribute.
This is done in our library by the |adhocTPZ| combinator:


%> adhocTPZ :: (Monad m, Typeable a, Typeable b) => TP m -> (b -> Zipper a -> m b) -> TP m
> adhocTPZ :: TP m -> (b -> Zipper a -> m b) -> TP m

Notice that instead of taking a function of type |(b -> m b)|, as does the combinator |adhocTP| introduced in Section~\ref{ztr:sec2},
it receives a function of type |(b -> Zipper a -> m b)|, with the zipper as a parameter.
Then, we can define a worker function with this type, that implements rule $7$:


\begin{code}
expC :: Exp -> Zipper Root -> Maybe Exp
expC (Var x)  z  =  expand (i, lev z) (env z)
expC  _       _  = Nothing 
\end{code}
\label{code:ExpC}


\noindent where |expand| is a simple lookup function that
replaces a name |x| for its definition in the environment (given by
attribute |env|). This strategic function also uses attribute |lev| to look for the
current or closest scope where name |x| is defined. 
As a final step,
we combine this rule with the previously defined |expr| (rules $1$ to $6$)
and apply them to all nodes.


\begin{code}
opt''  :: Zipper Root -> Maybe (Zipper Root)
opt''  r = applyTP (innermost (failTP `adhocTPZ` expC `adhocTP` expr)) r  
\end{code} 

\medskip
\noindent
\textit{Synthesizing Attributes via Strategies:}
We showed how attributes and strategies are combined by using the former while defining the latter.
Now we show how to combine them the other way around; i.e. to express attribute computations as strategies.
As an example, let us define 
the |errors| attribute, that returns
the list of names that violate the scope rules. %:
\begin{comment}
. Moreover, to
 provide better error reporting, we wish to produce the list of errors
following the structure of input program.
We want to evaluate
the following input to the list |["b","b","z","c"]|:

\begin{minipage}[htb!]{.50\textwidth}
\begin{code}
letWithErrors =  let  a  = b + 3     
                      c  = 2
		      w  =  let  c = a - b in   c + z
                      c  = c + 3 - c
                 in   (a + 7) + c + w             
\end{code}
\end{minipage}
\end{comment}

%Recall that duplicated definitions that are efficiently detected when
%a new |Name| (defined in nodes |Assign| and |NestedLet|) is
%accumulated in |dcli|. Thus. the newly defined |Name| \textit{must not
%be in} the environment |dcli| accumulated prior to that
Note that duplicated definitions are efficiently detected when
a new |Name| (defined in nodes |Assign| and |NestedLet|) is
accumulated in |dcli|. The newly defined |Name| \textit{must not
be in} the environment |dcli| accumulated prior to that 
definition. Invalid uses are detected when a |Name| is used in an
arithmetic expression (|Exp|). In this case, the |Name| \textit{must
be in}\footnote{Functions |mNBIn| and |mBIn| are trivial lookup
functions. They are presented in~\citep{ztrategicRepo}.} the accumulated
environment |env|.
This is expressed by the following zipper functions:

\noindent
%\begin{minipage}[htb!]{.70\textwidth}
\begin{code}
decls :: List -> Zipper Root -> [Name]
decls (Assign       _ _ _) z  = mNBIn (lexeme_Name z, lev z)  (dcli z)
decls (NestedLet    _ _ _) z  = mNBIn (lexeme_Name z, lev z)  (dcli z) 
decls _ _                     = []
\end{code}
%\end{minipage}
%\begin{minipage}[htb!]{.35\textwidth}
%\vspace{-.7cm}
\begin{code}
uses :: Exp -> Zipper Root -> [Name]
uses (Var _) z  = mBIn (lexeme_Name z) (env z)
uses _       z  = []
\end{code}
%\end{minipage}


Now, we define a type-unifying strategy that produces the
list of errors. 

\begin{code}
errors :: Zipper Root -> [Name]
errors  t = applyTU (full_tdTU (failTU `adhocTUZ` uses `adhocTUZ` decls)) t
\end{code}

Although the applied function combines |decls| and |uses| in this order,
the resulting list does not report duplicates first, and invalid
uses after. The strategic function |adhocTUZ| combines the two
functions and the default failing function 
into one, which is applied while traversing the tree in a top-down
traversal, producing the errors in the order they occur.
If we define |errors| as an attribute, most
of the attribute equations are just propagating attribute values
upwards without doing useful work!
This is particularly relevant when we
consider the |Let| sub-language as part of a real programming language
(such as Haskell with its 116 constructors across 30 data
types). Thus, combining attribute grammars with strategic term rewriting
allows the leverage of the best of both worlds.


%The difference in complexity between the strategic definition
%of |errors| and its AG counterpart is much higher.


\begin{comment}
Attribute grammar systems provide the so-called attribute propagation
patterns to avoid polluting the specification with
\textit{copy-rules}. In most systems, a special notation and pre-fixed
behavior is associated with a set of off-the-shelf patterns that can
be reused across AGs~\citep{eli,uuag}. For example, in the UUAG
system~\citep{uuag}, the propagation patterns are the default rules
for any attribute. Thus, only the specific/interesting equations
have to be specified. However, being
a special notation, hard-wired to the AG system, makes the extension
or change of the existing rules almost impossible: the full system has
to be updated. Our embedding of strategic term rewriting provides a
powerful setting to express attribute propagation patterns: no special
additional notation/mechanism is needed.



\subsection{Higher-Order, Circular Attribute Grammars}


As we have presnted in~\citep{}, in order to evaluate a |let|
expression we do need to compute a symbols table, and then evaluate it. Higher-order attribute grammars allows this.

Moreover, to evaluate such environment we need to express a fix-point
computation.  Circular attribnute grammars ...





\todo{review the whole section}
\todo{add some concluding words}

\end{comment}