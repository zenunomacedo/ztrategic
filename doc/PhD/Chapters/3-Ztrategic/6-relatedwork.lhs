
%<kiama>

The work we present in this paper is inspired by the pioneering work of
Sloane who developed Kiama~\cite{kiama,10.1007/978-3-642-00722-4-11}: an embedding of strategic
term rewriting and AGs in the Scala programming language. While our
approach expresses both attribute computations and strategic term
rewriting as pure functions, Kiama caches attribute values in a global
cache, in order to reuse attribute values computed in the original
tree that are not affected by the rewriting. Such global caching,
however, induces an overhead in order to keep it updated, 
%that is,
for example, 
attribute values associated to subtrees discarded by the rewriting
process need to be purged from the cache~\cite{respectyourparents}. In
our purely functional setting, we only compute attributes in the
desired re-written tree (as is the case of the let example shown in
section~\ref{sec:zipperAg}).
%<Eric>
Influenced by Kiama, Kramer and Van Wyk~\cite{strategicAG} present
\emph{strategy attributes}, which is an integration of strategic term
rewriting into attribute grammars. Strategic rewriting rules can use
the attributes of a tree to reference contextual information during
rewriting, much like we present in our work. They present several
practical application, 
%examples
 namely the evaluation of $\lambda$-calculus, a
regular
expression matching via Brzozowski derivatives, and the normalization
of for-loops. All these examples can be directly expressed in our
setting. They also present an application to optimize translation of
strategies. Because our techniques rely on shallow embeddings, we are unable to
express strategy optimizations without relying on meta-programming
techniques~\cite{templatehaskell}. Nevertheless, our embeddings result
in very simple libraries that are easier to extend and
maintain, specially when compared to the complexity of extending 
a full language system such as Silver~\cite{silver}. 
%<JastAdd>
JastAdd is a reference attribute grammar based system~\cite{jastadd}. It
supports most of AG extensions, including reference and circular
AGs~\cite{rag2013}. It also supports tree rewriting, with
rewrite rules that can reference attributes.  JastAdd, however,
provides no support for strategic programming, that is to say, there is
no mechanism to control how the rewrite rules are applied.
The zipper-based AG embedding we integrate in Ztrategic supports all
modern AG extensions, including reference and circular
AGs~\cite{scp2016,memoAG19}. Because strategies and AGs are first-class 
citizens we can smoothly combine any such extensions with
strategic term rewriting.

%<Strafunski>

In the context of strategic term rewriting, our Ztrategic library
is inspired by Strafunski~\cite{strafunski}. In fact, Ztrategic
already provides almost all Strafunski functionality. There is,
however, a key difference between these libraries: while Strafunski
accesses the data structure directly, Ztrategic operates on
zippers. As a consequence, we can easily access attributes from
strategic functions and strategic functions from attribute equations.


%<Stratego>


\begin{comment}
The Stratego program transformation language (now part of the Spoofax Language Workbench~\cite{spoofax}) supports the definition of rewrite rules and programmable rewriting strategies, and it is able to construct new rewrite rules at runtime, to propagate contextual information for concerns such as lexical scope. It is argued in~\cite{strategicAG} that contextual information is better specified through the usage of inherited attributes for issues such as scoping, name-binding, and type-checking. We present this same usage of inherited attributes in our attribute grammar examples. 
\end{comment}


