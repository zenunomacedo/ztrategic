

%This section briefly describes functional Zippers~\citep{thezipper}
%which are the building blocks of the embedding of strategic term
%rewriting we introduce in this paper.
Before we present our
embedding in detail
later in the section,
 let us consider a motivating example we will use
throughout the chapter. Consider the (sub)language of |Let| expressions
as incorporated into most functional languages, including
Haskell. Next, we show an example of a valid Haskell |let|
expression 
and we define the heterogeneous data type |Let|, taken
from~\citep{scp2016}, that models such expressions in Haskell itself.


\begin{minipage}[t]{.4\textwidth}
\begin{code}
p =  let   a = b + 0     
           c = 2
           b = let c = 3 in c + c
     in    a + 7 - c  
\end{code}
\end{minipage}
\begin{minipage}[t]{.4\textwidth}
\begin{code}
data  Let   =  Let List Exp
data  List  =  NestedLet  String Let List
            |  Assign     String Exp List
            |  EmptyList
data  Exp   =  Add    Exp Exp  |  Sub    Exp Exp
            |  Neg    Exp  |  Const  Int   |  Var    String
            
\end{code}
\end{minipage}
%\begin{center}
%caption
%\end{center}
%The layout breaks if *CERTAIN* conditions are met in terms of 
%spacing before / after the mini pages. Keep 1 before / after 
%and 0 inbetween!!!

%Having introduced these data types,


\noindent We can write |p| as a Haskell 
%(syntax) tree 
value with type |Let|:

\begin{code}
p =  Let  (Assign     "a"  (Add (Var "b") (Const 0))
          (Assign     "c"  (Const 2)
          (NestedLet  "b"  (Let  (Assign "c" (Const 3) EmptyList)
                                 (Add (Var "c") (Var "c")))
          EmptyList)))
          (Sub (Add (Var "a") (Const 7)) (Var "c"))
\end{code}

Consider now that we wish to implement a simple arithmetic optimizer
for our language. Let us start with a trivial optimization: the
elimination of additions with $0$. In this context, strategic term
rewriting is an extremely suitable formalism, since it provides a
solution that just defines the work to be done in the constructors
(tree nodes) of interest, and ``ignores" all the others. In our example,
the optimization is defined in |Add| nodes, and thus we express
the worker function as follows:

\begin{code}
expr :: Exp -> Maybe Exp
expr (Add e (Const 0))  = Just e 
expr (Add (Const 0) e)  = Just e  
expr _                  = Nothing
\end{code}

\noindent The first two alternatives define the optimization: when either of the
sub-expressions of an |Add| expression is the constant $0$, then it
returns the other sub-expression.
A type-specific transformation function returns a |Maybe| result,
transformations that fail or do not change the input return |Nothing|.
This is the case of the last alternative of |expr|,
that defines the default behavior.

%The last alternative defines the |default|
%behaviour for all other cases, returning the original
%expression. Because we also need to express transformations that may
%fail (that is, do nothing), 
%a type-specific transformation function returns a |Maybe|
%result. 

This function applies to |Exp| nodes only. To express our |Let|
optimization, however, we need a generic mechanism that traverses 
|Let| trees, applying this function when visiting |Add|
expressions.  This is where strategic term rewriting comes to the
rescue: It provides recursion patterns (\textit{i.e.}, strategies) to
traverse the (generic) tree, like, for example, top-down or bottom-up
traversals. It also includes functions to apply a node specific
rewriting function (like |expr|) according to a given strategy. Next,
we show the strategic solution of our optimization where |expr| is
applied to the input tree in a full top-down strategy. This is a Type
Preserving (|TP|) transformation since the input and result trees have
the same type:

\begin{code}
opt :: Zipper Let -> Maybe (Zipper Let)
opt  t = applyTP (full_tdTP step) t    
     where step = idTP `adhocTP` expr
\end{code} 

We have just presented our first zipper-based strategic
function.  Here, |step| is a transformation to be applied by function
|applyTP| to all nodes of the input tree |t| (of type |Zipper Let|)
using a full top-down traversal scheme (function |full_tdTP|). The
rewrite step behaves like the identity function (|idTP|) by default
with our |expr| function to perform the type-specific transformation,
and the |adhocTP| combinator joins them into a single function.

This strategic solution relies on our |Ztrategic|~\citep{ztrategicRepo}
%\footnote{The library and complete examples showed in this paper are available at 
%\url{https://bitbucket.org/zenunomacedo/ztrategic/}}
%\url{https://bitbucket.org/anonymized/ztrategic/}}
library: a purely
functional embedding of strategic term rewriting in Haskell. In this
solution we clearly see that the traversal function |full_tdTP| needs
to navigate heterogeneous trees, as it is the case of the |Let|
expression |p|.  In a functional programming setting,
zippers~\citep{thezipper} provide a simple, but generic tree-walk
mechanism that we will use to embed strategic programming in
Haskell. In fact, our strategic combinators work with zippers as in
the definition of |opt|. In the remaining of this section, we start
by briefly describing zippers, and, next, we present in detail the
embedding of strategies using this powerful mechanism.

\subsection{The Zipper Data Structure}

Zippers were introduced by Huet~\citep{thezipper} to represent a tree
together with a subtree that is the \emph{focus} of attention. During a
computation the focus may move left, up, down or right within the
tree. Generic manipulation of a zipper is provided through a set of
predefined functions that allow access to all the nodes of a tree
for inspection or modification.

A generic implementation of this concept is available as the \emph{generic
zipper} Haskell library~\citep{genericZipper}, which works for both
homogeneous and heterogeneous data types. In order to illustrate the
use of zippers,
%and its Haskell library
 let us consider again the
tree used as an example for our |Let| program. 

\begin{comment}
This 
tree contains nodes of the types |Let|, |List| and |Exp|, and thus it
is an heterogeneous tree. Traditionally, a functional implementation
of a traversal of this tree would need three functions, one for each
different type that needs to be processed. Generic zippers, however,
provide a way to navigate in such heterogeneous data structures,
independently from the type of node it is traversing.
\end{comment}
We build a zipper |t_1| from the previous |Let| expression |p| through
the use of the |toZipper :: Data a => a -> Zipper a| function. This
function produces a zipper out of any data type, requiring only that
the data types have an instance of the |Data| and |Typeable| type
classes\footnote{These can be easily obtained via the Haskell data
type |deriving| mechanism.}.

\begin{code}
t_1 = toZipper p
\end{code} 

We can navigate |t_1| using pre-defined functions from the zipper
library. The function |down'| moves the focus down to the
leftmost child of a node, while |down| moves the focus to the
rightmost child instead. Similarly, functions |right|, |left| and
|up|, move towards the corresponding directions.
They all have type |Zipper a -> Maybe (Zipper a)|, meaning that such functions
take a zipper and return a new zipper in case the navigation does not fail.
%They have types:
%
%\begin{code}
%down,down',right,left,up ::  Zipper a -> Maybe (Zipper a)
%\end{code}

Finally, the zipper function |getHole :: Typeable b => Zipper a ->
Maybe b| extracts the actual node the zipper is focusing on.
Notice that the type of the hole (|b|) can be different from the type
of the root of the Zipper (|a|), since the tree can be heterogeneous.
Using these functions, we can freely navigate through this newly created
zipper. Consider our expression |p|, we can 
\textit{unsafely}\footnote{By using the function |fromJust :: Maybe a -> a| we assume a |Just| value is returned.} 
move the focus of the
zipper towards the |b+0| sub-expression 
and obtain its value 
as follows:

\begin{code}
sumBZero :: Maybe Exp
sumBZero = (getHole  .  fromJust . right  .  fromJust . down' .  fromJust . down') t_1
\end{code} 

%Because the navigation functions can fail, the data type |Maybe| is
%used to make them total. To simplify our example we are unwrapping it
%using the library function |fromJust|.
\begin{comment}
To avoid the repeated use of |fromJust| and to define total
functions, which also express a more natural top-down
writing/reading of the navigation on trees, we can rewrite these
functions using the monadic do-notation\footnote{A pure monadic
definition can also be used, which make these definitions even
simpler. In section~\ref{ztr:sec5} we will show how to obtain them.}, as
follows:

\begin{code}
sumBZero' :: Maybe Exp
sumBZero' =  do  t_2 <- down'  t_1
                 t_3 <- down'  t_2 
                 t_4 <- right  t_3
                 getHole t_4 
\end{code}
\end{comment}

The zipper library also contains functions for the transformation of the
data structure being traversed. The function |trans :: GenericT ->
Zipper a -> Zipper a| applies a generic transformation to the node the
zipper is currently pointing to; while |transM ::
GenericM m -> Zipper a -> m (Zipper a)| applies a generic monadic
transformation.

\begin{comment}
%exhibits 
%a similar behaviour, but it can also encode additional information 
%relating to the transformation.


In order to show a zipper-based transformation, let us consider 
that we wish to increment a constant in a |Let| expression.
We begin by defining a function |incConstant| that increments constants,
and use the generic function |mkT| (from the generics library~\citep{syb}) to generalize this type-specific function to all types:


\begin{code}
incConstant :: Exp -> Exp
incConstant (Const n) = Const (n+1)

incConstantG :: GenericT
incConstantG = mkT incConstant
\end{code}

This function has type |GenericT| (meaning \emph{Generic Transformation})
that is the required type of |trans|. To transform the assignment |c=2|
(in |p|) to |c=3| we just have to navigate to the desired constant
and then apply |trans|, as follows\footnote{Given that |Maybe| is a monad in this time we avoided the repeated use of |fromJust| writing the code in a monadic style.}:


\begin{code}  
incrC :: Maybe (Zipper Let) 
incrC =  down'  t_1 >>= \t_2 -> down'  t_2 >>= \t_3 -> right  t_3 >>= \t_4 ->
         right  t_4 >>= \t_5 -> down'  t_5 >>= \t_6 -> right  t_6 >>=  \t_7 -> 
         return (trans incConstantG t_7)
\end{code}
\end{comment}

\begin{comment}
\begin{code}  
incrC :: Maybe (Zipper Let) 
incrC = do  t_2 <- down'  t_1
            t_3 <- down'  t_2
            t_4 <- right  t_3
            t_5 <- right  t_4
            t_6 <- down'  t_5
            t_7 <- right  t_6
            return (trans incConstantG t_7)
\end{code}
\end{comment}

\begin{comment}
In fact, generic zippers is a simple, but very expressive technique to
navigate in heterogeneous data structures. Since strategic term
rewriting relies on generic traversals of trees, and on the
transformation of specific nodes, zippers provide the necessary
machinery to embed strategic programming in Haskell, as we will show
in the next section. It should also me mentioned that zippers also
provide a powerful embedding of attribute grammars in
Haskell~\citep{zipperAG,scp2016,memoAG19}. In Section~\ref{ztr:sec3}
we will show how these two language engineering techniques/embeddings
can be easily combined as a result of being expressed on the same
setting; \textit{i.e.} via zippers.
\end{comment}

\subsection{Strategic Programming}


In this section we introduce |Ztrategic|, our embedding of strategic programming
using generic zippers. The embedding directly follows the work of
Laemmel and Visser~\citep{strafunski} on the Strafunski
library~\citep{StrafunskiAppLetter}.
%Before we present the powerful and
%reusable strategic functions providing control on tree traversals,
%such as top-down, bottom-up, innermost, etc.,
%Before we present the strategic functions, let us show some simple
%basic combinators that work at the zipper level, and are the building
%blocks of our embedding.

We start by defining a function 
%that expresses how a given transformation function is elevated to the zipper level. 
that elevates a transformation to the zipper level.
In other
words, we define how a function that is supposed to operate directly
on one data type is converted into 
a zipper transformation.

%a transformation that operates on a
%zipper.

%Ultimately, the user does not need to use this
%function directly, because it is only used as a building block for
%other functions.
\begin{comment}
\begin{code}
zTryApplyM  :: (Typeable a, Typeable b)
            => (a -> Maybe b) -> TP c
zTryApplyM f = transM (join. cast. f. fromJust. cast)
\end{code} 
\end{comment}

\begin{code}
elevate :: (Monad m, Typeable b) => (b -> m b) -> Zipper a -> m (Maybe (Zipper a))
\end{code}

The definition of |elevate|, which we omit for brevity, relies on transformations on zippers,
thus reusing the generic zipper library |transM| function.
\begin{comment}
To build a valid transformation for the |transM| function, we
use the |cast :: a -> Maybe b| function, that tries to cast a given
data from type |a| to type |b|. In this case, we use it to cast the
data the zipper is focused on into a type our original transformation
|f| can be applied to.  Then, function |f| is applied and its result
is cast back to its original type. Should any of these casts, or the
function |f| itself, fail, the failure propagates and the resulting
zipper transformation will also fail. The use of the monadic version
of the zipper generic transformation guarantees the handling of such
partiality.  It should be noticed that failure can occur in two
situations: the type cast fails when the types do not match. Moreover,
the function |f| fails when the function itself dictates that no
change is to be applied.  Signaling failure in the application of
transformations is important for strategies where a transformation is
applied once, only.
\end{comment}

%This function has a single argument: the original transformation
%function as argument, named |f|. This function |f| generically
%receives an argument |a| and can transform it into a |b| or fail (as
%defined by the |Maybe| type result). 

|elevate| elevates any transformation |b -> m b| to the zipper level, 
in which |m| is a monad encapsulating the transformation. Later on, 
we use the type |TP m|, in which |TP| is a type for specifying
Type-Preserving transformations on zippers, and |m| is the type of a
monad encapsulating the operation. For example, if we are applying 
transformations that can sometimes fail on a zipper
built upon the |Let| data type, then the original transformation could 
be of type |Exp -> Maybe Exp|, and the elevated transformations 
 are of type |TP Maybe|.

%> type TP a = Zipper a -> Maybe (Zipper a)
> newtype TP m = MkTP (forall a. Zipper a -> m (Zipper a))

Very much like Strafunski, we introduce the type |TU m
d| for Type-Unifying operations, which aim to gather data of type |d|
into the (monadic) data structure |m|.

> type TU m d = (forall a. Zipper a -> m d)

For example, to collect in a list all the
defined names in a |Let| expression, the corresponding type-unifying
strategy would be of type |TU [] String|. We will present such a
transformation and implement it later in this section.

Next, we define a combinator to compose two transformations, building
a more complex zipper transformation that tries to apply each of the
initial transformations in sequence, 
skipping transformations that fail.
%. Because each of the
%transformations may fail, we have to skip transformations that fail.

\begin{comment}
\begin{code}
adhocTP  ::  (Typeable a, Typeable b)
         =>  TP e -> (a -> Maybe b) -> TP e
adhocTP f g z = maybeKeep f (zTryApplyM g) z
\end{code} 
\end{comment}

\begin{code}
adhocTP :: (Monad m, Typeable b) => TP m -> (b -> m b) -> TP m
adhocTP f g  =  MkTP $ \z -> do  val <- elevate g z 
                                 maybe (applyTP f z) return val
\end{code}

The |adhocTP| function receives transformations |f| and |g| as
parameters, as well as zipper |z|. It elevates |g|, 
which is a simple (\textit{i.e.} non-zipper) Haskell function,
into a zipper.
Because the elevation into the zipper level can fail, for example 
if the types do not match, |val| is a value of type |Maybe|; 
we use function |maybe| to return it if the elevation succeeded 
and |val| is a valid value, or to otherwise forget |g| entirely and 
return the result of |applyTP f z|.  
%"`maybeKeep` is too mysterious" - review 3
%Then, the zipper transformations |f| and |g| are passed as arguments to |maybeKeep|, which is an auxiliary function that applies the transformations in sequence, discarding either failing transformation (\textit{i.e.} that produces |Nothing|). We omit the definition of |maybeKeep| for brevity.
%
%, and uses the auxiliary function |maybeKeep| to try
%to apply the two transformations to the zipper |z|, 
%ignoring either if it fails.
%
%
%apply each of the transformations to the zipper |z|, ignoring the
%transformations that fail.  

%Note that |f| is of type |TP e|, meaning
%it is a transformation on zippers, while |g| is a normal Haskell
%function. Because |g| is a non-zipper based function, |adhocTP| allows
%the definition of transformations where we use simple (\textit{i.e.}
%non-zipper) Haskell functions. 
Next, we  use  |adhocTP|,
written as an infix operator, which combines the zipper function
|failTP| with our basic transformation |expr| function:

\begin{code}
step = failTP `adhocTP` expr
\end{code} 

Thus, we do not need to express type-specific transformations as
functions that work on zippers. It is the use of |elevate| in
|adhocTP| that transforms a  Haskell function (|expr| in this
case) to a zipper one, hidden from these definitions.

The transformation |failTP| is a pre-defined transformation that always
fails (returning |mzero|, a monadic value representing failure) and |idTP| is the identity transformation that always
succeeds (returning the input unchanged). They provide the basis for construction of complex
transformations through composition.
%\begin{code}
%failTP  = const mzero
%idTP    = return . id
%\end{code} 
%We omit here their simple definitions.

The functions we have presented already allow the definition of
arbitrarily complex transformations for zippers. Such transformations,
however, are always applied on the node the zipper is focusing on. Let
us consider a combinator that navigates in the zipper.

\begin{code}
allTPright :: TP m -> TP m
allTPright f = MkTP $ \z -> moveM right left (return z) (applyTP f) z
\end{code} 

This function is a combinator that, given a type-preserving
transformation |f| for zipper |z|, will attempt to apply |f| to the
node that is located to the right of the node the zipper is pointing
to. To do this, we use pre-defined zipper function |moveM|, that will 
use the first function |right| to navigate, then |applyTP f|, and then
return with function |left|. If anything fails, this operation will 
|return z| unchanged. 
%the zipper function |right| is used to try to
%navigate to the right; if it fails, we return the original zipper. If
%it succeeds, we apply transformation |f| and then we navigate |left|
%again. 
There is a similar combinator |allTPdown| that navigates downwards and then upwards.
%There are also two similar combinators, that fail when there is no
%|right| node to travel to - these allow for more freedom in defining
%partial strategies.

With all these tools at our disposal, we can define generic traversal schemes by combining them. Next, we define the traversal scheme used in the function |opt| we defined at the start of the section. This traversal scheme navigates through the whole data structure, in a top-down approach. 

\begin{code}
full_tdTP :: TP m -> TP m
full_tdTP f =  f `seqTP` allTPdown (full_tdTP f) `seqTP` allTPright (full_tdTP f)
\end{code} 

We skip the explanation of the |seqTP| operator as it is relatively
similar to the |adhocTP| operator we described before, albeit simpler;
we interpret this as a sequence operator. This function receives as
input a type-preserving transformation |f|, and (reading the code
from left to right) it applies it to the focused node itself, then to the
nodes below the currently focused node, then to the nodes to the right
of the focused node. To apply this transformation to the nodes below
the current node, for example, we use the |allTPdown| combinator we
mentioned above, and we recursively apply |full_tdTP f| to the node
below. The same logic applies with regard to navigating to the right.

We can define several traversal schemes similar to this one by changing the combinators used, or their sequence. For example, by inverting the order in which the combinators are sequenced, we define a bottom-up traversal. By using different combinators, we can define choice, allowing for partial traversals in the data structure. 
We previously defined a rewrite strategy where we use
|full_tdTP| to define a full, top-down traversal, which is not
ideal. Because we intend to optimize |Exp| nodes, changing one node
might make it possible to optimize the node above, which would have
already been processed in a top-down traversal. Instead, we define a
different traversal scheme, for repeated application of a
transformation until a fixed point is reached:

\begin{code}
innermost  :: TP m -> TP m
innermost s  =  repeatTP (once_buTP s)
\end{code} 

We omit the definitions of |once_buTP| and |repeatTP| as they are
similar to the presented definitions. The combinator |repeatTP|
applies a given transformation repeatedly until a fixed point is
reached, that is, until the data structure stops being changed by the
transformation. The transformation being applied repeatedly is defined
with the |once_buTP| combinator, which applies |s| once, anywhere on
the data structure. When the application |once_buTP| fails, |repeatTP|
understands a fixed point is reached. Because the |once_buTP|
bottom-up combinator is used, the traversal scheme is |innermost|, since
it prioritizes the innermost nodes. The pre-defined 
|outermost| strategy
uses the |once_tdTP| combinator
instead.

Let us return to our |Let| running example. Obviously there are more
arithmetic rules that we may use to optimize let expressions. In
Fig.~\ref{rules} we present the rules given in~\citep{strategicAG}.

\begin{figure}
\begin{minipage}[t]{.75\textwidth}
\begin{align}\label{eq:rules}
add(e, const(0)) \rightarrow e \\
add(const(0), e) \rightarrow e \\
add(const(a), const(b)) \rightarrow const(a+b) \\
sub(e1, e2) \rightarrow add(e1, neg(e2))\\ 
%\end{align}
%\end{minipage}
%\begin{minipage}[t]{.45\textwidth}
%\begin{align}\label{eq:rules}
neg(neg(e)) \rightarrow e \\
neg(const(a)) \rightarrow const(-a) \\
var(id) \mid (id, just(e)) \in env \rightarrow e
\end{align}
\end{minipage}
\caption{Optimization Rules}
\label{rules}
\end{figure}

In our definition of the function |expr|, we already defined
rewriting rules for optimizations $1$ and $2$. Rules $3$ through $6$
can also be trivially defined in Haskell:

\begin{code}
expr :: Exp -> Maybe Exp
expr (Add e (Const 0))          = Just e 
expr (Add (Const 0) t)          = Just t  
expr (Add (Const a) (Const b))  = Just (Const (a+b)) 
expr (Sub a b)                  = Just (Add a (Neg b))    
expr (Neg (Neg f))              = Just f           
expr (Neg (Const n))            = Just (Const (-n)) 
expr _                          = Nothing
\end{code}

% we may look at Eric's paper to see how he mention rule 7.

Rule $7$, however, is context dependent and it is not easily expressed
within strategic term rewriting.  In fact, this rule requires to first
compute the environment where a name is used (according to
the scope rules of the |Let| language). We will return to this rule in
Section~\ref{ztr:sec3}.

Having expressed all rewriting rules from $1$ to $6$ in function |expr|, now we need
to use our strategic combinators that navigate in the tree while
applying the rules. To guarantee that all the possible optimizations
are applied we use an |innermost| traversal scheme. Thus, our
optimization is expressed as:

\begin{code}
opt'  :: Zipper Let -> Maybe (Zipper Let)
opt'  t =  applyTP (innermost step) t  
      where step = failTP `adhocTP` expr
\end{code}
\label{code:opt'}

Function |opt'| combines all the steps we have built until now. We
define an auxiliary function |step|, which is the composition of the
|failTP| default failing strategy with |expr|, the optimization
function; we compose them with |adhocTP|.  Our resulting
Type-Preserving strategy will be |innermost step|, which applies
|step| to the zipper repeatedly until a fixed-point is
reached.  The use of |failTP| as the default strategy is required, as
|innermost| reaches the fixed-point when |step| fails.
If we use |idTP| instead, |step| always succeeds, resulting
in an infinite loop. We apply this strategy using the function 
|applyTP :: TP m -> Zipper a -> m (Zipper a)|, 
which effectively applies a strategy to a zipper. This function is defined
in our library, but we omit the code as it is trivial. 

Next, we show an example using a Type-Unifying strategy. We define a function |names| that collects all defined
names in a |Let| expression. First, we define a function |select| that focuses on
the |Let| tree nodes where names are defined, namely, |Assign| and
|NestedLet|. This function returns a singleton list (with the defined
name) when applied to these nodes, and an empty list in the other
cases.

\begin{code}
select :: List -> [String]
select (Assign    s _ _)  = [s]
select (NestedLet s _ _)  = [s]
select _                  = []
\end{code}

Now, |names| is a Type-Unifying function that traverses a given |Let|
tree (inside a zipper, in our case), and produces a list with the
declared names. 

\begin{code}
names  :: Zipper Let -> [String]
names  r = applyTU (full_tdTU step) r  
       where step = failTU `adhocTU` select
\end{code}

The traversal strategy influences the order of the names in the
resulting list. We use a top-down traversal so that the list result
follows the order of the input. This is to say that |names t_1 ==
["a","c","b","c"]| (a bottom-up strategy produces the reverse of this
list).


As we have shown, our strategic term rewriting functions rely on
zippers built upon the data (trees) to be traversed. This results in
strategic functions that can easily be combined with a zipper-based
embedding of attribute grammars~\citep{scp2016,memoAG19}, since both
functions/embedding work on zippers. In the next section we present in
detail the zipping of strategies and \gls{AG}s.

\begin{comment}
There is, however, a key difference between both libraries: while
Strafunski operates directly on the data to be traversed, Ztrategic
relies on a zipper built upon that data. This results in strategic
functions that can easily be combined with a zipper-based embedding of
attribute grammars, since both functions/embedding work on zippers. We
will present in detail the zipping of AGs and strategies in
Section~\ref{sec4}.
\end{comment}
