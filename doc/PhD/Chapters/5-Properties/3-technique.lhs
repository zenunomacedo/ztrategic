In this section, we discuss our technique for property-based testing of attribute grammars. We do this by introducing an example and gradually building on it, and concepts are introduced and explained as they are needed. In Section~\ref{prop:implementation}, we follow the same steps for a concrete |Haskell| implementation. 

\subsection{The RepMin Attribute Grammar}

When discussing attribute grammars, the \texttt{RepMin} problem~\citep{Bird84} has been a recurring example in the literature~\citep{SPS99,kiama,scp2016}, mainly due to its simplicity and potential of expressing recursive algorithms, which attribute grammars tend to be used for. We can define a binary leaf tree grammar as such: 

\lstset{
%  basicstyle=\itshape,
  basicstyle=\small,
  xleftmargin=0em,
  literate={->}{$\rightarrow$}{2}
%           {α}{$\alpha$}{1}
%           {δ}{$\delta$}{1}
           {Tree1}{{$Tree_1$}}{4}
           {Tree2}{{$Tree_2$}}{4}
           {Tree3}{{$Tree_3$}}{4}
           {epsilon}{{$\epsilon$}}{1}
}

\begin{comment}
\begin{lstlisting}
Tree -> Fork Tree Tree
Tree -> Leaf integer
\end{lstlisting}
\end{comment}
\begin{lstlisting}
Tree -> Tree Tree
Tree -> integer
\end{lstlisting}
Meaning that a \lstinline{Tree} can either unfold into two subtrees, or into a single integer which would be a leaf in the tree. Now, we can build trees using this grammar. 
%The following trees follow this structure: 
The trees shown in Figure~\ref{fig:leaftree} follow this structure.
\begin{comment}
\begin{figure}[h]
%bandaid fix for now
\vspace{-1cm}
\hspace*{-2cm} 
\begin{minipage}{0.2\textwidth}
\centering
\digraph[scale=0.6]{simple}{
root->fork1;
root->v3;
fork1->v1;
fork1->v2;
root [label="Tree"];
fork1 [label="Tree"];
v1 [label="4"];
v2 [label="0"];
v3 [label="18"];
}
\end{minipage}
\begin{minipage}{0.2\textwidth}
\centering
\digraph[scale=0.6]{simple2}{
root->fork1;
root->v3;
fork1->v1;
fork1->v2;
root [label="Tree"];
fork1 [label="Tree"];
v1 [label="0"];
v2 [label="0"];
v3 [label="0"];
}
\end{minipage}
\vspace{-1cm}
\end{figure}
\end{comment}

%% MUST GENERATE GRAPHS IN A SEPARATE PASS, ELSE LATEXMK EATS DIRT GENERATING THEM INFINITELY
\begin{figure}[h]
\begin{minipage}{0.5\textwidth}
\hspace{3cm}
\includegraphics[width=0.6\textwidth,keepaspectratio]{simple.pdf}
\end{minipage}
\begin{minipage}{0.5\textwidth}
\includegraphics[width=0.6\textwidth,keepaspectratio]{simple2.pdf}
\end{minipage}
\caption{Examples of binary leaf trees.}
\label{fig:leaftree}
\end{figure}

Let us now extend this definition with the \texttt{RepMin} problem in mind. Given an existing tree, we want to replace all integer values in it with the global minimum value on the tree. In the trees shown previously, the right tree is the result of computing \texttt{RepMin} on the left tree. Next, we add attributes to our grammar, which represent computations on values of said grammar. We will keep the definitions separate for simplicity of reading. 
\begin{lstlisting}
Tree1 -> Tree2 Tree3 [ Tree1.locMin = min(Tree2.locMin, Tree3.locMin) ]
Tree -> integer   [ Tree.locMin = integer ]
\end{lstlisting}
The attribute \texttt{locMin} represents the local minimum value of a tree. Logically, the local minimum of a tree \lstinline{Tree1} that contains two subtrees \lstinline{Tree2} and \lstinline{Tree3} is the minimum between the local minimum values of those subtrees. The local minimum of a tree that only contains an integer value is the value itself. This is a synthesized attribute - \textbf{synthesized attributes} are calculated by using the information from the children nodes relative to the current node. This is how programming languages usually behave - when operating on a given node, we have access to the data in the subtrees inside it.

This attribute is enough to compute the local minimum at any given location in a tree. However, as the name states, this computation is local, and the global minimum may be located somewhere on the tree not visible from a given location. Thus, we define a global minimum attribute.
\begin{lstlisting}
Root -> Tree [ Root.globMin = Tree.locMin;
               Tree.globMin = Root.globMin ]
Tree1 -> Tree2 Tree3 [ Tree2.globMin = Tree1.globMin; 
                    Tree3.globMin = Tree1.globMin ]
Tree -> integer
\end{lstlisting}
Note the addition of a new production ``\lstinline{Root -> Tree}'' in which a starting state is defined for any tree. We use this production to identify the starting state, where the local minimum is in fact the global minimum as it encompasses the whole tree. Thus, the global minimum of \lstinline{Root} is the local minimum of the tree that it holds. For any other production, the global minimum value of any \textit{child} is the same as the global minimum of their \textit{parent} - it could be said that they \textit{inherit} the attribute from the left-hand side of the production. \textbf{Inherited attributes} are calculated by using information that is \textit{inherited} from the parent nodes relative to the current node. In many programming settings, this is a complex idea to express and it is counter-intuitive to the language.

We round up the solution to this problem with a higher-order attribute~\citep{VSK89} that will build a new tree out of an existing tree, with each integer being replaced with the global minimum of the tree. 
\begin{lstlisting}
Root -> Tree      [ Root.replace = Root Tree.replace ]
Tree1 -> Tree2 Tree3 [ Tree1.replace = Tree Tree2.replace Tree3.replace]
Tree -> integer   [ Tree.replace = Tree Tree.globMin ]
\end{lstlisting}

For each production, the \texttt{replace} attribute will return a new tree, similar in shape to the original tree. For the first two productions, the subtrees in the new returned tree are obtained by computing \texttt{replace} on the original subtrees. In the last production, the leaf of the tree is dropped and the value of \texttt{globMin} is used instead. 

We have defined an attribute grammar for binary leaf trees and the \texttt{locMin}, \texttt{globMin} and \texttt{replace} attributes, with each being relatively simple yet their conjugation defines a more complex algorithm. Of course, we could implement \texttt{replace} without the use of attribute grammars relying instead on purely iterative or recursive code, but the attribute grammar approach is desirable due to its clarity and simplicity. 

\subsection{Property-based Testing}
\label{prop:pbt}
To validate the correctness of our attribute grammar, we define properties that they should obey. Note that \gls{PbT} does not guarantee that the artifact being tested is bug-free. In practical terms, it is unreasonable to test every single possible value on the input range, so we test for many values until it is deemed ``good enough''. If any property fails, then we know the artifact contains a bug, but if no properties fail, we can only be more assured that there are no bugs, but we can never be certain. 

For our binary leaf tree attribute grammar, we expect computing \texttt{replace} only changes the values inside the leaves, and not the leaves themselves. We can quickly define an attribute for counting the number of leaves on a tree: 
\begin{lstlisting}
Root -> Tree      [ Root.count = Tree.count ]
Tree1 -> Tree2 Tree3 [ Tree1.count = Tree2.count + Tree3.count ]
Tree -> integer   [ Tree.count = 1 ]
\end{lstlisting}

Next, we define a property which states that the number of leaves in the original tree is the same as the number of leaves in the tree after applying \texttt{replace}.
\begin{equation}
\label{eq:count}
\forall t \in Tree: t.count = t.replace.count
\end{equation}

If we can validate this property, then we know that the attribute \texttt{replace} does not change the number of leaves in the tree. However, this property uses a quantifier to define all trees \textit{t}. As mentioned before, in practical terms it is unfeasible to generate what would be an infinite amount of trees. 
%Moreover, we use \textit{generators} to randomly generate  large set of  Abstract Syntax Trees  (AST) - corresponding to inputs to the attribute evaluators. Such generators may use attribute values to steer the AST generation process. In fact, our generators may produce not only syntax-correct trees (as regular PbT does), but also semantically correct ASTs. That is, AST that follow the static semantics specified by the underlying attribute grammar.
Instead, let us define generators, which will produce random trees to be used in the properties. 
\begin{lstlisting}
genRoot = Root genTree
genTree = oneof [
        Tree genTree genTree,
        Tree genInteger 
    ]
\end{lstlisting}

To generate a tree, we must first have the starting \texttt{Root} production given by \texttt{genRoot}. Then, we generate the rest of the tree with \texttt{genTree}, which will randomly choose one of two alternatives: either generate a junction of two subtrees also generated recursively by \texttt{genTree}, or generate a leaf with an integer generated by \texttt{genInteger}. With the \texttt{oneof} function picking one option at random, and \texttt{genInteger} generating a random integer value, this is enough to theoretically generate all possible leaf trees. This is still a naive implementation of a generator - in Section~\ref{prop:implementation} we dive into more complex generators. 
We can also incorporate the usage of some attributes inside the generator, to use the information they provide to better guide the data generation. One such example would be to use the \texttt{count} attribute to aid in generating balanced trees. In Section~\ref{prop:circular}, we use an attribute to provide contextual information to a generator.
In practical terms, because computing resources are not infinite, properties are verified for a given number of randomly generated values or until a counterexample is found.

\subsection{Quantification on Specific Nodes}

Let us now define a property to verify that the global minimum in a tree is, in fact, the lowest value of the whole tree. One possible implementation of this is to validate that, for any node inside a tree, the \texttt{locMin} attribute must be greater than or equal to the \texttt{globMin} attribute. 
\begin{equation}
\label{eq:locmin}
\forall t \in Tree, \forall n \in \text{nodes}(t): n.locMin \geq n.globMin
\end{equation}
We use \texttt{nodes(t)} to represent all nodes inside \texttt{t}. In this property, we compare the attributes \texttt{locMin} and \texttt{globMin}, and we do this comparison for all existing nodes inside any tree \texttt{t}. Similarly, we can look at only the leaves, and compare the value in them with \texttt{globMin}. 
\begin{equation}
\label{eq:integerglobmin}
\forall t \in Tree, \forall l \in \text{leaves}(t): l.integer \geq t.globMin
\end{equation}
Once again, we use \texttt{leaves(t)} to represent all leaf nodes inside \texttt{t}. In fact, this pattern is common when writing properties: instead of looking at the tree as a whole, we want to test attributes in specific nodes of a tree, and for this, we wish to quantify over all specific types of nodes. 

While the previous properties relied exclusively on \textit{universal quantification} $(\forall)$, \textit{existential quantification} $(\exists)$ is also a powerful mechanism for writing properties. For example, we can define a property stating that at least one leaf of the tree must contain the global minimum value of the tree. 
\begin{equation}
\label{eq:globminInTree}
\forall t \in Tree, \exists n \in \text{leaves}(t): n.integer = t.globMin
\end{equation}

To implement these quantifiers, we use generators when data needs to be produced, such as for the generation of trees \texttt{t} in the previous properties. However, to quantify over all nodes of an existing tree, we cannot use generators - we do not want to create new data, instead we want to traverse over existing data. This could be implemented with typical recursive functions, which will have to be rewritten for each different setting / attribute grammar we consider. 

For this type of quantification, we use strategic programming. With this approach, the developer only has to write code for the specific data components that need to be handled, and all the data traversal and recursion schemes are managed automatically by the selected traversal strategy. In our example, we can use a strategy to traverse through all leaves on a tree ignoring the other nodes. We suggest the implementation of universal and existential quantifiers though strategic programming for easier writing of properties. We explore this implementation in Section~\ref{prop:strategies}. 

We round up this section by discussing other properties over leaf trees. Previously, we defined Property~\ref{eq:count} stating that the number of leaves in a tree does not change after applying \texttt{replace}. We can be more precise by stating that the entire shape of the resulting tree must be similar to the shape of the original tree. For this, we need an \texttt{isomorphic} function that compares the shapes of the trees. 
\begin{equation}
\label{eq:isomorphic}
\forall t \in Tree: \text{isomorphic}(t, t.replace)
\end{equation}

We also know that \texttt{replace} is expected to be idempotent, that is, applying \texttt{replace} once or twice should yield the same result. 
\begin{equation}
\label{eq:idempotent}
\forall t \in Tree: t.replace = t.replace.replace
\end{equation}

Finally, the tree produced by \texttt{replace} should have all values be replaced by the global minimum of the original tree, and therefore all the values of the leaves on the new tree must be equal. 
\begin{equation}
\label{eq:allvaluesequal}
%\forall t \in Tree, \forall n_1, n_2 \in \text{leaves}(t.replace), n_1.integer = n_2.integer
\begin{split}
%\forall t \in Tree, &\forall n_1, n_2 \in \text{leaves}(t.replace) : \\ 
%& n_1.integer = n_2.integer
\forall t \in Tree, \forall n_1, n_2 \in \text{leaves}(t.replace) : n_1.integer = n_2.integer
\end{split}
\end{equation}

%\begin{equation}    
%\begin{split}
%\forall n \in Let, \forall v \in vars(n) & :v.name \notin \text{nest}(v).dcli \\
%& \land \text{nest}(v).errs = \emptyset \\
%& \rightarrow v.name \in \text{nest}(v).dclBlock
%\end{split}
%\end{equation}