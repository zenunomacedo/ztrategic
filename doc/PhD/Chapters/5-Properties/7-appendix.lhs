\section{Generators}
\label{appendix}
\subsection{Generator for Let Expressions}
\label{appendix:gen}

\begin{code}
genLet :: [Name] -> Int -> Gen Let
genLet names n  =  do  size              <- choose (1, n)
                       (newNames, list)  <- genList names size
                       expr              <- genExp newNames
                       return $ Let list expr

genList :: [Name] -> Int -> Gen ([Name], List)
genList names 0  =  return (names, EmptyList)
genList names n  =  frequency  [  (1, genNestedLet  names  n)
                               ,  (4, genAssign     names  n)
                               ]

genNestedLet :: [Name] -> Int -> Gen ([Name], List)
genNestedLet names n  =  do  name         <-       genName `suchThat` (not . flip elem names)
                             nLet         <-       genLet names n
                             (new, list)  <-       genList (name:names) (n-1)
                             return (new, NestedLet name nLet list)

genAssign :: [Name] -> Int -> Gen ([Name], List)
genAssign names n  =  do  name          <-      genName `suchThat` (not . flip elem names)
                          nExp          <-      genExp names
                          (new, list)   <-      genList (name:names) (n-1)
                          return (new, Assign name nExp list)

genExp :: [Name] -> Gen Exp
genExp names  =  frequency $  [  (25, Add    <$>  genExp names <*> genExp names)
                              ,  (25, Sub    <$>  genExp names <*> genExp names)
                              ,  (50, Neg    <$>  genExp names)
                              ,  (50, Const  <$>  arbitrary)
                              ]
              ++ if null names   then  []
                                 else  [(50, Var <$> elements names)]
\end{code}

\subsection{Circular Generator for Let Expressions}
\label{appendix:circ}

\begin{code}
genRoot :: Gen Root
genRoot  =  sized genRootSized

genRootSized :: Int -> Gen Root 
genRootSized n  =  mdo  box_z <- Root <$> genLet n ((toZipper box_z).$1)
                        return z

genLet :: Int -> Zipper Root -> Gen Let
genLet n z  =  do  randomV  <-  choose (1, n)
                   zList    <-  genList randomV (z.$1)
                   zExp     <-  genExp (z.$2)
                   return (Let zList zExp)

genList :: Int -> Zipper Root -> Gen List
genList 0 z  =  return EmptyList
genList n z  =  frequency  [  (1, genNestedLet n z)
                           ,  (4, genAssign n z) 
                           ]

genAssign :: Int -> Zipper Root -> Gen List
genAssign n z  =  do  name     <-  genName
                      nExp     <-  genExp (z.$2)
                      list     <-  genList (n-1) (z.$3)
                      return (Assign name nExp list)

genNestedLet::Int -> Zipper Root -> Gen List
genNestedLet n z  =  do  named       <-  genName
                         nestedSize  <-  choose (1,5)
                         nLet        <-  genLet nestedSize (z.$2)
                         list        <-  genList (n-1) (z.$3)
                         return (NestedLet named nLet list)

genExp :: Zipper Root -> Gen Exp
genExp z  =  let decls = map (\(a,b,c) -> a) (env z)
             in frequency  [  (1, Add    <$>  genExp (z.$1) <*> genExp(z.$2))
                           ,  (1, Sub    <$>  genExp (z.$1) <*> genExp(z.$2))
                           ,  (5, Neg    <$>  genExp (z.$1))
                           ,  (5, Const  <$>  arbitrary)
                           ,  (5, Var    <$>  elements decls)
                           ]

\end{code}