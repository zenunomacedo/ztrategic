We will explore an implementation of attribute grammars, with corresponding generators and properties, in the |Haskell| programming language. The approach discussed in previous sections is not |Haskell|-specific - we expect languages capable of defining attribute grammars and properties to be capable of intertwining these concepts, and if strategic data traversal schemes are available, then the implementation of properties is expected to be more elegant. 
%say/itemize what we need exactly, maybe with examples: pattern matching, higher-order attributes allow more expressive properties, (generic?) recursion on trees to apply the properties, generators, properties themselves


\subsection{RepMin}
We begin by studying the RepMin problem discussed in the previous section, as its simplicity is useful to introduce the concepts we use without overloading the reader. We follow the same structure of the previous section, with focus on the implementation details in our framework. 

\subsubsection{Attribute Grammars}

%Synthesized attributes are calculated by using the information from the children nodes relative to the current node. This is how programming languages usually behave - when operating on a given node, we have acess to the data in the subtrees inside it. An example of a synthesized attribute would be the \texttt{locMin} attribute defined before, where only the subtrees are looked at to compute the local minimum. 

%Inherited attributed, however, are calculated by using information that is \textit{inherited} from the parent nodes relative to the current node. In many programming settings, this is a complex idea to express and it is counter-intuitive relative to the language. One example of an inherited attribute would be \texttt{globMin}, where most nodes inherit the \texttt{globMin} value of their parent node. 

We use the work of~\cite{zipperAG} to express attribute grammars as an embedding in |Haskell|. It relies on the zipper~\citep{thezipper} as a generic data traversal mechanism, which can move inside a data structure in any direction. For us, this means any data structure must be converted to a zipper before we access its attributes. Some tools from this library we will be using: 
\begin{itemize}
    \item \textbf{toZipper} and \textbf{fromZipper} - functions to convert any data value to a zipper and then back to the original type. 
    \item \textbf{.\$n} - function to access the \textit{n}$^{th}$ child of a node. 
    \item \textbf{parent} - function to access the parent node of the current node.
    \item \textbf{constructor} - function that visualizes the grammar production the zipper is pointing towards. 
    \item \textbf{lexeme} - set of various functions to read values the zipper is pointing towards. 
\end{itemize}
We note that the |constructor| and |lexeme| functions are boilerplate functions that are not included with the library and must be manually defined. However, they are trivial to write and we
%expect they should be possible to generate automatically, 
are also able to generate them automatically 
using meta-programming tools such as |Template Haskell|~\citep{templatehaskell}. 

First, we define a |Haskell| data type to represent the leaf trees used in the attribute grammar shown in the previous section. It shares many similarities to the grammar productions shown before. A |Tree| node can be either a root node representing the start of a tree, a fork node with two subtrees or a leaf node with one integer value. 
\begin{code}
data Tree  =  Root Tree 
           |  Fork Tree Tree 
           |  Leaf Int
\end{code}


Let us revisit the |locMin| attribute. We present an implementation of this attribute using this library: 
%[language=Haskell]
\begin{code} 
locmin :: Zipper Tree -> Int
locmin t  =  case constructor t of
                       CLeaf   -> lexeme t 
                       CFork   -> min (locmin (t.$1)) (locmin (t.$2))
\end{code}
Here, our zipper |t| is pointing towards a node whose type we check with the function |constructor|. If it is a leaf node, we return its |lexeme|, that is, the integer value stored in it. If it is a fork node, we recursively compute the |locmin| of the first and second child nodes which we access via |t.$1| and |t.$2|, and we return the minimum of these two values. 

Definitions of attributes using this library tend to follow this same structure. Next, we define |globMin| and |replace|. 

\begin{code}
globmin :: Zipper Tree -> Int
globmin t  =  case constructor t of
                        CRoot   -> locmin (t.$1)
                        CLeaf   -> globmin (parent t)
                        CFork   -> globmin (parent t)

replace :: Zipper Tree -> Tree
replace t  =  case constructor t of
                        CRoot   -> Root (replace (t.$1))
                        CLeaf   -> Leaf (globmin t)
                        CFork   -> Fork (replace (t.$1)) (replace (t.$2))
\end{code}

Our attribute grammar is complete, and the next step is to test it properly to ensure there are no bugs or any mistakes in our implementation. 

\subsubsection{Generators}

Before defining properties to test the correctness of our grammar, we need actual data values where the attributes can be computed. For this, we will be defining generators in QuickCheck~\citep{quickcheck}, a |Haskell| library for \gls{PbT} that includes tools for defining properties, generating values for those properties, and optimizing the counter-examples for properties that fail. 

We can use |QuickCheck| to define the previously-discussed generator as such: 
\begin{code}
genRoot  =  Root <$> genTree
genTree  =  oneof [
                      Fork <$> genTree <*> genTree,
                      Leaf <$> arbitrary 
    ]
\end{code}

This syntax is mostly similar to the example showcased before, but some operators are included so that the code type-checks correctly. 
%Nevertheless, the code should be understandable even without a deep understanding of Haskell. 
Instead of |genInteger|, we use |arbitrary|, which is the default generator |QuickCheck| attempts to use when no generator is specified. In this case, |arbitrary| will generate a random integer value. In fact, we can make our new generator the default generator for leaf trees with this declaration: 
\begin{code}
instance Arbitrary Tree where 
    arbitrary = genRoot
\end{code}
With this, |genRoot| becomes the generator that is used when |arbitrary| is required for leaf trees. However, this generator can be improved. Let us add a parameter |n| such that the higher |n| is, the larger the generated trees become. We replace the |oneof| function with the |frequency| function, which is similar but also accepts a weight value for each generator supplied to it. With this, we can now supply a value |n| to the weight of the generation of a fork node, and thus the higher |n| is, the higher the chance that a fork node is chosen to be generated. When a fork node is indeed generated, the value of |n| is lowered in the recursive calls
(we opt to do this by dividing it by 2). This also ensures that the generated tree is finite, as |n| will eventually reach 0 ensuring no more recursive calls are made. 
\begin{code}
genRoot n  =  Root <$> genTree n
genTree n  =  frequency [
                        (n, Fork <$> genTree (div n 2) <*> genTree (div n 2)),
                        (1, Leaf <$> arbitrary)
    ]

instance Arbitrary Tree where 
    arbitrary = sized genRoot
\end{code}
The value of |n| is produced by a library |sized| function which will produce increasingly big values when a large number of values need to be generated. 





\subsubsection{Quantifiers as strategies}
\label{prop:strategies}
We consider the implementation of quantifiers for properties as two separate problems: quantifiers over all trees, and quantifiers over a set of nodes inside a tree. For the former, the generators presented in the previous section are used as they are capable to produce any amount of trees, and if needed, we could define specialized generators that produce only certain types of trees. For the latter, we need to be able to operate or iterate over specific nodes on an existing tree. As an example, when we write $\forall n \in leaves(t)$, we need to have any mechanism to grab all the leaves in $t$. The baseline approach would be to define code that recursively traverses a tree, operating on it when a leaf is reached, and calling itself recursively on subtrees while no leaves are found. This approach would work as well, but then we'd need to define a similar function for each different problem we attempt to solve with attribute grammars. 

We solve this problem instead by using strategic programming~\citep{strategies}. With strategic programming, we only define the work to be done on the nodes that interest us, and generic recursion schemes will handle all data structure traversal automatically for us. We use the |Ztrategic|
%\cite{flops22} 
library, as it also builds upon zippers which we use for attribute grammars. 

First, we need to adapt our current code so that it supports strategies. We change the definition of our tree data type, asking the compiler to automatically derive the |Data| and |Typeable| classes. This means the compiler generates for us the tools for the data type to be handled as generic data, which the strategies require. We also need to define our tree as an instance of |StrategicData|, which \textit{can} be used to specify terminal symbols that are to be skipped in the traversals. We opt to use the default by-omission definition where there are no terminals defined in the language. 
\begin{code}
data Tree  =  Root Tree 
           |  Fork Tree Tree 
           |  Leaf Int
      deriving (Data, Typeable) 

instance StrategicData Tree
\end{code}

While we could use a custom strategy for each property we intend to define, we found it best to define two functions that use strategies internally and emulate the \textit{universal} and \textit{existential quantifiers}. Next, we present function |forallNodes|, which intends to emulate an \textit{universal quantifier}. 
\begin{code}
forallNodes p ast  =  let  astZipper  =  toZipper ast
                           step       =  failTU `adhocTUZ` p
                           props      =  applyTU (full_tdTU step) astZipper
                      in   conjoin props
\end{code}

This function receives two arguments: |p| represents the property to be tested in the nodes we care about, and |ast| represents the data type \gls{AST} we want to traverse. 

First, we create \textbf{astZipper}, a zipper on the given \gls{AST}. Then, we define \textbf{step} representing the concrete work to be done on each node, which is |failTU| joined with |p| (here, |adhocTUZ| is a combinator to join functions). This |step| function therefore applies |p|, and if it fails, it resorts to the default behavior which is |failTU| and does nothing. 

We will apply a strategy |full_tdTU| (full, top-down, type-unifying) where we apply |step| to each node of |astZipper|. Full means we traverse the whole tree, top-down is the order of traversal (which could be bottom-up instead), and type-unifying means we are gathering values from the \gls{AST} and unifying them into a list. The result of this will be \textbf{props}, a list of properties where each property was calculated by |p| in a node where it was able to operate. 

We end up with a list of properties, each calculated in a different node. If all properties in that list are true, then the conjunction of that list is also true. This represents \textit{universal quantification}, as the property represented by |p| is true in all applicable nodes. On the other side, if at least one of the properties in the list is true, then the disjunction of the list is also true, which represents \textit{existential quantification}, as property |p| is true in at least one node. 

Next, we show the code for |existsNode|, the equivalent of |forallNodes| for \textit{existential quantification}. The only change in terms of code is the replacement of |conjoin| for |disjoin|.

\begin{code}
existsNode p ast  =  let  astZipper  =  toZipper ast
                          step       =  failTU `adhocTUZ` p
                          props      =  applyTU (full_tdTU step) astZipper
                     in   disjoin props
\end{code}

With these, we are ready to begin writing properties on our leaf trees. For more complex properties, we can resort to writing custom strategies, but we expect to combat complexity of property writing by using the |forallNodes| and |existsNode| auxiliary functions. Ideally, these two functions would cover all necessities we have for quantification. 

Next, we define properties over leaf trees using the tools presented until now. 

\subsubsection{Properties over leaf trees}

In this subsection, we define the properties described in subsection~\ref{prop:pbt} with the tools presented before. With |QuickCheck|, while not mandatory, it is recommended to have the names of all properties start with |prop_|, which we follow. 

We begin by recalling property~\ref{eq:count}, where the number of leaves on a tree is the same when compared to the tree produced by its |replace| attribute. We have defined this attribute using the |Haskell| attribute grammar embedding, and we now define |count| as an attribute. 
\begin{code}
count :: Zipper Tree -> Int 
count t  =  case constructor t of 
                      CRoot -> count (t.$1)
                      CFork -> count (t.$1) + count (t.$2)
                      CLeaf -> 1 
\end{code}

Next, we define the property itself. There is a small nuance to consider here: the attributes operate on a zipper of a tree, and not the tree itself. Thus, we need to convert trees into their respective zippers before applying attributes. There are workarounds such as defining a function |count'| which transforms a value into a zipper and then applies |count|, but we use the original attributes for the sake of transparency. 

\begin{code}
prop_count :: Tree -> Bool
prop_count ast  =  count (toZipper t) 
                == count (toZipper (replace (toZipper t)))
\end{code}

%\discuss{MARCOS: shouldn't be more natural if the properties had type \texttt{Zipper Tree -> Bool}?}
The type of this property is |Tree -> Bool|, meaning it takes a |Tree| as an input and it outputs a boolean value representing whether the property passes. We do not need to worry about the input as the previously-defined generator will handle it for us. For the output, using boolean values is fine but there is a type designed specifically for this, named |Property|. This type can contain more information relating to the property besides its success, and it is required when writing complex properties. While it is not required for the properties we are defining, we future-proof our work by working with this type. Fortunately, the pre-defined function |property| converts a |Bool| value into a |Property|, so we can adapt our previous code to use this. 
\begin{code}
prop_count :: Tree -> Property
prop_count ast  =  property (count (toZipper t) 
                == count (toZipper (replace (toZipper t))))
\end{code}

With this, we have defined a property that is capable of generating several trees and testing if their number of leaves remain unchanged with the computation of |replace|. To test this property, we use a pre-defined function |quickCheck| that takes a property and tests it with random input values, in this case, using the generator we defined before. We note that, by default, the property will be tested 100 times. 
\begin{lstlisting}
quickCheck prop_count
+++ OK, passed 100 tests.
\end{lstlisting}

Let us now consider property~\ref{eq:locmin}, which states that, for any node, the value of |locmin| must be greater or equal to |globmin|. We will be using our |forallNodes| function defined previously to test all nodes on a given tree. First, we define an auxiliary function that details the properties to be tested on each node. 
\begin{code}
aux :: Tree -> Zipper Tree -> [Property]
aux _ z = [property (locmin z >= globmin z)]
\end{code}

This function receives two arguments: the |Tree| node it is operating on, as well as the zipper pointing towards it, and it returns a list of |Property|. More specifically, we ignore the node itself and use only the zipper |z| pointing to it to compute the attributes. Next, we test this property on all nodes. 

\begin{code}
prop_locMin :: Tree -> Property
prop_locMin ast = forallNodes aux ast
\end{code}

We now consider property~\ref{eq:integerglobmin}, which is fairly similar to the previous property. It states that all leaves must have their integer be greater than or equal to the value of |globMin|. The code is fairly similar to the previous property, but now we do not ignore the node as we did before, because we want to extract the integer value from it directly when it is a leaf. When the node is not a leaf, we return an empty list of properties, meaning that no properties are checked on the node. 

\begin{code}
prop_globMin :: Tree -> Property
prop_globMin ast = forallNodes aux ast

aux :: Tree -> Zipper Tree -> [Property]
aux  (Leaf n)  z  =  [property (n >= globmin z)]
aux  _         _  =  []
\end{code}

We now consider property~\ref{eq:globminInTree}, where we assert that at least one leaf in the tree must contain the minimum value computed by |globmin|. For this, we define a worker function that only operates on leaves, and for each one, compares the value of the leaf with |globmin|. We are checking if at least one node that obeys this property exists, so we use |existsNode| to write the property. 

\begin{code}
prop_globminInTree :: Tree -> Property
prop_globminInTree ast = existsNode aux ast 

aux :: Tree -> Zipper Tree -> [Property]
aux  (Leaf n)  z  =  [property (n == globmin z)]
aux  _         _  =  []
\end{code}

As for property~\ref{eq:isomorphic}, we require a simple |Haskell| function comparing the shapes of two trees. Then, for any generated tree in our property, that tree must be isomorphic with the result of computing \texttt{replace} on that tree.
\begin{code}
isomorphic :: Tree -> Tree -> Bool 
isomorphic  (Leaf _)      (Leaf _)      = True 
isomorphic  (Root x)      (Root y)      = isomorphic x y 
isomorphic  (Fork l1 r1)  (Fork l2 r2)  = isomorphic l1 l2 && isomorphic r1 r2
isomorphic  _             _             = False 

prop_isomorphic :: Tree -> Property
prop_isomorphic t = property (isomorphic t (replace (toZipper t)))
\end{code}

%idempotent replace 
In property~\ref{eq:idempotent} we state that |replace| is idempotent, meaning that using this attribute once or two times should yield the same result. 
\begin{code}
prop_idempotent :: Tree -> Property
prop_idempotent t = property (  replace (toZipper t) == 
                                replace (toZipper (replace (toZipper t))))
\end{code}

%forall n1 n2 in leaves of replace, n1.integer = n2.integer
Finally, for property~\ref{eq:allvaluesequal}, the implementation is a bit more tricky. Here, we state that all the values on the |replace| tree must be the same, and we validate this by comparing all values with all values, that is, for each value in the tree, we traverse the whole tree again and compare all values against it. 

We implement this with two usages of |forallNodes|, such that the first traversal (which we call |it1|) produces all $n1$ values in the property and the second traversal (which we call |it2|) produces all values of $n2$. 
\begin{code}
prop_allEqual :: Tree -> Property
prop_allEqual ast  =  let x = replace (toZipper ast) 
                      in forallNodes (it1 x) x
 
it1  x   (Leaf n1)  _  =  [ forallNodes (it2 n1) x ]
it1  _   _          _  =  []

it2  n1  (Leaf n2)  _  =  [ property (n1 == n2) ]
it2  _   _          _  =  []
\end{code}
Both iteration auxiliary functions only operate on leaves and ignore other nodes. Whenever |it1| finds a leaf, it runs |it2| through the whole tree again while holding the value of $n1$. Then, whenever |it2| finds a leaf, it compares its value $n2$ with the $n1$ value produced before. 

We conclude this section by testing all properties we defined. For this, the |QuickCheck| library provides |Template Haskell|~\citep{templatehaskell} tools for ease of testing - we can run all defined properties at once if we enable the |Template Haskell| flag and add the two following lines at the end of the code file: 
\begin{code}
return []
runTests = $quickCheckAll
\end{code}

Executing |runTests| runs all the properties defined before. In fact, some of them fail due to a bug in |locmin|: the attribute we defined before is not total, and when |locmin| is calculated on a |Root|, the code crashes. The fix is that the |locmin| of the root of a tree is the |locmin| of the subtree it holds. Since systems like |QuickCheck| generate large amounts of varied inputs and test all defined properties with ease, they are extremely powerful for the detection of edge cases and inconsistencies in code. 

Next, we consider a different attribute grammar, for a small language of |Let| expressions. 


\subsection{The Let Language}

The |Let| language is a small language that allows the definition of Let expressions, which several programming languages (such as |Haskell|) support. In it, there are declarations of names that contain either arithmetic expressions or nested |Let| expressions, and then a final expression to evaluate. 
%We refer to existing work with Let expressions~\cite{Macedo2024} where their attribute grammars are defined and discussed in detail. 
We refer to Chapter~\ref{ztr:sec3} where their attribute grammars are defined and discussed in detail. 
Next, we show an example of a |Let| expression. 
\begin{lstlisting}
let a = b + 0
    c = 2
    b = let c = 3 in c + c
in a + 7 - c
\end{lstlisting}

Note that variables do not need to be declared before use, as long as they are in the same scope. In this example, the variable $b$ is used before its declaration later in the scope. This example is a valid |Let| program, where all variables that are used have been declared in their scope, and no variables are declared twice in the same scope. 

Next, we show a grammar for this language. 
\begin{lstlisting}
Root -> Let 

Let -> 'let' List 'in' Exp 

List -> Name '=' Let List 
List -> Name '=' Exp List 
List -> epsilon

Exp -> Exp '+' Exp 
Exp -> Exp '-' Exp 
Exp -> '-' Exp 
Exp -> Name 
Exp -> integer 

Name -> string
\end{lstlisting}
And the respective Haskell data type representation. 
\begin{code}
data Root   =  Root Let
               deriving (Data, Typeable)

data Let    =  Let List Exp
               deriving (Data, Typeable)

data  List  =  NestedLet  Name Let List
            |  Assign     Name Exp List
            |  EmptyList
               deriving (Data, Typeable)

data Exp    =  Add   Exp Exp
            |  Sub   Exp Exp
            |  Neg   Exp
            |  Var   Name
            |  Const Int
               deriving (Data, Typeable)

instance StrategicData Root
\end{code}
We already include the boilerplate needed for strategic traversal of this data type as it will be useful later when using quantification on properties. 
%Next, we describe attributes we have defined for this grammar - we omit them for brevity, but we provide a replication package with all the code and examples of this paper. 
Next, we recall some previously-defined attributes for this grammar, as well as new attributes. They are available at the repository of this work~\citep{ztrategicRepo}.
\begin{itemize}
    \item \textbf{dcli} - Declarations visible when entering the current production. For a language where names must be declared before use, this would correspond to the environment available for that production. 
%suggest we hide dclo as it only causes confusion / how is it different from env
%    \item dclo declarations outgoing 
    \item \textbf{env} - A set of all declared names that are known at the current production, which we name environment. This differs from \textit{dcli} as any names declared later in the same scope are still part of the environment. 
    \item \textbf{dclBlock} - A set of all names that are declared in the current nesting level only.  
    \item \textbf{errs} - A set of all semantic errors in a Let expression. We define as a semantic error in this context a use of an undeclared name, as well as multiple declarations of the same name in the same scope.  
    \item \textbf{opt} - A higher-order attribute that refactors a |Let| expression via optimization of its arithmetic expressions. 
    \item \textbf{eval} - An attribute that evaluates a |Let| expression to an integer result. This attribute assumes that the expression it is applied on contains no errors or loops. 
\end{itemize}
We will be using these attributes to write properties for |Let| expressions. 


\subsubsection{Generator}

Defining a generator for |Let| expressions is not an easy task when compared to defining a generator for leaf trees. For starters, the data type is more complex; however the added difficulty derives mainly from |Let| expressions having certain semantics that should be obeyed. We can generate random strings for the variable names, we need to ensure that we only use variables that were actually declared before. 

One possible solution is to generate |Let| expressions with errors. If we do, then it becomes more complicated to test our attributes - for example, we are never able to test |eval| as it requires a well-formed expression. 

An alternative solution is to still generate |Let| expressions with errors, but then computing the |errs| attribute to list all errors, and solve them before finishing the generation. To solve the errors, we simply add declarations of variables that aren't declared before use, and remove duplicated declarations. While it is a viable solution, it is an unnatural two-step process and it also throws off attempts of controlling the size of the generated expression, as it will grow or shrink when solving the errors. 

%\todo{JNM: Do we include the code for this? Seems like a lot of code+text to explain a generator, and we will need this again later for the circular generator}

\begin{comment}
This will cause some clutter in the code, as now most generators will need to receive an additional argument which is the available names. They will also need to return two values, which is what they generated and the updated list of available names. We begin by using \texttt{sized} to produce a number that will later represent the maximum size of any list of declarations in a Let expression. 

\begin{lstlisting}
genRoot = Root <$> sized (genLet [])
\end{lstlisting}

For our \texttt{genLet}, we generate a random integer value between 1 and $n$, which represents the size of the list of declarations in the Let expression. This $n$ value is provided by \texttt{sized} so we know it will be increasingly larger when generating large amounts of expressions. Then, we call \texttt{genList}, passing it both the list of available names and the size we just generated. From this, we get both the updated list of available names and the \texttt{List} component of the Let. 

\begin{lstlisting}
genLet :: [Name] -> Int -> Gen Let
genLet names n = do
    size <- choose (1, n)
    (newNames, list) <- genList names size
    expr <- genExp newNames
    return $ Let list expr
\end{lstlisting}
\end{comment}

We opt to define a generator where we pass around the list of generated names that are possible to be used. This generator is included in Appendix~\ref{appendix:gen}, and it will be used throughout the next section for the generation of values for properties. 
While the code in this generator does end up becoming more cluttered, with lists of names being passed around and updated accordingly, at least we ensure that the |Let| expressions we generate are semantically correct. We refer to subsection~\ref{prop:circular} for an elegant circular solution using attributes for the generation of |Let| expressions. 

\subsubsection{Properties}

We describe several properties over |Let| expressions in this subsection, in both formal syntax and the respective |Haskell| code. While in the leaf trees examples there were no restrictions on the values we consider, here we assume the values we consider are part of the language of valid |Let| expressions.  

In practice, this means the |Let| expressions are being produced by our |Let| expression generator which \textit{should} only produce valid expressions. We can validate this by writing a property where we use the |errs| attribute to check the errors of all |Let| expressions, which should always be empty if they are valid. 
%prop_errors
%For any Let n in the of Let expressions, the set of errors (errs) must be empty:
\begin{equation}
\label{eq:errs}
\forall n \in Let : n.errs = \emptyset
\end{equation}
\begin{code}
prop_errors :: Root -> Property
prop_errors t = property (null (errs (toZipper t)))
\end{code}
We use the Haskell |null| function to check if a list is empty, which we expect the list of errors to be. 

We know that the |env| attribute should produce a set of all declared names in a |Let| expression. We can define a property where all variables inside a |Let| must be contained inside its environment as computed by this attribute. 
%prop_nameInEnv
%For any Let node n, the name defined within n must be contained in the environment (env(n)):
\begin{equation}
\label{eq:nameInEnv}
\forall n \in Let, \forall v \in \text{vars}(n) : v.name \in n.env
\end{equation}
\begin{code}
prop_nameInEnv :: Root -> Property
prop_nameInEnv t = forallNodes aux t 

aux :: Exp -> Zipper Root -> [Property]
aux  (Var v)  z  =  [property (v `varIn` env z)]
aux  _        _  =  []
\end{code}
To implement this property, we once again resort to the |forallNodes| construct. Here, we check all |Var| nodes, which is where variables are both declared and used, and we compare the string |v| stored within with the result of computing the environment |env| at this node. We do this comparison with auxiliary function |varIn| which checks if a given variable is stored inside an environment set. 

We recall the previously-mentioned |dclBlock| attribute. It should compute, at a given node, the environment of variables declared only in the current nesting level. Therefore, we expect it to be a subset of |env|, which is the environment of all variables visible at the current node. We define a property where we quantify over all |Let| nodes inside a |Let| expression, as those nodes represent the start of a new nesting level in an expression. For each, we ensure that |dclBlock| is computing a subset of |env|.

%prop_localInGlobal
%for any node n in the Let construct, the set of declarations in the block (dclBlock) of n is a subset of the environment (env) visible from node n
\begin{equation}
\forall n \in Let, \forall l \in \text{lets}(n) : l.dclBlock \subseteq l.env    
\end{equation}
\begin{code}
prop_localInGlobal :: Root -> Property
prop_localInGlobal t = forallNodes aux t

aux :: Let -> Zipper Root -> [Property]
aux _ z = [ property ( dclBlock z `isIn` env z ) ]
\end{code}
As before, we resort to the |forallNodes| construct to find all |Let| nodes inside an expression, and we use an auxiliary function |isIn| to compare the two sets we compute. 

A refactoring is a source-to-source transformation that preserves the semantics of the original program. The |opt| attribute defines arithmetic refactorings/optimizations on |Let| expressions, and |eval| computes the result of a |Let| expression. We expect that |opt| should change the shape of the expression but not the semantics, and thus computing |eval| before and after |opt| should yield the same result. 

%prop_sameEval
%%Note that we have 3 different opt functions: circular (fixed point) opt, strategic opt, and strategic block-by-block opt
%%also note that this breaks because some Lets will have loops FOR THE CIRCULAR GENERATOR ONLY
%%also note that FOR CIRCULAR ONLY we need timeouts to deal with loops
%For any Let node n, the evaluation of the node should be equal to the evaluation of the optimized version of the node:
\begin{equation}
\label{eq:sameEval}
\forall n \in Let : n.eval = n.opt.eval
\end{equation}
\begin{code}
prop_sameEval :: Root -> Property
prop_sameEval t = property (eval (toZipper t) == eval (toZipper (opt (toZipper t))))
\end{code}

This property seems simple on a surface level, but depending on the used generator, there are some considerations we have to take when implementing it. If the |Let| expression contains errors, or if there are loops inside it (a simple example would be declaring |x = x|), our current implementation of |eval| loops. We could resort to mechanisms provided by |QuickCheck| to fail a property if it takes too long to compute, but with a generator producing only well-formed expressions, the attribute works flawlessly. 

We know that the environment should contain all declared variables. As such, whenever we find a variable, if it is not on the environment, then we know we have found an error. This property relates to property~\ref{eq:nameInEnv}, which states all names must be in the environment; here we state that if a name is not in the environment, then that's an error. We can express this in the form of a logical implication: 

%prop_errorIsInErrs
%%Forgot to implement the strategy for this... only the worker function
%For any Let node n, if the name defined in n is not in the environment (env(n)), then the set of errors (errs(n)) is not empty:
\begin{equation}
\forall n \in Let, \forall v \in \text{vars}(n) : v.name \notin v.env \rightarrow v.errs \neq \emptyset
\end{equation}
\begin{code}
prop_errFound :: Root -> Property 
prop_errFound t = forallNodes errFound t

errFound :: Exp -> Zipper Root -> [Property]
errFound  (Var v)  z  =  [ not (v `varIn` env z) ==> not (null (errs z)) ]
errFound  _        _  =  []
\end{code}

To implement this property, we use an arrow to denote implication. Internally, |QuickCheck| will run the pre-condition of the implication, and if it fails, the test case is discarded. When a test case is discarded, |QuickCheck| will attempt to generate another input and test the property again, eventually giving up if too many test cases are discarded. The implication arrow builds the |Property| type by itself, and requires this type as a return value because a simple boolean can report success or failure, but not the discarded cases. We note that, for generators of well-formed |Let| expressions, the pre-condition will always fail and thus |QuickCheck| gives up on testing this property. 

For the next property, we refer to |dclBlock| again. Any name that exists in a |Let| expression must be declared appropriately. This property states that for any variable, if a name is not declared in any nesting level above the current one, and there are no errors, then the variable must be declared in the current nesting level. To express the environment coming in from nesting levels above the current one, we use the |dcli| attribute of the start of the current nesting level, which we denote by $nest(n)$. 

%prop_localNames
%%chatgpt didnt really enjoy creating this one! 
%For any node n in a Let, if the name defined in n is not contained in the ingoing declarations (dcli) at the beginning of the current nesting block for that name, and the nesting block does not have errors, then the name defined in n must be contained inside the declarations block (dclBlock) of the current nesting block for that name
%\begin{equation}
%\forall n \in \text{Let} : \text{name}(n) \notin \text{dcli}(\text{nest}(n)) \land \text{errs}(\text{nest}(n)) = \emptyset \rightarrow \text{name}(n) \in \text{dclBlock}(\text{nest}(n))
%\end{equation}
\begin{equation}    
\begin{split}
\forall n \in Let, \forall v \in \text{vars}(n) & :v.name \notin \text{nest}(v).dcli \\
& \land \text{nest}(v).errs = \emptyset \\
& \rightarrow v.name \in \text{nest}(v).dclBlock
\end{split}
\end{equation}
\begin{code}
prop_localNames :: Root -> Property
prop_localNames t = forallNodes localNames t

localNames::Exp -> Zipper Root -> [Property]
localNames (Var v) z  =  let l = nest z in 
                      [ null (dcli l) && null (errs l) ==> v `varIn` dclBlock l ]
localNames _       _ = []
\end{code}

To implement this, we use an auxiliary function |nest| to move the zipper |z| upwards until it reaches the start of the nesting block, which we label |l|. Then, we apply our attributes on this variable |l| we just created, and we write an implication as we did in the previous attribute. 

Finally, we run all properties with |QuickCheck| using the same |runTests| function defined before, which will test all properties found in the source code file. Due to the use of implications in some properties, |QuickCheck| gives up on property |prop_errFound| after discarding 1000 tests, and passes |prop_localNames| but discards around 500 tests during execution. In an example run, it took several minutes to fully test properties |prop_sameEval| and |prop_localNames|, which we attribute to the generator producing input |Let| expressions with extremely large sizes. It should also be stated that these properties were tested on an interpreter, and compiled code is expected to run much faster. Nevertheless, the focus of \gls{PbT} is not optimization of runtime, and we only mention these values to caution about the possible impact of defining generators without too much care about input sizes.  
%props that fail: 
%prop_sameEval fails with 10sec timeout after 50 tests (all would pass with no timeouts)
%prop_errFound gives up after discarding 1000 tests
%prop_localNames discarded around 500 tests but passed 



\subsubsection{Circular generator}
\label{prop:circular}

It can be a complicated task to define a generator for a specific data type. As shown before, defining a generator for trivial types such as leaf trees can be trivial in itself, but things get more complicated when several data types intertwine, and when the generator must obey any special rules defined by the language being modeled by the data type. This is our case: a |Let| expression might contain values of types |Root|, |Let|, |List| and |Exp|, and the scoping rules of the language must be obeyed. 

To aid us in the correct generation of |Let| expressions, we can consider the usage of the attributes we have resorted to before. When generating a usage of a variable, we need to ensure that the variable we are using is defined beforehand. Ideally, we could just compute the environment at that stage with the |env| attribute, and extract a random variable from it. However, to compute this attribute, we need to have the entire |Let| expression already generated, which we do not have because we need to compute the attribute. 

We solve this problem by defining a circular generator. We generate a |Let| expression with our generator, and we feed this result to the generator itself for it to calculate attributes. This sounds counter-intuitive and may at first look like a deadlock, but the |Haskell| programming language has lazy evaluation by default, and this enables circular programs to terminate when built correctly. 
In this case, it will be possible for the generator to produce the structure of a |Let| expression and to generate almost all values inside it correctly. When it needs to calculate an attribute (which requires a complete |Let| expression), the evaluator skips this temporarily and goes on to build the rest of the |Let| expression. Effectively, there are \textit{temporary holes} in the computation that are left to be solved later. If the program is not completely circular and it is possible to "tie the knot", the evaluator will eventually fill those holes. 
%For our generator, we know that the \texttt{env} attribute ignores all variable usages and only cares about variable declarations, so it will eventually be able to use a full Let expression even if it has \textit{holes} in the variable usages. 
Next, we show the circular generator's main function |genRoot|. Here, we again use |sized| to generate integer |n| with increasingly bigger values, ensuring the generator can produce both small and big |Let| expressions. Then, we generate a |Let| expression with |genLet|, append the |Root| constructor at the start and assign it to variable |z|. Finally, we return the variable |z| which contains the full |Let| expression this generator should produce. 

\begin{code}
genRoot :: Gen Root
genRoot  =  sized (\n -> mdo
                      z <- Root <$> genLet n ((toZipper z).$1)
                      return z)
\end{code}
%
Note that this code is circular - to produce the value stored in variable |z|, we must first compute |genLet|, which in itself depends on |z|. In fact, it does not use |z| directly, instead this value is transformed into a zipper and we pass its first child onto |genLet|. The logic is that the generator will always have a zipper pointing to the value it is currently generating, so here |genLet| is generating a |Let| while also having a zipper pointing to it. This is useful when we need to compute attributes because this zipper always contains the full |Let| expression, which we have not finished generating yet, almost like peeking into the future. 

Next, we show the generator for arithmetic expressions |genExp|. It receives a zipper pointing to itself as an argument. 

\begin{code}
genExp :: Zipper Root -> Gen Exp
genExp z  =  let decls = map (\(a,b,c) -> a) (env z)
             in frequency [
                                    (1, Add    <$>  genExp (z.$1) <*> genExp(z.$2)),
                                    (1, Sub    <$>  genExp (z.$1) <*> genExp(z.$2)),
                                    (5, Neg    <$>  genExp (z.$1)),
                                    (5, Const  <$>  arbitrary),
                                    (5, Var    <$>  elements decls)]
\end{code}
We define a variable |decls| which extracts all variable names from the environment, which we compute with attribute |env|. Here, we are computing an attribute over a zipper that we are still generating, but the lazy machinery is able to delay actually computing |env| and consequently |decls| until the entire |Let| expression is generated and the attribute is finally able to be computed. 

We use the |frequency| combinator to generate one of various possible types of arithmetic expressions, some of which are recursive. For example, to generate a sum of two expressions, |Add| is joined to the result of calling |genExp| recursively twice, to which we pass the first and second child nodes of the zipper, respectively. We can generate a sum, subtraction or negation of expressions, a constant integer value or a use of a variable. When we attempt to generate a use of a variable, we select a random name in |decls| as the variable name.  

We include the full definition of this generator on Appendix~\ref{appendix:circ}. We test the previously defined properties with this new generator, and two properties raise problems with this generator. 

Because this generator uses |env| to find usable names for variables, loops are generated frequently. For example, consider |x = 1 + _| where the underline represents what is being generated. Here, |x| is a valid name which appears in |env|, so it can be generated, resulting in a loop. When we have loops, we cannot calculate the |eval| attribute and therefore property~\ref{eq:sameEval} will hang, unless we define a time limit in which case it will fail. 

We are ensuring we do not have undeclared variable usage errors by only using variables in the environment. However, we do not have safeguards against duplicated declaration errors, and if the variable names for declarations are generated naively, property~\ref{eq:errs} can very occasionally fail due to these errors. This can be fixed with a more comprehensive variable name generator that does not produce duplicates. 