

%\todo{from/slightly adapted from: A Theory of Name Resolution:}

Naming is a pervasive concern in the design and implementation of
programming languages. Names identify declarations of program entities
(variables, functions, types, modules, etc.) and allow these entities
to be referenced from other parts of the program. Name resolution
associates each reference to its intended declaration(s), according to
the semantics of the language. Name resolution underlies most
operations on languages and programs, including static checking,
translation, mechanized description of semantics, and provision of
editor services in \gls{IDE}s. Resolution is often complicated, because it
cuts across the local inductive structure of programs (as described by
an abstract syntax tree). For example, the name introduced by a |Let|
node in an \gls{AST} representing a functional program may be referenced by
an arbitrarily distant node. Languages with explicit name spaces lead
to further complexity; for example, resolving a qualified reference in
|Java| requires first resolving the class or package name to a context,
and then resolving the member name within that context. But despite
this diversity, it is intuitively clear that the basic concepts of
resolution reappear in similar form across a broad range of
lexically-scoped languages.

Resolving names can often be complex, as a name introduced by an
assignment node in an \gls{AST} may be referenced by an arbitrarily distant
node. Moreover, different languages use different scope rules which
may require algorithms having to perform multiple \gls{AST} traversals to
validate whether a program obeys the scope rules of the language.
Although, very similar concepts of name analysis reappear across a
broad range of programming languages, there is no modular and reusable
technique that can provide an off-the-shelf approach for name analysis.


Complex name analysis algorithms can be conveniently specified using
\gls{AG}s~\citep{Knuth68}. In fact, name analysis was in
the genesis of Knuth novel work on \gls{AG}s~\citep{Knuth90}. Expressing the
name analysis algorithm in a (\gls{AST}) representation of a full general
purpose or domain-specific language, however, may lead to complex \gls{AG}
specifications: \gls{AST}s of real languages are complex data structures
consisting of a large set of types/nonterminal symbols and
constructors/productions, and although most of such
constructors/productions may not influence the name analysis algorithm
they do have to be included in a classical \gls{AG}
specification\footnote{A similar situation occurs if we express name
analysis in a functional programming setting as a recursive function
over the data type of the original/large AST.}. 
\gls{HOAG}s~\citep{hoag91} provide a perfect setting to
handle these cases: when a computation cannot be easily expressed in
terms of the inductive structure of the underlying tree, a better
suited tree structure can be computed before. Thus, under the
higher-order extension of \gls{AG}s, it is possible to specify the
transformation of the large/complex \gls{AST} of the original language into a
smaller/simpler \gls{AST} of a \gls{DSL} tailored for expressing name analysis
within the \gls{AG} setting as proposed by Knuth.

To allow modular and extensible definition of name analysis
algorithms, we rely on our previous work where we introduced a shallow
embedding of \gls{AG}s via functional zippers~\citep{scp2016,Macedo2024}. In this
embedding we are able to extend the semantics of our name resolution
\gls{DSL} with new scope rules by reusing existing rules (i.e., attributes)
and by adding new ones. In fact, we already developed generic \gls{AG} rules
that express the semantics of |Haskell|-like and |C|-like name analysis
semantics. These \gls{AG}s are defined in separate modules, can be
incrementally extended, and easily reused.


In this chapter we build on our previous work and we propose an
automated approach for name analysis based on the
following contributions:


\begin{itemize}


\item First, we define a generic technique to easily allow name
analysis designers to specify which nodes/constructors of the \gls{AST}
 do \textit{define}, \textit{use} and
\textit{introduce} new nested scopes in their language. This is the
only information a name analysis developer has to provide to have
a full implementation for their language. %This technique is presented
%in Section~\ref{if:interfaceapi} and its use in expressing name
%resolution of the |Haskell| language in
%Section~\ref{if:haskellanalysis}.


%Based on this information, the generic traversal function
%automatically maps the original AST into an higher-order 
%Domain Specific Language (DSL) AST.

\item Secondly, we introduce a generic function that traverses the
\gls{AST} representing the original program's source
code, and builds an \gls{AST} that represents a program in a 
\gls{DSL}
tailored for name analysis. This generic function is
implemented with generic zippers~\citep{thezipper} and it works for any
heterogeneous tree types~\citep{genericZipper}. Thus, our function maps
the \gls{AST} representing any source language into our name analysis
\gls{AST}. The attribute instances of those trees are evaluated by the
attribute evaluator provided by our executable embedded \gls{AG}s.
%We present this in Section~\ref{if:astToBlock}.

\item Thirdly, we also introduce a generic bidirectional
mechanism to allow the access to attributes of the higher-order tree
from the first order one. Because all our techniques use a generic
mechanism to navigate on trees - zippers - we use navigation
instructions (i.e., paths) to be able to move attributes from the
higher-order tree back to the original one. Thus, our technique is
able to report invalid declarations/uses in the source code location
they occur (that is, in the source \gls{AST}/program), and not as a
synthesized attribute of the name resolution higher-order \gls{AST}.


\gls{HOAG}s provide a component-based style of
programming~\citep{Saraiva02}. However, they have a severe limitation:
attribution rules defined in the (first-order) \gls{AST} can not access
attributes of the higher-order one. The bidirectional mechanism we
introduce in this chapter is the first technique to offer \gls{HOAG} this
expressiveness. %Our technique is presented in
%Section~\ref{if:blockToAst}.

\item Finally, we validate our technique by implementing name
resolution for the |Haskell| language. The full implementation consists
of 60 lines of |Haskell|: it just marks the nodes of the |Haskell|
\gls{AST}~\footnote{\url{https://hackage.haskell.org/package/haskell-src}}
that declare and/or use a name, and a nested scope. Our generic
techniques are able to traverse the source code |Haskell| \gls{AST}, build the
name resolution higher-order tree, run the (higher-order) attribute
evaluator to detect errors, and port the errors marked in the
higher-order tree back to the source code one. %We discuss our
%implementation details in Section~\ref{if:haskellanalysis}, while the
%complete name resolution code is included in Appendix~\ref{sec:fullhaskell}.



\end{itemize}


%The remain of this paper is structured as follows: