\section{The full Haskell Name Analysis implementation} 
\label{appendix:fullhaskell}

\begin{code}
instance Scopes HsModule where
    isDecl t = case constructor t of
                 CHsPVar     -> True
                 CHsFunBind  -> True
                 CHsConDecl  -> True 
                 CHsRecDecl  -> True 
                 _           -> False
    isUse t = case constructor t of
                 CHsVar        -> True
                 CHsVarOp      -> True 
                 CHsQVarOp     -> case lexeme_HsQVarOp t of 
                                    Special _  -> False
                                    _          -> True
                 CHsConOp      -> True 
                 CHsQConOp     -> case lexeme_HsQConOp t of 
                                    Special _  -> False
                                    _          -> True
                 CHsPApp       -> True 
                 CHsPInfixApp  -> True
                 _             -> False
    isBlock t = case constructor t of
                 CHsLet           -> True
                 CHsMatch         -> True
                 CHsGuardedRhss   -> True
                 CHsUnGuardedRhs  -> True
                 CHsAlt           -> True
                 _                -> False
    getUse t = case constructor t of
                CHsVar        -> hsQName2Str $ lexeme_HsVar t
                CHsVarOp      -> hsName2Str  $ lexeme_HsVarOp t
                CHsQVarOp     -> hsQName2Str $ lexeme_HsQVarOp t
                CHsConOp      -> hsName2Str  $ lexeme_HsConOp t
                CHsQConOp     -> hsQName2Str $ lexeme_HsQConOp t
                CHsPApp       -> hsQName2Str $ lexeme_HsPApp t
                CHsPInfixApp  -> hsQName2Str $ lexeme_HsPInfixApp_2 t
    getDecl t = case constructor t of
                CHsFunBind  -> case head $ lexeme_HsFunBind t of 
                                 HsMatch _ hsname _ _ _ -> qualify t $ hsName2Str hsname
                CHsPVar     -> qualify t $ hsName2Str $ lexeme_HsPVar t
                CHsConDecl  -> qualify t $ hsName2Str $ lexeme_HsConDecl_2 t
                CHsRecDecl  -> qualify t $ hsName2Str $ lexeme_HsRecDecl_2 t
    setUse t s = case constructor t of
                CHsVar       -> setHole (HsVar (appendHsQNameStr (lexeme_HsVar t) s)) t
                CHsVarOp     -> setHole (HsVarOp (appendHsNameStr (lexeme_HsVarOp t) s)) t
                CHsQVarOp    -> setHole (HsQVarOp (appendHsQNameStr (lexeme_HsQVarOp t) s)) t 
                CHsConOp     -> setHole (HsConOp (appendHsNameStr (lexeme_HsConOp t) s)) t 
                CHsQConOp    -> setHole (HsQConOp (appendHsQNameStr (lexeme_HsQConOp t) s)) t 
                CHsPApp      -> setHole (HsPApp (appendHsQNameStr (lexeme_HsPApp t) s) 
                    (lexeme_HsPApp_2 t)) t 
                CHsPInfixApp -> setHole (HsPInfixApp (lexeme_HsPInfixApp t) 
                    (appendHsQNameStr (lexeme_HsPInfixApp_2 t) s) (lexeme_HsPInfixApp_3 t)) t
    setDecl t s = case constructor t of
                    CHsPVar -> setHole (HsPVar $ appendHsNameStr (lexeme_HsPVar t) s) t
                    CHsPatBind -> case lexeme_HsPatBind_2 t of
                        HsPVar hsname -> setHole (HsPatBind (lexeme_HsPatBind t) (HsPVar $ 
                            appendHsNameStr hsname s) (lexeme_HsPatBind_3 t) 
                                                      (lexeme_HsPatBind_4 t)) t
                    CHsFunBind -> case head $ lexeme_HsFunBind t of 
                        HsMatch q hsname w e r -> setHole (HsMatch q (appendHsNameStr 
                                        hsname s) w e r : tail (lexeme_HsFunBind t)) t
                    CHsConDecl -> setHole (HsConDecl (lexeme_HsConDecl t) (appendHsNameStr 
                        (lexeme_HsConDecl_2 t) s) (lexeme_HsConDecl_3 t)) t
                    CHsRecDecl -> setHole (HsRecDecl (lexeme_HsRecDecl t) (appendHsNameStr 
                        (lexeme_HsRecDecl_2 t) s) (lexeme_HsRecDecl_3 t)) t
    initialState = const ["error", "show", "$"]
\end{code}

\section{The Haskell program example}

\subsection{Original Source Code} 
\label{appendix:originalprogram}
\begin{code}
module Example where
import Language.Haskell.Parser 

data Example  =  ParseOk String 
               | ParseOk Int

parseExample x =  case parseModule example2 of
                        ParseOk x  -> x
                        err        -> error $ show err
\end{code}

\subsection{Same Source Code, after Name Analysis} 
\label{appendix:analysedprogram}

\begin{code}
module Example where
import Language.Haskell.Parser

data Example  =  ParseOk String
               | ParseOk <- [Duplicated declaration!] Int

parseExample x  =  case
                    parseModule <- [Undeclared use!] 
                    example2 <- [Undeclared use!] of
                        ParseOk x  -> x
                        err        -> error $ show err
\end{code}