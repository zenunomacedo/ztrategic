
In this section, we describe how we implemented name analysis of the |Haskell| language using our interface. 
%We do not show the complete implementation, but it is included in the \hyperref[sec:replpackage]{replication package} we provide at the end of the paper. 
The complete implementation is provided in Appendix~\ref{appendix:fullhaskell}.

We express the name resolution of Haskell in an \gls{AST} provided by library \texttt{haskell-src}\footnote{\url{https://hackage.haskell.org/package/haskell-src}}. This library not only includes the \gls{AST} data types, but also the associated parser and unparser/pretty-printer. As expected when handling a real language, the data types used to model the entire |Haskell| language are large and complex. Most declarations and statements are polluted with |SrcLoc|, which is a type representation of their location in the source code file. However, only in a small subset of these constructors names and new scopes are defined.
Next, we show a tiny fragment of the data types defining |Haskell| abstract syntax that are relevant for name resolution.

%\smallskip
\begin{code}
data HsPat  =  HsPVar    HsName  
            |  (...) 

data HsDecl  =  HsFunBind    [HsMatch]
            |   (...)

data HsMatch  =  HsMatch   SrcLoc   HsName   [HsPat]   HsRhs   [HsDecl]
\end{code}
%\smallskip

In fact, we only consider a small number of  constructors for declarations, out of a total 116 constructors. For example, constructor |HsFunBind| will declare one function, and |HsPVar| is used to declare a name whenever a pattern is matched. Thus, we can straightforwardly define |isDecl| as follows:

%\smallskip
\begin{code}
instance Scopes HsModule where
    isDecl ag   = constructor ag `elem` [CHsFunBind,CHsPVar]
\end{code}
%\smallskip

Whenever we need to expand this definition, we just need to add more constructors to our list of valid constructors. As long as the correct constructors are identified, our system handles all data structure traversal, name analysis and error reporting for free.

To build the |Block| \gls{DSL} and then reflect errors on the original \gls{AST}, our interface needs to be able to find and change the names themselves, in this case the |HsName| types inside |HsPat| and |HsDecl|. The interface contains a default implementation that uses the first |String| found from the node that contains the declaration/usage. This logic would work for the |HsPVar| node, but for the |HsFunBind| node, the first occurring string is part of the node's source code location |SrcLoc|. 
Thus, we define getters and setters for these nodes: 

%\smallskip
\begin{code}
    getDecl t    =  case constructor t of
                       CHsPVar    ->  hsName2Str (lexeme_HsPVar t)
                       CHsFunBind ->  case head (lexeme_HsFunBind t) of 
                                         HsMatch _ hsname _ _ _ -> hsName2Str hsname

    setDecl t s  =  case constructor t of
                       CHsPVar    ->  setHole (HsPVar (appendHsNameStr (lexeme_HsPVar t) s)) t
                       CHsFunBind ->  case head (lexeme_HsFunBind t) of 
                                         HsMatch q hsname w e r ->
                             setHole (HsMatch q (appendHsNameStr hsname s)
                             w e r : tail (lexeme_HsFunBind t)) t

\end{code}
%\smallskip

This code is comparatively more complicated as it has to do many things: remove values from the zipper with the |lexeme_| auxiliary functions, remove the name strings from the more complicated |HsName| type using auxiliary function |hsName2Str|, and modify the name strings from said |HsName| type using auxiliary function |appendHsNameStr|. Nevertheless, these are still much simpler functions in the context of performing name analysis on a real programming language. 

We omit definition of |isUse| as it is very similar to |isDecl|. Let us now look into declaring new nesting levels with |isBlock|. 

%\smallskip
\begin{code}
    isBlock ag   = constructor ag `elem` [CHsUnGuardedRhs,CHsMatch]
\end{code}
%\smallskip

The constructor |HsUnGuardedRhs| is always used to signal the start of the right-hand side of a function, which should always be a new nesting level for name analysis, so we declare that node to obey |isBlock|. The constructor |HsMatch| declares one line of a function, so it also signals the start of a new nesting level.

%This implementation of name analysis in |Haskell| using our interface is still very incomplete, of course. 
%\discuss{It should be extended to all relevant nodes of the AST, by following the same step as shown above for all missing constructors. At any time, an user can test their implementation by generating the |Block| program and comparing it to the original program - a correct use of the interface should always generate a |Block| that correctly represents the name defitions, uses, and scope levels of the original program. } 
%The appropriate course of action would be to read the documentation of the AST being used, and to add all relevant nodes into the interface. Additionally, we recommend testing the resulting name analysis system with examples, and if the name analysis does not seem correct, looking into the generated |Block| program helps to understand what is missing or behaving incorrectly in the interface. 


We include the full name resolution implementation for |Haskell| in Appendix~\ref{appendix:fullhaskell}. 
%of our complete implementation (available in the \hyperref[sec:replpackage]{replication package})
We use an excerpt of the |Haskell| code we use to parse examples as an example program in itself to test our implementation, which we include in Appendix~\ref{appendix:originalprogram}. We use the interface's |applyErrors_a68| function on the generated \gls{AST} for the |Haskell| test program, and we pretty-print the resulting \gls{AST} in Appendix~\ref{appendix:analysedprogram}.
For this example, we get one duplicated declaration error, and two undeclared use errors. The duplicated declaration occurs because the constructor |ParseOk| is declared twice. The undeclared use errors correctly point out that variables |parseModule| and |example2| are used but not defined - in the original code, |parseModule| comes from an imported package and |example2| is defined elsewhere, but we do not provide these declarations in this example. 

We have no inherent support for qualified names in our interface. We attempted to solve this problem at the user-level by making all declarations of a given name |x| also declare |ModuleName.x|, and this partially solves the problem, but this also means that if we find a duplicate definition of |x|, we also find a duplicate definition of |ModuleName.x|, thus reporting duplicate declaration errors twice.

Thus, we propose an extension to the |Block| \gls{DSL} to include qualified names:

%\smallskip
\begin{code}
data It     =  DeclQ    Name   ModuleName   Directions
            |  Decl     Name   Directions
            |  Use      Name   Directions
            |  Block    Its
\end{code}
%\smallskip

The new |DeclQ| constructor will behave similarly to |Decl|,
but also contain the name of the module where the declaration is
made. If we straightforwardly extend the data types of the Block
language we lose modularity, since attributes such as |dcli| and |env| will
have to consider this new constructor of the language. However, we may
use the deforested attribute evaluators proposed in~\cite{SS99} where
no data type is defined and \gls{AST} explicitly constructed. In such a
setting we can extend our \gls{DSL} with new constructors without losing
modularity.


The |Haskell| programming language has several extensions which are not supported by the library we use in this chapter, but they are supported by the \texttt{haskell-src-exts}\footnote{\url{https://hackage.haskell.org/package/haskell-src-exts}} package; we did not fully test whether our name resolution supports all such extensions. Nevertheless, we are convinced that, very much like qualified names, we can easily incorporate any extension in our setting.

%For a real programming language such as |Haskell|, with (for the \texttt{haskell-src} package) 116 constructors across 30 data types, being able to perform name analysis with a 60-line implementation strikes us as a success, and we intend to improve our system's usability and expressiveness to iron out any remaining faults such as the ones described above.