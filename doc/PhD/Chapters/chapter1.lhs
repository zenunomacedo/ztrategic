%!TEX root = ../template.tex
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% chapter1.tex
%% NOVA thesis document file
%%
%% Chapter with introduction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%include lhs2TeX.fmt
%include polycode.fmt
%include Config/formats.lhs 


\typeout{NT FILE chapter1.tex}%

\chapter{Introduction}
\label{sec:intro}

Since Algol was designed in the 60s as the first high-level programming language~\citep{algol68}, languages have evolved dramatically. With the evolution of hardware capabilities, performance has become less of a concern, with other factors such as readability and ease of use rising in priority. The continuous development of open-source, easily accessible libraries enables developers to reuse code that has been tried and tested over the years, with continuous improvements by the community. Languages can be functional, imperative, object-oriented, logic, or a mix of these paradigms. They can be strongly-typed or weakly-typed. They can be interpreted or compiled, or even compiled during interpretation with Just-in-time compilation. Some languages have different available compilers, with each having some advantages over others. To write code, software developers can get the help of tools, frequently integrated in \gls{IDE}, to report and fix bugs, code smells, typos, and to suggest and auto-complete excerpts of code. More advanced tools, such as GitHub Copilot, employ artificial intelligence to write entire blocks of code for the software developer. 

Overall, there is a myriad of tools available for the software developer to facilitate their tasks. In the same vein, the coding practices themselves have evolved tremendously since the early times. Students learn programming languages, but they also learn programming patterns~\citep{designpatterns}, code smells, software testing and quality and all sorts of good practices. Programmers are conscious of not copy-pasting the same block of code repeatedly, and instead create a reusable function for that code to be invoked easily. They discuss whether to open curly brackets in the line the function is declared, or in the following line instead. Large scale projects include coding guidelines that define the naming conventions for variables. 

The previously described efforts and evolutions converge into one idea: We should be able to write more functionality with less code, and whatever code we write should be elegant, easy to understand, and expressive. In this thesis, we elaborate on this idea, by building tools and concepts that software developers can use to write more concise code, with as much expressiveness as possible, 
and at the same time not impacting negatively on performance. 

%In fact, modern languages offer powerful syntactic and semantic mechanisms that improve programmers productivity. In response to such developments, the software language engineering community also developed advanced techniques to specify such new mechanisms.
%\section{Generic Programming}
Generic programming techniques focus on abstracting parts of the problem away, such that the implementation is not specific for a single domain, covering instead a set of domains that share similarities. Quoting~\cite{musser1988generic}, "Generic programming centers around the idea of abstracting from concrete, efficient algorithms to obtain generic algorithms that can be combined with different data representations to produce a wide variety of useful software". 

To the modern software developer, this notion is familiar: in |Java|, the default collections libraries are parameterized with the type the collection is stored, for example, an ArrayList does not exist by itself, but one can have an 
%|ArrayList<String>| or an |ArrayList<Integer>|. 
|arrayliststring| or an |arraylistinteger|. 
Here, the ArrayList and its operations are generic algorithms, and they eliminate the need to have a specialized collection for strings and another one for integer values. Similarly, we can have a list of any values of type |a|, denominated |[a]|, in |Haskell|. In |OCaml|, with |Set.Make(String)| we use the |Set.Make| functor to produce a new module, which is a set that stores strings. The \textit{Modern C++ Design: Generic Programming and Design Patterns Applied} book~\citep{Alexandrescu_2001} is considered a very influential book for |cplusplus|, and it focuses on generic programming patterns. 

%template haskell
Software developers can also use metaprogramming tools to write code that generates code. We define the distinction between generic programming and metaprogramming: 
\begin{itemize}
    \item \textbf{Generic Programming} refers to algorithms and data types that are available for the software developer to parameterize with their concrete data. It is a subset of metaprogramming. Automatically deriving a hashtable collection for a provided data object is an example of generic programming. 
    \item \textbf{Metaprogramming} refers to writing code that can manipulate other code as data, often dynamically generating, modifying, or transforming code at runtime or compile-time. It colloquially tends to refer to the generation of large, less-structured software, possibly performing several tasks at once, as opposed to generic programming. The automatic instrumentation of an executed program to guide test coverage measurements by tools like JaCoCo~\citep{hoffmann2009jacoco} is an example of metaprogramming.  
\end{itemize}
However, metaprogramming is considered to have a large barrier of entry for its use. Software developers will need to have at least some knowledge of the internal \gls{AST} of the language they are manipulating. This problem is partially mitigated by the usage of programmable syntax macros~\citep{quasiquotes}, sometimes referred to as "quasi-quotations", in which users do not need to work on the \gls{AST} directly and instead write the code that the \gls{AST} would represent. One some metaprogramming systems such as Template Haskell~\citep{templatehaskell}, the error messages produced by the compiler are hard to understand, therefore making the debugging of metaprograms harder. 


The work on this thesis is presented in the |Haskell| programming language. The following works are important contributions in generic programming for this language:
\begin{itemize} 
%generics library
\item The Scrap your Boilerplate~\citep{syb1} work reduces boilerplate by providing a |Haskell| design pattern that can write boilerplate code once only, and have it adapt to the data structure that needs to be manipulated. For this, the work relies on a |cast| operator, which attempts to safely type-cast any data to a specific type. With this foundation, it presents combinators to generically navigate, query and modify data structures, operating only on data that can be correctly |cast| to the relevant types. 

%lens
\item Lenses~\citep{lemmer2015haskell}, also known as functional references, are a set of tools that allow for the focus on a particular part of a data structure. A lens can be used to identify and read a specific field of a data structure, but also correctly update it, making it very helpful for the manipulation of complex data structures - having a lens pointed at the field we care about also removes all the boilerplate of data structure traversal to reach that field. 
Furthermore, bidirectional transformations~\citep{fosterbx} define pairs of transformations such that one transformation --- frequently a lens --- extracts a view from the original data structure, and another transformation takes the original structure and an updated view, and updates the original structure accordingly. 
The |lens| |Haskell| package contains a complete implementation as well as many guides and tutorials for the software developer to get accustomed to the library. Other languages, notably |JavaScript|, have seen lenses gain popularity, for example to bridge the gap between data stored on a server and values embedded in HTML forms~\citep{lensesweb}. 
%\todo{Citar Benjamin Pierce sobre BX que sao relevantes } - Done

%zipper
\item Zippers were introduced by~\cite{thezipper} to represent a tree together with a subtree that is the \textit{focus} of attention. During a computation the focus may move left, up, down or right within the tree. Generic manipulation of a zipper is provided through a set of pre-defined functions that allow access to all the nodes of a tree for inspection or modification.
A generic implementation of this concept is available as the \textit{generic zipper} |Haskell| library~\citep{genericZipper}, which works for both homogeneous and heterogeneous data types. The |Clojure| programming language provides native support for creation and manipulation of zippers.  

\item The |Traversable| type class is part of the |Haskell| |Prelude| base library, being readily available to any |Haskell| software developer. It defines a \textit{class of data structures that can be traversed from left to right, performing an action on each element}. Any data type that implements this type class will have many data structure traversal functions at its disposal, and several pre-defined data types such as lists and arrays are already defined as |Traversable|. With this type class, developers can specify code that traverses data structures left-to-right without needing to reason about the internal specifics of the data structure itself. 
\end{itemize}

%traversable is bad because...
%Visitor
%
A book titled "Design patterns: Elements of reusable object-oriented software"~\citep{designpatterns} details various design patterns for object-oriented software; among them is the \textit{Visitor} pattern. In this pattern, the \textit{visitor} class type defines |visit| methods tasked with performing an operation specific to the object type for each object of the \textit{element} class type. The \textit{element} class type defines an |accept| method, which will take a \textit{visitor} as an argument and use it to perform work on itself. There are no defined rules for data structure traversal, and this logic can be defined on the \textit{visitor}, the \textit{element}, or outside the pattern. In theory, this pattern is very useful as it allows for the easy definition of new \textit{visitor} objects to perform different operations on an existing data structure.  

We contrast the \textit{Visitor} pattern to the concept of strategic programming. In strategic programming, strategies define the traversal scheme over the data structure to be manipulated, and they can be extended with behavior specific for certain data components~\citep{strategicdesign}. We elaborate on strategic programming in more detail in Chapter~\ref{rw:strategic}. For both the \textit{Visitor} pattern and strategic programming, adding new operations is trivial: in the \textit{Visitor} pattern, we add a new \textit{visitor} class, and in strategic programming, we add a new data manipulation function. However, when the original data structure is changed, the \textit{Visitor} pattern is only kept correct by modifying all visitors, whereas strategic programming techniques typically need no further changes --- this is because in strategic programming, data structure traversal is hidden inside of generic, reusable strategies, and in the \textit{Visitor} pattern, it is explicitly coded. This also implies a much higher volume of boilerplate for the usage of the \textit{Visitor} pattern when compared to strategic programming. 

Strategic programming techniques tend to separate data manipulation from data structure traversal, such that each of these components is as pure as possible. For this, the data manipulation operations are \textit{mostly} context-independent, being only aware of the node they are operating on. One objective of the work presented in this thesis is to extend the expressiveness of strategic programming techniques by adding support for context-dependent operations, specifically though the \gls{AG} formalism. 
%This has been done before
%\todo{Nao dizer diretamente que já foi feito, em vez disso dizer que é preliminary work e/ou criticar}
%~\citep{kiama, silver, spoofax}, and we elaborate on \gls{AG}s, and compare our work to the relevant state-of-the-art libraries, in Chapter~\ref{sec:sota}.
We elaborate on \gls{AG}s, and compare our work to relevant state-of-the-art libraries that also integrate strategic programming and \gls{AG}s~\citep{kiama, silver, spoofax}, in Chapter~\ref{sec:sota}. 

In this thesis, we explore the combination of strategic programming and \gls{AG}s, which we denominate as the \gls{SAG} formalism. These software development techniques integrate well together: \gls{AG}s can integrate with strategies to enable contextual data manipulation, and strategies can lessen the copy-rule boilerplate of \gls{AG}s. In Section~\ref{sec:strategicAG}, we show we can access attribute values from strategies and synthesize attributes via strategies. 
We build this atop the generic Zipper data structure, as it possesses the generic capabilities required to implement strategies, as well as the expressiveness required for the definition of \gls{AG}s. 

\section{Problem Statement}
\label{sec:rq}

%With the overall evolution of generic programming techniques in many programming languages, strategic programming has been comparatively been left behind. There are no strategic programming systems available in most programming languages, and the strategic programming systems that are available for the average software developer are generally not well-known, and definitely not widely used and supported by the community. 
With the overall evolution of generic programming techniques across many programming languages, both strategic programming and \gls{AG}s have been comparatively left behind. Strategic programming lacks widespread support, with no established systems available in most programming languages. 
%The systems that do exist are often obscure, under-documented, 
The systems that do exist are often large, complex and difficult to maintain, 
and not widely adopted or supported by the developer community. Similarly, while \gls{AG}s have a long history and proven utility for tasks such as compiler construction and tree-based computations, they have not kept pace with modern programming trends. Many developers remain unaware of their potential, and practical frameworks for working with \gls{AG}s are either absent or limited in scope, making them inaccessible to the broader community. Together, these paradigms remain underutilized, despite their potential to provide elegant and powerful solutions for a variety of computational problems.

Many factors could be attributed to the unpopularity of strategic programming techniques and \gls{AG}s, despite their theoretical strengths. 
%For example, it could be argued that because they are generic techniques, they are not expressive enough to deal with the concrete, specific cases the software developers want to solve, and therefore they are useless in the real world. 
For example, it could be argued that strategic programming, as a generic technique, often lacks the expressiveness needed to address the concrete, domain-specific problems that software developers face, making it appear impractical for real-world use. 
Similarly, \gls{AG}s, while powerful for defining declarative specifications and enabling efficient computations on structured data, are sometimes perceived as too rigid or unintuitive for dynamically evolving software systems.
%
%It could be argued that they are too complex to use, possibly due to the lack of available material to guide users, or that the available tools are not mature enough. 
Both paradigms may also suffer from perceptions of excessive complexity, compounded by the lack of accessible educational resources and mature tools to support their use.
%

Ultimately, strategic programming techniques and \gls{AG}s, both individually and in combination, have potential to be much more than what they are. 
As such, this thesis aims to push the boundaries of strategic programming and \gls{AG}s by providing techniques, tools and applications for these paradigms. 

We summarize the main research question we answer in this thesis as such: 

\rgbox{Main Research Question}{Can an expressive, reliable and powerful Strategic Attribute Grammar system be designed based on zippers?}

%\todo{See notes}
\begin{comment}
conseguimos expressar estrategica usando zippers? 

conseguimos combinar com AG embed + strategic? 
(ficamos com o melhr dos dois mundos) 

memo -> conseguimos combinar memoAG com estrategias? 

can this be applied in real languages / language enginnering tasks for real languages 
(a apontar para os exemplos que foram feitos)
\end{comment}

We divide this main research question into several research questions of smaller scope, such that answering those research questions bridges us closer to an answer of the main research question. 

%1 
The generic zipper allows for simple navigation in any heterogeneous data structure using primitives for movements in the left, right, up and down directions, while focusing on a single node of the data structure. Furthermore, it is possible to read the node the zipper is focusing on, query it and transform it. While strategic programming frameworks provide much higher level constructs, the internal machinery of such frameworks is expected to behave similarly to the generic zipper, moving in a data structure, querying and transforming it. Thus, we ask the following question: 

\rqbox{Research Goal 1}{Is it possible to express strategic programming on top of the generic zipper mechanism?}

%2
In a strategic programming environment, the software developer typically works in a context-free setting. As an abstraction, this helps isolate the problem to be solved from any irrelevant contextual information, such as traversal on the \gls{AST} being manipulated. However, this also limits expressiveness, as the developer is not able to implement context-specific algorithms. 

The work of \cite{scp2016} provides a zipper-based embedding of \gls{AG}s and their extensions as first class citizens. \gls{AG}s are a powerful formalism for the specification of context-dependent behavior, precisely what a pure strategic programming environment lacks, and so we raise the following question:

\rqbox{Research Goal 2}{Can a zipper-based strategic programming framework be reliably combined with a zipper-based embedding of attribute grammars?}

%3

The zipper-based embedding of \gls{AG}s has been extended with a memoization mechanism~\citep{memoAG19}, which can be applied to selected attributes, skipping re-computation for attributes that are computationally intensive and repeatedly used. However, this memoization mechanism incurs changes in the \gls{AG} system itself, making our \gls{SAG} system unable to handle it out-of-the-box. 
This incompatibility results in the following question: 

\rqbox{Research Goal 3}{Are memoized Attribute Grammars possible to be integrated within a zipper-based Strategic Attribute Grammar system?}

%4 

Software development techniques and tools, as the name suggests, serve the final goal of the development of software. In this thesis, we argue in favor of \gls{SAG} techniques for software development, but many of the examples shown in both the literature and this thesis are academic examples, which are small by nature of showcasing features of the paradigm concisely. We concern ourselves with this by asking the following question: 

\rqbox{Research Goal 4}{Can large-scale language engineering tasks be reliably implemented with Strategic Attribute Grammar tools?}

\begin{comment}
In a strategic programming environment, the software developer typically works in a context-free setting. As an abstraction, this helps isolate the problem to be solved from any irrelevant contextual information, such as traversal on \gls{AST} being manipulated. However, this also limits expressiveness, as the developer is not able to implement context-specific algorithms. Thus, we ask the following question: 
%\rqbox{Research Goal 1}{Can generic programming tools improve their expressiveness with contextual information?}
\rqbox{Question 1}{Is it possible to properly use attribute grammars to handle contextual information in strategic programming tools?}

Developing software using strategic programming techniques contributes to the quality of the code by cutting unnecessary data structure traversal code, and the approach to the automatic traversal is also clearly stated in the strategy name. However, code quality encompasses much more: we can measure code quality of a software project by studying what code smells are present in that project. Moreover, code quality can also refer to actual bugs in code, which can be tracked by the usage of software testing techniques. None of these approaches are directly related to \gls{SAG}s, and so raise the following question:
\rqbox{Question 2}{Can Strategic Attribute Grammar tools be used to improve robustness and quality of software?}

Software development techniques and tools, as the name suggests, serve the final goal of the development of software. In this thesis, we argue in favor of \gls{SAG} techniques for software development, but many of the examples shown in both the literature and this thesis are academic examples, which are small by nature of showcasing features of the paradigm concisely. We concern ourselves with this by asking the following question: 
%
%\rqbox{Research Question 3}{Can real software development tasks be reliably implemented with strategic programming tools?}
\rqbox{Question 3}{Can large-scale software development tasks be reliably implemented with Strategic Attribute Grammar tools?}
\end{comment}

\section{Contributions}

We list the contributions made by this thesis:
\begin{itemize}

\item We present |Ztrategic|, an embedding of both strategic tree rewriting and attribute grammars in a zipper-based, purely functional setting. The embedding of the two techniques in the same setting has several advantages: First, we easily combine/zip attribute grammars and strategies, thus providing language engineers the best of the two worlds. Second, the combined embedding is easier to maintain and extend since it is written in a concise and uniform setting.
%This results in a very small library  which is able to express advanced (static) analysis and transformation tasks.
We show the expressive power of our library in optimizing |Haskell| |Let| expressions, expressing several |Haskell| refactorings and solving several language processing tasks for an Oberon-0 compiler.

\item This embedding of both strategic tree rewriting and attribute grammars has a severe limitation since it recomputes attribute values. We present a proper and efficient embedding of both techniques for |Ztrategic|. First, attribute values are memoized in the zipper data structure, thus avoiding their re-computation. Moreover, strategic zipper based functions are adapted to access such memoized values. In addition, we 
%increase the functionalities of strategic programming, enabling the definition of outwards traversals; i.e. outside the starting position.
define \textit{outwards traversal} strategies, providing new expressiveness that is only possible due to the usage of the zipper as the underlying tree-walk structure. 
%
%pyztrategic
%\item We showcase the expressive power of our embedding by also implementing it in |Python|, creating the |pyZtrategic| library. Similarly to |Ztrategic|, it relies on the zipper to define an embedding of both strategic tree rewriting and attribute grammars. 
We develop several language engineering problems in |Ztrategic|, and we compare it to well established strategic programming and attribute grammar systems, as well as |pyZtrategic|, an implementation of our embedding in |Python|. %Our results show that our library offers similar expressiveness as such systems, but, unfortunately, it does suffer from the current poor runtime performance of the |Python| language.

%property-based AG
\item We introduce property-based testing of attribute grammars that combines \gls{PbT}~\citep{quickcheck} and the Attribute Grammar formalism. In the property-based testing of attribute grammars, we specify \textit{properties} on attribute instances that decorate abstract syntax trees. Moreover, we define generators that use attribute values to steer the generation process. Such generators can ensure that the generated abstract syntax trees not only obey the syntactic rules of the language (as in \gls{PbT}) but also the static semantics defined by the Attribute Grammar.

%name analysis
\item We present a generic interface for name resolution for any programming language. Users of our interface only have to specify which nodes in their language represent name definitions, usages and new nested scopes. From here, our system relies on the zipper to produce a \gls{DSL} representing only the information relevant for name resolution, and we use the Attribute Grammar formalism to effectively report errors on this \gls{DSL}, which we then reflect on the original source code. To validate this approach, we use it to implement name resolution on the |Haskell| programming language.

\end{itemize}

\section{Thesis Outline and Publications}

\textbf{Chapter~\ref{sec:intro}} introduces the thesis, its context, motivation and relevance. It also lists the thesis contributions and their respective publications. 

\textbf{Chapter~\ref{sec:sota}} elaborates on the State of the Art more directly related to this work, for attribute grammars and strategic programming. 

\textbf{Chapter~\ref{sec:ztrategic}} introduces |Ztrategic|, an embedding of both strategic tree rewriting and attribute grammars in a zipper-based, purely functional setting. This chapter is based on the following publication:

\begin{publications}
\centering
 \paperbox
    {Zipping strategies and attribute grammars}
    {José Nuno Macedo, Marcos Viera, João Saraiva}
    {In: 16th International Symposium on Functional and Logic Programming (FLOPS 2022)}
    {112 -- 132}
    {10.1007/978-3-030-99461-7\_7}
\end{publications}

\textbf{Chapter~\ref{sec:memoztrategic}} presents the memoization of attributes in |Ztrategic|, as well as new \textit{upwards} strategic traversal combinators. 
It also includes benchmarks comparing |Ztrategic| and |pyZtrategic| with existing state-of-the-art libraries. The |pyZtrategic| library was developed in the context of the Ph.D. thesis of Emanuel Rodrigues.
This chapter is based on the following publications:

\begin{publications}
\centering
 \paperbox
    {Efficient Embedding of Strategic Attribute Grammars via Memoization}
    {José Nuno Macedo, Emanuel Rodrigues, Marcos Viera, João Saraiva}
    {In: ACM SIGPLAN Workshop on Partial Evaluation and Program Manipulation (PEPM2023)}
    {41 -- 54}
    {10.1145/3571786.3573019}

 \paperbox
    {Zipper-based embedding of strategic attribute grammars}
    {José Nuno Macedo, Emanuel Rodrigues, Marcos Viera, João Saraiva}
    {In: Journal of Systems and Software, Volume 211, Issue C, May 2024}
    {42 pages}
    {10.1016/j.jss.2024.111975}
%\end{publications}
%
%\textbf{Chapter~\ref{sec:pyztrategic}} reimplements these concepts in |Python|, proving the versatility of our approach. It also includes benchmarks comparing |Ztrategic| and |pyZtrategic| with existing State of the Art libraries. This work was developed in the context of the MSc thesis of Emanuel Rodrigues, and it is based on the following publication:

%\begin{publications}
%\centering
 \paperbox
    {pyZtrategic: A Zipper-Based Embedding of Strategies and Attribute Grammars in Python}
    {Emanuel Rodrigues, José Nuno Macedo, Marcos Viera, João Saraiva}
    {In: Evaluation of Novel Approaches to Software Engineering (ENASE 2024)}
    {615 -- 524}
    {10.5220/0012704000003687}
\end{publications}

\textbf{Chapter~\ref{sec:pbtAG}} details property-based testing of attribute grammars both abstractly and in |Haskell|. This work is based on the following publication: 

\begin{publications}
\centering
 \paperbox
    {Property-based Testing of Attribute Grammars}
    {José Nuno Macedo, Marcos Viera, João Saraiva}
    {Submitted to: The ACM SIGPLAN International Conference on Software Language Engineering (SLE24)}
    {12 pages long}
    { }
\end{publications}

\textbf{Chapter~\ref{sec:interface}} contains a generic interface for name analysis in |Haskell|, where users only have to specify which nodes in their language represent name definitions, usages and new nested scopes. This work was developed in the context of the M.Sc. thesis of André Nunes, and it is based on the following publication:

\begin{publications}
\centering
 \paperbox
    {A Zipper-based, Modular Embedding of Scope Rules}
    {José Nuno Macedo, André Nunes, Marcos Viera, João Saraiva}
    {Submitted to: The International Conference on the Art, Science, and Engineering of Programming (Programming), Vol. 9, Issue 3}
    {22 pages long}
    { }
\end{publications}

\textbf{Chapter~\ref{sec:conclusion}} concludes the thesis, discussing the obtained results and their impact, and pointing towards future avenues of research and improvement of the presented work. 

\subsection{Other contributions}

In this subsection, we detail publications that were developed throughout the duration of this thesis, but do not directly follow its main themes. Nevertheless, they are still relevant and are here highlighted. 

%func languages green
Many of the strategic programming and attribute grammar systems that we explore in the related work section (Chapter~\ref{sec:sota}) are based on functional programming languages. In fact, many constructs that these languages usually provide, such as support for higher-order functions and strong typing, are very useful for defining these systems, hence the preference for functional languages. In this thesis, we compare the performance of our solutions with some state-of-the-art systems, but the performance of the systems are expected to be directly correlated to the performance of their base programming language. 

We have carefully studied and compared the performance of various functional programming languages in the work "Functional Going Green: An Empirical Evaluation of Functional Languages Performance". For future work of this thesis, we propose selecting the most efficient languages of this evaluation, implementing strategic programming and attribute grammars in these languages, and comparing the performance of these new implementations with the work presented in this thesis. 

\begin{publications}
\centering
\paperbox
	{Functional Going Green: An Empirical Evaluation of Functional Languages Performance}
	{José Nuno Macedo, Francisco Ribeiro, Rui Rua, Marco Couto, Jácome Cunha, João Paulo Fernandes, João Saraiva, Rui Pereira}
	{Lecture Notes in Computer Science \\ (to be published)}
	{28}
	{}
\end{publications}

%gpt repair
We have developed a system for automated code repair using \textit{Large Language Models (LLMs)}, such as GPT-3. In consists of a pre-processing phase, where programs are transformed and data is extracted from them; this data is then fed into the LLM being used, and finally the results are collected and processed to find a repair for the original program. 

This work was developed in the |OCaml| programming language, which has no strategic programming systems available. The program pre-processing phase required many lines of code for traversals in the |OCaml| program abstract syntax tree, just to perform transformations in very specific nodes. This would be done trivially with strategic programming systems, and the participation in this work solidified the need these systems to be readily available in different programming languages. 

\begin{publications}
\centering
\paperbox
	{Beyond Code Generation: The Need for Type-Aware Language Models}
	{Francisco Ribeiro, José Nuno Macedo, Kanae Tsushima}
	{In: 2023 IEEE/ACM International Workshop on Automated Program Repair (APR2023)}
	{21 -- 22}
	{10.1109/APR59189.2023.00011}

\paperbox
	{GPT-3-Powered Type Error Debugging: Investigating the Use of Large Language Models for Code Repair}
	{Francisco Ribeiro, José Macedo, Kanae Tsushima, Rui Abreu, and João Saraiva}
	{In: Proceedings of the 16th ACM SIGPLAN International Conference on Software Language Engineering (SLE2023)}
	{111 -- 124}
	{10.1145/3623476.3623522}
\end{publications}