%!TEX root = ../template.tex
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% abstract-en.tex
%% NOVA thesis document file
%%
%% Abstract in English([^%]*)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%include lhs2TeX.fmt
%include polycode.fmt

\typeout{NT FILE abstract-en.tex}%

Data structure traversal and transformation is an ever-present concern in software development. It is trivial to write code for these tasks without concern for readability and reusability, but some settings lend themselves to complex, recursive, multi-traversal algorithms that are hard to understand and maintain once expressed. Among solutions for these problems can be found Attribute Grammars --- a powerful formalism for expressing context-dependent algorithms --- and Strategic Programming --- an employment of recursion schemes to easily define term rewrite rules for data transformation. 

This thesis focuses on utilizing Strategic Programming, Attribute Grammars and other underlying generic formalisms to build a powerful toolset for data structure traversal and transformation, resulting in a |Haskell| embedded library named |Ztrategic|. 
%We introduce |Ztrategic|, a |Haskell| embedded library where Strategic Programming and Attribute Grammars can be freely intertwined, with the full expressive power of both techniques. 
%
%Then, we extend the |Ztrategic| library with attribute memoization. %Selected attributes can be stored and reused, therefore avoiding their recomputation. The strategic traversal functions are adapted to freely access these memoized values. 
%
A |Python| version of this library named |pyZtrategic| was implemented, proving that our approaches are reasonably language-agnostic. 
%It also relies on the generic Zipper mechanism for data structure navigation, and provides similar functionality to |Ztrategic|, including its memoized variant. 
%
We benchmark our libraries |Ztrategic| and |pyZtrategic| by comparing them against each other
%
and against |Strafunski| and |Kiama|, two state-of-the-art libraries that provide similar functionalities. 
%, with the former outperforming the latter, which is to be expected considering their base programming language. We also compare them to |Strafunski| and |Kiama|, two state-of-the-art libraries that provide similar functionalities. 

Moreover, we provide a foundation for testing and validation of Attribute Grammars by defining Property-Based Testing for Attribute Grammars.
%, where properties are defined on attribute instances. 
This technique is presented abstractly and then implemented in |Haskell| using |Ztrategic|.
%We define Property-Based Testing for Attribute Grammars, where properties are defined on attribute instances. Properties are tested on large sets of randomly generated (abstract syntax) trees by evaluating their attributes. We use strategies to encode logic quantifiers defining the properties. We present this technique abstractly and then implement it in |Haskell| using |Ztrategic|. 
We present a generic interface for name resolution for any programming language. Users of this interface only have to specify which nodes in their language represent name definitions, usages and new nested scopes, and a full name analysis system is automatically derived. %From here, our system uses the generic Zipper mechanism to produce a Domain-Specific Language representing only the information relevant for name resolution, and we use the Attribute Grammar formalism to effectively report errors on this DSL, which we then reflect on the original source code. 

With this work we intend to broaden the horizons for software development, and push Strategic Programming and Attribute Grammars as two powerful data analysis and transformation tools.% which can be either used separately or combined. 

\keywords{
  Attribute Grammars \and
%  Functional Programming \and
  Generic Traversal \and
  Strategic Programming \and
  Term Rewriting
}

\begin{comment}
Regardless of the language in which the dissertation is written, usually there are at least two abstracts: one abstract in the same language as the main text, and another abstract in some other language.

The abstracts' order varies with the school.  If your school has specific regulations concerning the abstracts' order, the \gls{novathesis} (\LaTeX) template will respect them.  Otherwise, the default rule in the \gls{novathesis} template is to have in first place the abstract in \emph{the same language as main text}, and then the abstract in \emph{the other language}. For example, if the dissertation is written in Portuguese, the abstracts' order will be first Portuguese and then English, followed by the main text in Portuguese. If the dissertation is written in English, the abstracts' order will be first English and then Portuguese, followed by the main text in English.
%
However, this order can be customized by adding one of the following to the file \verb+5_packages.tex+.

\begin{verbatim}
    \ntsetup{abstractorder={<LANG_1>,...,<LANG_N>}}
    \ntsetup{abstractorder={<MAIN_LANG>={<LANG_1>,...,<LANG_N>}}}
\end{verbatim}

For example, for a main document written in German with abstracts written in German, English and Italian (by this order) use:
\begin{verbatim}
    \ntsetup{abstractorder={de={de,en,it}}}
\end{verbatim}

Concerning its contents, the abstracts should not exceed one page and may answer the following questions (it is essential to adapt to the usual practices of your scientific area):

\begin{enumerate}
  \item What is the problem?
  \item Why is this problem interesting/challenging?
  \item What is the proposed approach/solution/contribution?
  \item What results (implications/consequences) from the solution?
\end{enumerate}

% Palavras-chave do resumo em Inglês
% \begin{keywords}
% Keyword 1, Keyword 2, Keyword 3, Keyword 4, Keyword 5, Keyword 6, Keyword 7, Keyword 8, Keyword 9
% \end{keywords}
\keywords{
  One keyword \and
  Another keyword \and
  Yet another keyword \and
  One keyword more \and
  The last keyword
}
\end{comment}


