Strategic term re-writing~\citep{strategies} and \gls{AG}s~\citep{Knuth68} are two powerful language engineering
techniques. The former provides an abstraction to define
program/tree transformations: a set of re-write rules is applied while
traversing the tree in some pre-defined recursion pattern, the
strategy. The latter extends context-free grammars with attributes in order to specify static, context-dependent language algorithms.

There are many tools that support these techniques for the
implementation of (domain specific) programming
languages~\citep{eli,syngen,lrc,lisa,jastadd,uuag,silver,asfsdf,BallandBKMR07,strafunski,txl,kiama,stratego}. Unfortunately,
most of these tools are large systems supporting one of the
techniques, using their own \gls{AG} or strategic specification language. As
a consequence, they would require a considerable effort to extend and
combine. There are, however, two exceptions: the Silver
system~\citep{silver} and the Kiama library~\citep{kiama} do support
both techniques.

More recently, a combined embedding of the two techniques has been
proposed in~\citep{flops22}. This embedding relies on a generic
mechanism to navigate on both homogeneous and heterogeneous trees:
generic zippers~\citep{thezipper,genericZipper}. Since both
attribute grammars and strategies rely on the same generic tree
traversal mechanism, each of the techniques can be expressed by
generic zippers as shown in the work of~\cite{zipperAG,scp2016}, for \gls{AG}s, and
in the work of~\cite{flops22}, for strategic term re-writing. The embedding of the
two techniques in the same simple setting has a key advantage: \gls{AG}s and
strategies embeddings can be easily combined, thus providing language
engineers the best of the two worlds.

As previously shown in the work of~\cite{memoAG19}, the simple zipper-based
embedding of \gls{AG}s~\citep{zipperAG,scp2016} does not provide a proper
embedding of the formalism: attribute values are re-calculated during
the decoration of the tree. This not only goes against the semantics
of \gls{AG} formalism, where one attribute value is computed at most once,
but it also dramatically affects the attribute evaluator's
performance. The combined embedding of strategies and \gls{AG}s in that
setting, as proposed in the work of~\cite{flops22}, has exactly the same
performance issues.

In order to provide an efficient zipper-based embedding of strategic
term re-writing and attribute grammars, that we call \gls{SAG}s, we
implement zipper-based strategies on top of the memoized zipper-based
embedding of \gls{AG}s~\citep{memoAG19}. Thus, strategies access memoized
attribute values in the tree nodes, rather than having to re-compute
such attribute values via the inefficient (non-memoized) embedding of
\gls{AG}s, as proposed in~\citep{flops22}. The purpose of this chapter is four-fold:

\begin{itemize}


\item Firstly, we define zipper-based strategic combinators that can
access memoized attribute values as supported by the efficient memoized
embedding of zipper-based \gls{AG}s~\citep{memoAG19}. Thus, we extend the
|Ztrategic| library
%, developed in~\citep{flops22}, 
 with new combinators
which work on trees where attribute values are memoized in the tree's
nodes~\citep{pepm23}.

\item Secondly, because the zipper data structure is the key
ingredient for our embedding of strategic programming and attribute
grammars, we host our embedding in the |Python| language via available
libraries supporting algebraic data types and zippers. As a result, we
provide an embedding of strategic \gls{AG}s as a |Python| library.


\item Thirdly, zippers are a generic and very flexible mechanism to
navigate on trees. Thus, we use the power of zippers to express
advanced navigation strategies which, for example, can express usual
attribute propagation patterns offered in most \gls{AG}-based systems. While
classical \gls{AG} systems have a fix, pre-defined notation for such
patterns, via our strategic embedding we can express such patterns as
first class citizens: new patterns can be defined via strategies.
Moreover, influenced by the (attribute) grammar formalism, where
terminal symbols are more suitable handled outside the formalism
(usually specified via regular expressions and processed via efficient
automata-based recognizers), we introduce the notion of non-navigable
symbol which are not traversed by zippers. This does not limit the
expressiveness of the strategic library, but does result in a
considerable performance improvement of the implementations.
While typical strategic traversal libraries navigate inwards from
the starting position, we introduce outwards strategies that enable
traversals outside the starting position.



\item Fourthly, we perform a detailed study on the performance of the
non-memoized implementation proposed 
%in~\citep{flops22} 
in Chapter~\ref{sec:ztrategic}
and our
implementations.  We consider four well-known language engineering
tasks, namely, name analysis, program optimization, code smell
elimination and pretty printing, which we elegantly expressed in the
strategic and/or \gls{AG} programming styles. Then, we compare the
performance of our |Haskell| and |Python| implementations against the
state of the art |Strafunski|~\citep{strafunski} system - the |Haskell|
incarnation of strategic term re-writing - and |Kiama|~\citep{kiama} -
the combined embedding of strategies and \gls{AG}s in |Scala|.


\end{itemize}

Our results show that the |Haskell| embedding of strategic
term re-writing behaves similarly to |Strafunski|. However, the
embedding of \gls{SAG}s vastly outperforms |Kiama|'s solutions. Being
a dynamic, interpreted language it is not surprising that our |Python|
embedding presents the poorest performance.

%This paper is an extended and revised version of the work presented at PEPM 2023~\citep{pepm23}. In this new paper we are extending the embeddings with new navigation strategies and attribute propagation patterns. We express Embedded Strategic Attribute Grammars in Python and we include the Python implementations in our detailed performance evaluation.

\begin{comment}
The rest of the paper is organized as follows: Section~\ref{memo:sec2} introduces
strategic term re-writing, attribute grammars, a combined embedding of
SAGs, and memoized AGs.  Section~\ref{memo:memoZippers}
combines memoized AGs with strategies, and details a different
implementation of |Ztrategic| that maximizes efficiency for the usage
of memoized AGs; the concept of navigable symbols is also introduced
in this library. In Section~\ref{memo:newdir} we introduce some
extensions to strategies and AGs that exploit the powerful navigation
features of zippers.  Section~\ref{memo:python} presents the
zipper-based embeddding of Stratetic AGs, where we discuss in detail
the key differences from its Haskell counterpart.
Section~\ref{memo:performance} compares the performance of our work 
with the Strafunski and Kiama libraries, and elaborates on the 
obtained results. 
Section~\ref{memo:related} details the relevant state of the art on 
strategic programming and AGs. 
Section~\ref{memo:conc} concludes our work and below it we present links to 
the relevant libraries and to a replication package. 
\end{comment}