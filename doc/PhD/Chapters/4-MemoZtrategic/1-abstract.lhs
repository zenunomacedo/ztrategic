Strategic term re-writing and attribute grammars are two powerful
programming techniques widely used in language engineering. The
former relies on strategies to apply term re-write rules in defining
large-scale language transformations, while the latter is suitable to
express context-dependent language processing algorithms. These two
techniques can be expressed and combined via a powerful navigation
abstraction: generic zippers. This results in a concise
zipper-based embedding offering the expressiveness of both techniques.
In addition, we increase the functionalities of strategic programming, enabling the definition of outwards traversals; i.e. outside the
starting position.

Such elegant embedding has a severe limitation since it recomputes
attribute values. This chapter presents a proper and efficient embedding
of both techniques. First, attribute values are memoized in the zipper
data structure, thus avoiding their re-computation. Moreover,
strategic zipper based functions are adapted to access such memoized
values. We have hosted our memoized zipper-based embedding of
strategic attribute grammars both in the |Haskell| and |Python|
programming languages. Moreover, we benchmarked the libraries
supporting both embedding against the state-of-the-art |Haskell|-based
|Strafunski| and |Scala|-based |Kiama| libraries. The obtained results show
that our |Haskell| |Ztrategic| library is very competitive against those
two well established libraries.
