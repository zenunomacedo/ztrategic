%\begin{figure*}
%\begin{strip}
%\section{API}
%\label{sec:memoZtrAPI}
%\begin{minipage}[t]{.50\textwidth}
\textbf{Strategy types}
%> type TP m = Zipper a -> Maybe (Zipper a)
> newtype TP m = MkTP (forall d mm. Zipper (d mm) -> m (Zipper (d mm)))
> type TU m r = (forall a. Zipper (d mm) -> (m r, Zipper (d mm))) 
\textbf{Strategy Application}
> applyTP :: TP m -> Zipper (d mm) -> m (Zipper (d mm))
> applyTP_clean :: TP m -> Zipper (d mm) -> m (Zipper (d mm))
> applyTU :: TU m r -> Zipper (d mm) -> (m r, Zipper (d mm)) 
> applyTU_clean :: TU m r -> Zipper (d mm) -> (m r, Zipper (d mm)) 
\textbf{Primitive strategies}
> idTP      :: TP m
> constTU   :: r -> TU m r
> failTP    :: TP m 
> failTU    :: TU m r
> tryTP     :: TP m -> TP m
> repeatTP  :: TP m -> TP m
\textbf{Strategy Construction}
> monoTP    :: (b -> m b) -> TP m
> monoTU    :: (a -> m r) -> TU m r
> monoTPZ   :: (b -> Zipper a -> m (Zipper a)) -> TP m
> monoTUZ   :: (b -> Zipper a -> (m r, Zipper a)) -> TU m r
> adhocTP   :: TP m -> (b -> m b) -> TP m
> adhocTU   :: TU m r -> (b -> m r) -> TU m r
> adhocTPZ  :: TP m -> (b -> Zipper a -> m (Zipper a)) -> TP m
> adhocTUZ  :: TU m r -> (b -> Zipper a -> (m r, Zipper a)) -> TU m r
\textbf{Composition / Choice}
> seqTP     :: TP m -> TP m -> TP m
> choiceTP  :: TP m -> TP m -> TP m
> seqTU     :: TU m r -> TU m r -> TU m r
> choiceTU  :: TU m r -> TU m r -> TU m r
\textbf{AG Combinators}
> (.$) :: Zipper a -> Int -> Zipper a
> parent :: Zipper a -> Zipper a
> (.|) :: Zipper a -> Int -> Bool
>(.^) :: (Zipper a -> b) -> Zipper a -> b 
>(.^^) :: (Zipper a -> b) -> Zipper a -> b
>inherit :: (n -> Bool) -> (Zipper a -> b) -> Zipper a -> b 
%\end{minipage}
%\begin{minipage}[t]{.40\textwidth}
\textbf{Traversal Combinators}
> allTPright  :: TP m -> TP m
> oneTPright  :: TP m -> TP m
> allTUright  :: TU m r -> TU m r
> allTPdown   :: TP m -> TP m
> oneTPdown   :: TP m -> TP m
> allTUdown   :: TU m r -> TU m r
> atRoot      :: TP m -> TP m
\textbf{Traversal Strategies}
> full_tdTP   :: TP m -> TP m
> full_buTP   :: TP m -> TP m
> once_tdTP   :: TP m -> TP m
> once_buTP   :: TP m -> TP m  
> stop_tdTP   :: TP m -> TP m
> stop_buTP   :: TP m -> TP m
> full_uptdTP :: TP m -> TP m
> full_upbuTP :: TP m -> TP m
> once_uptdTP :: TP m -> TP m
> once_upbuTP :: TP m -> TP m
> full_tdTPupwards :: Proxy a -> TP m -> TP m 
> innermost   :: TP m -> TP m
> outermost   :: TP m -> TP m
> full_tdTU   :: TU m r -> TU m r     
> full_buTU   :: TU m r -> TU m r     
> once_tdTU   :: TU m r -> TU m r   
> once_buTU   :: TU m r -> TU m r  
> stop_tdTU   :: TU m r -> TU m r  
> stop_buTU   :: TU m r -> TU m r
> full_uptdTU :: TU m r -> TU m r  
> full_upbuTU :: TU m r -> TU m r  
> once_uptdTU :: TU m r -> TU m r  
> once_upbuTU :: TU m r -> TU m r  
> full_tdTUupwards :: Proxy a -> TU m r -> TU m r   
> foldr1TU    :: TU m r -> Zipper a -> (r -> r -> r) -> r
> foldl1TU    :: TU m r -> Zipper a -> (r -> r -> r) -> r
> foldrTU     :: TU m r -> Zipper a -> (r -> s -> s) -> s -> s
> foldlTU     :: TU m r -> Zipper a -> (s -> r -> s) -> s -> s
\textbf{Memoized AG Combinators}
> (.@.) :: (Zipper a -> (r, Zipper a)) -> Zipper a -> (r, Zipper a)
> atParent :: (Zipper a -> (r, Zipper a)) -> Zipper a -> (r, Zipper a)
> atRight :: (Zipper a -> (r, Zipper a)) -> Zipper a -> (r, Zipper a)
> atLeft :: (Zipper a -> (r, Zipper a)) -> Zipper a -> (r, Zipper a)
> memo :: attr -> AGTree_m d m a -> AGTree_m d m a
%\end{minipage}
%\caption{Full Memoized Ztrategic API and AG Combinators}
%\captionof{figure}{Full Memoized Ztrategic API and AG Combinators}
%\label{code:api}
%\end{figure*}
%\end{strip}

\begin{comment}
\newpage

\section{Let optimization as an Higher-order Attribute Grammar}
\label{sec:appLetAG}
\begin{code}

opt :: Root -> Root
opt = fix (\f t -> if t == (optRoot $ mkAG t) 
			then t else f (optRoot $ mkAG t))

optRoot :: Zipper Root -> Root
optRoot ag = case (constructor ag) of
           CRoot      -> Root $ optLet (ag.$1)

optLet :: Zipper Root -> Let
optLet ag = case (constructor ag) of
           CLet       -> Let (optList (ag.$1)) (optExp (ag.$2))

optList :: Zipper Root -> List
optList ag = case (constructor ag) of
           CEmptyList -> EmptyList
           CAssign    -> Assign    (lexeme_Name ag) 
           					(optExp (ag.$2)) (optList (ag.$3))
           CNestedLet -> NestedLet (lexeme_Name ag) 
           					(optLet (ag.$2)) (optList (ag.$3))

optExp :: Zipper Root -> Exp
optExp ag = case (constructor ag) of
           -- rules 1, 2, 3
           CAdd       -> case (lexeme_Add1 ag, lexeme_Add2 ag) of 
                           (e, Const 0)       -> e 
                           (Const 0, t)       -> t 
                           (Const a, Const b) -> Const (a+b) 
                           _                  -> Add (optExp (ag.$1)) (optExp (ag.$2))
           -- rule 4                
           CSub       -> Add (lexeme_Sub1 ag) (Neg (lexeme_Sub2 ag)) 
           CConst     -> Const (lexeme_Const ag)
           -- rules 5, 6
           CNeg       -> case (lexeme_Neg ag) of 
                           (Neg (Neg f))   -> f
                           (Neg (Const n)) -> Const (-n)
                           _               -> Neg (optExp (ag.$1))  
           -- rule 7
           CVar       -> case expand (lexeme_Var ag, lev ag) (env ag) of 
                           Just e  -> e
                           Nothing -> Var (lexeme_Var ag)
\end{code}
\end{comment}