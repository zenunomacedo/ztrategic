In this section, we recall the zipper-based \gls{SAG}s embedding introduced in Chapter~\ref{ztr:sec3} that combines strategic programming
and attribute grammars. 

\begin{comment}
In this section, we describe the zipper-based Strategic Attribute Grammars embedding introduced in \citep{flops22} that combines strategic programming
and attribute grammars. 
Before we describe the embedding in detail,
let us consider a motivating example
that requires two widely used language engineering techniques: language analysis and language optimization.   Consider the (sub)language of |Let| expressions as incorporated in most functional languages, including |Haskell|. Next, we show an example of a valid |Haskell| |let|
expression


\begin{code}
p =  let   a = b + 0     
           c = 2
           b = let c = 3 in b + c
     in    a + 7 - c  
\end{code}

\noindent and, we define the heterogeneous data type |Let| that we use to model let expressions in |Haskell| itself. 
We take this definition from previous work with strategies and attribute grammars in \citep{flops22}.

\begin{code}
data  Let   =  Let List Exp
data  List  =  NestedLet  String Let List
            |  Assign     String Exp List
            |  EmptyList
data  Exp   =  Add    Exp Exp
            |  Sub    Exp Exp
            |  Neg    Exp
            |  Var    String
            |  Const  Int
\end{code}

Consider now that we wish to implement a  simple arithmetic optimizer for our language.  Figure~\ref{rules} presents such optimization rules directly taken from~\citep{strategicAG}.

\begin{figure}
\begin{align}\label{eq:rules}
add(e, const(0)) &\rightarrow e \\
add(const(0), e) &\rightarrow e \\
add(const(a), const(b)) &\rightarrow const(a+b) \\
sub(e1, e2) &\rightarrow add(e1, neg(e2)) \\
neg(neg(e)) &\rightarrow e \\
neg(const(a)) &\rightarrow const(-a) \\
var(id) \mid (id, just(e)) \in env &\rightarrow e
\end{align}
\caption{Optimization Rules}
\label{rules}
\end{figure}

The first six optimization rules define context-free arithmetic rules.
If we consider those six rules only, then strategic term re-writing is an extremely suitable formalism to express the desired optimization, since it provides a solution that just defines the work to be done in the constructors (tree nodes) of interest, and "ignores" all the others. 

%\subsection{Optimization Rules via Strategic Term Re-writing}

\paragraph{Strategic Term Re-writing:}
In fact, we can easily express this optimization in Ztrategic: the strategic term re-writing library of the combined embedding.
We start by defining the \textit{worker} function, that directly follows the six rules we are considering: 


\begin{code}
expr :: Exp -> Maybe Exp
expr (Add e (Const 0))          = Just e 
expr (Add (Const 0) t)          = Just t  
expr (Add (Const a) (Const b))  = Just (Const (a+b)) 
expr (Sub a b)                  = Just (Add a (Neg b))    
expr (Neg (Neg f))              = Just f           
expr (Neg (Const n))            = Just (Const (-n)) 
expr _                          = Nothing
\end{code}

The worker function |expr| takes an |Exp| value, pattern matches on it, and in the cases of the rules returns the optimized expression.
In all other cases, it returns |Nothing|. Notice that the optimizations are made locally, no recursion is involved.
Having expressed all re-writing rules in function |expr|, now we need to use strategic combinators that navigate in the tree while
applying the rules.

In this case, to guarantee that all the possible optimizations
are applied we use an |innermost| traversal scheme. Thus, our optimization is expressed as:

\begin{code}
opt  :: Zipper Let -> Maybe (Zipper Let)
opt  t =  applyTP (innermost step) t
      where step = failTP `adhocTP` expr
\end{code} 

Function $opt$ defines a Type Preserving (|TP|) transformation; i.e. the input and result trees have the same type. 
Here, |step| is the transformation applied by the function
|applyTP| to all nodes of the input tree |t| 
using the $innermost$ strategy combinator. 
The re-write |step| performs the transformation specified in the |expr| worker function for all the cases considered by the optimizations
and fails silently (|failTP|) in other cases. 
Even though the data type we consider is heterogeneous, we do not need special considerations for this as the default |failTP| case will handle all remaining data types.
Notice in the signature the use of |Zipper| to navigate through the structure (of type |Let|) to be transformed.
%\todo{should we explain the use of Maybe here?}

Let us now consider the context dependent rule $7$ in our optimization.
This rule requires the computation of the environment where a name is used.
This environment has to be computed according to the non-trivial scope rules of the |Let| language.
The semantics of |Let| does not force a
declare-before-use discipline, meaning that a variable can be declared
after its first use. Consequently, a conventional  implementation of the scope rules
naturally leads to an algorithm, that traverses each block twice: once
for accumulating the declarations of names and constructing an
environment and a second time to process the uses of names (using the
computed environment) in order to check for the use of non-declared
identifiers. 

In fact, both the scope rules and context dependent re-writing are not easily expressed within strategic term re-writing. 
%
%
%
%\subsection{Scope Rules via Attribute Grammars}
%
%

\paragraph{Attribute Grammars:} The formal specification of  scope rules is in the genesis of the Attribute Grammar formalism~\citep{Knuth90}. AGs are particularly suitable to specify language engineering tasks, where context information needs to be first collected before it can be used. 

%Before we show our previous work on combining AGs and strategic term re-writing in a single (\discuss{uniform}?) Haskell embedding,
We start by specifying the scope rules of |Let| via an AG. 
We adopt a visual AG notation that is often used by AG writers to sketch a first draft of their grammars.  Thus, the scope rules of |Let| are visually expressed in Figure~\ref{fig:AG}. 
We define an extra type Root, to identify the root of the tree:

> data Root = Root Let

\begin{figure*}[h]
%\begin{minipage}[htb!]{\textwidth}
\includegraphics[width=\textwidth,keepaspectratio]{figure/AG_shorterBW_String_alt.png}
%\end{minipage}
\caption{Attribute Grammar Specifying the Scope Rules of |Let|}
\label{fig:AG}
\end{figure*}



The diagrams in the figure are read as follows. For each constructor/production
(labeled by its name) we have the type of the production above and
below those of its children. To the left of each symbol we have the
so-called \emph{inherited attributes}: values that are computed
top-down in the grammar. To the right of each symbol we have the
so-called \emph{synthesized attributes}: values that are computed
bottom-up. The arrows between attributes specify the information flow
to compute an attribute. Thus, the AG expressed in Figure~\ref{fig:AG} is the following.
The inherited attribute |dcli| is used as an accumulator to
collect all |names| defined in a |Let|: it starts as an empty list in
the |Root| production, and when a new name is defined (productions
|Assign| and |NestedLet|) it is added to the accumulator. The total
list of defined names is synthesized in attribute |dclo|, which at
the |Root| node is passed down as the environment (inherited attribute
|env|). Moreover, a nested let inherits (attribute |dcli|) the environment of its outer let.
The type of the three attributes is a list of pairs,
associating the name to its |Let| expression definition.
%\footnote{We will use this definition to expand the |Name| as required by optimization rule |7|.}.

%\discuss{Root data type?}
%\discuss{Name versus String...}

%To allow functional programmers to write functions very much like attribute grammar programmers write their grammars, we have extended the generic zippers library~\citep{adams2010zippers} with
The ZipperAG~\citep{zipperAG} library of the combined embedding defines a set of simple AG-like combinators; namely the combinator ``\textit{child}'', written as the infix function $.\$$, to access the child of a tree node given its index, and the combinator $parent$  to move the focus to the parent of a tree node.
With these two zipper-based AG combinators, we are able to express in a AG programming style the scope rules of $Let$.  For example, let us consider the synthesized attribute 
|dclo|. In the diagrams of our visual AG
the |NestedLet| and |Assign| productions we see that |dclo| is defined
as the |dclo| of the third child. Moreover, in production |EmptyList|
attribute |dclo| is a copy of |dcli|. This is exactly how such
equations are written in the zipper-based AG, as we can see in the
next function\footnote{The function |constructor| and the constructors used in the case
alternatives is boilerplate code needed by the AG embedding. This code
is defined once per tree structure (\textit{i.e.}, AG), and can
be generated by  template |Haskell|~\citep{templatehaskell}}:  



\begin{code}
dclo :: AGTree Env
dclo t =  case (constructor t) of
             CLet        -> dclo (t.$1)
             CNestedLet  -> dclo (t.$3)
             CAssign     -> dclo (t.$3)
             CEmptyList  -> dcli t
\end{code}


This attribute returns a value of type |Env|, which is a list of names with their associated nesting level and definition. Thus, type |Env| is defined as the following type synonym:

\begin{code}
type Env = [(String,Int,Maybe Exp)]
\end{code}

Consider now the case of defining the inherited attribute
|env| that we will need to express optimization $(7)$. In most diagrams an occurrence of attribute |env| is defined as
a copy of the parent. There are two exceptions: in productions |Root|
and |NestedLet|. In both cases, |env| gets its value from the
synthesized attribute |dclo| of the same non-terminal/type. Thus, the
|Haskell| |env| function looks as follows:

\begin{code}
env :: AGTree Env
env t =  case (constructor t) of
          CRoot  -> dclo t
          CLet   -> dclo t
          _      -> env (parent t)
\end{code}

Let us define now the equations of inherited attribute |dcli|. As shown in
Figure~\ref{fig:AG}, the initial list of declarations |dcli| at the
|Root| of the tree is the empty list, since the outermost block is
context-free. Attribute |dcli| of |Let| occurs again in |NestedLet|. In this case, |dcli| is
defined as the inherited attribute of the parent. The |List|
nonterminal also has |dcli| as inherited attribute. As we can see in
Figure~\ref{fig:AG}, |List| occurs in productions |Let|, |Assign|
and |NestedLet|. Each of these occurrences is defined by different
equations. For example, in |Assign| the attribute |dcli| of |List| is
defined by adding the defined name (|String|) to the |dcli| of the
parent (Figure~\ref{fig:AG} omits the use of |lev| and |Exp|).  Thus,
the zipper-based function |dcli| directly follows our visual notation
and it is written as follows:

\smallskip 
\begin{code}
dcli :: Zipper Root -> Env
dcli ag = case (constructor ag) of
           CLet  -> case (constructor (parent ag)) of
                         CRoot       -> []
                         CNestedLet  -> env  (parent ag)
           _     -> case (constructor (parent ag)) of
                         CLet        ->  dcli (parent ag)
                         CAssign     ->  (lexemeName (parent ag), lev (parent ag)
			                 , lexemeExp (parent ag))
					 : (dcli (parent ag))
                         CNestedLet  ->  (lexemeName (parent ag), lev (parent ag), Nothing)
			                 : (dcli (parent ag))

\end{code}

\noindent
where |lexemeName| and |lexemeExp| implement the
so-called AG syntactic references~\citep{syngen}: the use of
non-terminals in the AG equations. We omit here their definition
because they directly follow from the language data types, and, they
can be generated via Template Haskell~\citep{templatehaskell}.
In this example we collect an environment with the  expressions defined by each variable. Thus, in the optimization rule we will have to consider only the variables bound to constant expressions. We could perform a more aggressive optimization if we define an evaluation attribute and store the evaluated expressions in the environment.

The definition of inherited attribute |lev| is straightforward:
it starts with value |0| at the root which is incremented when passed
to a nested |Let|. In all other cases |lev| is just a copy of its
parent definition. Its definition looks as follows:


\begin{code}
lev :: Zipper Root -> Int
lev ag =   case (constructor ag) of
           CLet  ->  case (constructor (parent ag)) of
                           CNestedLet -> (lev (parent ag)) + 1
                           CRoot      -> 0
           _     ->  lev (parent ag)
\end{code}



% We omit here the definition of attribute |dcli|, where the declared names are being accumulated.

\paragraph{Combining Strategies and Attribute Grammars:} AG evaluators decorate the underlying trees with attribute values. Thus, an instance of attribute |env| is associated to every |Var| node, defining its environment. Recall that  |env| of $var(id)$ is the missing ingredient to implement rule $(7)$.

%Unfortunately, Strafunski works on user defined tree structures, while the embedding of AGs works on zippers. As a consequence, Strafunski combinators are not able to access attribute values defined by the AG. Strategic term re-writing relies on a generic navigation mechanism to walk up and down heterogeneous trees while applying type specific re-write rules.  Such a generic navigation mechanism is offered by zippers.  As we have shown in previous work, generic zippers also offer the necessary abstractions to express strategic term re-writing in a functional programming setting~\citep{flops22}. 



Since we work with a combined embedding, we define a strategic re-writing worker function that implements rule 7:

\begin{code}
expC :: Exp -> Zipper Root -> Maybe Exp
expC (Var i)  z = expand (i, lev z) (env z)
expC _        z = Nothing 
\end{code}
\label{code:ExpC}

The variable |i| is expanded according to its environment, as defined by rule $7$. Because the |Let| language has nesting we use an attribute named |lev| to distinguish definitions with the same name at different nested levels. Thus, the |expand| function looks up the defined variable |i| in the level |lev| or a lower level, in its environment |env|. In case it is found, the expanded definition of variable |i| is returned, otherwise the optimization is not performed.
Next, we present the definition of |expand|:


\begin{code}
expand :: (Name,Int) -> Env -> Maybe Exp
expand (i, l) e = case level of 
                    ((nE, lE, Just (Const c)):_)  -> Just (Const c) 
                    _                             -> Nothing 
 where vars = filter (\(nE, lE, _) -> nE == i && lE <= l) e
       level = sortBy (\(nE1, lE1, _) (nE2, lE2, _) -> compare lE2 lE1) vars
\end{code}

While the code might be a bit daunting for non-|Haskell| programmers, the concept is simple: we remove everything from the environment except names that are equal to the variable |i| we wish to expand. Then, we sort by nesting level. Lastly, we take the first of these results, and if it is a constant value, we return it; else we signal failure.

In fact, we take care to only expand variables that will be replaced by constants. If we expand any variable into the expression it is defined as, and if the result contains other variables, we risk introducing a variable from a different block into the current block, which is an error as their value might have been re-defined in the current scope. We solve this by only replacing variables by constants. Alternatively, we could use an |eval| attribute to evaluate expressions before storing them in the environment, which would solve this issue, but would make the definition of |expand| even more complicated. Note that, even if we only expand specifically when the definition of a variable is just a constant, the other 6 optimization rules will make variable definitions tend towards a single constant, making this solution elegant and effective.

%The variable |x| is searched into the environment returned by the |env| attribute;
%in case it is found, the associated expression\footnote{The function |lexeme_Assign| is another syntactic reference that in this case takes a |Zipper| and, if it is focused on an |Assign|, returns its expression.} is returned, otherwise the optimization is not performed.



Now we combine this rule with the previously defined |expr|, implementing rules 1 to 6, and apply them to all nodes.

%\todo{maybe mention here that we are now using Ztrategic instead of Strafunski?}

\label{opt'}
\begin{code}
opt'  :: Zipper Root -> Maybe (Zipper Root)
opt'  r = applyTP (innermost step) r
       where step = failTP `adhocTPZ` expC `adhocTP` expr  
\end{code}

Our motivating example shows the abstraction and expressiveness provided by combining strategies and attribute grammars in the same zipper-based setting~\citep{flops22}. 
However, 
\end{comment}
This embedding of \gls{AG}s has a severe limitation since when decorating the tree, it re-computes the same attribute instances. The reader may have noticed that every time the worker function |expC| is called, then the call to 
|env z| 
does lead to the (re)decoration of the full tree. Thus, the number of calls to rule $(7)$ results in the same number of full tree (re)decorations. As expected, 
this drastically affects the performance of the \gls{AG} embedding\citep{memoAG19} and, consequently, of the combined one, as well. 

%In the next section we will use the same approach: First, we combine memoization with the zipper-based strategic combinators, which are then combined with the memoized zipper-based AGs. After that, we will study the performance of our solution by comparing it with the state-of-the-art, namely the Strafunski~\citep{strafunski} and Kiama~\citep{kiama} systems.

\subsection{Term Re-Writing via Higher Order AGs} \label{sec:circag}

Classical \gls{AG}s have a severe drawback: every computation has to be
expressed in terms of the underlying \gls{AST}. In fact, 
\gls{HOAG}s~\citep{VSK89} were introduced with the main goal of solving this
limitation. In \gls{HOAG}s when a computation cannot be easily expressed in
terms of the original \gls{AST}, a better suited data structure can be
computed before. Thus, \gls{HOAG} do support term re-writing as shown
in the work of~\cite{Saraiva02}. The zipper based embedding of \gls{AG}s supports this
extension~\citep{scp2016}. Next, we show the |Let| optimization in a
pure \gls{HOAG} setting.

\medskip

\begin{code}
optRoot :: Zipper Root -> Root
optRoot ag  =  case (constructor ag) of
                   CRoot       -> Root $ optLet (ag.$1)

optLet :: Zipper Root -> Let
optLet ag   =  case (constructor ag) of
                   CLet        -> Let (optList (ag.$1)) (optExp (ag.$2))

optList :: Zipper Root -> List
optList ag  =  case (constructor ag) of
                   CEmptyList  ->  EmptyList
                   CAssign     ->  Assign      (lexemeName ag)
                                               (optExp (ag.$2)) (optList (ag.$3))
                   CNestedLet  ->  NestedLet   (lexemeName ag) 
                                               (optLet (ag.$2)) (optList (ag.$3))

optExp :: Zipper Root -> Exp
optExp ag   =  case (constructor ag) of
                   CAdd        ->  case (lexemeAdd1 ag, lexemeAdd2 ag) of 
                                        (e, Const 0)        -> e 
                                        (Const 0, t)        -> t 
                                        (Const a, Const b)  -> Const  (a+b) 
                                        _                   -> Add   (optExp (ag.$1))
                                                                     (optExp (ag.$2))
                   CSub        ->  Add (lexemeSub1 ag) (Neg (lexemeSub2 ag)) 
                   CConst      ->  Const (lexemeConst ag)
                   CNeg        ->  case (lexemeNeg ag) of 
                                        (Neg (Neg f))       -> f
                                        (Neg (Const n))     -> Const (-n)
                                        _                   -> Neg (optExp (ag.$1))  
                   CVar        ->  case expand (lexemeVar ag, lev ag) (env ag) of 
                                        Just e              -> e
                                        Nothing             -> Var (lexemeVar ag)
\end{code}

This fragment expresses a single transformation/re-writing of the
original \gls{AST} into a new (higher-order) tree. In order to guarantee
that all the possible optimizations are applied, we define a circular
attribute higher-order attribute, named \textit{attributable
attribute} |ata|, that is evaluated until a fix point is
reached~\citep{rag2013}.

\medskip

\begin{code}
circ :: Root -> Root
circ = fix (\f ata ->  if ata == (optRoot $ mkAG ata) 
                       then ata else f (optRoot $ mkAG ata))
\end{code}

\medskip

The higher-order solution corresponds to the computation of this
higher-order attribute and we write |optHOAG = circ|. 

As clearly shown in |optHOAG|, term re-writing via circular \gls{HOAG} does
not offer the expressiveness offered by strategic
programming. Firstly, all non-terminals/types and their
productions/constructors are included in the \gls{HOAG} solutions, even when
there is no useful work to be performed there. Secondly, the recursion
scheme is fixed and coded directly in attribute equations. Thus, it
can not be reused. In fact, the definition of traversals and recursion
schemes is against the declarative nature of standard \gls{AG}s. In order to
avoid trivial and polluting equations, \gls{AG} systems offer a set of
copy-rule abstractions allowing the automatic generation of such
attribute equations. Thus, we may consider some form of attribute
equation generation that always generates the equations that call the
constructor without doing useful work. The automatic generation of copy
rules, however, may induce hidden (real) circular attribute
dependencies, that are hard to identify and debug.



\subsection{Memoized Attribute Grammars}

In order to avoid attribute re-computation and, consequently, to
improve the performance of the \gls{AG} embedding, memoization was
incorporated into the zipper-based \gls{AG}s~\citep{memoAG19}. To memoize
the computed attributes for a given data structure, a new similar data
structure is defined where a memoization table (here referred to as
|m|) is associated with each node. All dependent data structures are
merged into a single one, which allows for easier handling of the
memoization tables:

\begin{code}
data Let m  =  Root       (Let m)                  m 
            |  Let        (Let m) (Let m)          m
            |  NestedLet  String  (Let m) (Let m)  m 
            |  Assign     String  (Let m) (Let m)  m
            |  EmptyList                           m
            |  Add        (Let m) (Let m)          m
            |  Sub        (Let m) (Let m)          m
            |  Neg        (Let m)                  m
            |  Var        String                   m
            |  Const      Int                      m
\end{code}

Thus, the type of the memoization table for any given node can be, for example, a tuple
in which each value might contain a memoized attribute. In our |Let|
example, this tuple and an empty memoization table are defined as follows:

\begin{code}
type MemoTable =  ( Maybe Env     -- dcli
                  , Maybe Env     -- dclo
                  , Maybe Env   ) -- env

emptyMemo = (Nothing,Nothing,Nothing)
\end{code}

Next, we have to define |Let| as an instance of the |Memoizable| data
class, with a memoization table of type |MemoTable|. 

\begin{code}
instance Memoizable Let MemoTable where 
  updMemoTable :: (m -> m) -> Let m -> Let m
  updMemoTable = updMemoTable'
  getMemoTable :: Let m -> m
  getMemoTable = getMemoTable'
\end{code}

%\discuss{should we say that getMemoTable' is a lib function?}

We omit the definition of the functions |getMemoTable'| and
|updMemoTable'| as they are simple |Haskell| functions that get and
replace the memoization table of a node, respectively.

Each of the attributes to be memoized is defined as a data type. This
will be useful in determining which attribute is to be memoized when
computing attributes.


\begin{code}
data Att_dcli  = Att_dcli
data Att_dclo  = Att_dclo
data Att_env   = Att_env
\end{code}

Finally, we define how each of the attributes is stored in the memoization table. For this, we use the |Memo| data class, specifying, for example, how the attribute |dcli| interacts with our |MemoTable|, storing a value of type |Env|: 

\begin{code}
instance Memo Att_dcli MemoTable Env where
  mlookup _   (a,_,_)  = a
  massign _ v (a,b,c)  = (Just v,b,c)
\end{code}

Here, the function |mlookup| defines how to obtain a |dcli| value from the memoization table and |massign| defines how to update it. We define similar instances for the other attributes. 

The definition of the instances of |Let| and its attributes as instances of |Memo| and |Memoizable| allow for the usage of the |memo| function, which hides all the memoization work enabling writing memoized attributes similarly to the non-memoized examples shown before. Next, we re-defined the |dclo| attribute using memoization: 

\begin{code}
dclo  :: (Memo Att_dclo MemoTable Env) => AGTree_m Let MemoTable Env 
dclo = memo  Att_dclo $
             \ag ->  case (constructor ag) of
                        CRoot       -> dclo .@. (ag.$1)
                        CLet        -> dclo .@. (ag.$1)
                        CNestedLet  -> dclo .@. (ag.$3)
                        CAssign     -> dclo .@. (ag.$3)
                        CEmptyList  -> dcli ag
\end{code}

This attribute will be computed similarly to the non-memoized version when there is no value computed for it previously, and the result will be automatically stored in the memoization table. If the attribute was computed previously, then the previous value is re-used. 

The |env| attribute defined in this fashion will be stored automatically when computed, and further uses of this attribute will just re-use the previously computed value. Any attributes that are used to compute |env| are also memoized.

\begin{code}
env  :: (Memo Att_env MemoTable Env) => AGTree_m Let MemoTable Env 
env  = memo  Att_env $
             \ag ->  case (constructor ag) of
                        CRoot  ->  dclo ag
                        CLet   ->  dclo ag
                        _      ->  env `atParent` ag
\end{code}

%\todo{Show the types of the combinators |(.@.)| and |atParent|. Explain that they are monadic because we need to keep track of the changes in the memo tables}

%\discuss{Shouldn't this paragraph appear before presenting dclo, where we use |(.@.)|?}


The combinators |(.@.)| and |atParent| perform an attribute
computation at a given child and at the parent, respectively,
returning the result of the computation and new tree with the
memotables possibly updated. Thus, attribute computations are
represented by a |State|-monad with type:

\begin{code}
type AGTree_m dtype m a  = Zipper (dtype m) -> (a, Zipper (dtype m)) 
\end{code}

Note that we opt to memoize all attributes in this example. However, it is not strictly necessary to do so - we can still define simple attributes and use them in conjunction with memoized attributes, and for this we do not use the |memo| function. If an attribute value was changed frequently, its value would not be reused properly and thus it is preferable to define it as a regular, non-memoized attribute.