In this section, we compare the performance of our zipper-based
embedding of \gls{SAG}s in |Haskell| and |Python| with state-of-the-art
libraries |Strafunski| and |Kiama|. In terms of expressiveness, both our
zipper-based embeddings are capable of representing \gls{AG}s, strategies, and
the combination of \gls{AG}s and strategies in a unified setting. Thus, we
focus our analysis in the runtime and memory consumption of different
implementations of a |Haskell| code smell eliminator, a |repmin|
program, a |Let| optimizer (as described in this chapter), and an
advanced multiple layout pretty printing algorithm.

%All implementations of these strategic AGs, together with the
%necessary resources (tests, scripts, etc) to replicate our study, are
%available in our replication package as detailed in
%Section~\ref{sec:conc}.  
All tests were run 10 times and averaged 
in a ThinkPad 13 (2nd Gen,
Intel i7-7500U (4) 3.500GHz) laptop with 8 Gb RAM and EndeavourOS
Linux x86 64 bits.



\begin{comment}
\begin{figure*}[htb!]
\begin{minipage}[t]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{Chapters/Figures/smells_loc.png}
%\end{minipage}
%\begin{minipage}[t]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{Chapters/Figures/smellsM_loc.png}
\end{minipage}
\caption{Haskell Smells elimination: Ztrategic versus Strafunski}
\label{fig:smells}
\end{figure*}
\end{comment}

\begin{figure*}[htb!]
\includegraphics[width=\textwidth,keepaspectratio]{Chapters/Figures/smells_loc.png}
\caption{Haskell Smells elimination: Ztrategic versus Strafunski (Runtime)}
\label{fig:smellsR}
\end{figure*}

\begin{figure*}[htb!]
\includegraphics[width=\textwidth,keepaspectratio]{Chapters/Figures/smellsM_loc.png}
\caption{Haskell Smells elimination: Ztrategic versus Strafunski (Memory)}
\label{fig:smellsM}
\end{figure*}

\subsection{Strategic Haskell Smell Elimination}
\label{sec:memosmells}


Source code smells make code harder to comprehend. A smell is not an
error, but it indicates a bad programming practice. Smells occur in
any language and |Haskell| is no exception. For example, inexperienced
|Haskell| programmers often write |l == []| to check whether a list is
empty, instead of using the predefined |null| function. We implemented
this full |Haskell| language refactoring tools as a pure strategic
program. To parse |Haskell| code we reused the |Haskell| front-end
available as one of its libraries. Thus, in this benchmark we consider
the |Ztrategic| and |Strafunski| libraries, only. The two developed tools
detect and eliminate all |Haskell| smells as reported in the work of~\cite{Cowie}.


In order to compare |Ztrategic| with the |Haskell| state-of-the-art
|Strafunski| counterpart we run both strategic solutions with a large
\textit{smelly} input. We consider 150 |Haskell| projects developed by
first-year students as presented in the work of~\cite{projects}. In these projects
there are 1139 |Haskell| files totaling 82124 lines of code, of which
exactly 1000 files were syntactically correct\footnote{The student
projects used in this benchmark are available at this work's
repository.}. Both |Ztrategic| and |Strafunski| smell eliminators detected
and eliminated 850 code smells in those files. Figure~\ref{fig:smellsR} 
shows the runtime of running both libraries and Figure~\ref{fig:smellsM}
shows the memory consumption.

There are three entries in these figures: a normal |Ztrategic|
implementation, a |Ztrategic| implementation in which we skip
unnecessary nodes (corresponding to \textit{terminal symbols}) in the
traversal, and a similar implementation in |Strafunski|. 

|Strafunski| outperforms |Ztrategic|, which is to be expected as |Ztrategic|
library has an additional overhead of creating and handling a zipper
over the traversed data. However, when skipping terminals, |Ztrategic|
has almost the same performance of the well established and fully
optimized |Strafunski| system.




\subsection{Repmin as a Strategic Program}

The |repmin| problem is a
well-known problem widely used to show the power of circular, lazy
evaluation as shown by~\cite{Bird84}.  The goal of this program is
to transform a binary leaf tree of integers into a new tree with the
exact same shape but where all leaves have been replaced by the
minimum leaf value of the original tree. The |repmin| problem can be
easily implemented by two strategic functions: First, a type unifying
strategy traverses the tree and computes its minimum value. Then, a
type preserving strategy traverses again the tree and constructs the
new tree, using the previously computed minimum.


\begin{comment}
\begin{figure*}[htb!]
\begin{minipage}[t]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{Chapters/Figures/Strategic_RepMin_(Runtime).png}
%\end{minipage}
%\begin{minipage}[t]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{Chapters/Figures/Strategic_RepMin_(Memory).png}
\end{minipage}
\caption{Strategic Repmin: Ztrategic versus Strafunski versus Kiama versus Python}
\label{fig:repmin}
\end{figure*}
\end{comment}

\begin{figure*}[htb!]
\centering
\includegraphics[width=0.7\textwidth,keepaspectratio]{Chapters/Figures/Strategic_RepMin_(Runtime).png}
\caption{Strategic Repmin: Ztrategic versus Strafunski versus Kiama versus Python (Runtime)}
\label{fig:repminR}
\end{figure*}

\begin{figure*}[htb!]
\centering
\includegraphics[width=0.7\textwidth,keepaspectratio]{Chapters/Figures/Strategic_RepMin_(Memory).png}
\caption{Strategic Repmin: Ztrategic versus Strafunski versus Kiama versus Python (Memory)}
\label{fig:repminM}
\end{figure*}


In Figure~\ref{fig:repminR} we show the results of implementing these solutions using strategies in |Haskell| and |Python| |Ztrategic| libraries, |Strafunski| and |Kiama|, in terms of runtime. Figure~\ref{fig:repminM} displays the same comparison in terms of memory usage. 
%
%
Here, \textit{Repmin size} refers to the number of nodes the input binary tree contains. Again, the |Haskell| |Ztrategic| embedding behaves very similar to |Strafunski| and both outperform the |Kiama| and |Python| implementations in terms of runtime. The memory consumption of all implementations is similar, being |Kiama| the clear poor exception.


%
%
%our embedding of strategic attribute grammars is able to perform large scale, real language transformations. Moreover, our embedding \discuss{is comparable to the performance of the state-of-the-art strategic library for Haskell. Our library, however, is more expressive as it allows for the usage of AG constructs. }
%do compare to the performance of the state-of-the-art strategic library for Haskell.  
%
%
%
%
\subsection{Repmin as an Attribute Grammar}

We also compare the performance of |repmin| when fully expressed as an
\gls{AG}. Actually, the |Kiama| implementation of |repmin| is part of the
Kiama library. It is very similar to the |Haskell| |Ztrategic|
version of |repmin| in~\citep{memoAG19}, which we use here.

Figure~\ref{fig:repminAGAll} shows the results of comparing our memoized
implementation of |repmin| in |Ztrategic| using \gls{AG}s, with |Kiama|. To be
able to compare the performance the strategic and \gls{AG} solutions, 
we run them with exactly the same
inputs.

In Figure~\ref{fig:repminAGAll} we can see that the non-memoized
version of |Ztrategic| increases its execution time exponentially and is
much slower than the other versions. 
%~\footnote{All these runtime numbers, and the numbers used to produced all Figures, are available in a spreadsheet included in our replication package.}
However, when we zoom in to the
behavior of the other implementations in Figure~\ref{fig:repminAGMemos}, 
we can notice that memoized |Ztrategic| outperforms |Kiama|. In
fact, for a tree with $40000$
nodes, the memoized |Ztrategic| \gls{AG} runs in $0.3$ seconds, while |Kiama|
needs $0.78$ seconds to perform the same task. When we compare these
results with the strategic solution (shown in Figure~\ref{fig:repminR}), 
where |Kiama| is outperformed by |Ztrategic| and |Strafunski|, we see that, 
for $40000$ nodes, the \gls{AG} version of |Kiama|'s implementation is $5.65$ times
faster than the strategic implementation for the same library. 
Comparatively, |Ztrategic|'s non-memoized \gls{AG} is $292$ times slower than 
the strategic approach, while the non-memoized \gls{AG} is $2.4$ times faster
than the strategic approach. The overall faster implementation is the 
strategic |Strafunski|'s with a runtime of $0.27$ seconds, thus $1.11$ times 
faster than the memoized \gls{AG}. The memoized \gls{AG} is extremely competitive 
considering that is has added overload of handling zippers and 
a memoization table.

\begin{comment}
\begin{figure*}[htb!]
\begin{minipage}[t]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{Chapters/Figures/repminAG-expand.png}
%\end{minipage}
%\begin{minipage}[t]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{Chapters/Figures/repminAG.png}
\end{minipage}
\caption{Runtime of the AG Repmin. Left: Ztrategic versus memoized Ztrategic and Kiama. Right: memoized Ztrategic and Kiama.  
}
\label{fig:repminAG}
\end{figure*}
\end{comment}

\begin{figure*}[htb!]
\includegraphics[width=\textwidth,keepaspectratio]{Chapters/Figures/repminAG-expand.png}
\caption{Runtime of the AG Repmin: Ztrategic versus memoized Ztrategic and Kiama.}
\label{fig:repminAGAll}
\end{figure*}

\begin{figure*}[htb!]
\includegraphics[width=\textwidth,keepaspectratio]{Chapters/Figures/repminAG.png}
\caption{Runtime of the AG Repmin: memoized Ztrategic and Kiama.}
\label{fig:repminAGMemos}
\end{figure*}

\subsection{Let Optimization}

We have implemented the |Let| strategic \gls{AG} 
algorithm
in |Kiama|, as it also provides strategies and \gls{AG}s. 
%; 
%it is
%very similar to the one presented in this paper: the optimization
%rules are implemented via strategies, and strategies in Kiama also
% have access to attribute values.

Figures~\ref{fig:letKiamaAll} and~\ref{fig:letKiamaMemos}, \ref{fig:letNoPython}, and~\ref{fig:letKiamaMemAll} and~\ref{fig:letKiamaMemFocus}
show the performance of optimizing several
|Let| inputs, in terms of runtime and memory usage. Along the X axis,
\textit{Let size} refers to the number of nested |Let| blocks
contained in the input data to be optimized. As clearly shown in Figure~\ref{fig:letKiamaAll}, 
both non-memoized and memoized |Python| embeddings present the
poorest performance. On the contrary, the |Haskell| |Ztrategic| 
implementations once again vastly outperform |Kiama| in both runtime and
memory consumption, for any input size. Because the |Kiama|
implementation shows a poor performance, compared to our memoized
strategic \gls{AG}, in Figures~\ref{fig:letKiamaMemos} and \ref{fig:letNoPython} 
we also include |Kiama|'s baseline execution: it just
generates the input \gls{AST} and prints it without performing any
optimization. Figure~\ref{fig:letNoPython} removes the |Python| values 
so that it is easier to compare |Ztrategic| and |Kiama|. 
%That is to say, with no strategy nor attribute being
%computed. 
|Kiama|'s baseline task already takes more time than the optimization in
the memoized |Ztrategic|. |Kiama| uses an advanced mechanism to combine
strategies and \gls{AG}s where \gls{AST}s are defined by reachability
relations~\citep{respectyourparents}. The mechanism to transform a tree
into relations already induces a significant overhead in the |Kiama|
baseline execution. This also drastically influences the memory
usage of |Kiama|'s solutions as we can also see in the |Kiama|'s 
solution for the |repmin| problem in Figure~\ref{fig:repminM}.


\begin{comment}
\begin{figure*}[htb!]
\begin{minipage}[htb!]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{Chapters/Figures/Let_Expression_Optimization_(Runtime).png}
%\end{minipage}
%\begin{minipage}[htb!]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{Chapters/Figures/Let_Expression_Optimization_(Runtime)1_wbaseline.png}
\end{minipage}
\caption{Let Optimization - Runtime: Ztrategic versus Kiama versus Python}
\label{fig:letKiama}
\end{figure*}
\end{comment}
%
\begin{figure*}[htb!]
\centering
\includegraphics[width=0.7\textwidth,keepaspectratio]{Chapters/Figures/Let_Expression_Optimization_(Runtime).png}
\caption{Let Optimization - Runtime: Ztrategic versus Kiama versus both Python versions}
\label{fig:letKiamaAll}
\end{figure*}

\begin{figure*}[htb!]
\centering
\includegraphics[width=0.7\textwidth,keepaspectratio]{Chapters/Figures/Let_Expression_Optimization_(Runtime)1_wbaseline.png}
\caption{Let Optimization - Runtime: Ztrategic versus Kiama versus memoized Python}
\label{fig:letKiamaMemos}
\end{figure*}


\begin{figure*}[htb!]
\begin{minipage}[htb!]{\textwidth}
\centering
\includegraphics[width=0.7\textwidth,keepaspectratio]{Chapters/Figures/let.png}
%\end{minipage}
%\begin{minipage}[htb!]{\textwidth}
%\includegraphics[width=0.5\textwidth,keepaspectratio]{Chapters/Figures/Let_Expression_Optimization_(Runtime)1_wbaseline.png}
\end{minipage}
\caption{Let Optimization - Runtime: Ztrategic versus Kiama}
\label{fig:letNoPython}
\end{figure*}
%

\begin{comment}
\begin{figure*}[htb!]
\begin{minipage}[htb!]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{Chapters/Figures/Let_Expression_Optimization_(Memory).png}
%\end{minipage}
%\begin{minipage}[htb!]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{Chapters/Figures/Let_Expression_Optimization_(Memory)1.png}
\end{minipage}
\caption{Let Optimization - Memory: Ztrategic versus Kiama versus Python}
\label{fig:letKiamaMem}
\end{figure*}
\end{comment}
%
\begin{figure*}[htb!]
\centering
\includegraphics[width=0.7\textwidth,keepaspectratio]{Chapters/Figures/Let_Expression_Optimization_(Memory).png}
\caption{Let Optimization - Memory: Ztrategic versus Kiama versus Python}
\label{fig:letKiamaMemAll}
\end{figure*}
%
\begin{figure*}[htb!]
\centering
\includegraphics[width=0.7\textwidth,keepaspectratio]{Chapters/Figures/Let_Expression_Optimization_(Memory)1.png}
\caption{Let Optimization - Memory: Ztrategic versus Kiama versus Python}
\label{fig:letKiamaMemFocus}
\end{figure*}

\subsubsection{Local vs Global Attributes in Ztrategic}
\label{sec:globalvslocal}

In Section~\ref{sec:outwards}, we suggest that our implementation of a |Let| program optimization can use a local attribute |localEnv| instead of a global attribute |env|, which we expect to be more efficient. We assume so because |localEnv|, in the best case, only computes declarations of a block, instead of every declared variable as is the case with |env|. 

We present the performance of the |Let| optimization using both |env| and |localEnv| in Figures~\ref{fig:envR} and~\ref{fig:envM}. The global attribute implementation takes between $27\%$ and $64\%$ more runtime when compared to the local attribute implementation. It should be noticed that the benchmarks used to gather this data are |Let| blocks which benefit greatly from the way |localEnv| is implemented, by having variables declared close to their usage location.
In terms of memory consumption, |env| also performs worse, however there is less discrepancy of values here when compared to the runtime values.
%If the benchmarks, for example, had all variables declared in the first block with no new declarations in the nested blocks, the measurements of these two approaches would be more similar.

\begin{comment}
\begin{figure*}[htb!]
\begin{minipage}[htb!]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{Chapters/Figures/envVsLocalEnv.png}
%\end{minipage}
%\begin{minipage}[htb!]{\textwidth}
\includegraphics[width=0.5\textwidth,keepaspectratio]{Chapters/Figures/envVsLocalEnvM.png}
\end{minipage}
\caption{Let Optimization - Ztrategic: using AG-based |env| versus using outwards-based strategy |localEnv|}
\label{fig:env}
\end{figure*}
\end{comment}
%
\begin{figure*}[htb!]
\centering
\includegraphics[width=0.7\textwidth,keepaspectratio]{Chapters/Figures/envVsLocalEnv.png}
\caption{Let Optimization - Ztrategic: using AG-based |env| versus strategy-based |localEnv| (Runtime)}
\label{fig:envR}
\end{figure*}

\begin{figure*}[htb!]
\centering
\includegraphics[width=0.7\textwidth,keepaspectratio]{Chapters/Figures/envVsLocalEnvM.png}
\caption{Let Optimization - Ztrategic: using AG-based |env| versus strategy-based |localEnv| (Memory)}
\label{fig:envM}
\end{figure*}

\subsection{Multiple Layout Pretty Printing}

We have expressed the large and complex optimal pretty printing \gls{AG},
presented in the work of~\cite{SPS99}, both in the |Ztrategic| and
|Kiama| libraries. This \gls{AG} specifies a multiple layout pretty printer
 that adapts the layout according to the available width of
the page. Indeed, it defines a complex four traversal algorithm and it
is one of the most complex \gls{AG} available.

We used the |Ztrategic| and |Kiama| versions of this algorithm to pretty
print |Let| expressions from our running
example. Table~\ref{tab:prettyprint} presents the runtime in seconds
of executing the same pretty printing with the non-memoized and
memoized |Ztrategic| and |Kiama| \gls{AG} embeddings. Here, the number 
of a |Let| expression refers to its nesting level, such that |Let n| will
have |n| nested let declarations as well as $10$ variable declarations for 
each nested declaration.



\begin{table}[htb!]
\caption{Runtime of pretty printing |Let| Expressions for increasingly larger |let| inputs.  }
\centering \small
\begin{tabular}{||l||r||r||r||r||r||r||} 
\hline
\textbf{}                    & \textbf{let 1} & \textbf{let 2} & \textbf{let 3} & \textbf{let 4}              & \textbf{let 5}              & \textbf{let 6}                            \\ \hline
\textbf{AG}      & 0          & 0,02       & 0,1        & 0,91                    & 7,95                    & 68,49                                     \\ \hline
\textbf{MemoAG}  & 0,01       & 0,01       & 0,02       & 0,03                    & 0,06                    & 0,1                                       \\ \hline
\textbf{Kiama}   & 0,72       & 5,79       & 292,69     & \multicolumn{1}{c||}{--} & \multicolumn{1}{c||}{--} & \multicolumn{1}{c||}{--}  \\ \hline
\end{tabular}
\label{tab:prettyprint}
\end{table}

As expected the memoized |Ztrategic| solution is much faster than the
non-memoized counterpart. The |Kiama| solution shows the
poorest performance and is only able to pretty print the
smaller three |Let| expressions. This large \gls{AG} defines many attributes
and the tree/attribute relations mechanism used by |Kiama| to fully
support strategic \gls{AG}s do induce a considerable overhead.


%The results of these two benchmarks clearly show that
%Ztrategic is capable of consistently outperform Kiama for different
%input sizes for different benchmarks. These results could be partially
%explained due to the fact that Kiama runs on the Java Virtual Machine
%which is expected to underperform when compared to natively compiled
%software.
