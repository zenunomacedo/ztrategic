%!TEX root = ../template.tex
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% chapter6.tex
%% NOVA thesis document file
%%
%% Chapter with lots of dummy text
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%include lhs2TeX.fmt
%include polycode.fmt
%include Config/formats.lhs 


\typeout{NT FILE chapter6.tex}%

\chapter{A Zipper-based, Modular Embedding of Scope Rules}
\label{sec:interface}

\paragraph{Abstract:} 
%include Chapters/6-Interface/1-abstract.lhs

\section{Introduction} 
\label{if:intro}
%include Chapters/6-Interface/2-intro.lhs


\section{Motivation} 
\label{if:motivation}
%include Chapters/6-Interface/3-motivation.lhs


\section{Name Resolution via Higher-Order Attribute Grammars}  
\label{if:AG}
%include Chapters/6-Interface/4-AG.lhs

\section{A Generic Interface for Name Resolution} 
\label{if:interface}
%include Chapters/6-Interface/5-Interface.lhs

\section{Name Analysis of Haskell Programs} 
\label{if:haskellanalysis}
%include Chapters/6-Interface/6-Haskell.lhs

%\section{Related Work} 
%\label{if:relatedwork}
%%include Chapters/6-Interface/7-RelatedWork.lhs 

\section{Conclusions} 
\label{if:conclusions}
%include Chapters/6-Interface/8-Conclusions.lhs

%\section*{Replication Package} 
%\label{if:replpackage}
%All source code discussed in this work, as well as supporting code, is available at \url{https://tinyurl.com/ToolsProgramming25}.


%%include Chapters/6-Interface/9-Appendix.lhs
