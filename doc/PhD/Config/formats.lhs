
%format arrayliststring = "\ensuremath{\Conid{ArrayList}}\!\!\textless\!\!\!\ensuremath{\Conid{String}}\!\!\!\textgreater"
%format arraylistinteger = "\ensuremath{\Conid{ArrayList}}\!\!\textless\!\!\!\ensuremath{\Conid{Integer}}\!\!\!\textgreater"
%format junitquickcheck = "\ensuremath{\Conid{junit}}\!\!\mathbin{-}\!\!\ensuremath{\Conid{quickcheck}}"
%format cplusplus = "\ensuremath{\Conid{C}}\!+\!\!+"


%format <+> = "\textless\!\!+\!\!\textgreater"
%format .$< = ".\$\!\!\textless"


%format <$> = "\textless\!\!\mathbin{\$}\!\!\textgreater"
%format <*> = "\textless\!\!\mathbin{*}\!\!\textgreater"

%format box_e = "\fbox{e}"
%format box_err = "\fbox{e $\leftarrow$ error}"
%format box_b = "\fbox{b $\leftarrow$ error}"
%format box_b = "\fbox{b $\leftarrow$ error}"

%format box_z = "\fbox{\ensuremath{\Conid{z}}}"