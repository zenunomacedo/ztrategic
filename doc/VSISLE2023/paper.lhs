\documentclass[preprint,12pt]{elsarticle}

%% Use the option review to obtain double line spacing
%% \documentclass[authoryear,preprint,review,12pt]{elsarticle}

%% Use the options 1p,twocolumn; 3p; 3p,twocolumn; 5p; or 5p,twocolumn
%% for a journal layout:
%% \documentclass[final,1p,times]{elsarticle}
%% \documentclass[final,1p,times,twocolumn]{elsarticle}
%% \documentclass[final,3p,times]{elsarticle}
%% \documentclass[final,3p,times,twocolumn]{elsarticle}
%% \documentclass[final,5p,times]{elsarticle}
%% \documentclass[final,5p,times,twocolumn]{elsarticle}

%% For including figures, graphicx.sty has been loaded in
%% elsarticle.cls. If you prefer to use the old commands
%% please give \usepackage{epsfig}

%We can control the size of code blocks by changing these values. 
%We can obtain the original ones by commenting this out
\AtBeginDocument{
\setlength{\abovedisplayskip}{1pt}
\setlength{\belowdisplayskip}{1pt}
\setlength{\abovedisplayshortskip}{1pt}
\setlength{\belowdisplayshortskip}{1pt}
}

%include lhs2TeX.fmt
%include lhs2TeX.sty


%%%%% quick and dirty workaround to the Bbbk problem: read https://github.com/kosmikus/lhs2tex/issues/82
\let\Bbbk\undefined
%include polycode.fmt
\let\Bbbk\undefined


\usepackage{amsmath}
\usepackage{caption}
\usepackage{float}
\usepackage{xspace}
\usepackage{microtype}
\usepackage{enumitem}
\usepackage{cuted}
\usepackage{xcolor}
\usepackage{comment}
\usepackage{hyperref}
%\usepackage{cuted}
%\usepackage{portuguese}
%\usepackage[T1]{fontenc}

%\usepackage[usenames]{color}

%\input{haskell_newcolors.sty}
%\input{ag.sty}
\input{haskell_newcolorsBW.sty}
\input{agBW.sty}

\usepackage[finalizecache=true,cachedir=mintedcache]{minted}

%include formats.lhs


\newcommand{\discuss}[1]{\textcolor{blue}{#1}\xspace}
\newcommand{\todo}[1]{\textcolor{red}{#1}\xspace}
\renewcommand{\hscodestyle}{\small}


\newcommand{\new}[1]{}%\textcolor{blue}{#1}\xspace}


%%
%% \BibTeX command to typeset BibTeX logo in the docs
\AtBeginDocument{%
  \providecommand\BibTeX{{%
    \normalfont B\kern-0.5em{\scshape i\kern-0.25em b}\kern-0.8em\TeX}}}



%% The lineno packages adds line numbers. Start line numbering with
%% \begin{linenumbers}, end it with \end{linenumbers}. Or switch it on
%% for the whole article with \linenumbers.
%% \usepackage{lineno}

\journal{VSI:SLE}

\begin{document}

\begin{frontmatter}

%%
%% The "title" command has an optional parameter,
%% \title[short title]{full title}
%% allowing the author to define a "short title" to be used in page headers.
\title{Zipper-based Embedding of Strategic Attribute Grammars}

%%
%% The "author" command and its associated commands are used to define
%% the authors and their affiliations.
%% Of note is the shared affiliation of the first two authors, and the
%% "authornote" and "authornotemark" commands
%% used to denote shared contribution to the research.
\author[um]{Jos\'e Nuno Macedo}
\ead{jose.n.macedo@@inesctec.pt}
%\ead{jose.n.macedo@@inesctec.pt}
%\ead{0000-0002-0282-5060}
%\affiliation{
%  \institution{HASLab \& INESC TEC, University of Minho}
%  \city{Braga}
%  \country{Portugal}
%}

\affiliation[um]{organization={HASLab \& INESC TEC, University of Minho},%Department and Organization
%            addressline={}, 
            city={Braga},
%            postcode={}, 
%            state={},
            country={Portugal}}

%\address[um]{HASLab \& INESC TEC, University of Minho, Braga, Portugal}


\author[um]{Emanuel Rodrigues}
\ead{jose.e.rodrigues@@inesctec.pt}
% \ead{0000-0003-4317-1144}
% \affiliation{
%   \institution{HASLab \& INESC TEC, University of Minho}
%   \city{Braga}
%   \country{Portugal}
% }

\author[ur]{Marcos Viera}
\ead{mviera@@fing.edu.uy}
% \ead{0000-0003-2291-6151}
% \affiliation{
%   \institution{Universidad de la República}
%   \city{Montevideo}
%   \country{Uruguay}
% }

\affiliation[ur]{organization=Universidad de la Republica,%Department and Organization
%            addressline={}, 
            city={Montevideo},
%            postcode={}, 
%            state={},
            country={Uruguay}}

%\address[ur]{Universidad de la República, Montevideo, Uruguay}


\author[um]{Jo\~ao Saraiva}
\ead{saraiva@@di.uminho.pt}
%\ead{0000-0002-5686-7151}
%\affiliation{
%  \institution{HASLab \& INESC TEC, University of Minho}
%  \city{Braga}
%  \country{Portugal}
%}



%include chapters/abstract.lhs

\end{frontmatter}

\section{Introduction}
\label{sec1}

%include chapters/introduction.lhs


\section{Zipping Strategies and Attribute Grammars}
\label{sec2}

%include chapters/zipperStrategicAG.lhs






\section{Combining Attribute Memoization with Zippers}
\label{sec:memoZippers}

%include chapters/memo.lhs


%\section{Attribute Propagation Patterns}

\section{New Directions}
\label{sec:newdir}


%include chapters/patterns.lhs


\section{Strategies and Attribute Grammars in Python}
\label{sec:python}

%include chapters/python.lhs

\section{Performance}

\label{sec:performance}
%
%include chapters/performance.lhs
%
\section{Related Work}
\label{sec:related}
%
%include chapters/relatedWork.lhs
%
\section{Conclusion}
\label{sec:conc}
%
%include chapters/conclusions.lhs
%
%

\section*{Acknowledgements}
\label{ack}
%\small
This work is financed by National Funds through the Portuguese funding agency, FCT - Fundação para a Ciência e a Tecnologia, within project LA/P/0063/2020. The first author is sponsored by FCT grant \texttt{2021.08184.BD}.
We would like to thank Tony Sloane for his validation of the Kiama implementations used in Section~\ref{sec:performance}. 



\section*{Replication Packages} 

\noindent
All the necessary resources to replicate this study, as well as the full set of results, are publicly available for the base ZippersAG~\footnote{\url{https://hackage.haskell.org/package/ZipperAG-0.9}}, Strafunski~\footnote{\url{https://hackage.haskell.org/package/Strafunski-StrategyLib}}, Ztrategic~\footnote{\url{https://bitbucket.org/zenunomacedo/ztrategic/}} and Kiama~\footnote{\url{https://github.com/inkytonik/kiama}} libraries as well as Examples~\footnote{\url{https://tinyurl.com/VSISLE2023tools}} used in this paper.
%\fbox{%
%\parbox{0.9\columnwidth}{%
%\scriptsize
%All the necessary resources to replicate this study, as well as the full set of results, are publicly available:
%\begin{itemize}[leftmargin=*]
%\item  \textbf{ZippersAG:} \textit{\url{https://hackage.haskell.org/package/ZipperAG-0.9}}
%\item  \textbf{Strafunski:} \textit{\url{https://hackage.haskell.org/package/Strafunski-StrategyLib}}
%\item  \textbf{Ztrategic:} \textit{\url{https://bitbucket.org/zenunomacedo/ztrategic/}}
%\item  \textbf{Kiama:} \textit{\url{https://github.com/inkytonik/kiama}}
%\item  \textbf{Examples:} \textit{\url{https://tinyurl.com/pepm2023tools}}
%\end{itemize}
%}%
%}

%%
%% If your work has an appendix, this is the place to put it.



%%
%% The next two lines define the bibliography style to be used, and
%% the bibliography file.
\bibliographystyle{elsarticle-num}
\bibliography{bibliography}

\appendix
%include chapters/appendix.lhs


\end{document}
\endinput
