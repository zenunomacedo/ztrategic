Due to the fact that our tools are embedded as libraries in a programming
language, our strategic definitions are first class citizens and can
easily be extended with new useful and reusable traversal schemes.

In this section we introduce three new strategic combinators, which exploit the possibility that the zipper gives us to navigate in any direction of the tree.


\subsection{Outwards Traversals}
\label{sec:outwards}

While typical strategic traversal libraries navigate inwards from the
starting position, next we present strategies that also enables
outwards traversals, i.e. outside the starting position. This is
enabled by previously mentioned functional Zippers that offers a
powerful generic mechanism for navigation on heterogeneous data
structures. In fact, Zippers support upwards navigation, which is not
possible on the typical recursion traversal. As such, we introduce
three new traversal strategies:~\footnote{The API of Ztrategic in \ref{sec:appZtrAPI} includes  
all these new combinators. }

\begin{itemize}
      \item \textbf{atRoot} - Apply a given strategy at the root of a data structure that is accessed from any of its nodes. This guarantees a traversal of the whole data structure.
      \item \textbf{full\_uptdTP} - Apply a given strategy to all nodes in the path from the root of the data structure to the current node, in a top-down fashion. Similar strategies exist for bottom-up traversal, and for once-only application, for both Type-Preserving and Type-Unifying strategies. 
      \item \textbf{full\_tdTPupwards} - Navigate to the root and then apply a given strategy to the data structure, but not traversing from the starting node downwards. In fact, this is the complementary traversal of |full_tdTP|, such that $full\_tdTPUp \cup full\_tdTPUp \equiv atRoot$.  
\end{itemize}



%\new{As a proof of concept, recall optimization rule $7$, where an attribute |env| is used to compute the names in scope. In fact, this attribute computes all names that have been defined up to the level the attribute is computed in, which might be excessive in some situations. For example, if there are several definitions of a variable with the same name, we do not care to know the values of all of them, instead caring only for the one in the deepest level that is overriding the other ones. One such example is this |Let|:}

These outwards strategies provide powerful abstractions to express
transformations that rely on context information. Let us recall
optimization rule 7. Figure~\ref{fig:AG} presented the visual
definition of the AG collecting the environment (attribute |env|)
needed to expand an identifier, i.e. to replace it by its
definition. In the straightforward AG definition we collected a global
environment with all definitions occurring in the input program, which
were inherited (as attribute |env|) by all nodes in the AST.
Moreover, we used attribute |lev| to distinguish declarations of the
same identifier at different nested levels (recall, for example, the
definitions of type |Env| and function |expand|, presented
previously).

By using our new outward traversal schemes, however, we can avoid
the computation of such a global environment.  By using outwards
strategies we can express rule 7 using the following algorithm:

\begin{itemize}
\item First, we compute/synthesize the local environment of each |Let| block.
\item Then, we use an outward strategy to search for the definition of
an identifier in the block we need to expand it.
\begin{itemize}
\item If the identifier is
found in the the local environment of the (same) |Let| block, then we
use its definition to expand it.
\item Otherwise, the outwards strategy continues navigating upwards towards the root of the AST,
while looking for the identifier in the local environments of all |Let| blocks it is
traversing. As soon as it finds the definition of the desired identifier it stops 
traversing upwards since it found the closest definition of that
identifier.
\end{itemize}
\end{itemize}

Let us now express this algorithm as an strategic attribute
grammar. To synthesize the local environment of a |Let| block, we
define the new type of the environment:

\begin{code}
type LocalEnv = [(String,Maybe Exp)]
\end{code}

and we define a synthesized attribute |localEnv| that adds local
identifier definitions to the environment.


\begin{code}
localEnv :: Zipper Root ->  LocalEnv
localEnv ag = case (constructor ag) of
           CLet        ->  localEnv (ag.$1)
           CNestedLet  ->  (lexemeName ag, Nothing)      : localEnv (ag.$3)
           CAssign     ->  (lexemeName ag, lexemeExp ag) : localEnv (ag.$3)
           CEmptyList  ->  []
\end{code} 

Now, instead of using attributes to pass the (global)
environment to where it is needed (as we did in Section~\ref{sec2},
namely when expanding identifiers), we use our outwards
strategies to navigate upwards in the tree looking for such
definitions.

Before we define the traversal scheme, we implement a function
that searches for the name of an identifier in the synthesized attribute
|localEnv| of its |Let| block.

\begin{code}
definesVar :: String -> Let -> Zipper Root -> Maybe [Exp]
definesVar var _ z = case  lookup var (localEnv z) of 
                           Just (Just (Const c))  -> Just [Const c]
                           Just _                 -> Just []
                           _                      -> Nothing
\end{code} 

This worker function requires a type unifying strategy since it
(may) return the expression that is the expansion of the given
identifier (its first argument).\footnote{As we did in
Section~\ref{sec2}, to simplify the presentation we expand identifiers
that are defined as constants only.}

In order to express the algorithm that looks for the identifier
in all |Let| blocks while upwards traversing the AST towards the root,
we use the \textit{once upwards bottom up type unifying}
(|once_upbuTU|) strategy: \textit{once} because we want to stop in the
closest definition of the identifier and \textit{bottom up} since we
look for it going upwards from the current block towards the root.

\begin{code}
lookupVar :: String -> Zipper Root -> Maybe [Exp]
lookupVar  var z = applyTU (once_upbuTU step) z
           where step = failTU `adhocTUZ` (definesVar var)  
\end{code}

This function applies |definesVar| to check if the underlying
block defines |var|, and if so, it returns its value and stops the
traversal. If nothing is found, the strategy keeps on traversing
upwards until a value is found or it reaches the |Root|.

Now, the outwards strategy |expC'| that implements rule 7 is
expressed as follows:

\begin{code}
expC' :: Exp -> Zipper Root -> Maybe Exp
expC' (Var v) z  =  case lookupVar v z of 
                    Just [e]  -> Just e
                     _        -> Nothing
expC' _       z  = Nothing 
\end{code}


This strategic attribute grammar searches for variable
definitions in the environment one block at a time, without
necessarily computing the entire environment. On the contrary, the
AG-based definition of |expC| (shown on page~\pageref{code:ExpC})
relies on the |expand| function that searches the entire environment for
the definition of a given identifier at a given level (or higher).  By
performing lookup operations in smaller environments this new version
of rule $7$ (i.e. |expC'|) is also more efficient than |expC|, as the
benchmarks we present in Section~\ref{sec:gloabalvslocal} show.





\subsection{Attribute Propagation Patterns}


Attribute grammar systems often use a special notation~\cite{eli,syngen,uuag} to write
attribute propagation patterns in order to avoid polluting the AG
specification with too many copy rules: equations that just copy
attribute values upwards/downwards the AST. The typical example of a
propagation pattern is the remote access to an attribute value, which
avoids the need to pass such attributes downwards via copy
rules. Usually, AG based systems have a special syntactic notation to
specify pre-defined attribute propagation patterns they
support. Thus, the set of supported propagation patterns is
%predefined and
impossible to extend without a full update of the AG system itself.

In our zipper-based embedding of AG we see attribute propagation
patterns as first class citizens. Via zippers we can easily express
new propagation patterns and reuse predefined ones. For example, up until now, we
resort to a rather straightforward implementation of AGs where
inherited and synthesized attributes are computed by operating on the
immediate children and parent at any given point. We can extend this
with access to remote attributes, i.e. attributes that are defined
somewhere else on the tree.

Next, we present a zipper function |inherit| that navigates upwards
until a given predicate is true. When the predicate is true, the
function applies a given (zipper) function, to the current focus, and
stops the navigation. 

\medskip

\begin{code}
inherit :: Data n => (n -> Bool) -> (Zipper a -> b) -> Zipper a -> b 
inherit p f z = if query (mkQ False p) z then f z else (inherit p f).^ z
\end{code}
%\discuss{mkQ False p = true if p, false otherwise; query applies this to zipper. So if p we apply f, else we recurse "inherit" to immediate parent}
%
%
%
%
%
%
%This concept is built upon the the
%capacity of freely traversing data structures with no limitations that
%is provided by the usage of the Zipper.


This function can be used to access a remote attribute in the AG
specification. Let us consider the |Let| AG of the running example, where the inherited
attribute |env| is completely specified via copy-rules in order to pass
the global environment downwards to |Var| leaves. Recall the
definition of |expC| where an |id| is expanded to its definition. Next,
we specify such attribute via the |inherit| pattern as follows:


\medskip

\begin{code}
env' :: AGTree Env
env' t = inherit isNest dclo t 
  where  isNest (Let _ _)  = True
         isNest _          = False
\end{code}

\medskip


This zipper definition navigates upwards - function |inherit| -
until a |Let| node is reached, where it specifies that the |env|
attribute is the value of |dclo| at that node. 
%\todo{just review the following sentence please}
%\discuss{This can easily be seen
%on Figure~\ref{fig:AG} on the top |Let| diagram and the 
%bottom right |NestedLet| diagram.}

%Here, we navigate upwards until |isNest| is true, and then compute |dclo|. This is because the logic of the original |env| attribute is to simply traverse upwards until we reach the beginning the start of the current nesting level, and then compute |dclo| there - instead we can eliminate the copy rules and just access that node's |dclo| directly. For the |Let| data type, the definition of |isNest| we present below is enough to identify the start of a nesting level. 

%Some Attribute Grammar systems such as LRC~\ref{lrc} define remote attribute access by traveling upwards until a computation of an attribute is possible, that is, until a node where the attribute is defined is reached. We opt for instead allowing the user to define a predicate to search for, which can simply locate a node where an attribute is defined or it could check for any other condition on a node, therefore allowing for more expressiveness in locating remote attributes. 

We also define the |(.^^)| combinator to compute an attribute at the
root of a given tree.
This could have been used for example to inherit an environment of global variables.

%Here, we do not use it to define |env| as the 
%environment at the root of a |Let| does not contain all the definitions,
%and |env| should instead be computed at the correct nesting level. 
%, very much like the strategic combinator |atRoot|
%mentioned previously. 

% In fact, there are parallels between these
% attribute grammar combinators and some traversal strategy combinators,
% but the former reduces data structures into values and the latter
% transforms strategies into other strategies.


