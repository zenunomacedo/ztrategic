The embedding of strategic programming and AGs we have
presented does not rely on any advanced mechanism of the Haskell
programming language. The key ingredient for the proposed embeddings
is the zipper data structure. In fact, our simple embedding of
strategic AGs is language-agnostic, and consequently can be expressed
in any language providing functional zippers and algebraic data
types.

In this section we show that, in fact, these are the building blocks
of our embedding by re-implementing it in the Python language, reusing
existing zippers and ADT libraries. As result, we offer a Python
embedding of strategic AGs.

%For this, we build a generic zipper library, which we use as a base for the implementation 
%of attribute grammars and strategic traversal libraries. We also use a library that provides us
%with algebraic data types in Python.


To embed strategic AGs in Python we follow a similar approach to the one
described in Section~\ref{sec2}, namely:

\begin{itemize}

\item We reuse a zipper library available in the host language, in
this case the Python 
library named Zipper~\footnote{\url{https://pypi.org/project/zipper/}}, 
which we integrate with a
Python library providing support for algebraic data 
types~\footnote{\url{https://pypi.org/project/algebraic-data-types/}}.
%\item
The zipper library is the building block that supports the
definition of functions to model the attribute grammars and strategic
programming.

\item In order to avoid attribute re-calculation and provide a proper
embedding of AGs, we use memoization to store attribute values in a
memoization table.


\end{itemize}

Although the embedding of Haskell and Python rely on the same zipper
data structure, there are major differences between the two host
languages that we needed to consider:


\begin{itemize}

\item Firstly, the arguably less elegant syntax of Python does not allow
to define AG combinators using non-alphanumeric identifiers in infix
notation, which in Haskell allowed to have functions/combinators very
close to the AG notation. Moreover, the Python ADT library offers a
poor pattern matching mechanism when compared to Haskell, which makes the
definition of type-specific worker functions less elegant.

\item Secondly, we naturally handle failure in Haskell using the
|MonadPlus| class, which allows for easy handling and propagation of
failure; with no such mechanism available in Python, we resort to
throwing and catching a |StrategicError| exception.

\item Thirdly, while in Haskell we used a memoization table that 
was strongly typed inside a new data type built specifically for 
memoization,
in Python we directly store attribute values in the original tree nodes
 as side effects
with no concerns for type checking.

\end{itemize}



% on top of the (Python) zipper library we expressed several functions that model AG
%operators and strategic functions. Here we found the first main
%limitation of Python when compared to Haskell: arguably theless
%elegant syntax of Python, the lack of a type system holding the
%implementation together. Moreover, we needed to adapt some useful
%constructs found in Haskell. For example, we handle failure in Haskell
%using the |MonadPlus| class, which allows for easy handling and
%propagation of failure; with no such mechanism available in Python, we
%resort to throwing and catching a |StrategicError| exception.


%In order to embed SAGs in Python we follow a similar approach: on
%top of the (Python) zipper library we expressed several functions that
%model AG operators and strategic functions. Here we found the first
%main limitation of Python when compared to Haskell: arguably the less
%elegant syntax of Python, the lack of a type system holding the
%implementation together. Moreover, we needed to adapt some useful
%constructs found in Haskell. For example, we handle failure in Haskell
%using the |MonadPlus| class, which allows for easy handling and
%propagation of failure; with no such mechanism available in Python, we
%resort to throwing and catching a |StrategicError| exception.


%Initial versions of this library had a type error that 
%was not caught statically, which would trigger an exception during runtime, however our 
%mechanism was catching said exception and treating it as if it was an exception signalling 
%of failure, instead of the fatal error it actually was. Of course this error in the library 
%was already fixed, but we foresee similar situations possibly happening with less experienced 
%developers attempting to use this library. 

%Next, we show the implementation of the full |let| optimization rules (rules $1..7$) in Python. 


Let us return to our |let| optimization program of Section~\ref{sec2}.
Next, we define Python version of the
type specific function |expr|, that implements the optimization rules
$1$ to $6$.  The Python library for algebraic data types includes a |match|
function that allows the use of pattern matching. Unfortunately, this
function does not allow to express a partial matching, so we always
need to specify a behaviour for every case. The Python exception
|StrategicError| is used to signal that the strategy has to fail in
that node.



\begin{minted}{python}
def expr(exp):
    x = exp.match(
        add=lambda x, y: y if (x == exp.CONST(0)) else
                         x if (y == exp.CONST(0)) else
                         Exp.CONST(x.const() + y.const()) if
                         (lambda a, b: x == exp.CONST() and
                                       y == exp.CONST()) else
                         st.StrategicError,
        sub=lambda x, y: Exp.ADD(x, Exp.NEG(y)),
        neg=lambda x: x.neg() if (lambda a: x == Exp.NEG()) else
                      exp.CONST(-x.neg())
                              if (lambda b: x == Exp.CONST()) else
                      st.StrategicError,
        var=lambda x: st.StrategicError,
        const=lambda x: st.StrategicError
    )
    if x is st.StrategicError:
        raise x
    else:
        return x
\end{minted}


Now, we define optimization rule $7$, that needs to access attributes
|lev| and |env| to expand an identifier introduced in a |var|
node. This is implemenetd in function |expC|, when a |var| node is
matched. The implementation of the memoized AG-based Python functions
|lev| and |env| will be discussed in  Section~\ref{sec:pymemo}.


%\todo{should we mention taht Python ADT do not have a default case?
%and that we need to throw the execption in all exp nodes?}


\begin{minted}{python}
def expC(exp, z):
    x = exp.match(
        add=lambda x, y: st.StrategicError,
        sub=lambda x, y: st.StrategicError,
        neg=lambda x: st.StrategicError,
        var=lambda x: expand((x, lev(z)), env(z)),
        const=lambda x: st.StrategicError
    )
    if x is st.StrategicError:
        raise x
    else:
        return x
\end{minted}


Having defined the worker functions that model all optimization rules,
we express the full |let| optimization function |opt| by combining
% the two worker functions
|expr| and |expC| using the Python |adhocTP| and
|adhocTPZ| strategic combinators\footnote{The definition of the
strategic library in Python follows a similar naming scheme to the
Haskell version.  }. Such |step| function is applied using the Python
|innermost| strategy while traversing the tree.  In Python we do not
need to define the |apply| function to apply strategies, and actually
this is the main difference to the Haskell version on |opt'| shown on
page~\pageref{opt'}.

\begin{minted}{python}
def opt(z):
    def exp1(y):
        return st.adhocTPZ(st.failTP, expC, y)

    def step(x):
        return st.adhocTP(exp1, expr, x)

    return st.innermost(step, z).node()

\end{minted}



%\todo{move to the graphics with the runtime results?} \todo{Marcos: yes}
%We include this version of the library in our benchmarks, thus comparing similar libraries 
%in different languages. Our results, as expected, show that the Haskell library is more efficient runtime-wise. 
%Additionally, the Python implementation might require the increase of 
%the maximum recursion depth size of the interpreter 
%depending on the input size we use in the benchmarks. 
%we may need to increase the maximum recursion depth otherwise the optimization
%will not work.

\subsection{Attribute Memoization in Python}
\label{sec:pymemo}

Similarly to the approach we tackled in Haskell, we optimize the Python implementation of 
attribute grammars with memoization. For this, we initially explored the already available 
solutions for memoization in Python. 
The \texttt{functools}\footnote{\url{https://docs.python.org/3/library/functools.html}} 
library contains several tools 
for higher-order functions, including memoization annotations. In fact, we achieved linear 
performance improvements of around 50\% with very minimal effort, namely adding a |@cache| 
annotation before each attribute definition. While this is interesting by itself, we expected 
performance improvements to grow with input size, that is, the bigger the input, the more 
impactful the optimization should be. By using this library, we also have no control over how 
the memoization is achieved, meaning the discussion in Section~\ref{sec:memocorrect} would not 
translate to this new implementation.

Due to this, we implement memoization in Python using the same approach we use in Haskell,
we defined a memotable and associated it with each node and then re-defined the attributes to use
memoization.


Next, we show the implementation of the |dclo| attribute in Python. 

\begin{minted}{python}

def dclo(x):
    return memo(Att_DCLO(), dcloAux, x)


def dcloAux(x):
    match constructorM(x):
        case Constructor.CRoot:
            return at(dclo, x.z_dollar(1))
        case Constructor.CLet:
            return at(dclo, x.z_dollar(1))
        case Constructor.CNestedLet:
            return at(dclo, x.z_dollar(3))
        case Constructor.CAssign:
            return at(dclo, x.z_dollar(3))
        case Constructor.CEmpty:
            return dcli(x)

\end{minted}

In this example, the |at| function corresponds to |.@.| combinator in Haskell and the memoization takes place with the |memo| function.

Using memotables  we achieved performance improvements even better than the ones
achieved with the \texttt{functools} library and, in contrast, our improvements grow with the input size.
Despite that, as we will discuss in the next section, our results show that the Haskell library is more efficient, with even our memoized results in Python being slower than the non-memoized results in Haskell.
