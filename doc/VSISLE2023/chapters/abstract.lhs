
\begin{abstract}


Strategic term re-writing and attribute grammars are two powerful
programming techniques widely used in language engineering. The
former relies on strategies to apply term re-write rules in defining
large-scale language transformations, while the latter is suitable to
express context-dependent language processing algorithms. These two
techniques can be expressed and combined via a powerful navigation
abstraction: generic zippers. This results in a concise
zipper-based embedding offering the expressiveness of both techniques.
In addition, we increase the functionalities of strategic programming, enabling the definition of outwards traversals; i.e. outside the
starting position.

Such elegant embedding has a severe limitation since it recomputes
attribute values. This paper presents a proper and efficient embedding
of both techniques. First, attribute values are memoized in the zipper
data structure, thus avoiding their re-computation. Moreover,
strategic zipper based functions are adapted to access such memoized
values. We have hosted our memoized zipper-based embedding of
strategic attribute grammars both in the Haskell and Python
programming languages. Moreoever, we benchmarked the libraries
supporting both embedding against the state-of-the-art Haskell-based
Strafunski and Scala-based Kiama libraries. The first results show
that our Haskell Ztrategic library is very competitive against those
two well established libraries.



\end{abstract}

%%Research highlights
\begin{highlights}
\item Strategic traversal combinators that support memoized Attribute Grammars.
\item Showcase that the technique is language-agnostic by defining them in Haskell and Python.
\item Outward strategic traversals as well as Attribute Grammar propagation patterns as embedding first-class citizens.
\item Detailed study of the performance of this approach compared to state-of-the-art libraries. 
\end{highlights}

\begin{keyword}

Strategic Programming \sep Attribute Grammars \sep Functional Zippers
%% keywords here, in the form: keyword \sep keyword

%% PACS codes here, in the form: \PACS code \sep code

%% MSC codes here, in the form: \MSC code \sep code
%% or \MSC[2008] code \sep code (2000 is the default)

\end{keyword}
