%
This paper is inspired by the work of Sloane who developed
Kiama~\cite{kiama}: an embedding of strategic term re-writing and AGs
in the Scala programming language. 
%The Kiama library offers the
%functionality of our Ztrategic library. Kiama is a Scala based
%embedding of strategic AGs, which 
It is an embedding of strategic AGs, and
relies on Scala mechanisms to
navigate in the trees and memoizes attribute values on a global
memoization table to avoid attribute recalculation. Our library relies
on zippers and uses local memo tables to avoid attribute
recalculation. Kiama uses reachability relations and the notion of
attribute families to fully support strategic AGs.
Attributes that depend on their context are defined parametrized by
the tree they belong to. This allows sub-trees to be shared while maintaining the correctness of context-dependent attributes, since the same attribute has a different entry in the memoization table for the original tree and the one resulting from a re-write. On the other hand, context-independent attributes can use previously memorized values on modified trees. This approach is similar to our use of |applyTP| and |applyTP_unclean|, albeit finer-grained, as only context-dependent attributes are cleaned. 
%We can behave more similarly if we have two memoization tables, one for attributes that are context-dependent and one for those that are not.
%
%
%<Eric>

The extensible AG system Silver~\cite{silver} has also been extended
to support strategic term re-writing~\cite{strategicAG}.  Strategic
re-writing rules can use the attributes of a tree to reference
contextual information during re-writing, much like we present in our
work. While we use a functional embedding, Silver compiles its own
Strategic AG specification into low code. The paper includes several
practical application examples, namely the evaluation of
$\lambda$-calculus, a regular expression matching via Brzozowski
derivatives, and the normalization of for-loops. All these examples
can be directly expressed in our setting. They also present an
application to optimize translation of strategies. Because our
techniques rely on shallow embeddings, where no data type is used to
express strategies nor AGs, we are not able to express strategy
optimizations, without relying on meta-programming
techniques~\cite{templatehaskell}.  Nevertheless, our embeddings
result in very simple and small libraries that are easier to extend
and maintain, specially when compared to the complexity of extending
and maintaining a full language system such as Silver.
Silver translates strategies into equations for higher-order attributes, while our strategies are just data traversal mechanisms built on top of zippers, and its only relation to AGs is that our AGs also depend on zippers and therefore we can combine them.


%
%RACR

RACR \cite{christoff2015} is an embedding of Reference Attribute Grammars in Scheme, that allows graph re-writing and incremental attribute evaluation. A \emph{dynamic attribute dependency graph} is constructed during evaluation in order to determine which attributes are affected by a re-writing and therefore should be re-evaluated. To keep our embedding simple we do not perform that kind of analysis, although incorporating it is an interesting line of possible future work.
%
%<JastAdd>

JastAdd is a reference attribute grammar based
system~\cite{jastadd}. It supports most of AG extensions, namely
reference and circular AGs~\cite{rag2013}. It also supports tree
re-writing, with re-write rules that can reference attributes.
JastAdd, however, provides no support for strategic programming, that
is, there is no mechanism to control how the re-write
rules are applied.  The zipper-based AG embedding we integrate in
|Ztrategic| supports all modern AG extensions,
including reference and circular AGs~\cite{scp2016,memoAG19}. Because
strategies and AGs are first class citizens we can smoothly combine
any of such extensions with strategic term re-writing.
%
%<Strafunski>

In the context of strategic term re-writing, the
|Ztrategic| library is inspired by
Strafunski~\cite{strafunski}. 
In fact, |Ztrategic|
already provides almost all Strafunski functionality. 
There is,
however, a key difference between these libraries: while Strafunski
accesses the data structure directly, |Ztrategic|
operates on zippers. As a consequence, we can easily access attributes
from strategic functions and strategic functions from attribute
equations. Moreover, we can traverse not only into a data structure, 
but also outwards, that is, traverse regressing into previously-visited nodes.
This is possible also due to the powerful abstraction provided by the zippers data structure.
We show in Section~\ref{sec:outwards} these outward traversals and elaborate on 
how to implement rule $7$ of our running example using them.

%
%
%<Stratego>
%
%
\begin{comment}
The Stratego program transformation language (now part of the Spoofax Language Workbench~\cite{spoofax}) supports the definition of rewrite rules and programmable rewriting strategies, and it is able to construct new rewrite rules at runtime, to propagate contextual information for concerns such as lexical scope. It is argued in~\cite{strategicAG} that contextual information is better specified through the usage of inherited attributes for issues such as scoping, name-binding, and type-checking. We present this same usage of inherited attributes in our attribute grammar examples. 
\end{comment}
%
