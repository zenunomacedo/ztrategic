%\begin{figure*}
%\begin{strip}
\section{Ztrategic API}
\label{sec:appZtrAPI}
%\begin{minipage}[t]{.50\textwidth}
\textbf{Strategy types}
> type TP a = Zipper a -> Maybe (Zipper a)
> type TU m d = (forall a. Zipper a -> (m d, Zipper a)) 
\textbf{Strategy Application}
> applyTP :: TP a -> Zipper a -> Maybe (Zipper a)
> applyTP_unclean :: TP a -> Zipper a -> Maybe (Zipper a)
> applyTU :: TU m d -> Zipper a -> (m d, Zipper a) 
> applyTU_unclean :: TU m d -> Zipper a -> (m d, Zipper a) 
\textbf{Primitive strategies}
> idTP      :: TP a
> constTU   :: d -> TU m d
> failTP    :: TP a 
> failTU    :: TU m d
> tryTP     :: TP a -> TP a
> repeatTP  :: TP a -> TP a
\textbf{Strategy Construction}
> monoTP    :: (a -> Maybe b) -> TP e
> monoTU    :: (a -> m d) -> TU m d
> monoTPZ   :: (a -> Zipper e -> Maybe (Zipper e)) -> TP e
> monoTUZ   :: (a -> Zipper e -> (m d, Zipper e)) -> TU m d
> adhocTP   :: TP e -> (a -> Maybe b) -> TP e
> adhocTU   :: TU m d -> (a -> m d) -> TU m d
> adhocTPZ  :: TP e -> (a -> Zipper e -> Maybe (Zipper e)) -> TP e
> adhocTUZ  :: TU m d -> (a -> Zipper c -> (m d, Zipper c)) -> TU m d
\textbf{Composition / Choice}
> seqTP     :: TP a -> TP a -> TP a
> choiceTP  :: TP a -> TP a -> TP a
> seqTU     :: TU m d -> TU m d -> TU m d
> choiceTU  :: TU m d -> TU m d -> TU m d
\textbf{AG Combinators}
> (.$) :: Zipper a -> Int -> Zipper a
> parent :: Zipper a -> Zipper a
> (.|) :: Zipper a -> Int -> Bool
>(.^) :: (Zipper a -> b) -> Zipper a -> b 
>(.^^) :: (Zipper a -> b) -> Zipper a -> b
>inherit :: (n -> Bool) -> (Zipper a -> b) -> Zipper a -> b 
%\end{minipage}
%\begin{minipage}[t]{.40\textwidth}
\textbf{Traversal Combinators}
> allTPright  :: TP a -> TP a
> oneTPright  :: TP a -> TP a
> allTUright  :: TU m d -> TU m d
> allTPdown   :: TP a -> TP a
> oneTPdown   :: TP a -> TP a
> allTUdown   :: TU m d -> TU m d
> atRoot      :: TP a -> TP a
\textbf{Traversal Strategies}
> full_tdTP   :: TP a -> TP a
> full_buTP   :: TP a -> TP a
> once_tdTP   :: TP a -> TP a
> once_buTP   :: TP a -> TP a  
> stop_tdTP   :: TP a -> TP a
> stop_buTP   :: TP a -> TP a
> full_uptdTP :: TP a -> TP a
> full_upbuTP :: TP a -> TP a
> once_uptdTP :: TP a -> TP a
> once_upbuTP :: TP a -> TP a
> full_tdTPupwards :: Proxy a -> TP a -> TP a 
> innermost   :: TP a -> TP a
> outermost   :: TP a -> TP a
> full_tdTU   :: TU m d -> TU m d     
> full_buTU   :: TU m d -> TU m d     
> once_tdTU   :: TU m d -> TU m d   
> once_buTU   :: TU m d -> TU m d  
> stop_tdTU   :: TU m d -> TU m d  
> stop_buTU   :: TU m d -> TU m d
> full_uptdTU :: TU m d -> TU m d  
> full_upbuTU :: TU m d -> TU m d  
> once_uptdTU :: TU m d -> TU m d  
> once_upbuTU :: TU m d -> TU m d  
> full_tdTUupwards :: Proxy a -> TU m d -> TU m d   
> foldr1TU    :: TU m d -> Zipper e -> (d -> d -> d) -> d
> foldl1TU    :: TU m d -> Zipper e -> (d -> d -> d) -> d
> foldrTU     :: TU m d -> Zipper e -> (d -> c -> c) -> c -> c
> foldlTU     :: TU m d -> Zipper e -> (c -> d -> c) -> c -> c
\textbf{Memoized AG Combinators}
> (.@.) :: (Zipper a -> (r, Zipper a)) -> Zipper a -> (r, Zipper a)
> atParent :: (Zipper a -> (r, Zipper a)) -> Zipper a -> (r, Zipper a)
> atRight :: (Zipper a -> (r, Zipper a)) -> Zipper a -> (r, Zipper a)
> atLeft :: (Zipper a -> (r, Zipper a)) -> Zipper a -> (r, Zipper a)
> memo :: attr -> AGTree_m d m a -> AGTree_m d m a
%\end{minipage}
%\caption{Full Memoized Ztrategic API and AG Combinators}
%\captionof{figure}{Full Memoized Ztrategic API and AG Combinators}
%\label{code:api}
%\end{figure*}
%\end{strip}

\begin{comment}
\newpage

\section{Let optimization as an Higher-order Attribute Grammar}
\label{sec:appLetAG}
\begin{code}

opt :: Root -> Root
opt = fix (\f t -> if t == (optRoot $ mkAG t) 
			then t else f (optRoot $ mkAG t))

optRoot :: Zipper Root -> Root
optRoot ag = case (constructor ag) of
           CRoot      -> Root $ optLet (ag.$1)

optLet :: Zipper Root -> Let
optLet ag = case (constructor ag) of
           CLet       -> Let (optList (ag.$1)) (optExp (ag.$2))

optList :: Zipper Root -> List
optList ag = case (constructor ag) of
           CEmptyList -> EmptyList
           CAssign    -> Assign    (lexeme_Name ag) 
           					(optExp (ag.$2)) (optList (ag.$3))
           CNestedLet -> NestedLet (lexeme_Name ag) 
           					(optLet (ag.$2)) (optList (ag.$3))

optExp :: Zipper Root -> Exp
optExp ag = case (constructor ag) of
           -- rules 1, 2, 3
           CAdd       -> case (lexeme_Add1 ag, lexeme_Add2 ag) of 
                           (e, Const 0)       -> e 
                           (Const 0, t)       -> t 
                           (Const a, Const b) -> Const (a+b) 
                           _                  -> Add (optExp (ag.$1)) (optExp (ag.$2))
           -- rule 4                
           CSub       -> Add (lexeme_Sub1 ag) (Neg (lexeme_Sub2 ag)) 
           CConst     -> Const (lexeme_Const ag)
           -- rules 5, 6
           CNeg       -> case (lexeme_Neg ag) of 
                           (Neg (Neg f))   -> f
                           (Neg (Const n)) -> Const (-n)
                           _               -> Neg (optExp (ag.$1))  
           -- rule 7
           CVar       -> case expand (lexeme_Var ag, lev ag) (env ag) of 
                           Just e  -> e
                           Nothing -> Var (lexeme_Var ag)
\end{code}
\end{comment}