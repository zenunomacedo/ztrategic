This paper presented an embedding of strategic attribute grammars,
which combine strategic term re-writing and the attribute grammar
formalism. Both embeddings rely on memoized zippers: attribute values
are memoized in the zipper data structure, thus avoiding their
re-computation. Strategic zipper based functions access such memoized
values. The zipper data structure is the key ingredient of such an
embedding, and we have expressed it as libraries in the Haskell and
Python programming languages.

We introduced new strategies that navigate upwards in the data 
structure. We provide an usage example by defining an attribute for 
the local environment of each block and by using the new |once_upbuTU|
strategy with it to search for an identifier in the current block or by 
traversing upwards the AST. This approach is around 30\% faster when 
compared to the full AG-based definition of the full environment.

We compared the performance of both our embeddings with
state-of-the-art libraries Strafunski and Kiama. We have implemented
in these four libraries several language engineering tasks, namely, a
let optimizer, a code refactor, and an advanced pretty printing
algorithm. Our results show that the Haskell embedding strategic term
re-writing behaves very similar to Strafunski. Due to the dynamic
nature of the Python language, our Python embedding produces the
slowest implementations. On the contrary, our Haskell embedding of
SAGs vastly outperforms Kiama's solutions.
