
This paper presented a zipper-based embedding of strategic term
rewriting. By relying on zippers, we combined it with a zipper-based
embedding of attribute grammars so that (zipper-based) strategies can
access (zipper-based) AG functional definitions, and vice versa. Thus,
zippers and strategies are first class citizens. We have developed the
|Ztrategic| strategic programming library and we have used
it to implement several language engineering tasks.

To assess the expressiveness of our approach we compared our Ztrategic
solution to the largest strategic AG developed by the state-of-the-art
Kiama system. In terms of runtime performance we compared our
Ztrategic library to the well established and fully optimized
Strafusnki solution. The preliminary results show that in fact zippers
provided a uniform setting to express both strategic term rewriting
and AGs that are in par with the state-of-the-art. Moreover, since
zippers do not rely on any advanced mechanism of our |Haskell| hosting
language, namely lazy evaluation, they can be implemented in other
(non-lazy) declarative programming languages. As a consequence, our
joint embeddings can easily be ported to any programming settings
where zippers are available.


\subsection{Future Work}

To avoid the re-calculation of attribute values, memoized zippers have
been incorporated in the embedding of the zipper-based
AG~\cite{memoAG19}. This results in a considerable performance
improvement when we run the embedded AG. Our strategic combinators can
also be expressed as memoized zippers, which will provide an
incremental setting for both strategic term rewriting and attribute
grammars. This will result in the rewriting of the equal sub-trees to
be performed once, only.

Our |Ztrategic| library can still be generalized to work with any |Monad m|
instead of being restricted to the |Maybe| monad. In fact. Strafunski
generalizes the monadic infrastructure used in their combinators, by
using, for example, the |State| monad to gather state-dependent
information, such as number of nodes traversed or number of failed
transformation applications. We will extend our library to incorporate
this generalization, too.




