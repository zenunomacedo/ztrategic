
Since Algol was designed in the 60's, as the first high-level
programming language~\cite{algol68}, languages have evolved
dramatically. In fact, modern languages offer powerful syntactic and
semantic mechanisms that do improve programmers productivity. In
response to such developments, the software language engineering
community also developed advanced techniques to specify those new
language mechanisms.

Strategic term re-writing~\cite{strategies97} and Attribute Grammars
(AG)~\cite{knuth1968semantics} have a long history in supporting the
development of modern software language analysis, transformations and
optimizations. The former, relies on strategies to traverse a tree while
applying a set of re-write rules, while the latter is suitable to
express context-dependent language processing algorithms. Many
language engineering systems have been developed supporting both
AGs~\cite{eli,syngen,lrc,lisa,jastadd,uuag,silver} and re-writing
strategies~\cite{asfsdf,tom,strafunski,txl,kiama,stratego}. These
powerful systems, however, are large systems supporting their own AG
or strategic specification language, thus requiring a considerable
developing effort to extend and combine.

A more flexible approach is obtained when we consider the embedding of
such techniques in a general purpose language. Language embeddings,
however, usually rely on advanced mechanisms of the host language,
which makes them difficult to combine. For example,
Strafunski~\cite{strafunski} offers a powerful embedding of strategic
term re-writing in |Haskell|, but it can not be easily combined with
the |Haskell| embedding of AGs as provided
in~\cite{Moor00firstclass,zipperAG}. The former works directly on the
underlying tree, while the later on a \textit{zipper} representation
of the tree.  


In this paper, we present the embedding of both strategic tree
re-writing and attribute grammars in a zipper-based, purely functional
setting. Generic zippers~\cite{thezipper} is a simple, but generic
tree-walk mechanism to navigate both on homogeneous and heterogeneous
data structures. Traversals on heterogeneous data structures is the
main ingredient of both strategies and AGs. Thus, zippers provide the
building block mechanism we will reuse to express the
purely-functional embedding of both techniques. The embedding of the
two techniques in the same setting has several advantages: First, we
easily combine/zip attribute grammars and strategies, thus providing
language engineers the best of the two worlds. Second, the combined
embedding is easier to maintain and extend since it is written in a
concise and uniform setting. This results in a very small library (200
lines of |Haskell| code) which is able to express advanced (static)
analysis and transformation tasks.



The purpose of this paper is three-fold:

\begin{itemize}

\item Firstly, we present a simple, yet powerful embedding of
strategic term re-writing using generic zippers. This results in a
simple and concise library, named |Ztrategic|, that is easy to maintain and
update. Moreover, our embedding has the expressiveness of the
Strafunski library~\cite{strafunski}.

\item Secondly, this new strategic term re-writing embedding can
easily be combined with a zipper-based embedding of attribute
grammars~\cite{scp2016,memoAG19}. In fact, by relying on the same
generic tree-traversal mechanism, the zippers, (zipper-based)
strategies can access (zipper-based) AG functional definitions, and
vice versa. Such a joint embedding results in a multi-paradigm
embedding of the two language engineering techniques. We show two
examples of the expressive power of such embedding: First, we access
attribute values in strategies so that we express non-trivial
context-dependent tree re-writing. Second, strategies are used to
define \textit{attribute propagation patterns}~\cite{eli,uuag}, which
are widely used to eliminate (polluting) copy rules from AG
specifications.



\item Thirdly, we apply |Ztrategic| in real language engineering problems, namely, in optimizing
|Haskell| let expressions and expressing a set of refactoring rules 
that eliminate several |Haskell| smells.

\end{itemize}



This paper is organized as follows: Section~\ref{sec2} presents
generic zippers and describes in detail the zipper-based embedding
of strategic term re-rewriting. In Section~\ref{sec3}, we describe
zipper-based embedding of attribute grammars and we show how the two
techniques/embeddings can be easily combined. In Section~\ref{sec4} we
present our |Ztrategic| library and we use it in defining several
refactorings of |Haskell|. Section~\ref{sec5} discusses related work,
and in Section~\ref{sec6} we present our conclusions and future work.

