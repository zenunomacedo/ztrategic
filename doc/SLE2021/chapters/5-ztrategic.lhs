%\subsection{Refactoring |Haskell| Code}

As we have shown, zippers can express strategic term re-writing.  We 
developed a full library, named |Ztrategic|.
Figure~\ref{code:api} presents the full API, which is based on Strafunski's API,
but adding the possibility to manipulate the zipper that traverses the tree, in order to be able to, for example,
compute attributes.
We define two Strategy types, |TP| and |TU| which stand for Type-preserving and Type-Unifying. The former represents transformations, while the latter represents reductions. 

We define Primitive Strategies that can be used as building blocks, such as identities (|idTP| and |constTU|), failing strategies |failTP| and |failTU|, the |tryTP| combinator which applies a transformation once if possible and always succeeds, and |repeatTP| which applies a transformation as many times as possible. 
Strategy Construction combinators allow for composition of more complex transformations. The |adhoc| combinators compose an existing strategy with a |Haskell| function, and the |mono| combinators produce a strategy out of one |Haskell| function. There are variants that allow access to the zipper, denoted by a $Z$ suffix. 

For the composition of traversals, |seq| defines a sequence (perform both traversals) and |choice| an alternative (perform just one traversal). 
We use Traversal Combinators |right| and |down| to travel the zipper; the |all| variants always succeed and the |one| variants can fail. 
Traversal Strategies are defined by combining the previous tools, and come in |td| (top-down) and |bu| (bottom-up) variants. The |full| strategies traverse the whole tree, while |once| strategies perform at most one operation and |stop| strategies stop cut off the traversal in a specific sub-tree when any operation in it succeeds. Finally, |innermost| and |outermost| perform a transformation as many times as possible, starting from the inside or outside nodes, respectively.  


\begin{figure*}[h]
\begin{minipage}[t]{.55\textwidth}
\textbf{Strategy types}
> type TP a = Zipper a -> Maybe (Zipper a)
> type TU m d = (forall a. Zipper a -> m d) 
\textbf{Primitive strategies}
> idTP      :: TP a
> constTU   :: d -> TU m d
> failTP    :: TP a 
> failTU    :: TU m d
> tryTP     :: TP a -> TP a
> repeatTP  :: TP a -> TP a
\textbf{Strategy Construction}
> monoTP    :: (a -> Maybe b) -> TP e
> monoTU    :: (a -> m d) -> TU m d
> monoTPZ   :: (a -> Zipper e -> Maybe b) -> TP e
> monoTUZ   :: (a -> Zipper e -> m d) -> TU m d
> adhocTP   :: TP e -> (a -> Maybe b) -> TP e
> adhocTU   :: TU m d -> (a -> m d) -> TU m d
> adhocTPZ  :: TP e -> (a -> Zipper e -> Maybe b) -> TP e
> adhocTUZ  :: TU m d -> (a -> Zipper c -> m d) -> TU m d
\textbf{Composition / Choice}
> seqTP     :: TP a -> TP a -> TP a
> choiceTP  :: TP a -> TP a -> TP a
> seqTU     :: TU m d -> TU m d -> TU m d
> choiceTU  :: TU m d -> TU m d -> TU m d
\end{minipage}
\begin{minipage}[t]{.35\textwidth}
\textbf{Traversal Combinators}
> allTPright  :: TP a -> TP a
> oneTPright  :: TP a -> TP a
> allTUright  :: TU m d -> TU m d
> allTPdown   :: TP a -> TP a
> oneTPdown   :: TP a -> TP a
> allTUdown   :: TU m d -> TU m d
\textbf{Traversal Strategies}
> full_tdTP  :: TP a -> TP a
> full_buTP  :: TP a -> TP a
> once_tdTP  :: TP a -> TP a
> once_buTP  :: TP a -> TP a  
> stop_tdTP  :: TP a -> TP a
> stop_buTP  :: TP a -> TP a
> innermost  :: TP a -> TP a
> outermost  :: TP a -> TP a
> full_tdTU  :: TU m d -> TU m d     
> full_buTU  :: TU m d -> TU m d     
> once_tdTU  :: TU m d -> TU m d   
> once_buTU  :: TU m d -> TU m d  
> stop_tdTU  :: TU m d -> TU m d  
> stop_buTU  :: TU m d -> TU m d
\end{minipage}
\caption{Full Ztrategic API}
\label{code:api}
\end{figure*}

Let us show now the expressiveness of our |Ztrategic| library in
implementing useful transformations of a real programming language
such as |Haskell|. We reuse the available support for parsing and
pretty printing as part of the |Haskell| core libraries (in the
\textit{haskell-src} package).

\paragraph{Do-notation elimination}

We start by defining a refactoring that eliminates the syntactic sugar
introduced by the monadic \textit{do-notation}. In fact, we used this
notation in |sumBZero'|, and we can re-write in an applicative
functional style as expressed by the monadic binding function |(>>=)|.

\begin{code}
sumBZero'' :: Maybe Exp
sumBZero'' = down' t_1 >>= down' >>= right >>= getHole 
\end{code}

In order to automate this refactoring, a type-preserving strategy is
used to perform a full traversal in the |Haskell| tree, since such
expressions can be arbitrarily nested. The re-write step
behaves like the identity function by default with a
type-specific case for pattern matching the |do-notation| in the
|Haskell| syntax tree (nodes constructed by |HsDo|).


\begin{code}
refactor :: Zipper HsModule -> Maybe (Zipper HsModule)
refactor  h = applyTP (innermost step) h
    where step = failTP `adhocTP` doElim
\end{code}

The following type-specific transformation function |doElim| just
expresses the refactoring we showed for the concrete |sumBZero'| to
|sumBZero''| example. Obviously, it is expressed at abstract syntax
tree level. We omit here the details of its underlying representation
as |Haskell| data types.

\begin{code}
doElim    :: HsExp -> Maybe HsExp
doElim (HsDo [HsQualifier e])  = Just e
doElim (HsDo (HsQualifier e:stmts))    
          = Just (  (HsInfixApp  e (HsQVarOp (hsSymbol ">>"))
                    (HsDo stmts)))
doElim (HsDo (HsGenerator _ p e:stmts))
          = Just (letPattern p e stmts))
doElim (HsDo (HsLetStmt decls:stmts))
          = Just (HsLet decls (HsDo stmts))
doElim _  = Nothing
\end{code}

\paragraph{Smells elimination}

Source code smells make code harder to comprehend. A smell is not an
error, but it indicates a bad programming practice. They do occur in
any language and |Haskell| is no exception. For example, inexperienced
|Haskell| programmers often write |l == []| to check whether a list is
empty, instead of using the predefined |null| function.  Next, we
present a strategic function that eliminates several |Haskell| smells
as reported in~\cite{Cowie}.

\begin{code}
smellElim  :: Zipper HsModule -> Maybe (Zipper HsModule)
smellElim h = applyTP (innermost step) h
  where step =  failTP  `adhocTP` joinList 
                        `adhocTP` nullList 
                        `adhocTP` redundantBoolean 
                        `adhocTP` reduntantIf 
\end{code}

where the type-specific transformations are described next. 

\begin{itemize}

\item |joinList| detects patterns where list concatenations are
inefficiently defined; the pattern |[x] ++ xs| is refactored to |x :
xs|.

\item |nullList| detects patterns where a list is checked for
emptiness. Patterns such as |x == []| and |length x == 0| are
refactored to |null x|.

\item |redundantBoolean| detects redundant boolean checks, such as the
pattern |x == True| which is refactored to |x|.

\item |reduntantIf| detects redundant usages of |if| clauses, such as
the pattern |if x then True else False| which is refactored to |x|.
\end{itemize}

The full implementation of these functions is included in
Appendix~\ref{sec:appSmell}:


\begin{comment}
In fact, the code smells these optimizations solve are typical mistakes of beginner |Haskell| programmers. Through strategic application of these optimization functions, we can precisely define the transformations we intend to apply without needing to worry about boilerplate code. 
\end{comment}

\begin{comment}
This approach, of course, has the disadvantage
of having to build and maintain the zipper throughout the traversal of
the data structures. However, the loss in performance is compensated
by having more expressive power - for each node that is processed, the
context can be extracted by traversing the zipper. This will be
further explored in Section \ref{sec3}, by using a zipper-based
attribute grammar embedding.
\end{comment}

\begin{comment}
Focus on the technical part
Show code - explain that zippers (down, left etc) + overhead of actual zipper = heavy
Tradeoff performance - usability (step function only applies at most once = better performance, but more inconvenient)
\subsection{Benchmark? Repo?}
\end{comment}
