\section{The |Let| Attribute Grammar}
\label{sec:app}


The zipper-based AG function |errors| specifies the computation, via a
synthesized attribute, of the list of names that violate |Let| scope
rules. In Section~\ref{sec:strategicAG}, we have presented its
strategic counterpart definition that eliminates most of the copy (and gluing)
rules.

\begin{code}
errors :: AGTree [String]
errors  t = case (constructor t) of
        CRoot       -> errs (t.$1)
        CLet        -> (errs (t.$1)) ++ (errs (t.$2))
        CAdd        -> (errs (t.$1)) ++ (errs (t.$2))
        CSub        -> (errs (t.$1)) ++ (errs (t.$2))
        CEmptyList  -> []
        CConst      -> []
        CVar        -> mBIn (lexeme t) (env t)
        CAssign     -> mNBIn (lexeme t, t) (dcli t) 
                       ++ (errs (t.$2)) ++ (errs (t.$3))
        CNestedLet  -> mNBIn (lexeme t, t) (dcli t) 
                       ++ (errs (t.$2)) ++ (errs (t.$3))
\end{code}

To distinguish the same name declared at different nesting levels, we
define an (inherited) attribute |lev|: the outermost let has level $0$
and we increment the level when descending to a let node. The next
zipper-based AG straightforwardly specifies the necessary attribute
equations.


\begin{code}
lev :: Zipper Root -> Int
lev t =  case (constructor t) of
         CRoot  ->  0
         CLet   ->  (lev (parent t)) + 1
         _      ->  lev (parent t)
\end{code}

Attribute grammars system usually provide a declarative language to
define auxiliary semantic functions. Functions |mNBIn| and |mBIn|
stand, respectively, for \textit{``must not be in"} and
\textit{``must be in"}, are simple lookup functions. Function |mNBIn|
needs the level to detect whether a name is erroneously defined at the
same level, or not. Obviously, we could store the level in the
environment. However, in our setting we can access attributes
decorating a tree in our semantic functions. Thus, these two functions
can be defined as follows:



\begin{code}
mBIn :: String -> [(String, Zipper Root)] -> [String]
mBIn name []          = [name]
mBIn name ((n,l):es)  = if (n==name)  then []
                                      else mBIn name es

\end{code}

\begin{code}
mNBIn  :: (String, Zipper Root) -> [(String, Zipper Root)]
       -> [String]
mNBIn tuple []   =  [] 
mNBIn (a1,r1) ((a2,r2):es)
                 =  if (a1==a2) && (lev r1 == lev r2)
                    then [a1] else mNBIn (a1,r1) es
\end{code}


Zipper-based AG supports most modern extensions of attribute
grammars. For example, in~\cite{scp2016} these lookup functions are
expressed via higher-order attributes.  

\subsection{Boilerplate Code Induced by Data Types}

The code in this subsection is boilerplate code that needs to be built
once for each attribute grammar. Note that this code can be
automatically generated through Template
Haskell~\cite{templatehaskell}.

A |Constructor| data type is defined and it represents the data
constructor the zipper is focusing on and therefore must contain all
constructors in the data types.

\begin{code}
data Constructor  =  CRoot
                  |  CLet
                  |  CNestedLet
                  |  CAssign
                  |  CEmptyList
                  |  CAdd
                  |  CSub
                  |  CNeg
                  |  CConst
                  |  CVar
\end{code}

The |constructor| function looks at the zipper and returns the
aforementioned |Constructor| data value.

\begin{code}
constructor :: Zipper a -> Constructor
constructor ag =
   case (getHole ag :: Maybe Root) of
     Just (Root _) -> CRoot
     _ -> case (getHole ag :: Maybe Let) of
            Just (Let _ _) -> CLet
            _ -> case (getHole ag :: Maybe List) of
                   Just (NestedLet _ _ _   )  -> CNestedLet
                   Just (Assign _ _ _)        -> CAssign
                   Just (EmptyList       )    -> CEmptyList
                   _ ->  case (getHole ag :: Maybe Exp) of
                         Just (Add _ _)  -> CAdd
                         Just (Sub _ _)  -> CSub
                         Just (Neg _  )  -> CNeg
                         Just (Var _  )  -> CVar
                         Just (Const _)  -> CConst
                         _               -> error "Error"
\end{code}

The functions |lexeme| and |lexeme_Assign| model syntactic references
in the AG, and they access information of certain nodes.

\begin{code}
lexeme :: Zipper a -> String
lexeme ag =  case (getHole ag :: Maybe List) of
                  Just (Assign    v _ _) -> v
                  Just (NestedLet v _ _) -> v
                  _  ->  case  (getHole ag :: Maybe Exp) of
                               Just (Var s)  -> s 
                               _             -> error "Error"

lexeme_Assign  :: Zipper a -> Maybe Exp
lexeme_Assign  ag =  case (getHole ag :: Maybe List) of
                     Just(Assign _ e _)  -> Just e
                     _                   -> Nothing
\end{code}



\section{Smell elimination}
\label{sec:appSmell}

We define refactoring |smells| through an |innermost| strategy that
applies a myriad of transformations, as many times as possible. The
use of |innermost| is necessary because performing one
refactoring can enable the application of another refactoring.

\begin{code}
smells  :: Zipper HsModule -> Maybe (Zipper HsModule)
smells  h = applyTP (innermost step) h
        where step = failTP  `adhocTP` joinList 
                             `adhocTP` nullList 
                             `adhocTP` redundantBoolean 
                             `adhocTP` reduntantIf 
\end{code}

These functions are simple in the sense that they try to match a
pattern and replace them with another. They are similar in nature to
|expr| we defined in Section~\ref{sec2}, but the data types themselves
are more complex. We define a transformation to refactor the pattern
|[x] ++ xs| into |x : xs|.

\begin{code}
joinList :: HsExp -> Maybe HsExp
joinList    (HsInfixApp  (HsList [h])
                         (HsQVarOp (UnQual  (HsSymbol "++")))
                         (HsList t))
            = Just  (HsInfixApp  h
                                 (HsQConOp (Special HsCons))
                                 (HsList t))
joinList _  = Nothing
\end{code}

Next, we find patterns of bad null checking of lists, which are refactored to |null x|. The patterns are |length x == 0|, |0 == length x|, |x == []| and |[] == x|, each represented by one line of the function.
\begin{code}
nullList :: HsExp -> Maybe HsExp
nullList (HsInfixApp  (HsApp  (HsVar (UnQual
                                     (HsIdent "length")))
                              a)
                      (HsQVarOp (UnQual (HsSymbol "==")))
                      (HsLit (HsInt 0)))
  = Just $  HsApp (HsVar (UnQual (HsIdent "null"))) a
nullList (HsInfixApp  (HsLit (HsInt 0))
                      (HsQVarOp (UnQual (HsSymbol "==")))
                      (HsApp (HsVar (UnQual
                                    (HsIdent "length"))) a))
  = Just $ HsApp (HsVar (UnQual (HsIdent "null"))) a
nullList  (HsInfixApp  a
                       (HsQVarOp (UnQual (HsSymbol "==")))
                       (HsList []))
  = Just $  HsApp (HsVar (UnQual (HsIdent "null"))) a
nullList  (HsInfixApp  (HsList [])
                       (HsQVarOp (UnQual (HsSymbol "==")))
                       a)
  = Just $  HsApp (HsVar (UnQual (HsIdent "null"))) a
nullList _ = Nothing
\end{code}

We remove redundant boolean checks, such as |x==True| and |True==x|, by refactoring them to |x|. Similarly, |x==False| and |False==x| are refactored to |not x|. 

\begin{code}
redundantBoolean :: HsExp -> Maybe HsExp
redundantBoolean  (HsInfixApp
                  (HsCon (UnQual (HsIdent "True")))
                  (HsQVarOp (UnQual (HsSymbol "==")))
                  a)
  = Just a
redundantBoolean  (HsInfixApp
                  a
                  (HsQVarOp (UnQual (HsSymbol "==")))
                  (HsCon (UnQual (HsIdent "True"))))
  = Just a
redundantBoolean  (HsInfixApp
                  (HsCon (UnQual (HsIdent "False")))
                  (HsQVarOp (UnQual (HsSymbol "==")))
                  a)
  = Just $ (HsApp (HsVar (UnQual (HsIdent "not"))) a)
redundantBoolean  (HsInfixApp
                  a
                  (HsQVarOp (UnQual (HsSymbol "==")))
                  (HsCon (UnQual (HsIdent "False"))))
  = Just $ (HsApp (HsVar (UnQual (HsIdent "not"))) a)
redundantBoolean _ = Nothing
\end{code}

Finally, we remove redundant usages of |if| clauses by refactoring |if x then True else False| into |x| and, conversely, |if x then False else True| into |not x|. 

\begin{code}
reduntantIf :: HsExp -> Maybe HsExp
reduntantIf (HsIf  a
                   (HsCon (UnQual (HsIdent "True")))
                   (HsCon (UnQual (HsIdent "False"))))
  = Just a
reduntantIf (HsIf  a
                   (HsCon (UnQual (HsIdent "False")))
                   (HsCon (UnQual (HsIdent "True"))))
  = Just $ HsApp (HsVar (UnQual (HsIdent "not"))) a
reduntantIf _ = Nothing
\end{code}

We can easily extend |smells| by implementing more refactor
transformations as simple |Haskell| functions and appending them in
the definition of |step|.
