package org.bitbucket.inkytonik.kiama
package example.PP

import org.bitbucket.inkytonik.kiama.attribution.Attribution
import org.bitbucket.inkytonik.kiama.relation.Tree


sealed abstract class PPRoot extends Product
case class Best(PPS : PPS) extends PPRoot
case class All(PPS : PPS) extends PPRoot
case class Displ(PPS : PPS) extends PPRoot

sealed abstract class PPS extends PPRoot
case class Empty() extends PPS
case class Text(string : String) extends PPS
case class Indent(int : Int, PPS : PPS) extends PPS
case class Beside(PPSl : PPS, PPSr : PPS) extends PPS
case class Above(PPSu : PPS, PPSd : PPS) extends PPS
case class Dup(PPS1 : PPS, PPS2 : PPS) extends PPS
case class Join(PPS : PPS) extends PPS
case class Apply(PPC : PPC, PPSArgs : PPSArgs) extends PPS
case class Filla(FillList : FillList) extends PPS
case class FillBlock(int : Int, FillList : FillList) extends PPS

sealed abstract class PPC extends PPS
case class ParC() extends PPC
case class IndentC(int : Int, PPC : PPC) extends PPC
case class BesideC(PPCl : PPC, PPCr : PPC) extends PPC
case class AboveC(PPCu : PPC, PPCd : PPC) extends PPC
case class DupC(PPC1 : PPC, PPC2 : PPC) extends PPC
case class JoinC(PPC : PPC) extends PPC
case class ApplyC(PPC : PPC, PPCArgs : PPCArgs) extends PPC

sealed abstract class PPSArgs extends PPS
case class ConsArgs(PPSArgs : PPSArgs, PPS : PPS) extends PPSArgs
case class NilArgs() extends PPSArgs

sealed abstract class PPCArgs extends PPS
case class ConsPPCArgs(PPCArgs : PPCArgs, PPC : PPC) extends PPCArgs
case class NilPPCArgs() extends PPCArgs

sealed abstract class FillList extends PPS
case class ConsFillList(FillList : FillList, PPS : PPS) extends FillList
case class NilFillList() extends FillList


trait PPBase extends Attribution {

  type T_PW  = Int
  type T_PLL = Int
  type T_PH  = Int

  type T_MINS = Vector[Sizes]

  type Sizes = (T_PW, T_PLL, T_PH)

  sealed abstract class T_FRAME
  case class F(T_PW : T_PW, T_PLL : T_PLL) extends T_FRAME

  //implicit val ordering : Ordering[T_FRAME] = Ordering.by[T_FRAME, T_PW](tframe => tframe.asInstanceOf[F].T_PW)

  val max : (T_FRAME, T_FRAME) => T_FRAME = {
    case (x@F(w,_), y@F(ww,_)) => if (w > ww) x else y
  }

  type T_REQS = Vector[T_FRAME]

  type Formats = Vector[Format]

  sealed abstract class Format
  case class Elem(height : T_PH, last_w : T_PLL, total_w : T_PW, txtstr : Int => String => String) extends Format


  sealed abstract class T_FORMATS
  case class AFormat(Formats : Formats) extends T_FORMATS
  case class TFormats(Formats1 : Formats, Formats2 : Formats, T_ERROR1 : T_ERROR, T_ERROR2 : T_ERROR) extends T_FORMATS

  type T_FMTS = Vector[T_FORMATS]

  type T_ERROR = Boolean

  type T_ERRS = Vector[T_ERROR]

}

class Core extends PPBase {

  // ...................................................................
  // ..... Basic machinery .............................................

  def emptyFmts() : Formats = {
    Vector.empty
  }

  def textFmts(s : String) : Formats = {
    Vector(s2fmt(s))
  }

  val indentFmts : (T_FRAME, Int, Formats) => Formats = {
    case (F(pw,_), i, fmts) =>
      fmts.dropWhile(notFits(pw - i, _)).map(indentFmt(i, _))
  }

  val notFits : (T_PW, Format) => Boolean = {
    case (delta, Elem(_, _, total_w, _)) => total_w > delta
  }


  val besideFmts : (T_FRAME) => (Formats) => (Formats) => Formats = {
    case (F(pw,_)) => left => right => mergel(left.map(l => right.dropWhile(tooWide(pw, l, _)).map(besideFmt(l,_))))
  }


  def tooWide(pw : T_PW, x : Format, y : Format) : Boolean = {
    val xx = x.asInstanceOf[Elem]
    val yy = y.asInstanceOf[Elem]
    xx.total_w.max(xx.last_w + yy.total_w) > pw
  }

  def aboveFmts(up : Formats)(low : Formats) : Formats = {
    (up,low) match {
      case (Vector(),_) => Vector.empty
      case (_,Vector()) => Vector.empty
      case (upper +: ru, lower +: rl) => {
        val utw = upper.asInstanceOf[Elem].total_w
        val ltw = lower.asInstanceOf[Elem].total_w
        val firstelem = aboveFmt(upper,lower)
        if (utw > ltw) firstelem +: aboveFmts(ru)(low)
        else firstelem +: aboveFmts(up)(rl)
      }
      case _ => sys.error("")
    }
  }


  // Pretty-printing with error correction

  def errorIndent(i : Int, f : Formats)  : Formats = {
    f.map(indentFmt(i, _))
  }


  def errorBeside(left : Formats)(right : Formats) : Formats = {
    mergel(left.map(l => right.map(besideFmt(l,_))))
  }

  // -------------------------------------------------------------------
  // Formatting one layout ---------------------------------------------

  def s2fmt(s : String) : Format = {
    val l = s.length
    val fn = (x:Int) => (y:String) => s ++ y
    Elem(1,l,l,fn)
  }

  val indentFmt : (Int, Format) => Format = {
    case (i, Elem(dh,dl,dw,dt)) => {
      val f = (n : Int) => (y : String) => sp(i) ++ dt(i + n)(y)
      Elem(dh,i + dl,i + dw,f)
    }
  }

  def aboveFmt : (Format,Format) => Format = {
    case (Elem(uh,_,uw,ut), Elem(lh, ll, lw, lt)) => {
        Elem(uh + lh, ll, uw.max(lw), make_ts_above(ut, lt))
    }
  }

  def make_ts_above (u : (Int => String => String), l : (Int => String => String)) : Int => String => String = {
    (n : Int) => {
      val nl_skip : String => String = (('\n' +: sp(n)) ++ _)
      (str => u(n)(nl_skip(l(n)(str))))
    }
  }

  val besideFmt : (Format,Format) => Format = {
    case (Elem(lh,ll,lw,lt), Elem(rh, rl, rw, rt)) => {
      Elem(lh + rh - 1, ll + rl, lw.max(ll + rw), n => str => lt(n)(rt(ll+n)(str)))
    }
  }

  // -------------------------------------------------------------------
  // Display the layout found ------------------------------------------

  def best(fs : Vector[Format]) : String = {
    if (fs.isEmpty) "" else fs.head.asInstanceOf[Elem].txtstr(0)("")
  }

  def allf(fs : Vector[Format]) : String = {
    fs.map(_.asInstanceOf[Elem].txtstr(0)("\n\n")).mkString
  }

  def dispf(fs : Vector[Format]) : String = {
    if (fs.isEmpty) "" else fs.head.asInstanceOf[Elem].txtstr(0)("")
  }


  // -------------------------------------------------------------------
  // Utility functions -------------------------------------------------

  /*
  def merge[T <: Ordered[T]](first : Vector[T], second : Vector[T]) : Vector[T] = (first, second) match {
    case (Vector(), _) => second
    case (_, Vector()) => first
    case (x +: xs, y +: ys) => if (x == y) x +: merge(xs, ys) else if (x < y) x +: merge(xs, second) else y +: merge(first, ys)
  }
   */

  def merge(first : Formats, second : Formats) : Formats = (first, second) match {
    case (Vector(), _) => second
    case (_, Vector()) => first
    case (x +: xs, y +: ys) => if (x == y) x +: merge(xs, ys) else if ((x.asInstanceOf[Elem].height < y.asInstanceOf[Elem].height) || (x.asInstanceOf[Elem].height == y.asInstanceOf[Elem].height && (x.asInstanceOf[Elem].total_w < y.asInstanceOf[Elem].total_w))) x +: merge(xs, second) else y +: merge(first, ys)
    case _ => sys.error("")
  }

  val sp : Int => String = {
    case 0 => ""
    case n => " " ++ sp(n - 1)
  }

  def mergel(l : Vector[Formats]) : Formats = {
    l.foldRight(Vector[Format]())(merge(_,_))
  }

}

class Pretty(tree : Tree[PPRoot,PPRoot]) extends Core {

  val minw : PPRoot => T_PW =
    attr {
      case Empty() => 0
      case Text(s) => s.length
      case Indent(i,t) => i + minw(t)
      case Beside(l,r) => minw(l).max(minll(l) + minw(r))
      case Above(u,d) => minw(u).max(minw(d))
      case Dup(t1,t2) => minw(t1).min(minw(t2))
      case Join(t) => minw(t)
      case Apply(t,pps) => if (numPars(t) != len(pps)) setErrorMsg(numPars(t))(len(pps)).length else minw(t)
      case t@ParC() => fillmins(t).head._1
      case IndentC(i,t) => i + minw(t)
      case BesideC(l,r) => minw(l).max(minll(l) + minw(r))
      case AboveC(u,d) => minw(u).max(minw(d))
      case DupC(t1,t2) => minw(t1).min(minw(t2))
      case JoinC(t) => minw(t)
      case ApplyC(t,pps) => if (numPars(t) != len(pps)) setErrorMsg(numPars(t))(len(pps)).length else minw(t)
      case Filla(t) => minw(t)
      case FillBlock(_,t) => minw(t)
      case ConsFillList(t,_) => minw(t)
      case t@NilFillList() => minwi(t)
      case _ => sys.error("")
    }

  val minll : PPRoot => T_PLL =
    attr {
      case Empty() => 0
      case Text(s) => s.length
      case Indent(i,t) => i + minll(t)
      case Beside(l,r) => minll(l) + minll(r)
      case Above(u,d) => minll(d)
      case Dup(t1,t2) => minll(t1).min(minll(t2))
      case Join(t) => minw(t)
      case Apply(t,pps) => if (numPars(t) != len(pps)) setErrorMsg(numPars(t))(len(pps)).length else minll(t)
      case t@ParC() => fillmins(t).head._2
      case IndentC(i,t) => i + minll(t)
      case BesideC(l,r) => minll(l) + minll(r)
      case AboveC(u,d) => minll(d)
      case DupC(t1,t2) => minll(t1).min(minll(t2))
      case JoinC(t) => minll(t)
      case ApplyC(t,pps) => if (numPars(t) != len(pps)) setErrorMsg(numPars(t))(len(pps)).length else minll(t)
      case Filla(t) => minll(t)
      case FillBlock(_,t) => minll(t)
      case ConsFillList(t,_) => minll(t)
      case t@NilFillList() => minlli(t)
      case _ => sys.error("")
    }

  val frame : PPRoot => Int => T_FRAME = {
    attr {
      case t: PPRoot => f => tree.parent(t).head match {
        case g@Indent(i,_) => narrowFrame(i)(frame(g)(f))
        case g@Beside(l,r) => if (t == r) narrowFrame(minll(l))(frame(g)(f)) else narrowll(minw(r))(frame(g)(f))
        case g@Above(_,_) => frame(g)(f)
        case g@Dup(_,_) => frame(g)(f)
        case g@Join(_) => frame(g)(f)
        case g@Apply(_,ppc) => if (t == ppc) frame(g)(f) else sys.error("")
        case g@IndentC(i,_) => narrowFrame(i)(frame(g)(f))
        case g@BesideC(l,r) => if (t == r) narrowFrame(minll(l))(frame(g)(f)) else narrowll(minw(r))(frame(g)(f))
        case g@AboveC(_,_) => frame(g)(f)
        case g@DupC(_,_) => frame(g)(f)
        case g@JoinC(_) => frame(g)(f)
        case g@ConsArgs(_,p) if (t == p) => reqs(g)(f).head
        case g@ApplyC(_,ppc) if (t == ppc) => frame(g)(f)
        case g@ConsPPCArgs(_,p) if (t == p) => ireqs(g)(f).head
        case g@Filla(_) => {
          val l_pw = frame(g)(f).asInstanceOf[F].T_PW
          F(l_pw,l_pw)
        }
        case FillBlock(fill,_) => F(fill,fill)
        case g@ConsFillList(_,_) => frame(g)(f)
        case Best(_) => F(f,f)
        case All(_) => F(f,f)
        case Displ(_) => F(f,f)
        case _ => sys.error("")
      }
    }
  }

  val narrowFrame : Int => T_FRAME => T_FRAME = {
    i => {
      case F(s, l) => F(s - i, l - i)
    }
  }

  val narrowll : Int => T_FRAME => T_FRAME = {
    i => {
      case F(s, l) => F(s, l - i)
    }
  }

  val numPars : PPRoot => Int = {
    attr{
      case ParC() => 1
      case IndentC(_,t) => numPars(t)
      case BesideC(l,r) => numPars(l) + numPars(r)
      case AboveC(u,d) => numPars(u) + numPars(d)
      case DupC(t1,_) => numPars(t1)
      case JoinC(t) => numPars(t)
      case ApplyC(_,ppc) => numPars(ppc)
      case NilPPCArgs() => 0
      case ConsPPCArgs(ppc,p) => numPars(ppc) + numPars(p)
      case _ => sys.error("")
    }
  }

  val fillmins : PPRoot => T_MINS = {
    attr{
      case NilPPCArgs() => Vector.empty
      case ConsPPCArgs(ppc,p) => (minw(p), minll(p), maxh(p)) +: fillmins(ppc)
      case t : PPRoot => tree.parent(t).head match {
        case g@ConsPPCArgs(ppc,p) if (t == p) => fillmins(g).take(numPars(ppc))
        case Apply(p,pps) if (t == p) => mins(pps)
        case ApplyC(p,ppc) if (t == p) => fillmins(ppc)
        case g@BesideC(l,r) => if (t == r) fillmins(g).drop(numPars(l)) else fillmins(g).take(numPars(l))
        case g@AboveC(u,d) => if (t == d) fillmins(g).drop(numPars(u)) else fillmins(g).take(numPars(u))
        case g@DupC(_,_) => fillmins(g)
        case g@IndentC(_,_) => fillmins(g)
        case g@JoinC(_) => fillmins(g)
        case _ => sys.error("")
      }
    }
  }

  val mins : PPRoot => T_MINS = {
    attr {
      case NilArgs() => Vector.empty
      case ConsArgs(pps, p) => (minw(p), minll(p), maxh(p)) +: mins(pps)
      case _ => sys.error("")
    }
  }

  val reqs : PPRoot => Int => T_REQS = {
    attr {
      case t@ParC() => f => Vector(frame(t)(f))
      case IndentC(_,t) => reqs(t)(_)
      case BesideC(l,r) => f => reqs(l)(f) ++ reqs(r)(f)
      case AboveC(u,d) => f => reqs(u)(f) ++ reqs(d)(f)
      case DupC(t1,t2) => f => reqs(t1)(f).lazyZip(reqs(t2)(f)).map { case (x,y) => max(x,y)}
      case JoinC(t) => reqs(t)(_)
      case ApplyC(_,ppc) => reqs(ppc)(_)
      case ConsPPCArgs(ppc,p) => f => reqs(ppc)(f) ++ reqs(p)(f)
      case NilPPCArgs() => Vector.empty
      case t : PPRoot => f => tree.parent(t).head match {
        case Apply(p, pps) if (t == pps) => reqs(p)(f)
        case g@ConsArgs(pps, p) if (t == pps) => reqs(g)(f).tail
        case _ => sys.error("")
      }
      case _ => sys.error("")
    }
  }

  val ifillmins : PPRoot => T_MINS = {
    case t : PPRoot => tree.parent(t).head match {
      case g@ApplyC(_,ppc) if (t == ppc) => fillmins(g)
      case g@ConsPPCArgs(ppc,p) if (t == ppc) => fillmins(g).drop(numPars(p))
      case _ => sys.error("")
    }
  }

  val ireqs : PPRoot => Int => T_REQS = {
    case t : PPRoot => tree.parent(t).head match {
      case ApplyC(p,ppc) if (t == ppc) => reqs(p)(_)
      case g@ConsPPCArgs(ppc,p) if (t == ppc) => ireqs(g)(_).tail
      case _ => sys.error("")
    }
  }

  //-----------------------------------------------------

  val len : PPRoot => Int = {
    attr{
      case NilArgs() => 0
      case ConsArgs(a,_) => 1 + len(a)

      case NilPPCArgs() => 0
      case ConsPPCArgs(ppc,_) => 1 + len(ppc)

      case _ => sys.error("")
    }
  }

  val setErrorMsg : Int => Int => String = {
    numpars => {
      leng => "<Error: incorrect apply expression. #pars "
        ++ numpars.toString ++ " != #args "
        ++ leng.toString    ++ ">"
    }
  }

  val eqSetErrorMsg : Int => Int => String = {
    apars => {
      bpars => "<Error: incorrect choice expression. #pars left "
        ++ apars.toString ++ " != #pars right "
        ++ bpars.toString ++ ">"
    }
  }

  //-----------------------------------------------------
  // MaxH

  val maxh : PPRoot => T_PH = {
    attr{
      case Empty() => 0
      case Text(s) => 1
      case Indent(i,t) => maxh(t)
      case Beside(l,r) => besideHeight(maxh(l))(maxh(r))
      case Above(u,d) => maxh(u) + maxh(d)
      case Dup(t1,t2) => maxh(t1).max(maxh(t2))
      case Join(t) => maxh(t)
      case Apply(t,pps) => if (numPars(t) != len(pps)) 1 else maxh(t)
      case t@ParC() => fillmins(t).head._3
      case IndentC(i,t) => maxh(t)
      case BesideC(l,r) => besideHeight(maxh(l))(maxh(r))
      case AboveC(u,d) => maxh(u) + maxh(d)
      case DupC(t1,t2) => maxh(t1).max(maxh(t2))
      case JoinC(t) => maxh(t)
      case ApplyC(t,pps) => if (numPars(t) != len(pps)) 1 else maxh(t)
      case Filla(t) => maxh(t)
      case FillBlock(_,t) => maxh(t)
      case ConsFillList(t,_) => maxh(t)
      case t@NilFillList() => maxhi(t)
      case _ => sys.error("")
    }
  }

  val maxhi : PPRoot => T_PH = {
    attr {
      case t: PPRoot => tree.parent(t).head match {
        case g@ConsFillList(fl, pps) if (t == fl) => consHeight(maxh(pps), maxhi(g), true)
        case Filla(_) => 0
        case FillBlock(_, _) => 0
        case _ => sys.error("")
      }
    }
  }

  val besideHeight : Int => Int => Int = {
    lh => {
      rh => if (lh == 0 || rh == 0) 0 else 1
    }
  }

  def consHeight( pPh : Int, acth : Int, avail : Boolean) : Int = {
    acth match {
      case 0 => if (pPh > 0) 1 else 0
      case _ => acth + (if (avail) 0 else 1)
    }
  }

  // -----------------------------------------------------------------------
  // Fill

  val pw : PPRoot => Int => T_PW = {
    attr {
      case t : PPRoot => tree.parent(t).head match {
        case g@ConsFillList(fl,_) if (t == fl) => pw(g)(_)
        case g@Filla(_) => frame(g)(_).asInstanceOf[F].T_PW
        case FillBlock(i,_) => _ => i
        case _ => sys.error("")
      }
    }
  }

  val minwi : PPRoot => T_PW = {
    attr {
      case t : PPRoot => tree.parent(t).head match {
        case g@ConsFillList(fl,_) if (t == fl) => minwi(g)
        case Filla(_) => 0
        case FillBlock(_,_) => 0
        case _ => sys.error("")
      }
    }
  }

  val minlli : PPRoot => T_PW = {
    attr {
      case t : PPRoot => tree.parent(t).head match {
        case g@ConsFillList(fl,pps) if (t == fl) => minlli(g) + minwi(g) + minw(pps)
        case Filla(_) => 0
        case FillBlock(_,_) => 0
        case _ => sys.error("")
      }
    }
  }

  // ------------------------------------------------------------------------------------

  val fillfmts : PPRoot => Int => T_FMTS = {
    attr {
      case t: PPRoot => tree.parent(t).head match {
        case Apply(p,pps) if (t == p) => fmtsL(pps)(_)
        case ApplyC(p,ppc) if (t == p) => fmtsL(ppc)(_)
        case g@ConsPPCArgs(ppc,p) if (t == p) => ifillfmts(g)(_).take(numPars(t))
        case g@BesideC(l, r) => if (t == r) fillfmts(g)(_).drop(numPars(l)) else fillfmts(g)(_).take(numPars(l))
        case g@AboveC(u, d) => if (t == d) fillfmts(g)(_).drop(numPars(u)) else fillfmts(g)(_).take(numPars(u))
        case g@DupC(_, _) => fillfmts(g)(_)
        case g@IndentC(_, _) => fillfmts(g)(_)
        case g@JoinC(_) => fillfmts(g)(_)
        case _ => sys.error("")
      }
    }
  }

  val ifillfmts : PPRoot => Int => T_FMTS = {
    attr {
      case t: PPRoot => tree.parent(t).head match {
        case g@ApplyC(_, ppc) if (t == ppc) => fillfmts(g)(_)
        case g@ConsPPCArgs(ppc, p) if (t == ppc) => ifillfmts(g)(_).drop(numPars(p))
        case _ => sys.error("")
      }
    }
  }

  val fillerrs : PPRoot => Int => T_ERRS = {
    attr {
      case t: PPRoot => tree.parent(t).head match {
        //case g@Apply(p, pps) if (t == p) => errorL(pps)(_)
        //case g@ApplyC(p, ppc) if (t == p) => errorL(pps)(_)
        case g@ConsPPCArgs(ppc, p) if (t == p) => ifillerrs(g)(_).take(numPars(p))
        case g@BesideC(l, r) => if (t == r) fillerrs(g)(_).drop(numPars(l)) else fillerrs(g)(_).take(numPars(l))
        case g@AboveC(u, d) => if (t == d) fillerrs(g)(_).drop(numPars(u)) else fillerrs(g)(_).take(numPars(u))
        case g@DupC(_, _) => fillerrs(g)(_)
        case g@IndentC(_, _) => fillerrs(g)(_)
        case g@JoinC(_) => fillerrs(g)(_)
        case _ => sys.error("")
      }
    }
  }

  val ifillerrs : PPRoot => Int => T_ERRS = {
    attr {
      case t: PPRoot => tree.parent(t).head match {
        case g@ApplyC(_, ppc) if (t == ppc) => fillerrs(g)(_)
        case g@ConsPPCArgs(ppc, p) if (t == ppc) => ifillerrs(g)(_).drop(numPars(p))
        case _ => sys.error("")
      }
    }
  }


  // -----------------------------------------------------------------------------------
  // Formats

  val fmts : PPRoot => Int => T_FORMATS = {
    attr {
      case Empty() => _ => setFmtsEmpty
      case t@Text(s) => f => setFmtsText(s, s.length, s.length > frame(t)(f).asInstanceOf[F].T_PW)
      case t@Indent(i,p) => f => setFmtsIndent(i, fmts(p)(f), frame(t)(f).asInstanceOf[F].T_PW, i + minw(p), frame(t)(f), errorr(t)(f))
      case t@Beside(l,r) => f => setFmtsBeside(fmts(l)(f), fmts(r)(f), maxh(l), maxh(r), frame(t)(f), errorr(l)(f) || errorr(r)(f))._1
      case Above(u,d) => f => setFmtsAbove(fmts(u)(f), fmts(d)(f), maxh(u), maxh(d))._1
      case Dup(t1,t2) => f => semFmtsDup(fmts(t1)(f), fmts(t2)(f), errorr(t1)(f), errorr(t2)(f), minw(t1).min(minw(t2)))
      case Join(t) => f => setFmtsJoin(fmts(t)(f), errorr(t)(f))._1
      case Apply(t,pps) => f => eqSetFmtsApply(numPars(t) != len(pps), setErrorMsg(numPars(t))(len(pps)), fmts(t)(f))
      case t@ParC() => f => fillfmts(t)(f).head
      case t@IndentC(i,p) => f => setFmtsIndent(i, fmts(p)(f), frame(t)(f).asInstanceOf[F].T_PW, i + minw(p), frame(t)(f), errorr(t)(f))
      case t@BesideC(l,r) => f => setFmtsBeside(fmts(l)(f), fmts(r)(f), maxh(l), maxh(r), frame(t)(f), errorr(l)(f) || errorr(r)(f))._1
      case AboveC(u,d) => f => setFmtsAbove(fmts(u)(f), fmts(d)(f), maxh(u), maxh(d))._1
      case DupC(t1,t2) => f => semFmtsCdup(fmts(t1)(f), fmts(t2)(f), errorr(t1)(f), errorr(t2)(f), numPars(t1), numPars(t2), minw(t1).min(minw(t2)), eqSetErrorMsg(numPars(t1))(numPars(t2)))
      case JoinC(t) => f => setFmtsJoin(fmts(t)(f), errorr(t)(f))._1
      case ApplyC(t,pps) => f => eqSetFmtsApply(numPars(t) != len(pps), setErrorMsg(numPars(t))(len(pps)), fmts(t)(f))
      case Filla(t) => f => eqSetFmtsFill(fmtsFL(t)(f))
      case t@FillBlock(i,p) => f => setFmtsFillblock(i, fmtsFL(p)(f), frame(t)(f).asInstanceOf[F].T_PW)
      case _ => sys.error("")
    }
  }

  val fmtsi : PPRoot => Int => Formats = {
    attr{
      case t : PPRoot => tree.parent(t).head match {
        case g@ConsFillList(pps,p) if (t == pps) => f => setFmtsFilllist(fmtsi(g)(f), fmts(p)(f), maxhi(g), maxh(p), frame(g)(f), pw(t)(f) - (minlli(t) + minw(p)) >= 0)._1
        case Filla(_) => _ => emptyFmts()
        case FillBlock(_,_) => _ => emptyFmts()
        case _ => sys.error("")
      }
    }
  }

  val fmtsFL : PPRoot => Int => Formats = {
    attr {
      case ConsFillList(pps, p) => f => fmtsFL(pps)(f)
      case t@NilFillList() => f => fmtsi(t)(f)
      case _ => sys.error("")
    }
  }

  val fmtsL : PPRoot => Int => T_FMTS = {
    attr {
      case NilArgs() => Vector.empty
      case ConsArgs(pps,p) => f => fmts(p)(f) +: fmtsL(pps)(f)
      case NilPPCArgs() => Vector.empty
      case ConsPPCArgs(ppc,p) => f => fmts(p)(f) +: fmtsL(ppc)(f)
      case _ => sys.error("")
    }
  }

  val fmtsRoot : PPRoot => Int => String = {
    attr {
      case Best(pps) => f => eqBestFmts(f, fmts(pps)(f))
      case All(pps) => f => eqAllFmts(f, fmts(pps)(f))
      case Displ(pps) => f => eqDispFmts(f, fmts(pps)(f))
      case _ => sys.error("")
    }
  }

  def afmtTxt(s : String) : T_FORMATS = {
    AFormat(textFmts(s))
  }

  def setFmtsEmpty : T_FORMATS = {
    AFormat(emptyFmts())
  }

  def setFmtsText(string : String, mw : Int, err : T_ERROR) : T_FORMATS = {
    afmtTxt(if (err) asts(mw) else string)
  }

  def setFmtsIndent(int : Int, fmt : T_FORMATS, p : T_PW, mw : T_PW, fr : T_FRAME, err : T_ERROR) : T_FORMATS = {
    def setFmtsIndentAux(fmt_fc : Int => Formats => Formats) : T_FORMATS = {
      fmt match {
        case AFormat(fs) => AFormat(fmt_fc(int)(fs))
        case TFormats(as,bs,ae,be) => TFormats((fmt_fc(int)(as)), (fmt_fc(int)(bs)), ae, be)
      }
    }
    (int, err) match {
      case (int,_) if int < 0 => afmtTxt("<Error: negative indentation>")
      case (int,_) if int > p => afmtTxt(asts(mw))
      case (_,err) => {
        val f = (n : Int) => (y : Formats) => errorIndent(n,y)
        setFmtsIndentAux(f)
      }
      case _ => {
        val f = (n : Int) => (y : Formats) => indentFmts(fr,n,y)
        setFmtsIndentAux(f)
      }
    }
  }


  def setFmtsBeside(ls : T_FORMATS, rs : T_FORMATS, lh : T_PH, rh : T_PH, fr : T_FRAME, err : T_ERROR) : (T_FORMATS,Boolean) = {
    def setFmtsBesideAux(as : Formats)(bs : Formats) : Formats = {
      setFmtsAboveA((lh == 0), (rh == 0), as, bs, (if (err) errorBeside else besideFmts(fr)))
    }
    setFmtsAbAbove(ls)(rs)(setFmtsBesideAux)("<Error: can't beside two pairs>")
  }

  def setFmtsAbove(us : T_FORMATS, ls : T_FORMATS, uh : T_PH, lh : T_PH) : (T_FORMATS,Boolean) = {
    def setFmtsAboveAux(as : Formats)(bs : Formats) : Formats = {
      setFmtsAboveA((uh == 0), (lh == 0), as, bs, aboveFmts)
    }
    setFmtsAbAbove(us)(ls)(setFmtsAboveAux)("<Error: can't above two pairs>")
  }

  def setFmtsAboveA(aempty : Boolean, bempty : Boolean, as : Formats, bs : Formats, fmt_fc : (Formats => Formats => Formats)) : Formats = {
    (aempty,bempty) match {
      case(true,_) => bs
      case (_,true) => as
      case _ => fmt_fc(as)(bs)
    }
  }

  def setFmtsAbAbove(fs : T_FORMATS)(gs : T_FORMATS)(fmt_fc :(Formats => Formats => Formats))(etxt : String) : (T_FORMATS, Boolean) = {
    fs match {
      case AFormat(ffmts) => gs match {
        case AFormat(gfmts) => (AFormat(fmt_fc(ffmts)(gfmts)), false)
        case TFormats(as,bs,ae,be) => (TFormats(fmt_fc(ffmts)(as), fmt_fc(ffmts)(bs),ae,be), false)
      }
      case TFormats(as,bs,ae,be) => gs match {
        case AFormat(gfmts) => (TFormats(fmt_fc(as)(gfmts), fmt_fc(bs)(gfmts),ae,be), false)
        case _ => (afmtTxt(etxt), true)
      }
    }
  }

  def semFmtsDup(afs : T_FORMATS, bfs : T_FORMATS, ae : T_ERROR, be : T_ERROR, mw : T_PW) : T_FORMATS = {
    if (ae && be)
      afmtTxt(asts(mw))
    else {
      def get_fmts(fs : T_FORMATS) : Formats = {
        fs match {
          case AFormat(as) => as
          case TFormats(_,_,_,_) => textFmts("<Error: can't dup a dup>")
        }
      }
      val afmts = get_fmts(afs)
      val bfmts = get_fmts(bfs)
      TFormats(afmts,bfmts,ae,be)
    }
  }

  def setFmtsJoin(fs : T_FORMATS, err : T_ERROR) : (T_FORMATS, Boolean) = {
    fs match {
      case TFormats(as,bs,ae,be) =>
        (AFormat(if (be)
          if (as.isEmpty)
            bs
          else as
        else
          if (ae)
            if (bs.isEmpty) as else bs
        else merge(as,bs)
      ), false)
      case _ => sys.error("")
    }
  }

  val setFmtsApply : (T_ERROR,T_FORMATS,T_FORMATS) => T_FORMATS = {
    case (true, a, _) => a
    case (false, _, b) => b
  }

  def setFmtsFillblock(int: Int, fmt : Formats, w: Int) : T_FORMATS = {
    int match {
      case int if int < 0  => afmtTxt("<Error: negative page width in fillblock>")
      case int if int > w  => afmtTxt(asts(int))
      case _ => AFormat(fmt)
    }
  }

  def eqSetFmtsApply(err : T_ERROR, msg : String, fmt : T_FORMATS) : T_FORMATS = {
    setFmtsApply(err,(AFormat(textFmts(msg))),fmt)
  }

  val eqSetFmtsFill : Formats => T_FORMATS = {
    x => AFormat(x)
  }

  val asts : Int => String = {
    case 0 => ""
    case 1 => "*"
    case s => ":" ++ "*" * (s-2) ++ ">"
  }

  def semFmtsCdup(afs : T_FORMATS, bfs : T_FORMATS, ae : T_ERROR, be : T_ERROR, an : Int, bn : Int, mw : T_PW, em : String) : T_FORMATS = {
    if (an != bn) afmtTxt(em)
    else semFmtsDup(afs,bfs,ae,be,mw)
  }

  def setFmtsFilllist(ifmts : Formats, nfmts : T_FORMATS, ih : T_PH, nh : T_PH, fr : T_FRAME, avail : Boolean) : (Formats, Boolean) = {
    def setFmtsFilllistAux(fs : Formats) : Formats = {
      def choose_ab(bsd_fc : (Formats => Formats => Formats)) : (Formats => Formats => Formats) = {
        if (avail) bsd_fc else aboveFmts
      }
      setFmtsAboveA((ih == 0), (nh == 0), fs, ifmts, choose_ab(errorBeside))
    }
    nfmts match {
      case AFormat(ns) => {
        if (ih == 0) (ns, false)
        else if (nh == 0) (ifmts, false)
        else if (nh <= 1) ((chooseAbBesideFmts(avail, ifmts, ns, fr)), false)
        else ((chooseAbErrorBeside(avail, ifmts, textFmts("<Error: element in fill higher than 1>")), true))
      }
      case _ => (setFmtsFilllistAux(textFmts("<Error: element in fill higher than 1>")), true)
    }
  }

  def chooseAbBesideFmts(avail : Boolean, fa : Formats, fb : Formats, f : T_FRAME) : Formats = {
    if (avail) besideFmts(f)(fa)(fb) else aboveFmts(fa)(fb)
  }

  def chooseAbErrorBeside(avail : Boolean, fa : Formats, fb : Formats) : Formats = {
    if (avail) errorBeside(fa)(fb) else aboveFmts(fa)(fb)
  }

  def eqAllFmts(p : T_PW, fmt : T_FORMATS) : String = {
    allf(setFmtsRender(p,fmt))
  }

  def eqBestFmts(p : T_PW, fmt : T_FORMATS) : String = {
    best(setFmtsRender(p,fmt))
  }

  def eqDispFmts(p : T_PW, fmt : T_FORMATS) : String = {
    dispf(setFmtsRender(p,fmt))
  }

  def setFmtsRender(p : T_PW, fmt : T_FORMATS) : Formats = {
    if (p<0) textFmts("<Error: negative page width >")
    else fmt match {
      case AFormat(fm) => fm
      case _ => textFmts("<Error: can\'t render a pair>")
    }
  }

  // --------------------------------------------------------------------------
  // -- Error

  val errorr : PPRoot => Int => T_ERROR = {
    attr {
      case Empty() => _ => false
      case t@Text(s) => s.length > frame(t)(_).asInstanceOf[F].T_PW
      case t@Indent(i, s) => f => (i < 0) || i > frame(t)(f).asInstanceOf[F].T_PW || errorr(s)(f)
      case t@Beside(l, r) => f => errorr(l)(f) || errorr(r)(f) || setFmtsBeside(fmts(l)(f), fmts(r)(f), maxh(l), maxh(r), frame(t)(f), errorr(l)(f) || errorr(r)(f))._2
      case Above(u, d) => f => errorr(u)(f) || errorr(d)(f) || setFmtsAbove(fmts(u)(f), fmts(d)(f), maxh(u), maxh(d))._2
      case Dup(t1, t2) => f => errorr(t1)(f) && errorr(t2)(f)
      case Join(t) => f => errorr(t)(f) || setFmtsJoin(fmts(t)(f), errorr(t)(f))._2
      case Apply(t, pps) => f => numPars(t) != len(pps) || errorr(t)(f)
      case t@ParC() => f => fillerrs(t)(f).head
      case t@IndentC(i, s) => f => i < 0 || i > frame(t)(f).asInstanceOf[F].T_PW || errorr(s)(f)
      case t@BesideC(l, r) => f => errorr(l)(f) || errorr(r)(f) || setFmtsBeside(fmts(l)(f), fmts(r)(f), maxh(l), maxh(r), frame(t)(f), errorr(l)(f) || errorr(r)(f))._2
      case AboveC(u, d) => f => errorr(u)(f) || errorr(d)(f) || setFmtsAbove(fmts(u)(f), fmts(d)(f), maxh(u), maxh(d))._2
      case DupC(t1, t2) => f => numPars(t1) != len(t2) || (errorr(t1)(f) && errorr(t2)(f))
      case JoinC(t) => f => errorr(t)(f) || setFmtsJoin(fmts(t)(f), errorr(t)(f))._2
      case ApplyC(t, pps) => f => numPars(t) != len(pps) || errorr(t)(f)
      case Filla(t) => f => errorr(t)(f)
      case t@FillBlock(i, p) => f => i < 0 || i > frame(t)(f).asInstanceOf[F].T_PW || errorr(p)(f)
      case ConsFillList(fl, pps) => f => errorr(fl)(f) || errorr(pps)(f)
      case t@NilFillList() => f => errori(t)(f)
      case _ => sys.error("")
    }
  }

    val errori : PPRoot => Int => T_ERROR = {
      attr {
        case t : PPRoot => tree.parent(t).head match {
          case g@ConsFillList(fl, pps) if (t == fl) => f => errori(g)(f) || setFmtsFilllist(fmtsi(g)(f), fmts(pps)(f), maxhi(g), maxh(pps), frame(g)(f), pw(t)(f) - (minlli(t) + minw(pps)) >= 0)._2
          case Filla(_) => _ => false
          case FillBlock(_,_) => _ => false
          case _ => sys.error("")
        }
      }
  }

  val errorL : PPRoot => Int => T_ERRS = {
    case NilArgs() => Vector.empty
    case ConsArgs(pps,p) => f => errorr(p)(f) +: errorL(pps)(f)
    case NilPPCArgs() => Vector.empty
    case ConsPPCArgs(ppc,p) => f => errorr(p)(f) +: errorL(ppc)(f)
    case _ => sys.error("")
  }

}

class Evaluator(tree : Tree[PPRoot,PPRoot]) extends Pretty(tree) {

  val render : PPS => Int => String = {
    case p => f => fmtsRoot(Best(p))(f)
  }

  val renderAll : PPS => Int => String = {
    case p => f => fmtsRoot(All(p))(f)
  }

  val disp : PPS => Int => String = {
    case p => f => fmtsRoot(Displ(p))(f)
  }


}

object Operators {

  // -------------------------------------------------------------------
  // Single layout -----------------------------------------------------


  val empty: PPS = {
    Empty()
  }

  val notext: PPS = {
    Text("")
  }

  val text: String => PPS = {
    case x => Text(x)
  }


  val indent: Int => PPS => PPS = {
    case i => p => Indent(i, p)
  }

  val >|< : PPS => PPS => PPS = {
    case l => r => Beside(l, r)
  }

  val >-< : PPS => PPS => PPS = {
    case u => d => Above(u, d)
  }

  val >#< : PPS => PPS => PPS = {
    case l => r => >|<(>|<(l)(text(" ")))(r)
  }

  val fill : Vector[PPS] => PPS = {
    case fl => Filla(fl.foldRight(NilFillList().asInstanceOf[FillList])((a,b) => ConsFillList(b,a)))
  }

  val fillblock : Int => Vector[PPS] => PPS = {
    case i => fl => FillBlock(i, fl.foldRight(NilFillList().asInstanceOf[FillList])((a,b) => ConsFillList(b,a)))
  }

  // -------------------------------------------------------------------
  // Multiple layout combinators ---------------------------------------

  val >/< : PPS => PPS => PPS = {
    case a => b => Dup(a,b)
  }

  val join : PPS => PPS = {
    case a => Join(a)
  }

  val cindent : Int => PPC => PPC = {
    case i => ppc => IndentC(i,ppc)
  }

  val >>|<< : PPC => PPC => PPC = {
    case l => r => BesideC(l, r)
  }

  val >>-<< : PPC => PPC => PPC = {
    case u => d => AboveC(u, d)
  }

  val >>/<< : PPC => PPC => PPC = {
    case a => b => DupC(a,b)
  }

  val cjoin : PPC => PPC = {
    case a => JoinC(a)
  }

  val par : PPC = {
    ParC()
  }

  val >>&< : PPC => Vector[PPS] => PPS = {
    case e => pl => Apply(e, pl.foldRight(NilArgs().asInstanceOf[PPSArgs])((a,b) => ConsArgs(b,a)))
  }

  val >>&<< : PPC => Vector[PPC] => PPC = {
    case e => pl => ApplyC(e, pl.foldRight(NilPPCArgs().asInstanceOf[PPCArgs])((a,b) => ConsPPCArgs(b,a)))
  }

  val hv2i : PPS => PPS => PPS = {
    case a => b => join(>/<(>|<(a)(b))(>-<(a)(b)))
  }

}