package org.bitbucket.inkytonik.kiama
package example.let

import org.bitbucket.inkytonik.kiama.example.let.PrettyPrinting.*
import org.bitbucket.inkytonik.kiama.relation.Tree

import scala.collection.mutable.ArrayBuffer

class Examples {

    val ex1 = Root(Let(Assign("a", Add(Const(1), Const(0)), Assign("b", Add(Const(0), Const(1)), EmptyList())), Add(Var("a"), Var("b"))))

    val ex2 = Root(Let(Assign("a", Add(Var("b"), Const(-16)), Assign("c", Const(8), NestedLet("w", Let(Assign("z", Add(Var("a"), Var("b")), EmptyList()), Add(Var("z"), Var("b"))), Assign("b", Sub(Add(Var("c"), Const(3)), Var("c")), EmptyList())))), Sub(Add(Var("c"), Var("a")), Var("w"))))

    val ex3 = Root(Let(Assign("a", Add(Var("b"), Const(0)), Assign("c", Const(2), Assign("b", Sub(Add(Var("c"), Const(3)), Var("c")), EmptyList()))), Sub(Add(Var("a"), Const(7)), Var("c"))))

    val ex6 = Root(Let(Assign("a", Add(Var("b"), Var("d")), Assign("c", Const(2), Assign("b", Const(3), Assign("a", Const(3), EmptyList())))), Sub(Add(Var("a"), Const(7)), Var("c"))))

    val z = Let(Assign("d", Const(100), Assign("c", Const(400), EmptyList())), Var("w"))

    val ex7 = Root(Let(Assign("a", Add(Var("b"), Var("d")), Assign("c", Const(2), Assign("b", Const(3), NestedLet("z", z, Assign("a", Const(3), EmptyList()))))), Sub(Add(Var("a"), Const(7)), Var("c"))))

    val plev = Root(Let(Assign("a", Const(1), NestedLet("b", Let(Assign("c", Var("a"), Assign("a", Const(2), EmptyList())), Var("c")), EmptyList())), Var("b")))

    val res1 = Root(Let(Assign("a", Const(1), Assign("b", Const(1), EmptyList())), Const(2)))

    val res2 = Root(Let(Assign("a", Const(-13), Assign("c", Const(8), NestedLet("w", Let(Assign("z", Const(-10), EmptyList()), Const(-7)), Assign("b", Const(3), EmptyList())))), Add(Const(-5), (Neg(Var("w"))))))

    val res3 = Root(Let(Assign("a", Const(3), Assign("c", Const(2), Assign("b", Const(3), EmptyList()))), Const(8)))

    val res4 = Root(Let(Assign("a", Const(1), NestedLet("b", Let(Assign("c", Const(2), Assign("a", Const(2), EmptyList())), Const(2)), EmptyList())), Var("b")))

}

class Tests extends Examples {

    def test(test : Root, result : Root) : Boolean = {
        val tree = new Tree[LetTree, LetTree](test)
        val lets = new Evaluator(tree)
        result == lets.evaluate1(tree.root)
    }

    def err(test : Root, result : ArrayBuffer[String]) : Boolean = {
        val tree = new Tree[LetTree, LetTree](test)
        val lets = new Evaluator(tree)
        result == lets.errs(tree.root)
    }

    def opt(test : Root) = {
        val tree = new Tree[LetTree, LetTree](test)
        val lets = new Evaluator(tree)
        lets.evaluate1(tree.root)
    }

    def errors(test : Root) = {
        val tree = new Tree[LetTree, LetTree](test)
        val lets = new Evaluator(tree)
        lets.errs(tree.root)
    }

    /*
  def testTree(a:Int, b:Int, c:ArrayBuffer[String]) : Root = {
    Root(genLet(a,b,c))
  }

  val genLet: (Int,Int,ArrayBuffer[String]) => lets = {
    case (x,y,l) => Let (genList(x,y,l), Add (aux1(x,y), genExp(l)))
  }

  val aux1 : (Int, Int) => Exp = {
    case (0,_) => Const(0)
    case (_,0) => Const(0)
    case (x,y) => Add (Var ("nest" + x + y), aux1(x,(y-1)))
  }

  def genList (x:Int,y:Int,l:ArrayBuffer[String]) : List = {
    def aux(d:ArrayBuffer[String]): List = {
      if (d.isEmpty)
        genNest(x,y,l)
      else
        Assign (d.head, Const(1), aux(d.tail))
    }
    aux(l)
  }

  val genExp: ArrayBuffer[String] => Exp = {
    case ArrayBuffer() => Const(0)
    case ArrayBuffer(x) => Var (x)
    case l => Add (Var (l.head), genExp (l.tail))
  }

  val genNest: (Int,Int,ArrayBuffer[String]) => List = {
    case (0,_,_) => EmptyList()
    case (_,0,_) => EmptyList()
    case (x,y,l) => {
      def aux3(yy:Int): List = {
        if (yy == 0)
          EmptyList()
        else
          NestedLet("nest" + x + yy, Let(genList((x - 1), y, l), Add(aux2(x, y), genExp(l))), aux3(yy - 1))
      }
      NestedLet("nest" + x + y, Let(genList((x - 1), y, l), Add(aux2(x, y), genExp(l))), aux3(y - 1))
    }
  }

  val aux2 : (Int, Int) => Exp = {
    case (1,_) => Const(0)
    case (_,0) => Const(0)
    case (x,y) => Add (Var ("nest" + (x-1) + y), aux1(x,(y-1)))
  }
 */

    def genLetProg(n : Int) : Root = {
        Root(Let(genLetTree(n), Var("n_" ++ n.toString)))
    }

    val addList : List => List = {
        case NestedLet(s, l, ll) => NestedLet(s, l, addList1(ll))
        case _                   => EmptyList()
    }

    val addList1 : List => List = {
        case EmptyList()         => Assign("va", Const(10), Assign("vb", Const(20), EmptyList()))
        case NestedLet(s, l, ll) => NestedLet(s, l, addList1(ll))
        case Assign(s, a, ll)    => Assign(s, a, addList1(ll))
    }

    def genLetTree(n : Int) : List = {
        addList(genNestedLets(n))
    }

    val oneList : Int => List = {
        n => Assign("zz_" ++ n.toString, Const(10), Assign("zz_" ++ (n - 1).toString, Var("va"), genNestedLets(n - 1)))
    }

    val genNestedLets : Int => List = {
        case 0                   => EmptyList()
        case (n : Int) if n == 1 => NestedLet("n_1", Let(oneList(n), Add(Var("z_10"), Var("z_9"))), genListAssign(n * 10))
        case (n : Int) if n > 1  => NestedLet("n_" ++ n.toString, Let(oneList(n), Add(Var("z_" ++ (n - 1).toString), Var("z_" ++ (n * 10 - 1).toString))), genListAssign(n * 10))
        case _                   => EmptyList()
    }

    val genListAssign : Int => List = {
        case 0                       => Assign("z_0", Const(10), EmptyList())
        case (n : Int) if n % 9 == 0 => Assign("z_" ++ n.toString, Var("va"), genListAssign(n - 1))
        case n                       => Assign("z_" ++ n.toString, Add(Var("z_" ++ (n - 1).toString), Const(1)), genListAssign(n - 1))
    }

    val nRoot : LetTree => Int = {
        case Root(x)              => nRoot(x)
        case Let(x, _)            => nRoot(x)
        case NestedLet(_, l, lst) => nRoot(l) + nRoot(lst)
        case Assign(_, _, lst)    => 1 + nRoot(lst)
        case _                    => 0
    }

}

object NameAnalysis {

    def main(args : Array[String]) : Unit = {

        val t = new Tests

        println(t.errors(t.genLetProg(2)))

        /*
        println(t.err(t.ex1,ArrayBuffer.empty))
        println(t.err(t.ex2,ArrayBuffer.empty))
        println(t.err(t.ex3,ArrayBuffer.empty))
        println(t.err(t.ex6,ArrayBuffer("use: d", "decl: a")))
        println(t.err(t.ex7,ArrayBuffer("use: d", "use: w", "decl: a")))
        */

    }
}

object Generation {
    def main(args: Array[String]): Unit = {
        val t = new Tests
        prettyN(t.genLetProg(args(0).toInt))
    }
}

object Optimization {

    def main(args : Array[String]) : Unit = {

        val t = new Tests

        prettyN(t.opt(t.genLetProg(args(0).toInt)))

        /*
        println(t.test(t.ex1,t.res1))
        println(t.test(t.ex2,t.res2))
        println(t.test(t.ex3,t.res3))
        println(t.test(t.plev,t.res4))
        */

    }
}

object Printing {

    def main(args : Array[String]) : Unit = {

        val t = new Tests

        println(pretty(t.genLetProg(args(0).toInt)))

    }
}