package org.bitbucket.inkytonik.kiama
package example.block

import org.bitbucket.inkytonik.kiama.relation.Tree

class Benchmark {

    val t = ConsIts(Decl("y"), ConsIts(Decl("x"), ConsIts(Use("x"), ConsIts(Use("y"), NilIts()))))

    val repeeat : Int => Its = {
        case 0 => t
        case x => ConsIts(Decl("y"), ConsIts(Decl("x"), ConsIts(Block(repeeat(x - 1)), ConsIts(Use("x"), ConsIts(Use("y"), NilIts())))))
    }

    val repeeeat : Int => Its = {
        case 0 => t
        case x => ConsIts(Decl("y"), ConsIts(Decl("x"), ConsIts(Block(repeeeat(x - 1)), ConsIts(Use("varRoot"), ConsIts(Use("y"), NilIts())))))
    }

    def test_NestedLevel(i : Int) : BlockTree = {
        Root(repeeat(i))
    }

    def test_UseTopLevelDecl(i : Int) : BlockTree = {
        Root(ConsIts(Decl("varRoot"), repeeeat(i)))
    }

    def testTree(i : Int) : BlockTree = {
        Root(repeeeat(i))
    }

}

object main {

    def main(args : Array[String]) : Unit = {

        val b : Benchmark = new Benchmark
        val tree = new Tree[BlockTree, BlockTree](b.testTree(4000))
        val block = new BlockT(tree)
        println(block.errors(tree.root).length)
    }
}