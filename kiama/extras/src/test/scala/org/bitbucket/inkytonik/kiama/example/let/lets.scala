

package org.bitbucket.inkytonik.kiama
package example.let
import org.bitbucket.inkytonik.kiama.attribution.Attribution
import org.bitbucket.inkytonik.kiama.relation.Tree

import scala.collection.mutable.ArrayBuffer

sealed abstract class LetTree extends Product
case class Root(let : lets) extends LetTree

sealed abstract class lets extends LetTree
case class Let(list : List, exp : Exp) extends lets

sealed abstract class List extends lets
case class NestedLet(string : String, let : Let, list : List) extends List
case class Assign(string : String, exp : Exp, list : List) extends List
case class EmptyList() extends List

sealed abstract class Exp extends lets
case class Add(exp1 : Exp, exp2 : Exp) extends Exp
case class Sub(exp1 : Exp, exp2 : Exp) extends Exp
case class Neg(exp : Exp) extends Exp
case class Var(string : String) extends Exp
case class Const(int : Int) extends Exp

trait LetBase extends Attribution {

    type Env = ArrayBuffer[(String, Int, Option[Exp])]

    type Expand = (String, Int, Env)

    val dclo : LetTree => Env

    val dcli : LetTree => Env

    val lev : LetTree => Int

    val env : LetTree => Env

    val expand : Expand => Option[Exp]

    val errors : LetTree => ArrayBuffer[String]

}

class LetT(tree : Tree[LetTree, LetTree]) extends LetBase {

    val dclo : LetTree => Env =
        attr {
            case Root(let)             => dclo(let)
            case Let(list, _)          => dclo(list)
            case NestedLet(_, _, list) => dclo(list)
            case Assign(_, _, list)    => dclo(list)
            case t @ EmptyList()       => dcli(t)
            case _                     => ArrayBuffer.empty
        }

    val dcli : LetTree => Env = {
        attr {
            case t @ Let(_, _) => tree.parent(t).head match {
                case Root(_)                => ArrayBuffer.empty
                case g @ NestedLet(_, _, _) => env(g)
                case _                      => ArrayBuffer.empty
            }
            case t : LetTree => tree.parent(t).head match {
                case g @ Let(_, _)               => dcli(g)
                case g @ Assign(string, exp, _)  => dcli(g) ++ ArrayBuffer((string, lev(g), Some(exp)))
                case g @ NestedLet(string, _, _) => dcli(g) ++ ArrayBuffer((string, lev(g), None))
                case h : LetTree                 => dcli(h)
            }
            case _ => ArrayBuffer.empty
        }
    }

    val lev : LetTree => Int = {
        attr {
            case t @ Let(_, _) => tree.parent(t).head match {
                case g @ NestedLet(_, _, _) => lev(g) + 1
                case Root(_)                => 0
                case _                      => 0
            }
            case t : LetTree => lev(tree.parent(t).head)
        }
    }

    val env : LetTree => Env = {
        attr {
            case t @ Root(_)   => dclo(t)
            case t @ Let(_, _) => dclo(t)
            case t : LetTree   => env(tree.parent(t).head)
        }
    }

    val expand : Expand => Option[Exp] = {
        attr {
            case (_, -1, _) => None
            case (n, l, e) => expandAux((n, l), e) match {
                case None => expand((n, l - 1, e))
                case t    => t
            }
        }
    }

    def expandAux(x : (String, Int), env : Env) : Option[Exp] = {
        if (env.isEmpty)
            None
        else if (x._1 == env.head._1 && x._2 == env.head._2)
            env.head._3
        else
            expandAux(x, env.tail)
    }

    val errors : LetTree => ArrayBuffer[String] = {
        attr {
            case Root(let)                        => errors(let)
            case Let(let, exp)                    => errors(let) ++ errors(exp)
            case Add(e1, e2)                      => errors(e1) ++ errors(e2)
            case Sub(e1, e2)                      => errors(e1) ++ errors(e2)
            case EmptyList()                      => ArrayBuffer.empty
            case Const(_)                         => ArrayBuffer.empty
            case t @ Var(string)                  => mustBeIn(string, env(t))
            case t @ Assign(string, exp, list)    => mustNotBeIn((string, lev(t)), dcli(t)) ++ errors(exp) ++ errors(list)
            case t @ NestedLet(string, exp, list) => mustNotBeIn((string, lev(t)), dcli(t)) ++ errors(exp) ++ errors(list)
            case _                                => ArrayBuffer.empty
        }
    }

    def mustBeIn(nome : String, env : Env) : ArrayBuffer[String] = {
        if (env.isEmpty)
            ArrayBuffer("use: " ++ nome)
        else if (env.head._1 == nome)
            ArrayBuffer.empty
        else
            mustBeIn(nome, env.tail)
    }

    def mustNotBeIn(x : (String, Int), env : Env) : ArrayBuffer[String] = {
        if (env.isEmpty)
            ArrayBuffer.empty
        else if ((x._1 == env.head._1) && (x._2 == env.head._2))
            ArrayBuffer("decl: " ++ x._1)
        else
            mustNotBeIn(x, env.tail)
    }

    def semantics(let : LetTree) : ArrayBuffer[String] = {

        val tree = new Tree[LetTree, LetTree](let)
        val lets = new LetT(tree)
        lets.errors(tree.root)
    }

}

class Evaluator(tree : Tree[LetTree, LetTree]) extends LetT(tree) {

    import org.bitbucket.inkytonik.kiama.rewriting.Rewriter._

    val expr =
        rule[Exp] {
            case Add(e, Const(0))        => e
            case Add(Const(0), t)        => t
            case Add(Const(a), Const(b)) => Const(a + b)
            case Sub(a, b)               => Add(a, Neg(b))
            case Neg(Neg(f))             => f
            case Neg(Const(n))           => Const(-n)
        }

    val normal = innermost(expr)

    def evaluate(e : LetTree) : LetTree =
        rewrite(normal)(e)

    var vars = ArrayBuffer[String]()

    val uses = {
        query[Exp] {
            case t @ Var(i) => vars = vars ++ mustBeIn(i, env(t))
        }
    }

    val decls = {
        query[List] {
            case t @ Assign(s, _, _)    => vars = vars ++ mustNotBeIn((s, lev(t)), dcli(t))
            case t @ NestedLet(s, _, _) => vars = vars ++ mustNotBeIn((s, lev(t)), dcli(t))
        }
    }

    def errs(e : LetTree) : ArrayBuffer[String] = {
        everywhere(decls <+ uses)(e)
        vars
    }

    val expC = {
        strategy[Exp] {
            case t @ Var(i) => expand((i, lev(t), env(t)))
        }
    }

    val normal1 = innermost(expr <+ expC)

    def evaluate1(e : LetTree) : LetTree =
        rewrite(normal1)(e)

}