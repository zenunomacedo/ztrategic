package org.bitbucket.inkytonik.kiama.example.oberon0

import org.bitbucket.inkytonik.kiama.example.oberon0.L0.TypeAnalyser

import org.bitbucket.inkytonik.kiama.example.oberon0.base.source.SourceTree.SourceTree
import org.bitbucket.inkytonik.kiama.example.oberon0.drivers.A2b
import org.bitbucket.inkytonik.kiama.parsing.{NoSuccess, Success}

object main {

    import org.bitbucket.inkytonik.kiama.util

    def main(args : Array[String]) : Unit = {
        val r = new Array[String](1)
        r(0) = "~/Repositories/ztrategic/src/Examples/Inputs/Oberon/case.ob"

        val t = A2b.parse(util.FileSource(r(0)))
        t match {
            case Success(result, _) =>
                println(result)
                val tree = new SourceTree(result)

                new L2.Lifter with L2.Desugarer {
                    override def buildAnalyser(atree : SourceTree) : TypeAnalyser =
                        new L2.NameAnalyser with L2.TypeAnalyser {
                            val tree : SourceTree = atree
                        }
                }.transform(tree)

            //println(A2b.format(ntree.root))
            case _ : NoSuccess =>

        }

    }

}
