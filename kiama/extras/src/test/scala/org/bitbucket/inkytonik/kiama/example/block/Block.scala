

package org.bitbucket.inkytonik.kiama
package example.block
import org.bitbucket.inkytonik.kiama.attribution.Attribution
import org.bitbucket.inkytonik.kiama.relation.Tree
import scala.collection.mutable.ArrayBuffer

sealed abstract class BlockTree extends Product
case class Root(its : Its) extends BlockTree

sealed abstract class Its extends BlockTree
case class ConsIts(it : It, its : Its) extends Its
case class NilIts() extends Its

sealed abstract class It extends Its
case class Decl(name : String) extends It
case class Use(name : String) extends It
case class Block(its : Its) extends It

trait BlockBase extends Attribution {

    type Env = ArrayBuffer[(String, Int)]

    type Errors = ArrayBuffer[String]

    val dclo : BlockTree => Env

    val dcli : BlockTree => Env

    val lev : BlockTree => Int

    val env : BlockTree => Env

    val errors : BlockTree => Errors

}

class BlockT(tree : Tree[BlockTree, BlockTree]) extends BlockBase {

    val dclo : BlockTree => Env =
        attr {
            case t @ NilIts()    => dcli(t)
            case ConsIts(_, its) => dclo(its)
            case t @ Decl(name)  => dcli(t) += ((name, lev(t))) : Env
            case t @ Use(_)      => dcli(t)
            case t @ Block(_)    => dcli(t)
            case _               => ArrayBuffer.empty
        }

    val dcli : BlockTree => Env = {
        attr {
            case t @ NilIts() => tree.parent(t).head match {
                case ConsIts(_, _) => dclo(tree.prev(t).head)
                case Block(_)      => env(tree.parent(t).head)
                case Root(_)       => ArrayBuffer.empty
                case _             => ArrayBuffer.empty
            }

            case t @ ConsIts(_, _) => tree.parent(t).head match {
                case ConsIts(_, _) => dclo(tree.prev(t).head)
                case Block(_)      => env(tree.parent(t).head)
                case Root(_)       => ArrayBuffer.empty
                case _             => ArrayBuffer.empty
            }

            case t @ Block(_) => dcli(tree.parent(t).head)
            case t @ Use(_)   => dcli(tree.parent(t).head)
            case t @ Decl(_)  => dcli(tree.parent(t).head)
            case _            => ArrayBuffer.empty
        }

    }

    val lev : BlockTree => Int = {
        attr {
            case Root(_)      => 0
            case t @ Block(_) => lev(tree.parent(t).head)
            case t @ Use(_)   => lev(tree.parent(t).head)
            case t @ Decl(_)  => lev(tree.parent(t).head)
            case t @ _ => tree.parent(t).head match {
                case Block(_)      => lev(tree.parent(t).head) + 1
                case ConsIts(_, _) => lev(tree.parent(t).head)
                case Root(_)       => 0
                case _             => 0
            }
        }
    }

    val env : BlockTree => Env = {
        attr {
            case t @ Root(_)  => dclo(t)
            case t @ Block(_) => env(tree.parent(t).head)
            case t @ Use(_)   => env(tree.parent(t).head)
            case t @ Decl(_)  => env(tree.parent(t).head)
            case t @ _ => tree.parent(t).head match {
                case Block(_)      => dclo(tree.parent(t).head)
                case ConsIts(_, _) => env(tree.parent(t).head)
                case Root(_)       => dclo(t)
                case _             => ArrayBuffer.empty
            }
        }
    }

    val errors : BlockTree => Errors = {
        attr {
            case Root(its)        => errors(its)
            case NilIts()         => ArrayBuffer.empty
            case ConsIts(it, its) => errors(it) ++ errors(its)
            case Block(its)       => errors(its)
            case t @ Use(name)    => mustBeIn(name, env(t))
            case t @ Decl(name)   => mustNotBeIn((name, lev(t)), dcli(t))
            case _                => ArrayBuffer.empty
        }
    }

    def mustBeIn(nome : String, env : Env) : Errors = {
        val e = env.filter { case (n, _) => nome == n }
        if (e.isEmpty)
            ArrayBuffer(nome)
        else
            ArrayBuffer.empty
    }

    def mustNotBeIn(x : (String, Int), env : Env) : Errors = {
        if (env.contains(x))
            ArrayBuffer(x._1)
        else
            ArrayBuffer.empty

    }

}