package org.bitbucket.inkytonik.kiama.example.benchmark

import org.bitbucket.inkytonik.kiama.example.let.*
import org.bitbucket.inkytonik.kiama.example.repmin.*

object Benchmark {

  def main(args: Array[String]): Unit = {
    args(0) match {
      case "repminNone" => RepminNone.main(args.tail)
      case "repminTUTP" => RepminTU.main(args.tail)
      case "repminAG" => RepminAG.main(args.tail)
      case "repminTP" => RepminTP.main(args.tail)
      case "letOpt" => Optimization.main(args.tail)
      case "letGen" => Generation.main(args.tail)
      case "letPrint" => Printing.main(args.tail)
    }
  }

}
