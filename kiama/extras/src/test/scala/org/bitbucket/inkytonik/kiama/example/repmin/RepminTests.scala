/*
 * This file is part of Kiama.
 *
 * Copyright (C) 2008-2021 Anthony M Sloane, Macquarie University.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.bitbucket.inkytonik.kiama
package example.repmin

import org.bitbucket.inkytonik.kiama.relation.Tree
import org.bitbucket.inkytonik.kiama.util.KiamaTests

class RepminTests extends KiamaTests {

    val t = Fork(Leaf(3), Fork(Leaf(1), Leaf(10)))
    val u = Fork(Leaf(1), Fork(Leaf(1), Leaf(1)))

    test("repmin actually reps and mins (traditional)") {
        val tree = new Tree[RepminTree, RepminTree](t)
        val repmin = new Repmin(tree)
        repmin.repmin(t) shouldBe u
    }

    test("repmin actually reps and mins (decorator)") {
        val tree = new Tree[RepminTree, RepminTree](t)
        val repmin = new RepminDec(tree)
        repmin.repmin(t) shouldBe u
    }

}

class Tests {

    val testTree : Int => RepminTree = {
        n => buildTree(List.range(1, n + 1))
    }

    val buildTree : List[Int] => RepminTree = {
        case x :: y :: Nil      => Fork(Leaf(x), Leaf(y))
        case x :: y :: z :: Nil => Fork(Fork(Leaf(x), Leaf(y)), Leaf(z))
        case t => {
            val half = t.length / 2
            Fork(buildTree(t.take(half)), buildTree(t.drop(half)))
        }
    }

    val sumTree : RepminTree => Int = {
        case Leaf(l)    => l
        case Fork(l, r) => sumTree(l) + sumTree(r)
    }

    val sumTreeOnes : RepminTree => Int = {
        case Leaf(_)    => 1
        case Fork(l, r) => sumTreeOnes(l) + sumTreeOnes(r)
    }

}

object RepminNone {
    def main(args : Array[String]) : Unit = {
        val t = new Tests
        println(t.sumTreeOnes(t.testTree(args(0).toInt)))
    }
}

object RepminAG {

    def main(args : Array[String]) : Unit = {
        val t = new Tests
        val tt = t.testTree(args(0).toInt)
        val tree = new Tree[RepminTree, RepminTree](tt)
        val repmin = new Repmin(tree)
        val test = repmin.repmin(tt)
        println(t.sumTree(test))
    }

}

object RepminTP {

    def main(args : Array[String]) : Unit = {
        val t = new Tests
        val tt = t.testTree(args(0).toInt)
        val tree = new Tree[RepminTree, RepminTree](tt)
        val repmin = new RepminS(tree)
        val test = repmin.rep(tt)
        println(t.sumTree(test))
    }

}

object RepminTU {

    def main(args : Array[String]) : Unit = {
        val t = new Tests
        val tt = t.testTree(args(0).toInt)
        val tree = new Tree[RepminTree, RepminTree](tt)
        val repmin = new RepminTUTP(tree)
        val test = repmin.rep(tt)
        println(t.sumTree(test))
    }

}
