package org.bitbucket.inkytonik.kiama.example.let

import org.bitbucket.inkytonik.kiama.example.PP.Operators.*
import org.bitbucket.inkytonik.kiama.example.PP.*
import org.bitbucket.inkytonik.kiama.relation.Tree

object PrettyPrinting {

    val pretty : LetTree => String = {
        case Root(l) => {
            val test = Displ(prettyLet(l))
            val p = new Tree[PPRoot, PPRoot](test)
            val f = new Evaluator(p)
            f.disp(test.PPS)(1000)
        }
        case _ => "error"
    }

    val prettyLet : lets => PPS = {
        case Let(NestedLet(s, l, r), e) => >-<(>-<(>#<(text("let"))(>#<(text(s))(>#<(text("="))(prettyLet(l)))))(indent(4)(prettyList(r))))(>#<(text("in"))(prettyExp(e)))
        case Let(Assign(s, exp, l), e)  => >-<(>-<(>#<(text("let"))(>#<(text(s))(>#<(text("="))(prettyExp(exp)))))(indent(4)(prettyList(l))))(>#<(text("in"))(prettyExp(e)))
        case _                          => empty
    }

    val prettyList : List => PPS = {
        case NestedLet(s, l, r) => >-<(>#<(text(s))(>#<(text("="))(prettyLet(l))))(prettyList(r))
        case Assign(s, e, l)    => >-<(>#<(text(s))(>#<(text("="))(prettyExp(e))))(prettyList(l))
        case EmptyList()        => empty
        case _                  => empty
    }

    val prettyExp : Exp => PPS = {
        case Add(a, b) => >#<(prettyExp(a))(>#<(text("+"))(prettyExp(b)))
        case Sub(a, b) => >#<(prettyExp(a))(>#<(text("-"))(prettyExp(b)))
        case Neg(a)    => >|<(text(" -("))(>|<(prettyExp(a))(text(")")))
        case Var(a)    => text(a)
        case Const(a)  => text(a.toString)
    }

    val prettyN : LetTree => Unit = {
        case Root(l) => println(prettyLetN(l))
        case _       => println("error")
    }

    val prettyLetN : lets => String = {
        case Let(l,e) => {
            val a = prettyListN(l).split("\n").toList
            val aa = a.head +: a.tail.map(y => "    " + y)
            val aaa = unlines(aa)
            "let " ++ aaa ++ "in " ++ prettyExpN(e)
        }
        case _ => "error"
    }

    val prettyListN : List => String = {
        case NestedLet(s, l, r) => {
            val a = prettyLetN(l).split("\n").toList
            val aa = a.head +: a.tail.map(y => sp(3 + s.length) ++ y)
            val aaa = unlines(aa)
            s ++ " = " ++ aaa ++ prettyListN(r)
        }
        case Assign(s, e, l)    => s ++ " = " ++ prettyExpN(e) ++ "\n" ++ prettyListN(l)
        case EmptyList()        => ""
        case _                  => ""
    }

    val prettyExpN : Exp => String = {
        case Add(a, b) => prettyExpN(a) ++ " + " ++ prettyExpN(b)
        case Sub(a, b) => prettyExpN(a) ++ " - " ++ prettyExpN(b)
        case Neg(a)    => " -(" ++ prettyExpN(a) ++ ")"
        case Var(a)    => a
        case Const(a)  => (a.toString)
    }

    val sp : Int => String = {
        case 0 => ""
        case n => " " ++ sp(n - 1)
    }

    val unlines : scala.List[String] => String = {
        case Nil => ""
        case (h : String) :: (t : scala.List[String]) => h ++ "\n" ++ unlines(t)
    }

}

